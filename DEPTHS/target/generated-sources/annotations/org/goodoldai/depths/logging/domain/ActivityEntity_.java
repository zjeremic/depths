package org.goodoldai.depths.logging.domain;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ActivityEntity.class)
public abstract class ActivityEntity_ {

	public static volatile SingularAttribute<ActivityEntity, Long> id;
	public static volatile SingularAttribute<ActivityEntity, UserSessionEntity> usersession;
	public static volatile SingularAttribute<ActivityEntity, ResourceEntity> resource;
	public static volatile SingularAttribute<ActivityEntity, ActivityTypeEntity> activitytype;
	public static volatile SetAttribute<ActivityEntity, ActivityPropertyEntity> properties;
	public static volatile SingularAttribute<ActivityEntity, Date> datetime;

}

