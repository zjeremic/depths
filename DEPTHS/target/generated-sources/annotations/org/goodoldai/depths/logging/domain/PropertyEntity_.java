package org.goodoldai.depths.logging.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(PropertyEntity.class)
public abstract class PropertyEntity_ {

	public static volatile SingularAttribute<PropertyEntity, Long> id;
	public static volatile SingularAttribute<PropertyEntity, ServiceEntity> service;
	public static volatile SingularAttribute<PropertyEntity, String> value;
	public static volatile SingularAttribute<PropertyEntity, String> key;

}

