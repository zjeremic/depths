package org.goodoldai.depths.logging.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ServiceTypeEntity.class)
public abstract class ServiceTypeEntity_ {

	public static volatile SingularAttribute<ServiceTypeEntity, Long> id;
	public static volatile SingularAttribute<ServiceTypeEntity, String> type;

}

