package org.goodoldai.depths.logging.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ActivityPropertyEntity.class)
public abstract class ActivityPropertyEntity_ {

	public static volatile SingularAttribute<ActivityPropertyEntity, Long> id;
	public static volatile SingularAttribute<ActivityPropertyEntity, String> value;
	public static volatile SingularAttribute<ActivityPropertyEntity, ActivityEntity> activity;
	public static volatile SingularAttribute<ActivityPropertyEntity, String> key;

}

