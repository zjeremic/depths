package org.goodoldai.depths.logging.domain;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ServiceEntity.class)
public abstract class ServiceEntity_ {

	public static volatile SingularAttribute<ServiceEntity, Long> id;
	public static volatile SingularAttribute<ServiceEntity, ServiceTypeEntity> servicetype;
	public static volatile SingularAttribute<ServiceEntity, UserSessionEntity> usersession;
	public static volatile SetAttribute<ServiceEntity, PropertyEntity> properties;
	public static volatile SingularAttribute<ServiceEntity, Date> datetime;

}

