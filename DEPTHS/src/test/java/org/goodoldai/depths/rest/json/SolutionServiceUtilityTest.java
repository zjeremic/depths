package org.goodoldai.depths.rest.json;


 

import org.apache.log4j.Logger;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.testutility.TestUtility;
import org.junit.Test;

public class SolutionServiceUtilityTest {
	private static final Logger LOGGER = Logger.getLogger(SolutionServiceUtilityTest.class);
	
	@Test
	public void testSaveNewSuggestedSolution() {
		/*
		 * User can create new project from the scratch mode:create
		 * He can edit his existing project mode:edit
		 * He can choose to create new project from the existing one, mode:create and relatedprojecturi is saved
		 * He can reuse projects created by other users mode:reuse
		 * 
		 * */
	 
		LOGGER.info("Some info message");
		
		JSONStringer js=new JSONStringer();
		 
		try {
			js.object();
			js.key("useruri").value(TestUtility.testuser);
			js.key("designproblem").value(TestUtility.testproblem);
			js.key("mode").value("create");//"create","edit","reuse"are possible values
			//If the user has created it or modified it before, then this is edit mode
			js.key("project");
			 
			js.object();
				js.key("projectid").value("someid");
				js.key("projecturi").value(TestUtility.testproject);
				js.key("parentprojecturi").value("http://someparentprojecturi");
				js.key("title").value("some title of the project");
				js.key("dateTimeSent").value("");
				js.key("fileUrl").value("some/url/location");
				js.key("createdBy").value("http//createdby.org");
				js.key("modifiedBy").value("http//modifiedby.org");
			
				
			js.endObject();
			js.key("solution");
			js.object();
				js.key("description").value("some description");
				js.key("hasAdditionalRequirements").value("additional requirements");
				js.key("hasCons").value("has cons");
				js.key("hasConsequences").value("has consequences");
				js.key("hasDesignConstraints").value("has design constraints");
				js.key("hasDesignRules").value("has design rules");
				js.key("hasPros").value("has pros");
			js.endObject();
		 
			js.endObject();
	  
		 
		 	SolutionServiceUtility.saveNewSuggestedSolution(js.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("Save solution finished");
	}
	@Test
	public void testGetProjectJsonByUri() {
		 
		String projectUri=TestUtility.testproject2;
		try {
			SolutionServiceUtility.getProjectSolutionJsonByUri(projectUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}
	@Test
	public void testSaveDiagrams(){
		 
		JSONStringer js=new JSONStringer();
		 
		try {
			js.object();
				js.key("useruri").value("http://www.goodoldai.org/depths/triplestore#User/df8422de-d310-4239-ae4b-33dcf03ede3e");
				js.key("projecturi").value("http://www.goodoldai.org/depths/triplestore#Project/214d1b71-b0a3-4688-b505-85ebf866fd70");
				js.key("diagrams");
			js.array();
				js.object();
					js.key("mode").value("create");//possible create (for new diagram), edit (for changed diagram)
					js.key("type").value("ClassDiagram");//UseCaseDiagram, Activity... (look at the package org.goodoldai.depths.domain.content)
					js.key("fileUrl").value("location/of/this/file");
					js.key("title").value("Class diagram title");
					js.key("id").value("1");//this value is unique in this json, so it can connect diagram with
					//generated URI in ArgoUML
				js.endObject();
				js.object();
				js.key("mode").value("edit");//possible create (for new diagram), edit (for changed diagram)
				js.key("type").value("UseCaseDiagram");//UseCaseDiagram, Activity... (look at the package org.goodoldai.depths.domain.content)
				js.key("fileUrl").value("location/of/this/file");
				js.key("title").value("use case diagram title");
				js.key("uri").value("http://www.goodoldai.org/depths/triplestore#ClassDiagram/84a34d2d-7ebe-4266-a211-4be0e6ffb7df");
				js.key("id").value("2");
			js.endObject();
			js.object();
				js.key("mode").value("create");//possible create (for new diagram), edit (for changed diagram)
				js.key("type").value("ActivityDiagram");//UseCaseDiagram, Activity... (look at the package org.goodoldai.depths.domain.content)
				js.key("fileUrl").value("location/of/this/file");
				js.key("title").value("activity diagram title");
				js.key("id").value("3");
			js.endObject();
			js.endArray();
			js.endObject();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 
		try {
		//	String output=SolutionServiceUtility.saveDiagrams(js.toString());
			//output is json. Each diagram contains pair id and uri
			//e.g. [{"id":"1","uri":"http:\/\/www.goodoldai.org\/depths\/triplestore#ClassDiagram\/033a994a-3358-4b12-b1cc-80ba95779502"},
			//{"id":"2","uri":"http:\/\/www.goodoldai.org\/depths\/triplestore#ClassDiagram\/84a34d2d-7ebe-4266-a211-4be0e6ffb7df"},
			//{"id":"3","uri":"http:\/\/www.goodoldai.org\/depths\/triplestore#ActivityDiagram\/5527c1b7-5cd2-4311-a877-193b69001d0c"}]

		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}

}
