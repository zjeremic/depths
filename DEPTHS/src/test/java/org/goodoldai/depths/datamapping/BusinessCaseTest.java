package org.goodoldai.depths.datamapping;

import java.util.Date;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.ClassDiagram;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.UseCaseDiagram;
import org.goodoldai.depths.domain.general.Scale;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.User;

import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.utility.StringUtility;
import org.junit.Test;


public class BusinessCaseTest {
	private static final Logger LOGGER = Logger.getLogger(BusinessCaseTest.class);
	@Test
	public void createBusinessCase(){
		try { 
		User us=new User();
		us.setUsername("zoran2");
		us.setFirstname("Zoran");
		us.setLastname("Jeremic");
		us.setPassword(StringUtility.md5("zoran2"));
		UserManagament.getInstance().saveResource(us,false);
		DesignProblem dProblem=new DesignProblem();
		dProblem.setCreator(us);
		dProblem.setDateCreated(new Date());
		dProblem.setMaker(us);
		dProblem.setTitle("Design problem title");
		dProblem.setDescription("Description of the problem...");
		ContentManagament.getInstance().saveResource(dProblem,false);
		
		Course course=new Course();
		course.setTitle("This is an example of some design pattern course");
		course.addDesignProblem(dProblem);
		course.addUser(us);
		ContentManagament.getInstance().saveResource(course, false);
		 us.addCourse(course);
		Project project=new Project();
		project.setCreatedBy(us);
		project.setDateCreated(new Date());
		project.setMaker(us);
		project.setTitle("Some project example");
		project.setDescription("This is description of some project");
		project.addHasDesignProblemRef(dProblem);
		ContentManagament.getInstance().saveResource(project,false);
		
		Project project2=new Project();
		project2.setCreatedBy(us);
		project2.setDateCreated(new Date());
		project2.setMaker(us);
		project2.setTitle("Some other project example");
		project2.setDescription("This is description of some other project");
		project2.addHasDesignProblemRef(dProblem);
		ContentManagament.getInstance().saveResource(project2,false);
		
		UseCaseDiagram ucd=new UseCaseDiagram();
		ucd.setId("some id");
		ucd.setFileUrl("some file url");
		ContentManagament.getInstance().saveResource(ucd,false);
		
		project.addDiagram(ucd);
		
		ClassDiagram  cd=new ClassDiagram();
		 cd.setId("some id");
		 cd.setFileUrl("some file url");
		ContentManagament.getInstance().saveResource( cd,false);
		
		 project.addDiagram(cd);
		 ContentManagament.getInstance().updateResource( project,false);
		 UserManagament.getInstance().updateResource(us,false);	 
		 
		 LOGGER.info("*****TEST VARIABLES*****COPY THIS TO src/test/java/org/goodoldai.depths.testutility");
		LOGGER.info(" public static String testuser=\""+us.getUri().toString()+"\";");
		LOGGER.info(" public static String testproblem=\""+dProblem.getUri().toString()+"\";");
		LOGGER.info(" public static String testcourse=\""+course.getUri().toString()+"\";");
		LOGGER.info(" public static String testproject=\""+project.getUri().toString()+"\";");
		LOGGER.info(" public static String testusecase=\""+ucd.getUri().toString()+"\";");
		LOGGER.info(" public static String testclassdiagram=\""+cd.getUri().toString()+"\";");
			//public static String testproblem="";
			//public static String testcourse="";
			//public static String testproject="";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}
	//@Test
	/*public void addTestData(){
		String user2Json="{'tablename':'user','id':'5','dataobject':{'MAX_FILE_SIZE':'2097152','course':'1','username':'test3','auth':'manual','mform_showadvanced_last':'0','newpassword':'test3','preference_auth_forcepasswordchange':'0','firstname':'test','lastname':'test','email':'testdeweva@gmail.com','maildisplay':'2','emailstop':'0','mailformat':'1','maildigest':'0','autosubscribe':'1','trackforums':'0','htmleditor':'1','ajax':'0','screenreader':'0','city':'rew','country':'BH','timezone':'99','lang':'en_utf8','description':'','imagealt':'','interests':'','url':'','icq':'','skype':'','aim':'','yahoo':'','msn':'','idnumber':'','institution':'','department':'','phone1':'','phone2':'','address':'','submitbutton':'Update profile','timemodified':1309335477,'mnethostid':'1','confirmed':1,'password':'1b25d6217b1591b4d2c8bd57f597047c'},'addon':{'additionaldata':'no'}}";
		String courseJson="{'tablename':'course','id':'9','dataobject':{'MAX_FILE_SIZE':'2097152','category':'1','fullname':'Test course 3','shortname':'tt','idnumber':'','summary':'','format':'weeks','numsections':'10','startdate':'1309384800','hiddensections':'0','newsitems':'5','showgrades':'1','showreports':'0','maxbytes':'2097152','metacourse':'0','enrol':'','defaultrole':'0','enrollable':'1','enrolstartdate':0,'enrolstartdisabled':'1','enrolenddate':0,'enrolenddisabled':'1','enrolperiod':'0','expirynotify':'0','notifystudents':'0','expirythreshold':'864000','groupmode':'0','groupmodeforce':'0','visible':'1','enrolpassword':'','guest':'0','lang':'','restrictmodules':0,'role_1':'','role_2':'','role_3':'','role_4':'','role_5':'','role_6':'','role_7':'','submitbutton':'Save changes','teacher':'Teacher','teachers':'Teachers','student':'Student','students':'Students','password':'','timemodified':1309336249,'timecreated':1309336249,'sortorder':'98'},'addon':{'additionaldata':'no'}}";


		String desingProblemJson="{'tablename':'modelling','id':'25','dataobject':{'MAX_FILE_SIZE':'2097152','name':'Design problem title','description':'Some design problem ','timeopen':'1309298400','timeclose':'1317247200','timelate':'1317247200','totalpoints':'100','projecttype':'individual','selectedproject':'','userating':'1','assessed':'2','scale':'50','assesstimestart':0,'assesstimefinish':0,'course':'9','coursemodule':'','section':'1','module':'12','modulename':'modelling','instance':'','add':'modelling','update':'0','return':'0','visible':'1','submitbutton2':'Save and return to course','groupingid':0,'groupmembersonly':0,'groupmode':0,'filename':'none','useprojectdates':'false','ratingtime':0,'timemodified':1309336391},'addon':{'courseuri':null,'scaleuri':null}}";


		String brainstormJson="{'tablename':'modelling_tasks','id':'10','dataobject':{'instance':'30','name':'Brainstorm','description':'description','type':'brainstorm','position':100},'addon':{'designproblemuri':'http:\\/\\/www.goodoldai.org\\/depths\\/triplestore#DesignProblem\\/87403b76-9d9e-4ea2-b0c1-567286b75293'}}";
		
		
		
		InsertMoodleTable imt=new InsertMoodleTable();
try {
	RDFDataServiceUtility.addMoodleTableToRDF(user2Json);
	LOGGER.info("TEST USER ADDED");
	RDFDataServiceUtility.addMoodleTableToRDF(courseJson);
	LOGGER.info("TEST COURSE ADDED");
	RDFDataServiceUtility.addMoodleTableToRDF(desingProblemJson);
	LOGGER.info("TEST PROBLEM ADDED");
	RDFDataServiceUtility.addMoodleTableToRDF(brainstormJson);
	LOGGER.info("TEST BRAINSTORM ADDED");
	 
} catch (JSONException e) {
	// TODO Auto-generated catch block
	LOGGER.error("Error:"+e.getLocalizedMessage());
}
	}
	*/
	@Test
	public void testScale(){
		try { 
		User us=new User();
		us.setUsername("zoran2");
		us.setFirstname("Zoran");
		us.setLastname("Jeremic");
		us.setPassword(StringUtility.md5("zoran2"));
		UserManagament.getInstance().saveResource(us,false);
		DesignProblem dProblem=new DesignProblem();
		dProblem.setCreator(us);
		dProblem.setDateCreated(new Date());
		dProblem.setMaker(us);
		dProblem.setTitle("Design problem title");
		dProblem.setDescription("Description of the problem...");
		
		Scale sc=new Scale();
		ContentManagament.getInstance().saveResource(sc,false);
		ContentManagament.getInstance().saveResource(dProblem,false);
		dProblem.setUsingScale(sc);
		Brainstorm br=new Brainstorm();
		br.setTitle( "name" );
		br.setDescription( "description" );
		br.setContentRef(dProblem);
		ContentManagament.getInstance().saveResource(br,false);
		
		
		
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}
	@Test
	public void loadDesignProblem(){
		String dpUri="http://www.goodoldai.org/depths/triplestore#DesignProblem/8886e32e-15e2-47eb-83b3-eeccb9682ec4";

		try {
			DesignProblem dp=ContentManagament.getInstance().loadResourceByURI(DesignProblem.class, dpUri,false);
			LOGGER.info("LOADED DP:"+dp.getUri());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	}
}
