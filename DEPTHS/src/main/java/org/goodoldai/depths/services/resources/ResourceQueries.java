package org.goodoldai.depths.services.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.annotation.UserRating;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.resources.OnlineRepository;
import org.goodoldai.depths.domain.resources.WebPage;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.rdfpersistance.urigenerator.URIBuilder;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
 
 

public class ResourceQueries  extends AbstractDAOImpl {
	private static OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(ResourceQueries.class);
	 
	private static class ResourceQueriesHolder{
		private static final ResourceQueries INSTANCE=new ResourceQueries();
	}
	public static ResourceQueries getInstance(){
		return ResourceQueriesHolder.INSTANCE;
	}
	
	public  LinkedList<Relevance> getAllWebPagesRelevancesForConcept(String userUri, String conceptUri) throws Exception{
		LinkedList<Relevance> relevances=new LinkedList<Relevance>();
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"SELECT  DISTINCT ?relevance \n" + 
			"WHERE  {\n" +
				"?relevance depths:isRelevantFor <"+conceptUri+">. \n"+
				"?webPage depths:hasRelevance ?relevance.\n"+
				"?webPage rdf:type depths:WebPage.\n"+
			"}";
		 		Collection<String> relevancesUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "relevance",
						getDataModel());

		if (relevancesUris != null && !relevancesUris.isEmpty()){
			relevances= (LinkedList<Relevance>) loadResourcesByURIs(Relevance.class, relevancesUris, false);
		}
		return relevances ;
	}
	public  LinkedList<Relevance> getAllContentRelevancesForConcept(String userUri, String conceptUri) throws Exception{
		LinkedList<Relevance> relevances=new LinkedList<Relevance>();
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"SELECT  DISTINCT ?relevance \n" + 
			"WHERE  {\n" +
				"?relevance depths:isRelevantFor <"+conceptUri+">. \n"+
				"?webPage depths:hasRelevance ?relevance.\n"+
				"?webPage rdf:type depths:Brainstorming.\n"+
			"}";
	 Collection<String> relevancesUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "relevance",
						getDataModel());

		if (relevancesUris != null && !relevancesUris.isEmpty()){
			User user=loadResourceByURI(User.class,userUri, false);
			 
			for(String relUri:relevancesUris){
				 
				Relevance relevance=loadResourceByURI(Relevance.class,relUri,false);
				boolean visibility=ResourceVisibilityUtility.getInstance().isVisibleToUser(user, relevance);
				 
				if(visibility){
				relevances.add(relevance);
				}
			}
			//relevances= (LinkedList<Relevance>) loadResourcesByURIs(Relevance.class, relevancesUris, false);
		}
		return relevances ;
	}

	public UserRating getPreviousRatingForResourceByUser(String userUri, String pageUri) throws Exception {
		UserRating uRating=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n"+
				"SELECT  DISTINCT ?userRating \n" + 
				"WHERE  {\n" +
					"<"+pageUri+"> loco:hasAnnotation ?userRating.\n"+
					"?userRating rdf:type loco:UserRating.\n"+
					"?userRating loco:madeBy <"+userUri+">.\n"+
				"}";
		 	Collection<String> userRatingUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "userRating",
							getDataModel());

			if (userRatingUris != null && !userRatingUris.isEmpty()){
				uRating=loadResourceByURI(UserRating.class, userRatingUris.iterator().next(), false);
			}else{
				uRating=new UserRating();
			}
			return uRating;
		
	}
	public  WebPage getWebPageByPageUrl(String pageUrl) throws Exception{
		WebPage webPage=null;
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n"+
			"PREFIX xsd: <"+Constants.XMLSCHEMA_NS+">\n"+
			"SELECT  DISTINCT ?webPage \n" + 
			"WHERE  {\n" +
				"?webPage rdf:type depths:WebPage.\n"+
				"?webPage loco:href \""+pageUrl+"\"^^xsd:string.\n"+
			"}";
		 Collection<String> pagesUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "webPage",
						getDataModel());

		if (pagesUris != null && !pagesUris.isEmpty()){
			webPage=loadResourceByURI(WebPage.class, pagesUris.iterator().next(), false);
		}else{
			webPage=new WebPage();
			String wpUri = URIBuilder.getInstance().uriGenerator.generateInstanceURI(webPage);
	 		webPage.setUri(wpUri);
		}
		return webPage ;
	}
	public OnlineRepository getOnlineRepository(String startingPage) {
	 	OnlineRepository repository=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n"+
				"PREFIX xsd: <"+Constants.XMLSCHEMA_NS+">\n"+
				"SELECT  DISTINCT ?repository \n" + 
				"WHERE  {\n" +
				"?repository loco:href \""+startingPage+"\"^^xsd:string.\n"+
				"?repository rdf:type depths:OnlineRepository.\n"+
				"}";
		 	Collection<String> repositoryUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "repository",
							getDataModel());

			if (repositoryUris != null && !repositoryUris.isEmpty()){
				String uri=repositoryUris.iterator().next();
				try {					
					repository=loadResourceByURI(OnlineRepository.class, uri, false);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					LOGGER.info("Error in annotationg Online repository:"+queryString+" loading resource with uri:"+uri);
				}
			}else{
				repository=new OnlineRepository();
				String relUri = URIBuilder.getInstance().uriGenerator.generateInstanceURI(repository);
				repository.setUri(relUri);
			}
			return repository ;
	}
	public Relevance getPatternWebPageRelevance(String conceptUri, String webPageUri) throws Exception {
		Relevance relevance=null;
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?relevance \n" + 
				"WHERE  {\n" +
					"<"+webPageUri+"> rdf:type depths:WebPage.\n"+
					"<"+webPageUri+"> depths:hasRelevance ?relevance.\n"+
					"?relevance depths:isRelevantFor <"+conceptUri+">. \n"+
					 
				"}";
		 	Collection<String> relevanceUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "relevance",
							getDataModel());

			if (relevanceUris != null && !relevanceUris.isEmpty()){
				relevance=loadResourceByURI(Relevance.class, relevanceUris.iterator().next(), false);
			}else{
				 
				relevance=new Relevance();
				String relUri = URIBuilder.getInstance().uriGenerator.generateInstanceURI(relevance);
		 		relevance.setUri(relUri);
			}
			return relevance ;
	}
	public Collection<Concept> getConceptsRelatedToBrainstormByProblem(String problemUri) throws Exception{
		
	 Collection<Concept> concepts=new ArrayList<Concept>();
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX skos: <"+Constants.SKOS_NS+"> \n" + 
				"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?concept \n" + 
				"WHERE  {\n" +
				"<"+problemUri+"> depths:taskRef ?task.\n"+
					"?task rdf:type depths:Brainstorm.\n"+
					"?brainstorming depths:inResponseTo ?task .\n"+
					"?brainstorming depths:hasRelevance ?relevance.\n"+
					"?relevance depths:isRelevantFor ?concept.\n"+
					"?concept rdf:type skos:Concept.\n"+
				"}";
	 		Collection<String> conceptsUris = queryService.executeOneVariableSelectSparqlQuery(queryString, "concept",
				getDataModel());

if (conceptsUris != null && !conceptsUris.isEmpty()){
	concepts=loadResourcesByURIs(Concept.class, conceptsUris, false);
} 
return concepts;
}
}