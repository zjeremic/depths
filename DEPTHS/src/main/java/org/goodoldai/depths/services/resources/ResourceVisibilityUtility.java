package org.goodoldai.depths.services.resources;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.Grouping;
import org.goodoldai.depths.domain.user.User;
 

public class ResourceVisibilityUtility {
private static final Logger LOGGER = Logger.getLogger(ResourceVisibilityUtility.class);
	
	private static class ResourceVisibilityUtilityHolder{
		private static final ResourceVisibilityUtility INSTANCE=new ResourceVisibilityUtility();
	}
	public static ResourceVisibilityUtility getInstance(){
		return ResourceVisibilityUtilityHolder.INSTANCE;
	}
	public boolean isVisibleToUser(User user, Relevance relevance) {
		Resource res=relevance.getIsRelevanceOf();
		 
		if(res instanceof Brainstorming){
			 
			User sentBy=((Brainstorming) res).getSentBy();
			 
			if(sentBy.equals(user)){
			 
				return true;
			}
			Brainstorm brainstormTask=((Brainstorming) res).getInResponseTo();
			DesignProblem dProblem=(DesignProblem) brainstormTask.getContentRef();
			if(dProblem!=null){
				if(dProblem.getGroupmembersonly()==0){
					 
					return true;
				}
				if(dProblem.getGrouping()!=null){
					Grouping grouping=dProblem.getGrouping();
					Collection<Group> groups=grouping.getGroupRef();
					boolean sentByUserIn=false;
					boolean currentUserIn=false;
					for(Group group:groups){
						sentByUserIn=group.containsMember(sentBy);
						currentUserIn=group.containsMember(user);
					}
					 
					if(sentByUserIn&&currentUserIn){
					 
						return true;
					}
				}else{
					//if not grouping
					if(dProblem.getGroupmode()==0){
						return true;
					}else{
						Collection<Course> courses= dProblem.getCourseRef();
						Course course=courses.iterator().next();
						Collection<Group> creatorGroupsInCourse=sentBy.getMembershipToGroupInCourse(course);
						Collection<Group> userGroupsInCourse=user.getMembershipToGroupInCourse(course);
						if(!creatorGroupsInCourse.isEmpty()){
							for(Group g:creatorGroupsInCourse){
								if(userGroupsInCourse.contains(g)){
									 
									return true;
								}
							}
						}
						 
					}
				}
			}
		}
		
		return false;
		 
		
	}
}
