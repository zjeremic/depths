package org.goodoldai.depths.services.datalayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.peers.PeersProcessing;
import org.goodoldai.depths.services.peers.UserModel;
import org.goodoldai.depths.utility.EvaluationUtility;
import org.goodoldai.depths.utility.PropertiesManager;
 

public class UserModelData {
	private static final Logger LOGGER = Logger
			.getLogger(UserModelData.class);
	private Map<String, List<String>> dProblemsPeersLists=new HashMap<String,List<String>>();
	private Map<String, HashMap<String, UserModel>> relevantPeers = new HashMap<String, HashMap<String,UserModel>>();
	String userURI=null;
	protected boolean userModelDataInitialized=false;
	public UserModelData(String userUri){
		userURI=userUri;
	}
	protected void setUserModelData() throws Exception{
		userModelDataInitialized=true;
	}
	public List<String> getSortedListOfPeers(String currUserUri, String courseUri,String dProblemUri) throws Exception{
		 
		if(!dProblemsPeersLists.containsKey(dProblemUri)){
			DesignProblem dProblem = SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(dProblemUri);;
					if (dProblem != null) {
				String projectType = dProblem.getProjectType();
				Collection<String> allowedPeers = null;
				if (projectType.equals("teacher_groups")) {
					Collection<Group> currentUserGroups = UserManagament
							.getInstance().getCurrentUserCourseGroups(currUserUri,
									courseUri);
					allowedPeers = new ArrayList<String>();
					if (currentUserGroups != null) {
						for (Group group : currentUserGroups) {
							Collection<User> possibleUser = group.getMembers();
							for (User us : possibleUser) {
								if (!allowedPeers.contains(us.getUri().toString())) {
									allowedPeers.add(us.getUri().toString());
								}
							}
						}
						}
						}
				PropertiesManager.setFactors();
				//PeersProcessing.getInstance().processLearningContext(this,dProblem, allowedPeers);
				List<String> sortedListOfPeers = PeersProcessing.getInstance().processRelevantPeers(dProblem);
				dProblemsPeersLists.put(dProblemUri, sortedListOfPeers);
				// json = PeersServiceUtility.getRecommendedPeersListAsJSON(
					//	currUserUri, sortedListOfPeers, relevantPeers);
			}
			 
		} 
			return dProblemsPeersLists.get(dProblemUri);
		 
	}
	private void processPeersForDesignProblem(DesignProblem dProblem){
		
	}
	public Map<String, HashMap<String, UserModel>> getRelevantPeers() {
		return relevantPeers;
	}
	public void setRelevantPeers(Map<String, HashMap<String, UserModel>> relevantPeers) {
		this.relevantPeers = relevantPeers;
	}
	public  HashMap<String, UserModel> getRelevantPeersForDesignProblem(String dProblemUri){
		if(!this.relevantPeers.containsKey(dProblemUri)){
			HashMap<String, UserModel> newRelPeersForDesignProblem=new HashMap<String,UserModel>();
			relevantPeers.put(dProblemUri,newRelPeersForDesignProblem);
		}
		return relevantPeers.get(dProblemUri);
	}
}
