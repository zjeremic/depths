package org.goodoldai.depths.services.resources;

import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.services.peers.PeersProcessing;
import org.goodoldai.depths.services.peers.PeersServiceManager;
 

public class ResourcesServiceManager {

private static final Logger LOGGER = Logger.getLogger(ResourcesServiceManager.class);
	
	private static class ResourcesServiceManagerHolder{
		private static final ResourcesServiceManager INSTANCE=new ResourcesServiceManager();
	}
	public static ResourcesServiceManager getInstance(){
		return ResourcesServiceManagerHolder.INSTANCE;
	}
	public String getRelevantResourcesForConcept(String userUri,
			String conceptUri) {
		LOGGER.info(" get Relevant Resources for concept:"+conceptUri);
		String json="";
		ResourceProcessing resourceProcessing=new ResourceProcessing();
		try {
			 
			json=resourceProcessing.getRelevantResourcesForConcept(userUri,conceptUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		return json;
		 //FindPeersThread peersThread=new FindPeersThread(courseId,designProblemId);
		// peersThread.start();
	}
	public void addResourceRating(final User user, 
			final String pageUri, final String ratingValue,
			final String conceptUri, final String sessionId) throws Exception{
		 Thread thread = new Thread(new Runnable() {
			    public void run() {
			    	ResourceProcessing resourceProcessing=new ResourceProcessing();
			    	try {
			    	 
						Resource ratedResource=resourceProcessing.addResourceRating(user,pageUri,ratingValue,conceptUri, sessionId);
						 
						resourceProcessing.processResourceRatings(ratedResource,conceptUri);
						 
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LOGGER.error("Error:"+e.getLocalizedMessage());
					}
			    }
		 });
		 thread.start();
	}
}

		
	 
 
