package org.goodoldai.depths.services.peers;

import java.util.ArrayList;

import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.utility.WeightFactorsUtility;

 

public class UserModel implements Comparable{
	protected User user;
	
	
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	private double activityCompetence=0.00;
	private double courseActivityCompetence=0.00;
	
	private double ideaSkillCompetence=0.00;
	private double sumOfIdeaSkillCompetenceValues=0.00;
	private int numberOfAssessedIdeas=0;
	
	private double courseIdeaSkillCompetence=0.00;
	private double sumOfCourseIdeaSkillCompetenceValues=0.00;
	private int numberOfAssessedCourseIdeas=0;
	
	private double submitionSkillCompetence=0.00;
	private double sumOfSubmitionSkillCompetenceValues=0.00;
	private int numberOfAssessedSubmitions=0;
	
	private double courseSubmitionSkillCompetence=0.00;
	private double sumOfCourseSubmitionSkillCompetenceValues=0.00;
	private int numberOfAssessedCourseSubmitions=0;

	private double totalCompetence=0.00;
	
	public UserModel(User currentUser){
		this.user=currentUser;
	}
	public void addSumOfIdeaSkillCompetenceValues(double valueToAdd){
		double value=valueToAdd*WeightFactorsUtility.getBrainstormingSkillWeightPeers();
		this.sumOfIdeaSkillCompetenceValues=this.sumOfIdeaSkillCompetenceValues+value;
		this.numberOfAssessedIdeas++;
		ideaSkillCompetence=this.sumOfIdeaSkillCompetenceValues/this.numberOfAssessedIdeas;
		
 
	}
	public void addSumOfCourseIdeaSkillCompetenceValues(double valueToAdd){
		this.sumOfCourseIdeaSkillCompetenceValues=this.sumOfCourseIdeaSkillCompetenceValues+valueToAdd;
		this.numberOfAssessedCourseIdeas++;
		courseIdeaSkillCompetence=this.sumOfCourseIdeaSkillCompetenceValues/this.numberOfAssessedCourseIdeas;
		 	}
	
	public void addSumOfSubmitionSkillCompetenceValues(double valueToAdd){
		double value=valueToAdd* WeightFactorsUtility.getSubmittingSkillWeightPeers();
		this.sumOfSubmitionSkillCompetenceValues=this.sumOfSubmitionSkillCompetenceValues+value;
		this.numberOfAssessedSubmitions++;
		submitionSkillCompetence=this.sumOfSubmitionSkillCompetenceValues/this.numberOfAssessedSubmitions;
		 	}
	public void addSumOfCourseSubmitionSkillCompetenceValues(double valueToAdd){
		this.sumOfCourseSubmitionSkillCompetenceValues=this.sumOfCourseSubmitionSkillCompetenceValues+valueToAdd;
		this.numberOfAssessedCourseSubmitions++;
		courseSubmitionSkillCompetence=this.sumOfCourseSubmitionSkillCompetenceValues/this.numberOfAssessedCourseSubmitions;
		 	}
	 
	 
	/**
	 * @return the activityCompetence
	 */
	public double getActivityCompetence() {
		return activityCompetence;
	}

	/**
	 * @param  the activityCompetence to set
	 */
	public void setActivityCompetence(double activityCompetence) {
		this.activityCompetence = activityCompetence;
	}

	/**
	 * @return the workCompetence
	 */
	public double getIdeaSkillCompetence() {
		return ideaSkillCompetence;
	}

	/**
	 * @param  the workCompetence to set
	 */
	public void setIdeaSkillCompetence(double problemCompetence) {
		this.ideaSkillCompetence = problemCompetence;
	}
	public void addActivityCompetence(double actComp){
		activityCompetence=activityCompetence+actComp;
		 
		 }

	/**
	 * @return the totalCompetence
	 */
	public double getTotalCompetence() {
		return totalCompetence;
	}

	/**
	 * @param  the totalCompetence to set
	 */
	public void setTotalCompetence(double totalCompetence) {
		this.totalCompetence = totalCompetence;
	}
	/**
	 * @return the notCurrentActivityCompetence
	 */
	public double getCourseActivityCompetence() {
		return courseActivityCompetence;
	}
	/**
	 * @param notCurrentActivityCompetence the notCurrentActivityCompetence to set
	 */
	public void setCourseActivityCompetence(double cActivityCompetence) {
		this.courseActivityCompetence = cActivityCompetence;
	}
	
	public void addCourseActivityCompetence(double courseActivityCompetenceToAdd){
		courseActivityCompetence=courseActivityCompetence+courseActivityCompetenceToAdd;
		 	}
	/**
	 * @return the courseSkillCompetence
	 */
	public double getCourseIdeaSkillCompetence() {
		return courseIdeaSkillCompetence;
	}
	/**
	 * @return the submitionSkillCompetence
	 */
	public double getSubmitionSkillCompetence() {
		return submitionSkillCompetence;
	}
	/**
	 * @return the courseSubmitionSkillCompetence
	 */
	public double getCourseSubmitionSkillCompetence() {
		return courseSubmitionSkillCompetence;
	}
 @Override
	public int compareTo(Object uModel) {
		// TODO Auto-generated method stub
		if(this.totalCompetence-((UserModel) uModel).getTotalCompetence()>0){
		
			return -1;
		}else  if (this.totalCompetence-((UserModel) uModel).getTotalCompetence()<0){
			return  1;
		} 
			return 0;
		 
		
		
	}
	

}
