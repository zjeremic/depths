package org.goodoldai.depths.services.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.annotation.Annotation;
import org.goodoldai.depths.domain.annotation.UserRating;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.jsonstuff.ResourcesJSONExporter;
import org.goodoldai.depths.logging.ActivityLogging;
import org.goodoldai.depths.semanticstuff.services.annotations.AnnotationManagament;
import org.goodoldai.depths.services.peers.UserModel;
import org.goodoldai.depths.services.peers.PeersProcessing.CustComparatorByTotalCompetence;
import org.goodoldai.depths.utility.PropertiesManager;
import org.goodoldai.depths.utility.WeightFactorsUtility;

public class ResourceProcessing {
	private static final Logger LOGGER = Logger.getLogger(ResourceProcessing.class);

	public String getRelevantResourcesForConcept(String userUri,
			String conceptUri) throws Exception {
		LinkedList<Relevance> relevances = ResourceQueries.getInstance().getAllWebPagesRelevancesForConcept(userUri, conceptUri);
		 
		sortRelevantResources(relevances);
		LinkedList<Relevance> contentRelevances = ResourceQueries.getInstance().getAllContentRelevancesForConcept(userUri, conceptUri);
		sortRelevantResources(contentRelevances);
		String json = ResourcesJSONExporter.getInstance()
				.exportRecommendedResourcesToJSON(relevances,contentRelevances,conceptUri);

		
		return json;
	}

	public Resource addResourceRating(User user, String pageUri,
			String ratingValue, String conceptUri,String sessionId) throws Exception {
		AnnotationManagament annMan = AnnotationManagament.getInstance();
		Resource ratedResource = annMan.loadResourceByURI(Resource.class,
				pageUri, false);
		UserRating uRating =ResourceQueries.getInstance().getPreviousRatingForResourceByUser(user.getUri().toString(),pageUri);
		 
		double ratingValueD = Double.valueOf(ratingValue);
		uRating.setRatingValue(ratingValueD);
		uRating.setMadeBy(user);
		annMan.saveResource(uRating, false);
		ratedResource.addHasAnnotation(uRating);
		annMan.saveResource(ratedResource, false);
		Map<String, String> properties=new HashMap<String,String>();
		properties.put("concept", conceptUri);
		properties.put("ratingValue", ratingValue);
		 ActivityLogging.getInstance().storeActivityRelatedLogData("resourceRating", sessionId, new Date(), user, ratedResource, properties);
		return ratedResource;

	}

	public void processResourceRatings(Resource ratedResource,String conceptUri) throws Exception {

		Collection<Relevance> relevances=ratedResource.getRelevances();
		 
		for(Relevance rel:relevances){
			 
			if(rel.getIsRelevantFor().getUri().toString().equals(conceptUri)){
				Collection<Annotation> annotations=ratedResource.getHasAnnotation();
				double ratingValues=0.00;
				int numbOfRatings=0;
				for(Annotation ann:annotations){
					
					if(ann instanceof UserRating){
						ratingValues=ratingValues+((UserRating) ann).getRatingValue();
						numbOfRatings++;
					 
						LOGGER.info("SOME BETTER ALGORITHM SHOULD BE USED");
						
					}
				}
				 double socRatingVal=ratingValues/numbOfRatings;
				rel.setHasSocialRatingValue(socRatingVal);
				double annRel=rel.getHasValue();
				PropertiesManager.setFactors();
				double annRelWeight=WeightFactorsUtility.getPatternRelevancePercent();
				double socRatWeight=WeightFactorsUtility.getRatingRelevancePercent();
				 
				double overalRelevance=(annRel*annRelWeight+socRatingVal*socRatWeight)/2;
				 
				rel.setHasOveralValue(overalRelevance);
				
				AnnotationManagament.getInstance().updateResource(rel, false);
			}
		}
		
	}

private void sortRelevantResources(LinkedList<Relevance> relevances){
	CustComparatorByOveralRelevance comp=new CustComparatorByOveralRelevance();
	Map<String, Relevance> relMap=new HashMap<String,Relevance>();
	comp.setMap(relMap);
    Collections.sort(relevances,comp);
}
public class CustComparatorByOveralRelevance implements Comparator<Relevance>
{
	 private Map map;  
	 	 
	 	 public void setMap(Map map) { this.map = map; }
		@Override
		public int compare(Relevance o1, Relevance o2) {
			if(map==null) return 0;
			double o1v=o1.getHasOveralValue();
			double o2v=o2.getHasOveralValue();
			if(o1v==o2v){
				return 0;
			}else if(o1v>o2v){
				return -1;
			}else
			return 1;
		}  
	 
} 
}
