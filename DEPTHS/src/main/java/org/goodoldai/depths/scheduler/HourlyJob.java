package org.goodoldai.depths.scheduler;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.logging.ActivityLogging;
import org.goodoldai.depths.semanticstuff.rdfpersistance.DataModelManager;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.services.datalayer.RecommendationDataLayer;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HourlyJob implements Job{
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(HourlyJob.class.getName());
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		LOGGER.info("HourlyJob executed at:"+System.currentTimeMillis());
		if(!Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.format){
		someRDFQuery();
		}
		cleanUserModelsCache();
		keepLoggingDataConnection();
		
	}
	private void someRDFQuery(){
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT DISTINCT ?course \n" + 
				"WHERE  {\n" +
					"?course rdf:type loco:Course.\n"+
		 
				"}";
			 
			Collection<String> coursesUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "course",
							DataModelManager.getInstance().getDataModel());
			 
	}
	private void cleanUserModelsCache(){
		RecommendationDataLayer.getDataLayerInstance().deleteAllUsersCache();
	}
	private void keepLoggingDataConnection(){
		ActivityLogging.getInstance().makeTestConnection();
	}
	

}
