package org.goodoldai.depths.scheduler;

import java.io.IOException;
import java.text.ParseException;
 
 

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.log4j.Logger;
 
 
import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Settings;
 
import org.quartz.SchedulerException;
 
 
public class CronInitializer extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3354678519490378151L;
	   private static final Logger LOGGER = Logger.getLogger(CronInitializer.class.getName());
	public void init()throws ServletException{
		 
		if (Settings.getInstance().config.cronSchedulerConfig.activated) {
			CronScheduler cScheduler = new CronScheduler();
			try {
					cScheduler.startScheduler();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					 LOGGER.info("Could not start Cron Scheduler!");
				 
			} catch (SchedulerException e) {
				 LOGGER.info("Could not start Cron Scheduler!");
			}
		}
	}
	public void service(HttpServletRequest request, HttpServletResponse response)
		    throws ServletException, IOException
		    {

		    }
}
