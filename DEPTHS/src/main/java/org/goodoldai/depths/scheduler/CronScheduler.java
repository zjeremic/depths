package org.goodoldai.depths.scheduler;

import java.text.ParseException;
import java.util.Date;
 

 
import org.apache.log4j.Logger;
import org.goodoldai.depths.config.CronSchedulerConfig;
import org.goodoldai.depths.config.Settings;
 
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
 
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


public class CronScheduler {
	private static final Logger LOGGER = Logger.getLogger(CronScheduler.class.getName());
	private CronSchedulerConfig notifierConfig=Settings.getInstance().config.cronSchedulerConfig;
	public void startScheduler() throws SchedulerException, ParseException{
		SchedulerFactory sf=new StdSchedulerFactory();
		Scheduler sched=sf.getScheduler();

		
		 
			if(notifierConfig.debug){
				scheduleOneTimeUpdate(sched);
			} 
				if(notifierConfig.hourly){
					scheduleHourlyUpdates(sched);
				}
				if(notifierConfig.daily){
					scheduleDailyUpdates(sched);
				}
				if(notifierConfig.weekly){
					scheduleWeeklyUpdates(sched);
				}
				if(notifierConfig.monthly){
					scheduleMonthlyUpdates(sched);
				}
		  sched.start();
		 

	}
	/**
	 * This is for testing purposes. 
	 * Should not be used in productive release.
	 * @param sched
	 * @throws SchedulerException 
	 */
	private void scheduleOneTimeUpdate(Scheduler sched) throws SchedulerException{
		LOGGER.info("------- Scheduling Jobs ----------------");
	      
        // get a "nice round" time a few seconds in the future...
        Date startTime = DateBuilder.nextGivenSecondDate(null, 15);
        
 
        // job1 will only fire once at date/time "ts"
        JobDetail job=(JobDetail) JobBuilder.newJob(DailyUpdatesJob.class).withIdentity("testjob","email_notification").build();
        SimpleTrigger trigger=(SimpleTrigger) TriggerBuilder.newTrigger().withIdentity("testjob","email_notification").startAt(startTime).build();
        // schedule it to run!
        sched.scheduleJob(job, trigger);
     
	}
	private void scheduleHourlyUpdates(Scheduler sched) throws ParseException, SchedulerException{
 		LOGGER.info("scheduleHourlyJob");
		JobDetail job = JobBuilder.newJob(HourlyJob.class).withIdentity("hourlyJob", "cron_scheduler").build();
		SimpleScheduleBuilder cSched=SimpleScheduleBuilder.repeatHourlyForever(1);
		 SimpleTrigger  trigger=  TriggerBuilder.newTrigger().withIdentity("hourlyJob","cron_scheduler").withSchedule(cSched).build();
		 sched.scheduleJob(job,trigger);
	}
	private void scheduleDailyUpdates(Scheduler sched) throws ParseException, SchedulerException{
		JobDetail job = JobBuilder.newJob(DailyUpdatesJob.class).withIdentity("dailyUpdate", "email_notification").build();
		CronScheduleBuilder cSched=CronScheduleBuilder.dailyAtHourAndMinute(0, 15);
		CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("dailyUpdate", "email_notification").withSchedule(cSched)
			    .build();
			sched.scheduleJob(job, trigger);
	}
	private void scheduleWeeklyUpdates(Scheduler sched) throws ParseException, SchedulerException{
		JobDetail job = JobBuilder.newJob(DailyUpdatesJob.class).withIdentity("weeklyUpdate", "email_notification").build();
		CronScheduleBuilder cSched=CronScheduleBuilder.weeklyOnDayAndHourAndMinute(DateBuilder.SUNDAY, 0, 15);
			CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("weeklyUpdate", "email_notification").withSchedule(cSched)
			    .build();
			sched.scheduleJob(job, trigger);
	}
	private void scheduleMonthlyUpdates(Scheduler sched) throws ParseException, SchedulerException{
		JobDetail job = JobBuilder.newJob(DailyUpdatesJob.class).withIdentity("weeklyUpdate", "email_notification").build();
		CronScheduleBuilder cSched=CronScheduleBuilder.monthlyOnDayAndHourAndMinute(1, 2, 45);
			CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("weeklyUpdate", "email_notification").withSchedule(cSched)
			    .build();
			sched.scheduleJob(job, trigger);
	}


}
