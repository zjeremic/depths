package org.goodoldai.depths.scheduler;

public class HTMLDocumentSimpleBodyElement extends HTMLDocumentBodyElement{
	private String who="";
	private String message="";
	private String what="";
	public void setBodyContentElements(String whoUser, String messageContent, String whatContent){
		who=whoUser;
		message=messageContent;
		what=whatContent;
		
	}
	@Override
	public String produceBodyElement(){
		String bodyElement="<tr class='subtitled'><td width='10%'>&nbsp;</td><td colspan='2'><br><strong>" +
				who +
				"</strong><br></td></tr>";
		

		bodyElement=bodyElement+"<tr class='notifelement";
		
		bodyElement=bodyElement+" bordered_bottom";

		
		bodyElement=bodyElement+"'><td width='10%'>&nbsp;</td>" +
				"<td colspan='2'>" +
				message + "<strong>"+what+"</strong>"+
				"</td></tr>";
		
		return bodyElement;
	}
}
