package org.goodoldai.depths.scheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.goodoldai.depths.domain.user.User;


import com.hp.hpl.jena.datatypes.xsd.XSDDateTime;

public class DailyUpdate {
	 
	protected void createUpdates(User user) throws Exception{
		checkData(user);
		 String html="";
		 
		HTMLDocument htmlDoc=new HTMLDocument();
		 HTMLDocumentFooter htmlFooter=new HTMLDocumentFooter();
		 HTMLDocumentHeader htmlHeader=new HTMLDocumentHeader();
		 htmlHeader.setTitle("Learning Organisation Daily Update");
		 
		 Calendar cal = Calendar.getInstance();
		   DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		   cal.add(Calendar.DATE, -1);
		    String yesterdayDate=dateFormat.format(cal.getTime());
		   
		   
		  
		 htmlHeader.setPeriod(yesterdayDate);
		 HTMLDocumentMainBody htmlMainBody=new HTMLDocumentMainBody();
		 htmlMainBody.setBodyTitle("Daily Updates for "+user.getName());
		 addHTMLMainBodyContent(htmlMainBody);
		
		 htmlDoc.setDocFooter(htmlFooter);
		 htmlDoc.setDocHeader(htmlHeader);
		 htmlDoc.setDocMainBody(htmlMainBody);
		 html=htmlDoc.produceHTMLDocument();
		 String subject="Daily updates for "+user.getName()+" - "+yesterdayDate;
		 SendEmail sEmail=new SendEmail();
	 
	}
	private void addHTMLMainBodyContent(HTMLDocumentMainBody htmlMainBody){
 
	}
	public void checkData(User user) throws Exception{
		  
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, -1);
		 XSDDateTime yesterday= new  XSDDateTime(cal); 
 
	}
 

}
