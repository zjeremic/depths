package org.goodoldai.depths.scheduler;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.user.User;
 

public class NotificationServiceQueries {

	/**
	 * The logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger( NotificationServiceQueries.class);

	/**
	 * Reference to OntModelQueryService.
	 */
//	private static OntModelQueryService queryService = new OntModelQueryServiceImpl();
//	private static EventManagementService ems = ServiceLocator.getInstance()
//			.getEventManagementService();
	
	
	public static Collection<User> getUsersToNotify(String frequency) throws Exception
	{
		Collection<User> users = new LinkedList<User>();
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
		 
			+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"
			+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
			+ "PREFIX dc: <" + Constants.DC_TERMS_NS + "> \n"
			+ "SELECT DISTINCT ?user \n"
			+ "WHERE { \n"
			+ "?user rdf:type um:User . \n"	 
			+ "?user um:hasPreferences ?compPref.\n" 
			+ "?compPref rdf:type um:EmailPreferences.\n"
			+ "?compPref um:notificationFrequency um:"+frequency+".\n"
			+"}";
	  
		/*Collection<String> uris = queryService
				.executeOneVariableSelectSparqlQuery(queryString,
						"user", DataModelManager.getInstance()
								.getDataModel());

		if (uris != null && !uris.isEmpty()) {
			users = ems.loadResourcesByURIs(User.class, uris, false);
		}*/
		
		return users;
	}
}
