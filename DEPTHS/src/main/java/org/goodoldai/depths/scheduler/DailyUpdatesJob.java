package org.goodoldai.depths.scheduler;

import java.util.Collection;

import org.apache.log4j.Logger;

import org.goodoldai.depths.domain.user.User;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DailyUpdatesJob implements Job {
	private static final Logger LOGGER = Logger.getLogger(DailyUpdatesJob.class);
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		Collection<User> users = null;
		try {
			users = NotificationServiceQueries.getUsersToNotify("Daily");
			for (User user : users) {
				DailyUpdate dUpdate = new DailyUpdate();
				dUpdate.createUpdates(user);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}

	}

}
