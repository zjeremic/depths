package org.goodoldai.depths.scheduler;

public class HTMLDocumentHeader {
	private String subtitle="DEPTHS";
	private String title="Learning Environment Updates";
	private String period="";
	public HTMLDocumentHeader(){
		
	}
	public HTMLDocumentHeader(String subtitle, String title, String period){
		this.subtitle=subtitle;
		this.title=title;
		this.period=period;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String produceDocumentHeader(){
		String header="<tr class='header'><td width='10%'>&nbsp;</td><td>"+subtitle+"<br><big>"+title;
		if(!period.equals("")){
			header=header+", "+period;
		}
		header=header+"</big><br></td></tr>";
		
		return header;
	}
	

}
