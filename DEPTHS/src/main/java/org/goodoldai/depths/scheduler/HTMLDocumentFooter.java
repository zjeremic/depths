package org.goodoldai.depths.scheduler;

public class HTMLDocumentFooter {
	private String subtitle="@2012, DEPTHS";
	private String title="Design Patterns Teaching Help System";
	
	public HTMLDocumentFooter(){
		
	}
	public HTMLDocumentFooter(String title, String subtitle){
		this.title=title;
		this.subtitle=subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String produceDocumentFooter(){
		String footer="<tr class='footer'><td width='10%'>&nbsp;</td><td><small>"+title+
				"<br><small>"+subtitle+"</small></small><br></td><td width='10%'>&nbsp;</td></td>";
		return footer;
	}
	
}
