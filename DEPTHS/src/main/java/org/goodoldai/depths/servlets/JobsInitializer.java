package org.goodoldai.depths.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.goodoldai.depths.annotation.AnnotationManager;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.semanticstuff.util.CacheInitializer;

public class JobsInitializer extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3708563262631947868L;
	private static final Logger LOGGER = Logger.getLogger(JobsInitializer.class.getName());

	public void init() throws ServletException {
		if (Settings.getInstance().config.webRepsConfig.annotateRepositories) {
			LOGGER.info("Annotation of repositories started");
			AnnotationManager.getInstance().annotateAllRepositories();
			LOGGER.info("Annotation of repositories finished");
		}
		Boolean cache = Settings.getInstance().config.caching;
		if (cache) {
			if (!Settings.getInstance().config.rdfRepositoryConfig.sdbConfig.format) {
				LOGGER.info("CACHING");
				CacheInitializer ci = new CacheInitializer();
				ci.initializeCacheForProjects();
				ci.initializeCacheForDesignProblems();
			}

		} else {
			LOGGER.info("NOT CACHING");
		}
	}
}
