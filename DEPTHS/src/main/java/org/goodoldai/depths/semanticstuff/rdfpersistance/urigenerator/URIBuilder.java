package org.goodoldai.depths.semanticstuff.rdfpersistance.urigenerator;

import org.apache.log4j.Logger;

public class URIBuilder {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(URIBuilder.class);

	public UriGenerator uriGenerator;

	private static class URIBuilderHolder {
		private static final URIBuilder INSTANCE = new URIBuilder();
	}

	public static URIBuilder getInstance() {
		return URIBuilderHolder.INSTANCE;
	}

	private URIBuilder() {
		uriGenerator = new UUIDBasedUriGenerator();
	}

}
