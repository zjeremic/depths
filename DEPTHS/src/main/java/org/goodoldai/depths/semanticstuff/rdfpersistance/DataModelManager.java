/**
 * 
 */
package org.goodoldai.depths.semanticstuff.rdfpersistance;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


import org.apache.log4j.Logger;
 

import thewebsemantic.Bean2RDF;
import thewebsemantic.RDF2Bean;

import com.hp.hpl.jena.rdf.model.Model;

public class DataModelManager {

	private static final Logger logger = Logger.getLogger(DataModelManager.class);

	private DataProvider dataProvider;

	private Model dataModel;

	private RDF2Bean rdf2BeanBinding;

	private Bean2RDF bean2RDFBinding;

	// private Integer cacheCounter = CleanUp.MAX_COUNTER;

	private boolean firstRun = true;

	  protected static ScheduledExecutorService executor = Executors
	  .newSingleThreadScheduledExecutor();

	private static class DataModelManagerHolder {
		private static final DataModelManager INSTANCE = new DataModelManager();
	}

	public static DataModelManager getInstance() {
		return DataModelManagerHolder.INSTANCE;
	}

	protected DataModelManager() {
		// new CleanUp().run();
	}

	/**
	 * @return the dataProvider
	 */
	public DataProvider getDataProvider() {
		if (null == dataProvider) {
			dataProvider = DataProviderFactory.getDataProvider();
		}
		return dataProvider;
	}

	/**
	 * @return the dataModel
	 */
//	public Model getDataModel() {
//		//  synchronized (executor) {
//		// cacheCounter = CleanUp.MAX_COUNTER;
//		if (null == dataModel) {
//			logger.debug("Retrieving data model...");
//			dataModel = getDataProvider().getDataModel(!firstRun);
//
//			firstRun = false;
//		}
//		//  }
//		return dataModel;
	 
//	}
	public Model getDataModel()
	{
		 synchronized (executor) {
//			cacheCounter = CleanUp.MAX_COUNTER;
			if (null == dataModel) {
				logger.debug("Retrieving data model...");
				dataModel = getDataProvider().getDataModel(!firstRun);
//				if (firstRun) {
//					if (Config.MySQL.equals(Settings.getInstance().config.dbConfig.dbType)) {
					// do this for each DB type just in case
//						new ConnectionKeepAlive().run();
//					}
//				}
				firstRun = false;
	 	}
		}
		return dataModel;
	}

	/**
	 * 
	 * @return the rdf2BeanBinding
	 */
	public RDF2Bean getRdf2BeanBinding() {
		if (null == rdf2BeanBinding) {
			synchronized (executor) {
			rdf2BeanBinding = new RDF2Bean(getDataModel());
			rdf2BeanBinding.bindAll("org.goodoldai.depths.domain");
			}
		}
		return rdf2BeanBinding;
	}

	/**
	 * 
	 * @return the bean2RDFBinding
	 */
	public Bean2RDF getBean2RDFBinding() {
		if (null == bean2RDFBinding) {
			bean2RDFBinding = new Bean2RDF(getDataModel());
		}
		return bean2RDFBinding;
	}

	/**
	 * 
	 */
	public void flush() {
		getDataProvider().flushDataModel(dataModel);
	}


}
