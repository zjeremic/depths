package org.goodoldai.depths.semanticstuff.util;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.DesignProblem;

import org.goodoldai.depths.semanticstuff.rdfpersistance.DataModelManager;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;

import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.services.peers.PeersProcessing;

public class CacheInitializer {
	
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(CacheInitializer.class);
	public void initializeCacheForProjects(){
	 LOGGER.info("INITIALIZE CACHE FOR PROJECTS");
		 Thread thread = new Thread(new Runnable() {
			    public void run() {
		String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT DISTINCT ?project \n" + 
				"WHERE  {\n" +
					"?project rdf:type depths:Project.\n"+
				"?project depths:hasDesignProblemRef ?dproblem.\n"+
					"?project depths:hasSolution ?solution.\n"+
		 
				"}";
			 
			Collection<String>uris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "project",
							DataModelManager.getInstance().getDataModel());
			if (uris != null && !uris.isEmpty()) {
				for(String uri:uris){
					try {
						LOGGER.info("caching project:"+uri);
						SolutionsDataLayer.getDataLayerInstance().getProjectByUri(uri);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LOGGER.error("Error:"+e.getLocalizedMessage());
					}
				//Project project=ProjectManagament.getInstance().loadResourceByURI(Project.class, uri, false);
				
				}
				
			}
			    }
			    });
		 thread.start();
	}
	public void initializeCacheForDesignProblems(){
		 LOGGER.info("INITIALIZE CACHE FOR DESIGN PROBLEMS");
			 Thread thread = new Thread(new Runnable() {
				    public void run() {
			String queryString = 
					"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
					"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
					"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
					"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
					"SELECT DISTINCT ?designProblem \n" + 
					"WHERE  {\n" +
						"?designProblem rdf:type depths:DesignProblem.\n"+
					
			 
					"}";
				 
				Collection<String>uris = queryService
						.executeOneVariableSelectSparqlQuery(queryString, "designProblem",
								DataModelManager.getInstance().getDataModel());
				if (uris != null && !uris.isEmpty()) {
					for(String uri:uris){
						try {
							LOGGER.info("caching design problem:"+uri);
							SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(uri);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							LOGGER.error("Error:"+e.getLocalizedMessage());
						}
					//Project project=ProjectManagament.getInstance().loadResourceByURI(Project.class, uri, false);
					
					}
					
				}
				initializeCacheForRecommendedPeers();
				    }
				    });
			 thread.start();
		}
	public void initializeCacheForRecommendedPeers(){
		 LOGGER.info("INITIALIZE CACHE FOR RECOMMENDED PEERS");
		 Thread thread = new Thread(new Runnable() {
			    public void run() {
			    	Collection<DesignProblem> dProblems=SolutionsDataLayer.getDataLayerInstance().getAllDesignProblems();
			    	LOGGER.info("****found problems:"+dProblems.size());
			    	for(DesignProblem dProblem:dProblems){
			    		PeersProcessing.getInstance().processPeersForDesignProblem(dProblem);
			    	}
			    	
			    }
		 });
		 thread.start();
	}


}
