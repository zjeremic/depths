package org.goodoldai.depths.semanticstuff.services.context;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.activity.Submitting;
import org.goodoldai.depths.domain.general.LearningContext;
 
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
 

public class LearningContextManagament extends AbstractDAOImpl {

	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(LearningContextManagament.class);
	
	private static class LearningContextManagamentHolder{
		private static final LearningContextManagament INSTANCE=new LearningContextManagament();
	}
	public static LearningContextManagament getInstance(){
		return LearningContextManagamentHolder.INSTANCE;
	}
	public Collection<Submitting> getEvaluatedSubmittionsForTask(String taskUri) throws Exception{
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"SELECT DISTINCT ?submitting \n" + 
			"WHERE  {\n" +
				"?submitting depths:inResponseTo ?submission.\n"+
 				"?submission rdf:type depths:Submission.\n"+
				 
				"<"+taskUri+"> depths:taskToAssess ?submission.\n"+
				"?assessing rdf:type depths:Assessing.\n"+
				" ?assessing depths:inResponseTo <"+taskUri+">.\n " +
			"}";
		 	Collection<String> submittingsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "submitting",
						getDataModel());

		if (submittingsUris != null && !submittingsUris.isEmpty()){
			return loadResourcesByURIs(Submitting.class, submittingsUris, false);
		}
		return null;
	}
	public Collection<LearningContext> getLearningContextForTask(String taskUri) throws Exception{
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"SELECT DISTINCT  ?learningContext \n" + 
			"WHERE  {\n" +
				"?learningContext rdf:type loco:LearningContext.\n"+
				" ?learningContext  loco:contentRef <"+taskUri+">.\n " +
				
			"}";
	 	Collection<String> learningContextUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "learningContext",
						getDataModel());

		if (learningContextUris != null && !learningContextUris.isEmpty()){
			Collection<LearningContext> contexts= loadResourcesByURIs(LearningContext.class, learningContextUris, false);
			return contexts;
		}
		return null;
	}
	public Collection<String> getEvaluationsForSubmitting(String submittingUri) throws Exception{
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"SELECT  ?value \n" + 
			"WHERE  {\n" +
				
				"?assessing depths:submittingRef <"+submittingUri+">.\n " +
					"?userRating rdf:type loco:UserRating.\n"+
					"?userRating loco:isPartOf ?assessing.\n"+
					"?userRating loco:scaleItemRef ?scaleItem.\n"+
					"?scaleItem loco:hasValue ?value.\n"+
					
 
				"}";
		 	Collection<String> evaluationValues = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "value",
						getDataModel());
		return evaluationValues;
	}
	public Collection<String> getEvaluationsForBrainstorming(String brainstormingUri) throws Exception{
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX foaf: <"+Constants.FOAF_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"SELECT ?value \n" + 
			"WHERE  {\n" +
				
				"<"+brainstormingUri+"> loco:hasUserEvaluation ?userRating.\n " +
				 
				"?userRating loco:ratingValue ?value.\n " +
				"}";
		Collection<String> evaluationValues = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "value",
						getDataModel());
		return evaluationValues;
	}
}
