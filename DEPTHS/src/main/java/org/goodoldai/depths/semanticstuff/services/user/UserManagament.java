package org.goodoldai.depths.semanticstuff.services.user;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.sioc.UserAccount;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.rest.json.SolutionServiceUtility;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.NotFoundException;

public class UserManagament extends AbstractDAOImpl {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(UserManagament.class);
	
	private static class UserManagamentHolder{
		private static final UserManagament INSTANCE=new UserManagament();
	}
	public static UserManagament getInstance(){
		return UserManagamentHolder.INSTANCE;
	}
	private UserManagament(){
		if(UserManagamentHolder.INSTANCE!=null){
			throw new IllegalStateException("Already instantiated");
		}
	}
	public User getUserByUsername(String username) {
		//User user=null;
		try {
			String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"SELECT  DISTINCT ?user \n" + 
				"WHERE  {\n" +
					"?user loco:username \""+username+"\"^^<http://www.w3.org/2001/XMLSchema#string>.\n" +
					"?user rdf:type loco:User ." +
				"}";
			Collection<String> userUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "user",
							getDataModel());

			if (userUris != null && !userUris.isEmpty()){
				return loadResourceByURI(User.class, userUris.iterator().next(), false);
			}
			return null;
			//user = loadResourceByURI(User.class, uri, false);
		}catch(NotFoundException nfe){
			 
			//LOGGER.error("User not found error:"+uri);
			return null;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 
			 LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		}
		//return user;
	}
	public User getUserByUri(String uri) {
		User user=null;
		try {
			user = loadResourceByURI(User.class, uri, false);
		}catch(NotFoundException nfe){
			 
			LOGGER.error("User not found error:"+uri);
			return null;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 
			 LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		}
		return user;
	}
	public Collection<Group> getCurrentUserCourseGroups(String currUserUri, String courseUri) {
		try {
			String queryString = 
				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				"SELECT  DISTINCT ?group \n" + 
				"WHERE  {\n" +
					"?group depths:member <"+currUserUri+">.\n" +
					"?group depths:subscribedToCourse <"+courseUri+">.\n" +
					"?group rdf:type depths:Group ." +
				"}";
			 
			Collection<String> groupUris = queryService
					.executeOneVariableSelectSparqlQuery(queryString, "group",
							getDataModel());

			if (groupUris != null && !groupUris.isEmpty()){
				return loadResourcesByURIs(Group.class, groupUris, false);
			}
			return null;
			//user = loadResourceByURI(User.class, uri, false);
		}catch(NotFoundException nfe){
			 
			//LOGGER.error("User not found error:"+uri);
			return null;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 
			 LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		
	}
	}
	public void deleteUserByUsername(String username) throws Exception{
		User userToDelete=getUserByUsername(username);
		if(userToDelete!=null){
			String uri=userToDelete.getUri().toString();
			RDFDataManagement.getInstance().deleteResource(userToDelete, false);
			LOGGER.info("Deleted user:"+username+" uri:"+uri);
		}
	}
	public void deleteUserByUri(String uri) throws Exception{
		User userToDelete=loadResourceByURI(User.class,uri,false);
		if(userToDelete!=null){
			RDFDataManagement.getInstance().deleteResource(userToDelete, false);
			LOGGER.info("Deleted user:"+uri);
		}
	}
 
}
