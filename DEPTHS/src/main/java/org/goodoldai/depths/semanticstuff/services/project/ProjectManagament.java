package org.goodoldai.depths.semanticstuff.services.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.activity.UploadingSolution;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Diagram;
import org.goodoldai.depths.domain.content.Submission;
import org.goodoldai.depths.domain.content.Task;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.NotFoundException;

public class ProjectManagament extends AbstractDAOImpl {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(ProjectManagament.class);

	private static class ProjectManagamentHolder {
		private static final ProjectManagament INSTANCE = new ProjectManagament();
	}

	public static ProjectManagament getInstance() {
		return ProjectManagamentHolder.INSTANCE;
	}

	private ProjectManagament() {
		if (ProjectManagamentHolder.INSTANCE != null) {
			throw new IllegalStateException("Already instantiated");
		}
	}

	public Project getProjectByUri(String uri) {
		Project project = null;
		try {
			//project = loadResourceByURI(Project.class, uri, false);
			project=SolutionsDataLayer.getDataLayerInstance().getProjectByUri(uri);
		} catch (NotFoundException nfe) {

			LOGGER.error("Project not found error:" + uri);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block

			LOGGER.error("Exception in loading project");
			LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		}
		return project;
	}

	public Diagram getDiagramByUri(String uri) {
		Diagram diagram = null;
		try {
			diagram = loadResourceByURI(Diagram.class, uri, false);
		} catch (NotFoundException nfe) {

			LOGGER.error("Diagram not found error:" + uri);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block

			LOGGER.error("Exception in loading diagram");
			LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		}

		return diagram;
	}

	public Collection<Project> getProjectsForUser(String userUri)
			throws Exception {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n" +

				"PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?project \n" + "WHERE  {\n"
				+ "?project rdf:type depths:Project\n"
				+ "{?project  depths:createdBy <" + userUri + ">}\n "
				+ "UNION \n" + "{?project  depths:modifiedBy <" + userUri
				+ ">}\n " + "}";
	 	Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "project",
						getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			return loadResourcesByURIs(Project.class, projectsUris, false);
		}
		return null;
	}

	public boolean checkProjectAvailabilityByDate(Project project, User user) {
		Collection<DesignProblem> dProblemRefs = project
				.getHasDesignProblemRef();
		if(project.getCreatedBy()!=null){
		if(project.getCreatedBy().equals(user)&&project.getModifiedBy()==null){
				return true;
		}
		}
		if(project.getModifiedBy()!=null){
		if(project.getModifiedBy().equals(user)){
			return true;
		}
		}
		if ((dProblemRefs != null) && (!dProblemRefs.isEmpty())) {
			Iterator<DesignProblem> dpIter=dProblemRefs.iterator();
			while(dpIter.hasNext()){
			DesignProblem dProblem = dpIter.next();
			Collection<Task> tasks=dProblem.getTaskRef();
			Date endDate = dProblem.getEndDate();
			for(Task t:tasks){
				if(t instanceof Submission){
					endDate=t.getEndDate();
				}
			}
			Date todayDate = new Date();
			String projectType = dProblem.getProjectType();
			if(projectType!=null){
			if (projectType.equals("teacher_groups")) {
				if(endDate != null){
				if (todayDate.before(endDate)) {
			 				return false;
				}
				}
			}
			}
		}
		}
	
		 
		return true;

	}

	public Collection<Project> getAllProjectsForUser(String userUri)
			throws Exception {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"
				+ "PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?project \n" + "WHERE  {\n"
				+ "?project rdf:type depths:Project\n" +

				".}";

		Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "project",
						getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			return loadResourcesByURIs(Project.class, projectsUris, false);
		}
		return null;
	}

	public Collection<Project> getAllProjectsForDesignProblem(String userUri,
			String designProblemUri) throws Exception {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"
				+ "PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?project \n" + "WHERE  {\n"
				+ "?project rdf:type depths:Project.\n"
				+ "?project depths:hasDesignProblemRef <" + designProblemUri
				+ ">.\n" +

				"}";
 
		Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "project",
						getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			return loadResourcesByURIs(Project.class, projectsUris, false);
		}
		return null;
	}

	public Collection<Project> getAllProjectsForDPNotSubmitted(String userUri,
			String designProblemUri) throws Exception {
		String queryString = "PREFIX rdf: <"
				+ Constants.RDF_NS
				+ "> \n"
				+ "PREFIX sioc: <"
				+ Constants.SIOC_NS
				+ "> \n"
				+ "PREFIX foaf: <"
				+ Constants.FOAF_NS
				+ "> \n"
				+ "PREFIX loco: <"
				+ Constants.LOCO_NS
				+ "> \n"
				+ "PREFIX depths: <"
				+ Constants.DEPTHS_NS
				+ "> \n"
				+ "SELECT  DISTINCT ?project \n"
				+ "WHERE  {\n"
				+ "?project rdf:type depths:Project.\n"
				+ "?project depths:hasDesignProblemRef <"
				+ designProblemUri
				+ ">.\n"
				+ "?uploadingSolution depths:projectRef ?project.\n"
				+ "?uploadingSolution rdf:type depths:UploadingSolution.\n"
				+ "?uploadingSolution loco:sentBy <"
				+ userUri
				+ ">.\n"
				+ "?uploadingSolution depths:submitted \"false\"^^<http://www.w3.org/2001/XMLSchema#boolean>.\n"
				+

				"}";
		 
		Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "project",
						getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			return loadResourcesByURIs(Project.class, projectsUris, false);
		}
		return null;
	}

	public UploadingSolution getUploadingSolution(String userUri,
			String projectUri) {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"
				+ "PREFIX loco: <" + Constants.LOCO_NS + "> \n"
				+ "PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?uploadingSolution \n" + "WHERE  {\n"
				+ "?uploadingSolution depths:projectRef <" + projectUri
				+ ">.\n" + "?uploadingSolution loco:sentBy <" + userUri
				+ ">.\n"
				+ "?uploadingSolution rdf:type depths:UploadingSolution.\n" +

				"}";

		Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString,
						"uploadingSolution", getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			try {
				return loadResourceByURI(UploadingSolution.class, projectsUris
						.iterator().next(), false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
		}
		return null;
	}

	public Collection<Project> getAllAllowedProjectsForUser(String string) throws Exception {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX foaf: <" + Constants.FOAF_NS + "> \n"
				+ "PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT  DISTINCT ?project \n" + "WHERE  {\n"
				+ "?project rdf:type depths:Project\n" +

				".}";
 
		Collection<String> projectsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "project",
						getDataModel());

		if (projectsUris != null && !projectsUris.isEmpty()) {
			return loadResourcesByURIs(Project.class, projectsUris, false);
		}
		return null;
	}


}
