package org.goodoldai.depths.semanticstuff.services.content;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.services.AbstractDAOImpl;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.datalayer.RecommendationDataLayer;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.NotFoundException;

public class ContentManagament  extends AbstractDAOImpl {
	private OntModelQueryService queryService = new OntModelQueryServiceImpl();
	private static final Logger LOGGER = Logger.getLogger(ContentManagament.class);
	
	private static class ContentManagamentHolder{
		private static final ContentManagament INSTANCE=new ContentManagament();
	}
	public static ContentManagament getInstance(){
		return ContentManagamentHolder.INSTANCE;
	}
	private ContentManagament(){
		if(ContentManagamentHolder.INSTANCE!=null){
			throw new IllegalStateException("Already instantiated");
		}
	}
	public DesignProblem getDesignProblemByUri(String uri) {
		DesignProblem dProblem=null;
		try {
			if(!uri.equals("")||(uri==null)){
			//dProblem = loadResourceByURI(DesignProblem.class, uri, false);
			dProblem=ProjectManagament.getInstance().loadResourceByURI(DesignProblem.class,uri,false);
			}
		}catch(NotFoundException nfe){
		 
			LOGGER.error("DesignProblem not found error:"+uri);
			return null;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 
			LOGGER.error("Exception in loading DesignProblem");
			 LOGGER.error("Error:"+e.getLocalizedMessage());
			return null;
		}
		return dProblem;
	}
	public Collection<Course> getCoursesForUser(String userUri) throws Exception{
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"SELECT DISTINCT ?course \n" + 
			"WHERE  {\n" +
				"?course rdf:type loco:Course.\n"+
				"?course  depths:hasSubscriber <"+userUri+">\n ." +
				 
			"}";
		 	Collection<String> coursesUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "course",
						getDataModel());

		if (coursesUris != null && !coursesUris.isEmpty()){
			return loadResourcesByURIs(Course.class, coursesUris, false);
		}
		return null;
	}

	public DesignProblem getDesignProblemForTask(String taskUri)
			throws Exception {
		String queryString = "PREFIX rdf: <" + Constants.RDF_NS + "> \n"
				+ "PREFIX sioc: <" + Constants.SIOC_NS + "> \n"
				+ "PREFIX loco: <" + Constants.LOCO_NS + "> \n"
				+ "PREFIX depths: <" + Constants.DEPTHS_NS + "> \n"
				+ "SELECT DISTINCT  ?designProblem \n" + "WHERE  {\n"
				+ "?designProblem\n depths:taskRef <" + taskUri + "> ." +

				"}";
		Collection<String> dProblemsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString,
						"designProblem", getDataModel());
		if (dProblemsUris != null && !dProblemsUris.isEmpty()) {
			return SolutionsDataLayer.getDataLayerInstance()
					.getDesignProblemByUri(dProblemsUris.iterator().next());
		}
		// if (dProblemsUris != null && !dProblemsUris.isEmpty()){
		// return loadResourceByURI(DesignProblem.class,
		// dProblemsUris.iterator().next(), false);
		// }
		return null;
	}
	public Collection<DesignProblem> getDesignProblemsForCourse(
			String courseUri) throws Exception {
		String queryString = 
			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			"SELECT DISTINCT  ?designProblem \n" + 
			"WHERE  {\n" +
				"<"+courseUri+">  depths:containsDesignProblem ?designProblem\n ." +
				 
			"}";
		 	Collection<String> dProblemsUris = queryService
				.executeOneVariableSelectSparqlQuery(queryString, "designProblem",
						getDataModel());

		if (dProblemsUris != null && !dProblemsUris.isEmpty()){
			return loadResourcesByURIs(DesignProblem.class, dProblemsUris, false);
		}
		return null;
	}
	public void deleteResourceByUri(String resourceUri) throws Exception{
		 Resource resource=loadResourceByURI(Resource.class, resourceUri, false);
		 if(resource!=null){
			 RDFDataManagement.getInstance().deleteResource(resource, false);
				LOGGER.info("Deleted resource:"+resourceUri);
		 }
	}

}
