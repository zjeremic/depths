package org.goodoldai.depths.semanticstuff.services;

public class AbstractDAOManager {
	private static class AbstractDAOHolder {
		private static final AbstractDAOImpl INSTANCE = new AbstractDAOImpl();
	}

	 
	public static AbstractDAOImpl getAbstractDAOInstance() {
		return AbstractDAOHolder.INSTANCE;
	}
}
