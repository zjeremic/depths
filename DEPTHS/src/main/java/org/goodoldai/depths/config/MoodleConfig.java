package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class MoodleConfig {
	@Element(name = "database-host", required = true)
	public String databaseHost;
	
	@Element(name = "database-name", required = true)
	public String databaseName;
	
	@Element(name = "database-port", required = true)
	public int port = 0;
	
	@Element(name = "database-user", required = true)
	public String databaseUser;
	
	@Element(name = "database-password", required = true)
	public String databasePassword;
	
	@Element(name = "moodle-root", required = true)
	public String moodleRoot;
	
	@Element(name = "table-prefix", required = true)
	public String tablePrefix;
}
