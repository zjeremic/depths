package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class SMTPConfig {

	@Element(name = "smtp-host", required = true)
	public String host;
	
	@Element(name = "smtp-user", required = true)
	public String user;
	
	@Element(name = "smtp-pass", required = true)
	public String pass;
	
	@Element(name = "smtp-port", required = true)
	public int port;
	
	@Element(name = "smtp-starttls-enable", required = false)
	public boolean starttlsenable = false;
	
	@Element(name = "smtp-auth", required = false)
	public boolean auth = false;
}
