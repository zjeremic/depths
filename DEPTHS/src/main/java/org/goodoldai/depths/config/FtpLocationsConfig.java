package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class FtpLocationsConfig {

	@Element(name = "ftp-url", required = true)
	public String ftpUrl;
	
	@Element(name = "ftp-port", required = true)
	public String ftpPort;
	
	@Element(name = "security-ssl", required = true)
	public String securitySSL;
	
	@Element(name = "ftp-location-projects", required = true)
	public String ftpLocationProjects;
	
	@Element(name = "ftp-location-images", required = true)
	public String ftpLocationImages;
	
	 
}
