package org.goodoldai.depths.config;

import org.simpleframework.xml.Element;

public class FtpRepositoryConfig {

	@Element(name = "ftp-user", required = true)
	public FtpUserConfig ftpUser;
	
	@Element(name = "ftp-locations", required = true)
	public FtpLocationsConfig ftpLocations;
}
