package org.goodoldai.depths.jsonstuff;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.services.resources.ResourceQueries;

public class ContentJSONExporter {
	private static final Logger LOGGER = Logger.getLogger(ContentJSONExporter.class);

	private static class ContentJSONExporterHolder {
		private static final ContentJSONExporter INSTANCE = new ContentJSONExporter();

	}

	public static ContentJSONExporter getInstance() {
		return ContentJSONExporterHolder.INSTANCE;
	}

	public String getCoursesAsJSON(Collection<Course> courses)
			throws JSONException {
		JSONStringer js = new JSONStringer();
		js.array();

		for (Course course : courses) {
			js.object();
			js.key("courseuri").value(course.getUri().toString());
			js.key("title").value(course.getTitle());
			js.key("coursedescription").value(course.getDescription());
			js.key("designproblemsnumber").value(
					course.getContainsDesignProblem().size());
			js.endObject();
		}

		js.endArray();

		return js.toString();
	}

	public String getDesignProblemsAsJSON(Collection<DesignProblem> dProblems)
			throws JSONException {
		JSONStringer js = new JSONStringer();
		js.array();
		if (dProblems != null) {
			for (DesignProblem problem : dProblems) {
				js.object();
				js.key("problemuri").value(problem.getUri().toString());
				js.key("title").value(problem.getTitle());
				js.key("problemdescription").value(problem.getDescription());
				js.key("datecreated").value(problem.getDateCreated());
				User creator = problem.getCreator();
				if (creator != null) {
					js.key("createdBy").value(creator.getUri().toString());
					js.key("creatorfirstname").value(creator.getFirstname());
					js.key("creatorlastname").value(creator.getLastname());
				}
				if (!problem.getIsDescribedByProject().isEmpty()) {
					Collection<Project> projectsIsDescribedBy = problem
							.getIsDescribedByProject();
					for (Project proj : projectsIsDescribedBy) {
						// js.object();
						js.key("describedbyprojecturi").value(
								proj.getUri().toString());
						js.key("describedbyprojecttitle")
								.value(proj.getTitle());
						// js.endObject();
					}

				}
				js.endObject();

			}
		} else {
			LOGGER.info("Design problems array is null");
		}

		js.endArray();

		return js.toString();
	}

	public String getDesignProblemDescriptionAsJSON(DesignProblem dProblem)
			throws JSONException {
		JSONStringer js = new JSONStringer();
		js.array();
		if (dProblem != null) {
			js.object();
			js.key("problemuri").value(dProblem.getUri().toString());
			js.key("title").value(dProblem.getTitle());
			js.key("problemdescription").value(dProblem.getDescription());
			js.key("datecreated").value(dProblem.getDateCreated());
			User creator = dProblem.getCreator();
			if (creator != null) {
				js.key("createdBy").value(creator.getUri().toString());
				js.key("creatorfirstname").value(creator.getFirstname());
				js.key("creatorlastname").value(creator.getLastname());
			}
			js.array();
			js.object();
				js.key("pagetitle").value("Google");
				js.key("href").value("www.google.com");
			js.endObject();
			js.object();
				js.key("pagetitle").value("Wikipedia");
				js.key("href").value("www.wikipedia.org");
			js.endObject();
			js.endArray();

			js.endObject();
		}
		js.endArray();

		return js.toString();
	}

	public String getListOfRelatedConceptsForProblem(DesignProblem dProblem) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		if (dProblem != null) {
			//js.key("concepts");
			 js.object();
			  js.key("problemuri").value(dProblem.getUri().toString());
			 
			
			 js.endObject();
			 Collection<Concept> recommendedConcepts=dProblem.getHasRelevantConcept();
			 js.object();
			 js.key("recommendedconcepts");
			 js.array();
			if(!recommendedConcepts.isEmpty()){
				
				for(Concept concept:recommendedConcepts){
					js.object();
					js.key("conceptname").value(concept.getTitle());
					js.key("concepturi").value(concept.getUri().toString());
					js.endObject();
				}
				
			}
			 js.endArray();
			 js.endObject();
			 Collection<Concept> relatedConcepts=ResourceQueries.getInstance().getConceptsRelatedToBrainstormByProblem(dProblem.getUri().toString());
			 js.object();
			 js.key("relatedconcepts");
			 js.array();
			if(!relatedConcepts.isEmpty()){
				
				for(Concept concept:relatedConcepts){
					js.object();
					js.key("conceptname").value(concept.getTitle());
					js.key("concepturi").value(concept.getUri().toString());
					js.endObject();
				}
				
			}
			 js.endArray();
			 js.endObject();
		}
		js.endArray();
		return js.toString();
	}

	public String getListOfRecommendedConceptsForProblem(DesignProblem dProblem) throws JSONException {
		JSONStringer js = new JSONStringer();
		js.array();
		if (dProblem != null) {

			 Collection<Concept> recommendedConcepts=dProblem.getHasRelevantConcept();

			
			if(!recommendedConcepts.isEmpty()){
				 js.object();
				 int i=0;
				for(Concept concept:recommendedConcepts){

					js.key(String.valueOf(i)).value(concept.getUri().toString());
					i++;

				}
				js.endObject();	
			}
			

		}
		js.endArray();
		return js.toString();
	}
}
