package org.goodoldai.depths.jsonstuff;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.annotation.ParentTag;
import org.goodoldai.depths.domain.annotation.Tag;
import org.goodoldai.depths.domain.annotation.UserNote;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.User;
 

public class TagsJSONExporter {
	private static class TagsJSONExporterHolder{
		private static final TagsJSONExporter INSTANCE=new TagsJSONExporter();
	}
	public static TagsJSONExporter getInstance(){
		return TagsJSONExporterHolder.INSTANCE;
	}
	public JSONStringer getTagsAsJsonObject(Collection<Tag> tags, JSONStringer js) throws JSONException{
		js.object().key("tags");
		js.array();
		 Map<Resource,Integer> tagsFrequency=new HashMap<Resource,Integer>();
		 if(tags!=null){
		for (Tag tag:tags) {
			Resource parentTag= tag.getHasParentTag();
			if (tagsFrequency.containsKey(parentTag)){
				 
				 Integer hits=tagsFrequency.get(parentTag);
				 hits=hits+1;
				 tagsFrequency.remove(parentTag);
				 tagsFrequency.put(parentTag, hits);
				 
			 }else{
				 Integer hits=Integer.valueOf(1);
				 //new Integer(1);
				 tagsFrequency.put(parentTag, hits);
				 
			 }
		}
		 }
		Set<Resource> set = tagsFrequency.keySet();
		 Iterator<Resource> iter = set.iterator();
		 while(iter.hasNext()){
			 Resource pTag =   iter.next();
			 Integer frequency = (Integer) tagsFrequency.get(pTag);
			 js.object();
			 if (pTag instanceof ParentTag){
			 	js.key("tag").value(((ParentTag) pTag).getHasContent());
			 }else if (pTag instanceof Concept){
				 js.key("tag").value(pTag.getTitle());
			 }
			 	js.key("freq").value(frequency);
			 js.endObject();
		 }
		 js.endArray();
		 js.endObject();
		 return js;
	}
	public String getTagsAsJSON(Collection<Tag> tags)
			throws JSONException {
		JSONStringer js = new JSONStringer();
		this.getTagsAsJsonObject(tags, js);
	
	 
 
		return "("+js.toString()+")";
	}
	public String getIdeaAnnotationsAsJSON(UserNote uNote, Collection<Tag> tags) throws JSONException {
		JSONStringer js = new JSONStringer();
		js.object();
		js.key("noteuri").value(uNote.getUri().toString());
		js.key("note").value(uNote.getHasContent());
		js.key("visibility").value(uNote.getVisibility());
	//	if(uNote.getNoteType()!=null){
			js.key("commenttype").value(uNote.getNoteType());
		//}
		js.key("tags");
		js.array();
		if(tags!=null){
		for(Tag tag:tags){
			js.object();
			js.key("tag").value(tag.getHasContent());
			js.endObject();
		}
		}
		js.endArray();
		js.endObject();
		
	 
		return js.toString();
	}
	public String getPeersNotesForIdeaJSON(User user, Collection<UserNote> uNotes) throws JSONException {
		JSONStringer js=new JSONStringer();
		js.object();
		js.key("success").value("true");
		js.key("notes");
		js.array();
		for(UserNote uNote:uNotes){
			User madeBy=uNote.getMadeBy();
			if(uNote!=null){
			if(uNote.getVisibility().equals("public")||(madeBy.equals(user))){
				js.object();
				String username=madeBy!=null ? madeBy.getUsername() : "";
				String firstname=madeBy!=null ? madeBy.getFirstname() : "";
				String lastname=madeBy!=null ? madeBy.getLastname() : "";
				String identifier=madeBy!=null ? madeBy.getIdentifier() : "";
				String useruri=madeBy!=null ? madeBy.getUri().toString() : "";
				js.key("username").value(username);
				js.key("firstname").value(firstname);
				js.key("lastname").value(lastname);
				js.key("userid").value(identifier);
				js.key("useruri").value(useruri);
				js.key("note").value(uNote.getHasContent());
				js.key("created").value(uNote.getDateCreated().toString());
				if(uNote.getNoteType()!=null){
				js.key("commenttype").value(uNote.getNoteType().toString());
				}else{
					js.key("commenttype").value("");
				}
				js.endObject();
			} 
		}
			 
		}
		js.endArray();
		js.endObject();
		
		return "("+js.toString()+")";
	}
	public String getConceptsAsTagsJSON(Map<Concept, Integer> conceptFrequency) throws JSONException {
		JSONStringer js=new JSONStringer();
		js.object().key("concepts");
		js.array();
		Iterator it=conceptFrequency.entrySet().iterator();
		while(it.hasNext()){
			js.object();
			Map.Entry<Concept,Integer> pairs=(Map.Entry<Concept, Integer>)it.next();
			Concept c=pairs.getKey();
			Integer frequency=pairs.getValue();
			js.key("concept").value(c.getTitle());
			 js.key("uri").value(c.getUri().toString());
		 	js.key("freq").value(frequency);
			 
			js.endObject();
		}
		js.endArray();
		js.endObject();
		
	 
		return "("+js.toString()+")";
		
	}
}
