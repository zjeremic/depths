package org.goodoldai.depths.jsonstuff;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
 

public class UserJSONExporter {

	private static class UserJSONExporterHolder{
		private static final UserJSONExporter INSTANCE=new UserJSONExporter();
	}
	public static UserJSONExporter getInstance(){
		return UserJSONExporterHolder.INSTANCE;
	}
	public String checkUserData(String username, String password) throws JSONException{
		 	JSONStringer js=new JSONStringer();
		js.array();
	 	User user=UserManagament.getInstance().getUserByUsername(username);
		 
		boolean userexists=true;
		boolean passcorrect=true;
		if (user==null){
			 
			userexists=false;
			passcorrect=false;
		}else{
			String pass=user.getPassword();
		 	password=password.toLowerCase();
			pass=pass.toLowerCase();
		  	if((password.equals(pass))||(pass.equals(""))){
				passcorrect=true;
			}else{
				passcorrect=false;
			}
		}
		
		String pass="";
			js.object();
			js.key("username").value(username);
		if(userexists==true){
			if(passcorrect==true){
			js.key("access").value("true");
			js.key("message").value("");
			js.key("uri").value(user.getUri());
			js.key("firstname").value(user.getFirstname());
			js.key("lastname").value(user.getLastname());
			}else{
				js.key("access").value("false");
				js.key("message").value("Wrong password.");
			}
		}else{
			js.key("access").value("false");
			js.key("message").value("User does not exists.");
		}
		js.endObject();
		
		js.endArray();
	 
		return js.toString();
	}

}
