package org.goodoldai.depths.eventsprocessor;

 

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONStringer;

import org.goodoldai.depths.annotation.AnnotationManager;
import org.goodoldai.depths.domain.activity.Assessing;
import org.goodoldai.depths.domain.activity.Brainstorming;
import org.goodoldai.depths.domain.activity.DiscussAsynchonously;
import org.goodoldai.depths.domain.activity.Submitting;

import org.goodoldai.depths.domain.annotation.UserNote;
import org.goodoldai.depths.domain.annotation.UserRating;
import org.goodoldai.depths.domain.content.Assessment;
import org.goodoldai.depths.domain.content.Brainstorm;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Posting;
import org.goodoldai.depths.domain.content.Submission;
import org.goodoldai.depths.domain.events.ReadingPost; 
import org.goodoldai.depths.domain.events.SubmittingPost;
import org.goodoldai.depths.domain.general.DiscussCategory;
import org.goodoldai.depths.domain.general.LearningContext;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.general.Scale;
import org.goodoldai.depths.domain.general.ScaleItem;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.Grouping;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.logging.ActivityLogging;
import org.goodoldai.depths.scheduler.EmailNotification;
import org.goodoldai.depths.semanticstuff.rdfpersistance.urigenerator.URIBuilder;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.semanticstuff.util.StringUtils;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.services.peers.PeersProcessing;
import org.goodoldai.depths.utility.NumberUtility;
import org.goodoldai.depths.utility.OntologyClassesUtility;
import org.goodoldai.depths.utility.StringUtility;

@SuppressWarnings("unchecked")
public class InsertMoodleTable {
	private static final Logger LOGGER = Logger
			.getLogger(InsertMoodleTable.class);

	@SuppressWarnings("rawtypes")
	public String addAssessingData(final JSONObject rating, JSONArray criterias,
			final String action, final String sessionId) throws Exception {
		 	JSONStringer jsStringer = new JSONStringer();
		jsStringer.array();
		final int ratId = Integer.parseInt(rating.getString("id"));
	 
		// Assessing
		    final Assessing assessing;
		String assessingUri = null;
		if (action.equals("update")) {
			assessingUri = rating.getString("assessinguri");
			assessing = (Assessing) new ClassFactory().createInstance(
					Assessing.class, assessingUri);
		} else {
			jsStringer.object();
			assessing = new Assessing();
			assessingUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(assessing);
			assessing.setUri(assessingUri);
			jsStringer.key("operation").value("setfield");
			jsStringer.key("assessinguri").value(assessingUri);
			jsStringer.endObject();
		}

		assessing.setDateTimeSent(new Date(rating.getLong("time")*1000));

		// Assessment
		String assessmentUri = rating.getString("assessmenturi");
		final Assessment assessment = (Assessment) new ClassFactory().createInstance(
				Assessment.class, assessmentUri);
		assessing.setInResponseTo(assessment);

		// User
		String userUri = rating.getString("useruri");
		//final User user = (User) new ClassFactory().createInstance(User.class,
				//userUri);
		LOGGER.info("Assessing info for user:"+userUri);
		final User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
		assessing.setSentBy(user);

		// Submitting submissionid
		String submittingUri = rating.getString("submissionuri");
		final Submitting submitting = (Submitting) new ClassFactory().createInstance(
				Submitting.class, submittingUri);
		assessing.setSubmittingRef(submitting);

		// UserNote
		UserNote uNote = null;
		if (action.equals("update")) {
			LOGGER.info("update is not finished");
		} else {
			uNote = new UserNote();
			String uNoteUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(uNote);
			uNote.setUri(uNoteUri);
			uNote.setIsPartOf(assessing);
		}
		uNote.setHasContent(rating.getString("overallcomments"));
		 
		RDFDataManagement.getInstance().saveResource(uNote, false);

		// LearningContext
		LearningContext lContext = new LearningContext();
		String lContextUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(lContext);
		lContext.setUri(lContextUri);
		lContext.setContentRef(assessment);
		lContext.setUserRef(user);
		lContext.setActivityRef(assessing);
		RDFDataManagement.getInstance().saveResource(lContext, false);

		RDFDataManagement.getInstance().saveResource(assessing, false);
		this.addUriMapping(jsStringer, "urimapping", assessing, ratId);
		PeersProcessing.getInstance().processLearningContext(lContext, assessment);

		// Collection<ScaleItem> assessings=new ArrayList<ScaleItem>();
		for (int i = 0; i < criterias.length(); i++) {
			String critStr = (String) criterias.get(i);
			final JSONObject criteriaObj = new JSONObject(critStr);
		 	// Assessing
			jsStringer.object();
			final Assessing cAssessing = new Assessing();
			final String cAssessingUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(cAssessing);
			jsStringer.key("operation").value("setotherfield");
			jsStringer.key("assessinguri").value(cAssessingUri);
			jsStringer.key("criteriaratingid").value(criteriaObj.getString("id"));
			jsStringer.endObject();
			Runnable r1 = new Runnable() {
				  public void run() {
					  try {		 
		 		cAssessing.setUri(cAssessingUri);
		 	
				cAssessing.setDateTimeSent(new Date(criteriaObj.getLong("time")));
			
			

			cAssessing.setInResponseTo(assessment);

			cAssessing.setSentBy(user);

			cAssessing.setSubmittingRef(submitting);

			// Scale
			String scaleUri = criteriaObj.getString("criteriauri");
			Scale scale = (Scale) new ClassFactory().createInstance(
					Scale.class, scaleUri);
			// RDFDataManagement.getInstance().saveResource(scale,false);
			cAssessing.setUsingScale(scale);
			 
			// UserRating
			UserRating uRating = new UserRating();
			String uRatingUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(uRating);
			uRating.setUri(uRatingUri);
			String cAssessmentUri = criteriaObj.getString("ratinguri");
		 	ScaleItem sItem = (ScaleItem) new ClassFactory().createInstance(
					ScaleItem.class, cAssessmentUri);
		 	uRating.setScaleItemRef(sItem);
		   uRating.setIsPartOf(cAssessing);
			RDFDataManagement.getInstance().saveResource(uRating, false);

			// UserNote
			UserNote cuNote = new UserNote();
			String cuNoteUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(cuNote);
			cuNote.setUri(cuNoteUri);
			cuNote.setIsPartOf(cAssessing);
			String content=StringUtils.checkInputContent(criteriaObj.getString("comment"));
			content=StringUtils.checkInputContentSize(content,430);
			cuNote.setHasContent(content);
			RDFDataManagement.getInstance().saveResource(cuNote, false);

			LearningContext clContext = new LearningContext();
			String clContextUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(clContext);

			clContext.setUri(clContextUri);
			clContext.setContentRef(assessment);
			clContext.setUserRef(user);
			clContext.setActivityRef(assessing);
			 
			RDFDataManagement.getInstance().saveResource(clContext, false);

			RDFDataManagement.getInstance().saveResource(cAssessing, false);
		 			  } catch (Exception e) {
							// TODO Auto-generated catch block
							LOGGER.error("Error:"+e.getLocalizedMessage());
						}
				  };
				  };
				  new Thread(r1).start();
		}
		ActivityLogging.getInstance().storeActivityRelatedLogData("assessing", sessionId,assessing.getDateTimeSent() , user, assessing, null);
		RDFDataManagement.getInstance().saveResource(assessing, false);
		jsStringer.endArray();

		return jsStringer.toString();
	}

	public String addCriteriaRatings(JSONObject criteria, JSONArray ratings,
			String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		int critId = Integer.parseInt(criteria.getString("id"));
		 
		String scaleUri = addScaleForAssessmentData(js, "modelling_crit_name",
				criteria, critId, action);
		js.object();
		js.key("operation").value("setfield");
		js.key("scaleuri").value(scaleUri);
		js.endObject();
		Collection<ScaleItem> scaleItems = new ArrayList<ScaleItem>();
		for (int i = 0; i < ratings.length(); i++) {
		 

			String ratingStr = (String) ratings.get(i);
			JSONObject ratingObj = new JSONObject(ratingStr);

			int ratingId = Integer.parseInt(ratingObj.getString("id"));

			scaleItems.add(addPossibleValueForScaleData(js,
					"modelling_crit_rating", ratingObj, ratingId, scaleUri,
					action));
		}
		for (ScaleItem sItem : scaleItems) {
			js.object();
			js.key("operation").value("setotherfield");
			js.key("tablename").value("modelling_crit_rating");
			js.key("scaleitemuri").value(sItem.getUri().toString());
			js.key("scaleitemid").value(sItem.getIdentifier());

			js.endObject();
		}
		js.endArray();
		 
		return js.toString();
	}

	public String checkInsertTableTypeAndProcess(String table,
			JSONObject dataobject, JSONObject addon, int id, String action, String sessionId)
			throws Exception {
		LOGGER.info("checkInsertTable:" + table);
			String response = "[]";
		if (table.equals("user")) {
			response = addUserData(table, dataobject,addon, id, action);
		} else if (table.equals("forum_posts")) {
			addForumPostsData(table, dataobject, id, action);
		} else if (table.equals("forum_read")) {
			addForumReadData(table, dataobject, id, action);
		}

		else if (table.equals("forum_ratings")) {
			addForumRatingsData(table, dataobject, id, action);
		} else if (table.equals("forum_discussions")) {
			addForumDiscussionsData(table, dataobject, id, action);
		} else if (table.equals("forum")) {
			addForumData(table, dataobject, id, action);
		} else if (table.equals("course")) {
			response = addCourseData(table, dataobject,addon, id, action);
		} else if (table.equals("lesson")) {
			response = addLessonData(table, dataobject, id, action);
		}

		else if (table.equals("resource")) {
			addResourceData(table, dataobject, id, action);
		} else if (table.equals("workshop")) {
			addWorkshopData(table, dataobject, id, action);
		} else if (table.equals("workshop_submissions")) {
			addWorkshopSubmissionsData(table, dataobject, id, action);
		} else if (table.equals("workshop_assessments")) {
			addWorkshopAssessmentData(table, dataobject, id, action);
		} else if (table.equals("quiz")) {
			addQuizData(table, dataobject, id, action);
		} else if (table.equals("question")) {
			addQuestionData(table, dataobject, id, action);
		} else if (table.equals("quiz_attempts")) {
			addQuizAttemptsData(table, dataobject, id, action);
		} else if (table.equals("quiz_question_instances")) {
			addQuizQuestionInstances(table, dataobject, id, action);
		} else if (table.equals("question_answers")) {
			addQuestionAnswersData(table, dataobject, id, action);
		} else if (table.equals("question_states")) {
			addQuestionStatesData(table, dataobject, id, action);
		} else if (table.equals("chat")) {
			addChatData(table, dataobject, id, action);
		} else if (table.equals("chat_messages")) {
			addChatMessagesData(table, dataobject, id, action);
		} else if (table.equals("message")) {
			addMessageData(table, dataobject, id, action);
		} else if (table.equals("message_read")) {
			addMessageReadData(table, dataobject, id, action);
		} else if (table.equals("modelling")) {
			response = addDesignProblemData(table, dataobject, addon, id, action);
		} else if (table.equals("modelling_files")){
			response=addFileSubmitted(table,dataobject,addon,id,action,sessionId);
		}
		
		else if (table.equals("modelling_tasks")) {
			String problemType = dataobject.getString("type");

			if (problemType.equals("brainstorm")) {
				response = addBrainstorm_Data(table, dataobject, addon, id,
						action);
			} else if (problemType.equals("submitproject")) {
				response = addSubmission_Data(table, dataobject, addon, id,action, sessionId);
			} else if (problemType.equals("projectassessment")) {
				response = addAssessment_Data(table, dataobject, addon, id,action);
			}else if(problemType.equals("submit")){
				response = addSubmission_Data(table, dataobject, addon, id,action, sessionId);
			}else if (problemType.equals("assessment")){
				response = addAssessment_Data(table, dataobject, addon, id,action);
			}
		} else if (table.equals("modelling_ideas")) {
			response = addDesignproblemIdeasData(table, dataobject, addon, id,action, sessionId);
		} else if (table.equals("scale")) {
			response = addScaleData(table, dataobject, addon,id, action);
		} else if (table.equals("modelling_idea_ratings")) {
			response = addDesignproblemIdeaRatingsData(table, dataobject,addon, id, action, sessionId);
		} else if (table.equals("modelling_projects")) {
			response = addDesignproblemSubmittingData(table, dataobject, addon,
					id,action, sessionId);
			 
		} else if (table.equals("modelling_crit_name")) {
			// response=addScaleForAssessmentData(table,dataobject, addon,id);
		} else if (table.equals("modelling_crit_rating")) {
			// response=addPossibleValueForScaleData(table,dataobject,addon,
			// id);
		} else if (table.equals("modelling_as_crit_rating")) {
			LOGGER.error("reached unprocessed code");
			// response=addAssessingData(table,dataobject, id, action);
		} else if (table.equals("modelling_as_rating")) {
			LOGGER.error("reached unprocessed code");
			// response=addAssessingOverallCommentData(table,dataobject, id,
			// action);
		} else if (table.equals("role_assignments")) {
			response = assignRoleToStudent(table, dataobject, addon, id, action);
		}else if (table.equals("groups")){
			response=addNewGroup(table,dataobject,addon,id,action);
		}else if(table.equals("groups_members")){
			addGroupMember(table,dataobject,addon,id,action);
		}else if (table.equals("groupings")){
			response=addNewGrouping(table,dataobject,addon,id,action);
		}else if (table.equals("groupings_groups")){
			connectGroupingsToGroup(table,dataobject,addon,id,action);
		}
		LOGGER.info("response:"+response);
		return response;
	}

	
	private String addFileSubmitted(String table, JSONObject dataobject,
			JSONObject addon, int id, String action, String sessionId) throws Exception {
		String userUri = addon.getString("useruri");
		User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
		String filename=dataobject.getString("file");
		
		
		String taskname=addon.getString("taskname");
		String taskUri=addon.getString("taskuri");
		Submission task=RDFDataManagement.getInstance().loadResourceByURI(Submission.class, taskUri, false);
		
		EmailNotification emailNotification=new EmailNotification();
		emailNotification.fileUploadedNotification(user,task);
		return null;
	}

	private void addGroupMember(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		// TODO Auto-generated method stub
		String groupUri=addon.getString("groupuri");
		 Group group = (Group) new ClassFactory().createInstance(
					Group.class, groupUri);
		 String userUri = addon.getString("useruri");
			//User user = (User) new ClassFactory().createInstance(User.class,
					//userUri);
			User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
			group.addMember(user);
			user.addGroup(group);
			RDFDataManagement.getInstance().saveResource(group, false);
			RDFDataManagement.getInstance().updateResource(user, false);
	}
	@SuppressWarnings("rawtypes")
	private void connectGroupingsToGroup(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		// TODO Auto-generated method stub
		String groupUri=addon.getString("groupuri");
				 Group group = (Group) new ClassFactory().createInstance(
					Group.class, groupUri);
		 String groupingUri = addon.getString("groupinguri");
			Grouping grouping = (Grouping) new ClassFactory().createInstance(Grouping.class,
					groupingUri);
			grouping.addGroup(group);
			 
			RDFDataManagement.getInstance().saveResource(group, false);
			RDFDataManagement.getInstance().updateResource(grouping, false);
	}

	@SuppressWarnings("rawtypes")
	private String addNewGroup(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();

		// Group
		Group group=null;
		String groupUri=null;
		if (action.equals("update")) {
			groupUri = addon.getString("groupuri");
			group = (Group) new ClassFactory().createInstance(
					Group.class, groupUri);
		}else{
			group = new Group();
			groupUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(group);
			group.setUri(groupUri);
			addUriMapping(js, "urimapping", group, id);
		}
			
		 
		 group.setGroupname(dataobject.getString("name"));
		 if(dataobject.has("description")){
		 group.setDescription(dataobject.getString("description"));
		 }
		 String courseUri=addon.getString("courseuri");
		 Course course = (Course) new ClassFactory().createInstance(
					Course.class, courseUri);
		  group.addCourse(course);
		  
		RDFDataManagement.getInstance().saveResource(group, false);
		
		js.endArray();
		 
		return js.toString();
	}
	private String addNewGrouping(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();

		// Grouping
		Grouping grouping=null;
		 String groupingUri=null;
		if (action.equals("update")) {
			groupingUri = addon.getString("groupinguri");
			grouping = (Grouping) new ClassFactory().createInstance(
					Grouping.class, groupingUri);
		}else{
			grouping = new Grouping();
			groupingUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(grouping);
			grouping.setUri(groupingUri);
			addUriMapping(js, "urimapping", grouping, id);
		}
			
		 
		 grouping.setGroupingname(dataobject.getString("name"));
		 if(dataobject.has("description")){
		 grouping.setDescription(dataobject.getString("description"));
		 }
		 String courseUri=addon.getString("courseuri");
		 Course course = (Course) new ClassFactory().createInstance(
					Course.class, courseUri);
		  grouping.setCourseRef(course);
		  
		RDFDataManagement.getInstance().saveResource(grouping, false);
		
		js.endArray();
		 
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String assignRoleToStudent(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		String userUri = addon.getString("useruri");
		//User user = (User) new ClassFactory().createInstance(User.class,
				//userUri);
		User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);

		String courseUri = addon.getString("courseuri");
		Course course = (Course) new ClassFactory().createInstance(
				Course.class, courseUri);
		 user.addCourse(course);
		 RDFDataManagement.getInstance().saveResource(user, false);
		
		course.addUser(user);
		RDFDataManagement.getInstance().saveResource(course, false);
		LOGGER.info("Some Role should be assigned to user here");
		return null;
	}

	private String addLessonData(String table, JSONObject dataobject, int id,
			String action) {

		return null;
	}

	private void addMessageReadData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addMessageData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addChatMessagesData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addChatData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuestionStatesData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuestionAnswersData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuizQuestionInstances(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuizAttemptsData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuestionData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addQuizData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addWorkshopAssessmentData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addWorkshopSubmissionsData(String table,
			JSONObject dataobject, int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addWorkshopData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addResourceData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addForumData(String table, JSONObject dataobject, int id,
			String action) {

		LOGGER.info("This method is not processed");
	}

	private void addForumDiscussionsData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	private void addForumRatingsData(String table, JSONObject dataobject,
			int id, String action) {

		LOGGER.info("This method is not processed");
	}

	@SuppressWarnings("rawtypes")
	private ScaleItem addPossibleValueForScaleData(JSONStringer js,
			String table, JSONObject dataobject, int id, String scaleUri,
			String action) throws Exception {
		 
		Scale scale = (Scale) new ClassFactory().createInstance(Scale.class,
				scaleUri);
		String scaleId = dataobject.getString("criteriaid");

		// ScaleItem
		ScaleItem scaleItem = new ScaleItem();
		String scaleItemUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(scaleItem);
		scaleItem.setUri(scaleItemUri);
		scaleItem.setHasLabel(dataobject.getString("name"));
		scaleItem.setHasValue(dataobject.getDouble("value"));
		scaleItem.setIdentifier(String.valueOf(id));
		RDFDataManagement.getInstance().saveResource(scaleItem, false);
		scale.addScaleItem(scaleItem);

		RDFDataManagement.getInstance().saveResource(scale, false);
		this.addUriMappingWithParameter(js, "urimappingwithparameter",
				scaleItem, id, scaleId);

		return scaleItem;
	}

	@SuppressWarnings("rawtypes")
	private String addScaleForAssessmentData(JSONStringer js, String table,
			JSONObject dataobject, int id, String action) throws Exception {

		// Scale
		String scaleUri = null;
		Scale scale = null;
		if (action == "update") {

		} else {
			scale = new Scale();
			scaleUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(scale);
			scale.setUri(scaleUri);
		}

		scale.setTitle(dataobject.getString("name"));
		RDFDataManagement.getInstance().saveResource(scale, false);
		// Assessment
		String assessmentUri = dataobject.getString("assessmenturi");
		Assessment assessment = (Assessment) new ClassFactory().createInstance(
				Assessment.class, assessmentUri);

		assessment.setUsingScale(scale);
		RDFDataManagement.getInstance().saveResource(assessment, false);

		this.addUriMapping(js, "urimapping", scale, id);
 

		return scaleUri;
	}

	@SuppressWarnings("rawtypes")
	private String addDesignproblemSubmittingData(String table,
			JSONObject dataobject, JSONObject addon, int id, String action, String sessionId) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		String projectUri = dataobject.getString("selectedproject");
		Project project=null;
		 if(projectUri.equals("")){
			 js.endArray();
			 return js.toString();
		 }else{
			 project=SolutionsDataLayer.getDataLayerInstance().getProjectByUri(projectUri);
			 //project=RDFDataManagement.getInstance().loadResourceByURI(Project.class, projectUri, false);
			
		 }
		 
			
		// Submitting
		Submitting submitting=null;
		String submittingUri=null;
		if (action.equals("update")) {
			submittingUri = addon.getString("submittinguri");
			submitting = (Submitting) new ClassFactory().createInstance(
					Submitting.class, submittingUri);
		} else {
			js.object();
		 submitting = new Submitting();
		 submittingUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(submitting);
			submitting.setUri(submittingUri);
		js.key("operation").value("setfield");
		js.key("submittinguri").value(submittingUri);
		js.endObject();
		this.addUriMapping(js, "urimapping", submitting, id);
		}
		submitting.setDateTimeSent(new Date(dataobject.getLong("time")*1000));
		// Submission
		String submissionUri = addon.getString("submissionuri");
		Submission submission = (Submission) new ClassFactory().createInstance(
				Submission.class, submissionUri);
		submitting.setInResponseTo(submission);
		// Project
		
		
		submitting.setProjectRef(project);
	// User
		String userUri = addon.getString("useruri");
		//User user = (User) new ClassFactory().createInstance(User.class,
			//	userUri);
		User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
		submitting.setSentBy(user);

		// LearningContext
		LearningContext lContext = new LearningContext();
		String lContextUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(lContext);
		 
		lContext.setUri(lContextUri);
		lContext.setContentRef(submission);
		lContext.setUserRef(user);
		lContext.setActivityRef(submitting);
	 
		RDFDataManagement.getInstance().saveResource(lContext, false);

		RDFDataManagement.getInstance().saveResource(submitting, false);
		PeersProcessing.getInstance().processLearningContext(lContext, submission);
		ActivityLogging.getInstance().storeActivityRelatedLogData("submitting", sessionId, submitting.getDateTimeSent(), user, submitting, null);
		
		js.endArray();
		 
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addDesignproblemIdeaRatingsData(String table,
			JSONObject dataobject, JSONObject addon, int id, String action, String sessionId)
			throws Exception {
		 
		// table modelling_idea_ratings
		JSONStringer js = new JSONStringer();
		js.array();
		js.object();

		// UserRating
		UserRating uRating=null;
		String uRatingUri=null;
		Assessing assessing=null;
		String assessingUri = null;
		if (action.equals("update")) {
			uRatingUri = addon.getString("userratinguri");
			uRating = (UserRating) new ClassFactory().createInstance(
					UserRating.class, uRatingUri);
			assessing=uRating.getIsPartOf();
		} else {
			
		
		 uRating = new UserRating();
		 uRatingUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(uRating);

		uRating.setUri(uRatingUri);
		js.key("operation").value("setfield");
		js.key("idearatinguri").value(uRatingUri);
		js.endObject();
		addUriMapping(js, "urimapping", uRating, id);
		assessing=new Assessing();
		assessingUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(assessing);
		assessing.setUri(assessingUri);
		}
		// User
		 
		String userUri = addon.getString("useruri");
	//	User user = (User) new ClassFactory().createInstance(User.class,
				//userUri);
		User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
		 
		uRating.setMadeBy(user);

		// Brainstorming
		 
		String bUri = addon.getString("brainstorminguri");
		Brainstorming brainstorming = (Brainstorming) new ClassFactory()
				.createInstance(Brainstorming.class, bUri);
		brainstorming.addHasUserEvaluation(uRating);
		RDFDataManagement.getInstance().saveResource(brainstorming, false);

		int scaleId = Integer.parseInt(addon.getString("scaleid"));
		 	if (scaleId < 0) {
			String scaleItemUri = addon.getString("scaleitemuri");
		 	ScaleItem scaleItem = (ScaleItem) new ClassFactory()
					.createInstance(ScaleItem.class, scaleItemUri);
			uRating.setScaleItemRef(scaleItem);
		} else {
			// ScaleItem
		 
			ScaleItem scaleItem = new ScaleItem();
			String scaleItemUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(scaleItem);

			scaleItem.setUri(scaleItemUri);
			scaleItem.setHasValue(scaleId);

			RDFDataManagement.getInstance().saveResource(scaleItem, false);
			uRating.setScaleItemRef(scaleItem);
			Double rating = Double.valueOf(dataobject.getString("rating"));
			uRating.setRatingValue(rating / scaleId);
			
		}

		// Assessing
	//	Assessing assessing = new Assessing();
		//String assessingUri = URIBuilder.getInstance().uriGenerator
		//		.generateInstanceURI(assessing);

		//assessing.setUri(assessingUri);
		assessing.setSentBy(user);

		RDFDataManagement.getInstance().saveResource(assessing, false);
		uRating.setIsPartOf(assessing);

		LearningContext lContext = new LearningContext();
		String lContextUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(lContext);
		lContext.setUri(lContextUri);
		lContext.setUserRef(user);
		lContext.setActivityRef(assessing);

		String brainstormUri = addon.getString("brainstormuri");
		Brainstorm brainstorm = (Brainstorm) new ClassFactory().createInstance(
				Brainstorm.class, brainstormUri);
		// RDFDataManagement.getInstance().saveResource(brainstorm,false);

		lContext.setContentRef(brainstorm);
		RDFDataManagement.getInstance().saveResource(lContext, false);
		RDFDataManagement.getInstance().saveResource(uRating, false);
		PeersProcessing.getInstance().processLearningContext(lContext, brainstorm);
		
		js.endArray();
		 ActivityLogging.getInstance().storeActivityRelatedLogData("assessingidea", sessionId, new Date(), user, assessing, null);
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addScaleData(String table, JSONObject dataobject, JSONObject addon,int id,
			String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();

		// Scale
		Scale scale = null;
		String scaleUri=null;
		if (action.equals("update")) {
			scaleUri = addon.getString("scaleuri");
			scale = (Scale) new ClassFactory().createInstance(
					Scale.class, scaleUri);
		} else {
		 scale = new Scale();
		 scaleUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(scale);
		 scale.setUri(scaleUri);
		 addUriMapping(js, "urimapping", scale, id);
		}
		scale.setTitle(dataobject.getString("name"));
		scale.setDescription(dataobject.getString("description"));
		Collection<String> scaleArray = makeScaleArrayFromListNotReversed(dataobject
				.getString("scale"));
		int scaleItemId = 0;
		for (String scaleItemTitle : scaleArray) {
			// ScaleItem
			scaleItemId++;
			double ratingNumValue = NumberUtility
					.getProjectAssessmentRatingNumberValue(scaleArray.size(),
							scaleItemId);
			ScaleItem scaleItem = new ScaleItem();
			String scaleItemUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(scaleItem);
		 
			scaleItem.setUri(scaleItemUri);
			scaleItem.setHasLabel(scaleItemTitle);
			scaleItem.setHasValue(ratingNumValue);
			 
			RDFDataManagement.getInstance().saveResource(scaleItem, false);
			scale.addScaleItem(scaleItem);
			LOGGER.info("check if should be added to database mapping");
			this.addUriMappingWithParameter(js, "urimappingwithparameter",
					scaleItem, id, String.valueOf(scaleItemId));
		}

		RDFDataManagement.getInstance().saveResource(scale, false);
		
		js.endArray();
		 
		return js.toString();
	}

	private List<String> makeScaleArrayFromListNotReversed(String scaleitems) {
		 
		String[] itemsList = StringUtility
				.commaSeparatedStringToStringArray(scaleitems);
		List<String> list = Arrays.asList(itemsList);
		// Collections.reverse(list);
		list.toArray();

		return list;
	}

	@SuppressWarnings("rawtypes")
	private String addDesignproblemIdeasData(String table,
			JSONObject dataobject, JSONObject addon, int id, String action, String sessionId)
			throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		
		// Brainstorming
		Brainstorming brainstorming = null;
		String brUri = null;
		if (action.equals("update")) {
			brUri = addon.getString("ideauri");
			brainstorming = (Brainstorming) new ClassFactory().createInstance(
					Brainstorming.class, brUri);
		} else {
			js.object();
			 brainstorming = new Brainstorming();
			 brUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(brainstorming);
			brainstorming.setUri(brUri);
			js.key("operation").value("setfield");
			js.key("ideauri").value(brUri);
			js.endObject();
			addUriMapping(js, "urimapping", brainstorming, id);
		}
 
		

		//String cleanedText=StringUtility.html2text(dataobject.getString("content")); 
		 
		 //brainstorming.setHasContent(cleanedText);
		brainstorming.setHasContent("");
 
		brainstorming.setDateTimeSent(new Date(dataobject.getLong("time")*1000));
		// User
		String userUri = addon.getString("useruri");
		User user = null;
		if(userUri.startsWith("http://")){
			 user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);
			brainstorming.setSentBy(user);
		}
		// Brainstorm
		String brainstormUri = addon.getString("brainstormuri");
		Brainstorm brainstorm = (Brainstorm) new ClassFactory().createInstance(
				Brainstorm.class, brainstormUri);
		// RDFDataManagement.getInstance().saveResource(brainstorm,false);

		brainstorming.setInResponseTo(brainstorm);

		// LearningContext
		LearningContext lContext = new LearningContext();
		String lContextUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(lContext);
		lContext.setUri(lContextUri);
		lContext.setContentRef(brainstorm);
		lContext.setUserRef(user);
		lContext.setActivityRef(brainstorming);
		RDFDataManagement.getInstance().saveResource(lContext, false);
		
		//LOGGER.info("Annotation of the content should be added here:annotateCourseContent($table,\"content\",$dataobject->content,$id);");
		// annotateCourseContent($table,"content",$dataobject->content,$id);

		RDFDataManagement.getInstance().saveResource(brainstorming, false);
		//PeersProcessing.getInstance().processLearningContext(lContext, brainstorm); 
		//ActivityLogging.getInstance().storeActivityRelatedLogData("brainstorming", sessionId, brainstorming.getDateTimeSent(), user, brainstorming, null);
		js.endArray();
		//String mdlTableName="modelling_ideas";
		//String contentFieldName="content";
		//String text=dataobject.getString("content");
		//String fieldName="id";
		//int fieldValue=id;
		//AnnotationManager.getInstance().annotateContent(mdlTableName,fieldName, String.valueOf(fieldValue), contentFieldName,text,brainstorming);
		//AnnotationManager.getInstance().annotateCourseContent(mdlTableName,fieldName,String.valueOf(fieldValue),contentFieldName,text,brainstorming);
		//EmailNotification emailNotification=new EmailNotification();
		//emailNotification.ideaSubmittedNotification(user, brainstorm, brainstorming);
		LOGGER.info("returning idea:"+js.toString());
		return js.toString();
	}

	private void addUriMapping(JSONStringer js, String operation,
			Resource domaininstance, int id) throws JSONException {
		js.object();
		js.key("operation").value(operation);
		js.key("domainconcept")
				.value(domaininstance.getClass().getSimpleName());
		js.key("instanceid").value(id);
		js.key("uri").value(domaininstance.getUri().toString());
		js.endObject();
	}

	private void addUriMappingWithParameter(JSONStringer js, String operation,
			Resource domaininstance, int id, String parameter)
			throws JSONException {
		js.object();
		js.key("operation").value(operation);
		js.key("domainconcept")
				.value(domaininstance.getClass().getSimpleName());
		js.key("instanceid").value(id);
		js.key("parameter").value(parameter);
		js.key("uri").value(domaininstance.getUri().toString());
		js.endObject();
	}

	@SuppressWarnings("rawtypes")
	private String addAssessment_Data(String table, JSONObject dataobject,
			JSONObject addon, int id,String action) throws Exception {
		// table projectassessment
 
		JSONStringer js = new JSONStringer();
		js.array();
		
		// Assessment
		Assessment assessment = null;
		String taskUri = null;
		if (action.equals("update")) {
			taskUri = addon.getString("assessmenturi");
			assessment = (Assessment) new ClassFactory().createInstance(
					Assessment.class, taskUri);
		} else {
			js.object();
			 assessment = new Assessment();
			 taskUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(assessment);
			assessment.setUri(taskUri);
			js.key("operation").value("setfield");
			js.key("taskuri").value(taskUri);

			js.endObject();
			this.addUriMapping(js, "urimapping", assessment, id);
		}
	 
		
		assessment.setTitle(dataobject.getString("name"));
		//assessment.setDescription(dataobject.getString("description"));
		if(dataobject.has("startdate")){
		assessment.setStartDate(new Date(dataobject.getLong("startdate")*1000));
		}
		if(dataobject.has("enddate")){
		assessment.setEndDate(new Date(dataobject.getLong("enddate")*1000));
		}

		String taskToAssessUri = addon.getString("taskToAssessUri");
		Submission submissionToAssess = (Submission) new ClassFactory<Submission>()
				.createInstance(Submission.class, taskToAssessUri);
		assessment.setTaskToAssess(submissionToAssess);

		// DesignProblem
		String dpUri = addon.getString("designproblemuri");
		 
		DesignProblem dProblem = (DesignProblem) new ClassFactory()
				.createInstance(DesignProblem.class, dpUri);
		 		assessment.setContentRef(dProblem);
		dProblem.addTaskRef(assessment);

		RDFDataManagement.getInstance().updateResource(dProblem, false);
		RDFDataManagement.getInstance().saveResource(assessment, false);
		
		 
		js.endArray();
 
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addSubmission_Data(String table, JSONObject dataobject,
			JSONObject addon, int id, String action, String sessionId) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		 
		// Submission
		Submission submission = null;
		String taskUri =null;
		if (action.equals("update")) {
			taskUri = addon.getString("submissionuri");
			submission = (Submission) new ClassFactory().createInstance(
					Submission.class, taskUri);
		} else {
		js.object();
		 submission = new Submission();
		 taskUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(submission);
		js.key("operation").value("setfield");
		js.key("taskuri").value(taskUri);
		submission.setUri(taskUri);
		js.endObject();
		this.addUriMapping(js, "urimapping", submission, id);
		}
		
		submission.setTitle(dataobject.getString("name"));
		//submission.setDescription(dataobject.getString("description"));
		 
		if(dataobject.has("startdate")){
		submission.setStartDate(new Date(dataobject.getLong("startdate")*1000));
		}
		if(dataobject.has("enddate")){
			Date endD=new Date(dataobject.getLong("enddate")*1000);
		submission.setEndDate(endD);
		 	}
		String dpUri = addon.getString("designproblemuri");
		 
		DesignProblem dProblem = (DesignProblem) new ClassFactory()
				.createInstance(DesignProblem.class, dpUri);
	 	submission.setContentRef(dProblem);
		dProblem.addTaskRef(submission);
		RDFDataManagement.getInstance().updateResource(dProblem, false);
		RDFDataManagement.getInstance().saveResource(submission, false);
		

		js.endArray();
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addBrainstorm_Data(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		
		// Brainsorm
		Brainstorm brainstorm =null;
		String taskUri =null;
		if (action.equals("update")) {
			taskUri = addon.getString("brainstormuri");
			brainstorm = (Brainstorm) new ClassFactory().createInstance(
					Brainstorm.class, taskUri);
		} else {
			js.object();
			 brainstorm = new Brainstorm();
			 taskUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(brainstorm);
			js.key("operation").value("setfield");
			js.key("taskuri").value(taskUri);
			brainstorm.setUri(taskUri);
			js.endObject();
			addUriMapping(js, "urimapping", brainstorm, id);
		}
		
		brainstorm.setTitle(dataobject.getString("name"));
		//String cleanedText=StringUtility.html2text(dataobject.getString("description"));
		//brainstorm.setDescription(cleanedText);
		if(dataobject.has("startdate")){
		brainstorm.setStartDate(new Date(dataobject.getLong("startdate")*1000));
		}
		if(dataobject.has("enddate")){
		brainstorm.setEndDate(new Date(dataobject.getLong("enddate")*1000));
		}
		// DesignProblem
		String dpUri = addon.getString("designproblemuri");
		DesignProblem dProblem = (DesignProblem) new ClassFactory()
				.createInstance(DesignProblem.class, dpUri);
		 	brainstorm.setContentRef(dProblem);

		dProblem.addTaskRef(brainstorm);
		RDFDataManagement.getInstance().updateResource(dProblem, false);

		RDFDataManagement.getInstance().saveResource(brainstorm, false);
		
	 
		js.endArray();
		 
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addDesignProblemData(String table, JSONObject dataobject,
			JSONObject addon, int id, String action) throws Exception {
 
		JSONStringer js = new JSONStringer();
		js.array();
		DesignProblem dProblem=null;
		String dpUri=null;
		if (action.equals("update")) {
			dpUri = addon.getString("designproblemuri");
 			//dProblem = (DesignProblem) new ClassFactory().createInstance(
					//DesignProblem.class, dpUri);
 			 dProblem=SolutionsDataLayer.getDataLayerInstance().getDesignProblemByUri(dpUri);
		} else {
		js.object();
		// DesignProblem
		 dProblem = new DesignProblem();
		 dpUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(dProblem);
		dProblem.setUri(dpUri);
		js.key("operation").value("setfield");
		js.key("designproblemuri").value(dpUri);
	 
		js.endObject();
		this.addUriMapping(js, "urimapping", dProblem, id);
		}
		dProblem.setTitle(dataobject.getString("name"));
	//	String desc=dataobject.getString("description");
	 
		//dProblem.setDescription(StringUtility.html2text(desc));
		boolean useprojectdates=dataobject.getBoolean("useprojectdates");
		if(useprojectdates){
		 
			dProblem.setStartDate(new Date(dataobject.getLong("timeopen")*1000));
			dProblem.setEndDate(new Date(dataobject.getLong("timeclose")*1000));
		}
		
		dProblem.setProjectType(dataobject.getString("projecttype"));
		// isDescribedBy - Project
		if (!dataobject.getString("selectedproject").equals("")) {
			String projectUri = dataobject.getString("selectedproject");
			Project project = (Project) new ClassFactory().createInstance(
					Project.class, projectUri);
 
			dProblem.addIsDescribedByProject(project);
			project.addIsDescriptionOfDesignProblem(dProblem);
			RDFDataManagement.getInstance().saveResource(project, false);
			
		}
		if(dataobject.has("groupmode")){
			 
			dProblem.setGroupmode(dataobject.getInt("groupmode"));
		}
		if(addon.has("groupinguri")){
			 
			String groupinguri=addon.getString("groupinguri");
			if((groupinguri!=null)&&(!groupinguri.equals("null"))){
			Grouping grouping=RDFDataManagement.getInstance().loadResourceByURI(Grouping.class,groupinguri,false);
			dProblem.setGrouping(grouping);
			}
		}
		if(dataobject.has("visible")){
			 
			dProblem.setVisible(dataobject.getInt("visible"));
		}
		if(dataobject.has("groupmembersonly")){
	 
			dProblem.setGroupmembersonly(dataobject.getInt("groupmembersonly"));
		}
		if(!dataobject.getString("selectedconcepts").equals("")){
			String selectedconceptsjson=dataobject.getString("selectedconcepts");
		 
		 	selectedconceptsjson=StringUtility.replace(selectedconceptsjson,"\\\"","\"");
			 JSONObject scObject = new JSONObject(selectedconceptsjson);
			 JSONArray selConceptsArray=scObject.getJSONArray("concepts");
			//JSONObject selConcJsonArray=new JSONObject(selectedconcepts);
			 dProblem.removeAllRelevantConcepts();
		 	for (int i = 0; i < selConceptsArray.length(); i++) {
		 		JSONObject selConceptObj=selConceptsArray.getJSONObject(i);
		 		String selConceptUri=selConceptObj.getString("concept");
				Concept concept=RDFDataManagement.getInstance().loadResourceByURI(Concept.class, selConceptUri, false);
				if(concept!=null){
					dProblem.addHasRelevantConcept(concept);
				}
			 }
		}
		 	String courseUri = addon.getString("courseuri");
		 
		Course course = (Course) new ClassFactory().createInstance(
				Course.class, courseUri);
		course.addDesignProblem(dProblem);
		RDFDataManagement.getInstance().saveResource(course, false);
		dProblem.addCourseRef(course);
		 
		if (dataobject.has("scale")) {
			int usedScale = dataobject.getInt("scale");
			String scaleUri = addon.getString("scaleuri");
			 
			Scale scale = (Scale) new ClassFactory().createInstance(
					Scale.class, scaleUri);
			if (!scaleUri.equals(scale.getUri().toString())) {
				this.addUriMapping(js, "urimapping", scale, usedScale);
			}
			if (usedScale > 0) {
				 

				scale.setTitle(String.valueOf(usedScale));
				scale.setMaxValue(usedScale);
				scale.setMinValue(0);
				RDFDataManagement.getInstance().saveResource(scale, false);
 
			}
			dProblem.setUsingScale(scale);
		}
		RDFDataManagement.getInstance().saveResource(dProblem, false);

		

		js.endArray();
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private String addCourseData(String table, JSONObject dataobject,JSONObject addon,  int id,
			String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();

		// Course
		Course course=null;
		String courseUri=null;
		if(action.equals("update")){
			courseUri=addon.getString("courseuri");
			course=(Course) new ClassFactory().createInstance(Course.class, courseUri);
		}else{
		 course = new Course();
		 courseUri = URIBuilder.getInstance().uriGenerator
				.generateInstanceURI(course);
		course.setUri(courseUri);
		this.addUriMapping(js, "urimapping", course, id);
		}
	 
		if (dataobject.has("summary")) {
			 
			//course.setDescription(dataobject.getString("summary"));
		}

		if (dataobject.has("fullname") ) {
			course.setTitle(dataobject.getString("fullname"));
		}

		RDFDataManagement.getInstance().saveResource(course, false);
		
		js.endArray();
		 
		return js.toString();
	}

	@SuppressWarnings("rawtypes")
	private void addForumReadData(String table, JSONObject dataobject, int id,
			String action) throws Exception {
		// DiscussAsynchronously
		String uri = OntologyClassesUtility
				.getNamespaceClassName(DiscussAsynchonously.class)
				+ "_read_"
				+ id;
		DiscussAsynchonously da = (DiscussAsynchonously) new ClassFactory()
				.createInstance(DiscussAsynchonously.class, uri);

		// ReadingPost
		String readPostUri = OntologyClassesUtility
				.getNamespaceClassName(ReadingPost.class) + "_" + id;
		ReadingPost rPost = (ReadingPost) new ClassFactory().createInstance(
				ReadingPost.class, readPostUri);

		// Posting
		String postingUri = OntologyClassesUtility
				.getNamespaceClassName(Posting.class) + "_" + id;
		Posting posting = (Posting) new ClassFactory().createInstance(
				Posting.class, postingUri);

		RDFDataManagement.getInstance().saveResource(posting, false);

		rPost.setPostingRef(posting);
		rPost.setDateTimeStart(new Date(dataobject.getLong("firstread")*1000));
		rPost.setDateTimeEnd(new Date(dataobject.getLong("lastread")*1000));

		RDFDataManagement.getInstance().saveResource(rPost, false);
		da.setEvent(rPost);

		RDFDataManagement.getInstance().saveResource(da, false);
	}

	@SuppressWarnings("rawtypes")
	private void addForumPostsData(String table, JSONObject dataobject, int id,
			String action) throws Exception {

		// DiscussAsynchronously
		String uri = OntologyClassesUtility
				.getNamespaceClassName(DiscussAsynchonously.class)
				+ "_submit_"
				+ id;
		DiscussAsynchonously da = (DiscussAsynchonously) new ClassFactory()
				.createInstance(DiscussAsynchonously.class, uri);

		// SubmittingPost
		String postUri = OntologyClassesUtility
				.getNamespaceClassName(SubmittingPost.class) + "_" + id;
		SubmittingPost sp = (SubmittingPost) new ClassFactory().createInstance(
				SubmittingPost.class, postUri);

		sp.setDateTimeStart(new Date(dataobject.getLong("created")*1000));
		sp.setDateTimeEnd(new Date(dataobject.getLong("modified")*1000));
		da.setEvent(sp);

		// Posting
		String postingRefUri = OntologyClassesUtility
				.getNamespaceClassName(Posting.class) + "_" + id;
		Posting posting = (Posting) new ClassFactory().createInstance(
				Posting.class, postingRefUri);
		posting.setHasContent(dataobject.getString("message"));
		posting.setDateTimeSent(new Date(dataobject.getLong("created")));

		sp.setPostingRef(posting);

		// Posting - parent
		String postingParentRefUri = OntologyClassesUtility
				.getNamespaceClassName(Posting.class)
				+ "_"
				+ dataobject.getString("parent");
		Posting postingParent = (Posting) new ClassFactory().createInstance(
				Posting.class, postingParentRefUri);

		// Posting - sentBy - User

		String userUri = OntologyClassesUtility
				.getNamespaceClassName(User.class)
				+ "_"
				+ dataobject.getString("userid");
	//	User user = (User) new ClassFactory().createInstance(User.class,
			//	userUri);
		User user = RDFDataManagement.getInstance().loadResourceByURI(User.class, userUri, false);

		RDFDataManagement.getInstance().saveResource(user, false);
		posting.setSentBy(user);

		RDFDataManagement.getInstance().saveResource(postingParent, false);
		posting.setInReplyTo(postingParent);

		// Posting - rootPosting
		String rootPostingUri = OntologyClassesUtility
				.getNamespaceClassName(Posting.class)
				+ "_"
				+ dataobject.getString("inreplyto");
		Posting rootPosting = (Posting) new ClassFactory().createInstance(
				Posting.class, rootPostingUri);

		RDFDataManagement.getInstance().saveResource(rootPosting, false);
		posting.setRootPosting(rootPosting);

		// Posting - categoryRef-DiscussCategory
		String categoryRefUri = OntologyClassesUtility
				.getNamespaceClassName(DiscussCategory.class)
				+ "_Discussion_"
				+ dataobject.getString("discussion");
		DiscussCategory dCategory = (DiscussCategory) new ClassFactory()
				.createInstance(DiscussCategory.class, categoryRefUri);

		RDFDataManagement.getInstance().saveResource(dCategory, false);
		posting.setCategoryRef(dCategory);

		if (!dataobject.getString("attachment").equals("")) {
			LOGGER.info("hasAttachment not processed");

		 
		}

		RDFDataManagement.getInstance().saveResource(posting, false);
		RDFDataManagement.getInstance().saveResource(sp, false);
		RDFDataManagement.getInstance().saveResource(da, false);
		LOGGER.info("add discuss acynchronously 2222");
	}

	@SuppressWarnings("rawtypes")
	private String addUserData(String table, JSONObject dataobject,JSONObject addon, int id,
			String action) throws Exception {
		JSONStringer js = new JSONStringer();
		js.array();
		 User nUser=null;
		 String userUri=null;
		if (action.equals("insert")) {
			
			js.object();
			// User
			 nUser = new User();
			 userUri = URIBuilder.getInstance().uriGenerator
					.generateInstanceURI(nUser);
		 
			 nUser.setUri(userUri);
			
			js.endObject();
			this.addUriMapping(js, "urimapping", nUser, id);
			
			 
			
		} else if (action.equals("update")) {
			 
			userUri = addon.getString("useruri");
			nUser = (User) new ClassFactory().createInstance(
					User.class, userUri);
		}
		if(dataobject.has("username")){
			nUser.setUsername(dataobject.getString("username"));
		}
		if(dataobject.has("firstname")){
		nUser.setFirstname(dataobject.getString("firstname"));
		}
		if(dataobject.has("lastname")){
		nUser.setLastname(dataobject.getString("lastname"));
		}
		nUser.setIdentifier(String.valueOf(id));
		
		if (dataobject.has("password")) {
			 String pass = dataobject.getString("password");
			//String md5pass = StringUtility.md5(pass);
			 
			nUser.setPassword(pass);
		}
		if (dataobject.has("newpassword")) {
			String pass = dataobject.getString("newpassword");
			String md5pass = StringUtility.md5(pass);
			 
			nUser.setPassword(md5pass);
		}

		RDFDataManagement.getInstance().saveResource(nUser, false);
		js.endArray();
		return js.toString();
	}

	private static boolean checkIfUriExists(String uri) {
		return RDFDataManagement.getInstance().checkIfResourceExists(uri);
	}
	
	private static class ClassFactory<E extends Resource> {
		E createInstance(Class<E> clazz, String uri) {

			E e = null;
			try {
				if (checkIfUriExists(uri) && !(uri == null) && !uri.equals("")
						&& !uri.equals("null")) {

					e = RDFDataManagement.getInstance().loadResourceByURI(
							clazz, uri, false);
				} else {
					LOGGER.info("Warning. Create instance for class:" + clazz
							+ " uri:" + uri);
					e = clazz.newInstance();
					// if((uri.equals(""))||(uri==null)||(uri.equals("null")))

					uri = URIBuilder.getInstance().uriGenerator
							.generateInstanceURI(e);
					// }
					e.setUri(uri);

					RDFDataManagement.getInstance().saveResource(e, false);
				}
			} catch (Exception e1) {

				e1.printStackTrace();
			}
			return e;
		}
	}

}