package org.goodoldai.depths.eventsprocessor;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.goodoldai.depths.domain.user.Group;
import org.goodoldai.depths.domain.user.Grouping;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;

public class DeleteMoodleTable {
	private static final Logger LOGGER = Logger
			.getLogger(DeleteMoodleTable.class);

	public void checkTableAndDeleteRow(String table, JSONObject tableJsonObject) throws Exception {
		if(table.equals("groupings_groups")){
			String groupinguri=tableJsonObject.getString("groupinguri");
			String groupuri=tableJsonObject.getString("groupuri");
			Grouping grouping=RDFDataManagement.getInstance().loadResourceByURI(Grouping.class,groupinguri,false);
			Group group=RDFDataManagement.getInstance().loadResourceByURI(Group.class,groupuri,false);
			grouping.removeGroup(group);
			RDFDataManagement.getInstance().saveResource(grouping, false);
		}
		
	}
}
