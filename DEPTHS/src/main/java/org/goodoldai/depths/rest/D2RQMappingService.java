package org.goodoldai.depths.rest;

 

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;
//import org.goodoldai.depths.datamapping.D2RQMapper;
//import org.goodoldai.depths.services.peers.FindPeersThread;

@Path("d2rqmapping")
public class D2RQMappingService {
	private static final Logger LOGGER = Logger.getLogger(D2RQMappingService.class);
	/**
	 * This method should be called in order to parse all data from existing Moodle database
	 * to LOC as well as all used ontologies defined in rdf or owl files
	 * It should be used only once prior to the initialization of the system.
	 */
	
	@GET
	@Path("mapallmoodledata")
	public String mapAllMoodleData()
	{
		//D2RQMapper dm=new D2RQMapper();
		//dm.parseAllMoodleDataToDatabase();
		LOGGER.info("Moodle data mapping finished");
		return "true";
	}
	
	/**
	 * This method is used to parse all ontologies used from rdf or owl files 
	 * to the MySQL (Sesame) database
	 */

	/*public void addAllOntologies(){
		 
		 
			 
			D2RQMapper d2rqm=new D2RQMapper();
		 
		 
		 
		URL locoURL = null;
		
		URL quizURL=null;
		URL depthsURL=null;
		String lURL="/res/LOCO-Cite-v7.owl";
		 
		locoURL = this.getClass().getResource(lURL);
		 
		
		String qURL="/res/quiz-ont.owl";
		quizURL = this.getClass().getResource(qURL);
		 
		
		String dURL="/res/depths.owl";
		depthsURL = this.getClass().getResource(dURL);
		 
		d2rqm.addLocoOntologies(locoURL.toString(), "http://www.lornet.org/loco-cite/2007/.owl");
		d2rqm.addLocoOntologies(quizURL.toString(), "http://www.lornet.org/loco-cite/quiz.owl");
		d2rqm.addLocoOntologies(depthsURL.toString(), "http://www.depths.org/zjeremic/software-solution/2008/depths.owl");
		 
		String kdURL="/res/kimdepths.owl";
		URL kimdepthsURL = this.getClass().getResource(kdURL);
		
		d2rqm.addLocoOntologies(kimdepthsURL.toString(), "http://www.depths.org.zjeremic/software-solution/2008/8/kimdepths.owl#");
		
		String gofURL="/res/GoFForm.owl";
		URL kimGoFFormURL=this.getClass().getResource(gofURL);
		
		d2rqm.addLocoOntologies(kimGoFFormURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/GoFForm.owl#");
		
		String pcrURL="/res/PatternCollectionRegistry.owl";
		URL patternCollectionRegistryURL=this.getClass().getResource(pcrURL);
		
		d2rqm.addLocoOntologies(patternCollectionRegistryURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/PatternCollectionRegistry.owl#");
		
		String pcURL="/res/PatternCollections.owl";
		URL patternCollectionsURL=this.getClass().getResource(pcURL);
		
		d2rqm.addLocoOntologies(patternCollectionsURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/PatternCollections.owl#");
		
		String pfowlURL="/res/PFOWLForm.owl";
		URL PFOWLURL=this.getClass().getResource(pfowlURL);
		
		d2rqm.addLocoOntologies(PFOWLURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/PFOWLForm.owl#");
		
		String plformURL="/res/PLForm.owl";
		URL PLFormURL=this.getClass().getResource(plformURL);
		
		d2rqm.addLocoOntologies(PLFormURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/PLForm.owl#");
		
		String ploPFormURL="/res/PLoPForm.owl";
		URL PLoPFormURL=this.getClass().getResource(ploPFormURL);
		
		d2rqm.addLocoOntologies(PLoPFormURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/PLoPForm.owl#");
		
		String posaFormURL="/res/POSAForm.owl";
		URL POSAFormURL=this.getClass().getResource(posaFormURL);
		
		d2rqm.addLocoOntologies(POSAFormURL.toString(), "http://cse.unl.edu/~scotth/SWont/Patterns/POSAForm.owl#");
		
		String protKMURL="/res/protonkm.owl";
		URL protonkmURL=this.getClass().getResource(protKMURL);
		
		d2rqm.addLocoOntologies(protonkmURL.toString(), "http://proton.semanticweb.org/2006/05/protonkm#");
		
		String protSURL="/res/protons.owl";
		URL protonsURL=this.getClass().getResource(protSURL);
		
		d2rqm.addLocoOntologies(protonsURL.toString(), "http://proton.semanticweb.org/2006/05/protons#");
		
		String protTURL="/res/protont.owl";
		URL protontURL=this.getClass().getResource(protTURL);
		
		d2rqm.addLocoOntologies(protontURL.toString(), "http://proton.semanticweb.org/2006/05/protont#");
		
		String protUURL="/res/protonu.owl";
		URL protonUURL=this.getClass().getResource(protUURL);
		
		d2rqm.addLocoOntologies(protonUURL.toString(), "http://proton.semanticweb.org/2006/05/protonu#");
		
		
	}
*/

}
