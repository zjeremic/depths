package org.goodoldai.depths.rest;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.logging.ServiceUseLogging;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.resources.ResourcesServiceManager;
 

@Path("recommendation")
public class RecommendationService {
	private static final Logger LOGGER=Logger.getLogger(RecommendationService.class);
	@GET
	@Path("relevantresources")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getRelevantResourcesForConcept(@QueryParam("username") String username, 
			@QueryParam("concepturi") String conceptUri, @QueryParam("sessionid") String sessionId){
		 
		User user=UserManagament.getInstance().getUserByUsername(username);
	 	String relevantResourcesJson="";
		try {
			 if(user!=null){
			relevantResourcesJson=ResourcesServiceManager.getInstance().getRelevantResourcesForConcept(user.getUri().toString(),conceptUri);
			ServiceUseLogging.getInstance().storeServiceUseRelatedLogData("resource", sessionId, new Date(), user, null);
			 }else{
				 relevantResourcesJson=ResourcesServiceManager.getInstance().getRelevantResourcesForConcept("",conceptUri);
			 }
			//projectsJson = ProjectServiceUtility.getListOfMyProjectsByUriJSON(user.getUri().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("relevantresources called for:"+username+" conceptUri:"+conceptUri);
		return relevantResourcesJson;
	}
	
	@GET
	@Path("processrating")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String processRatingForRecommendedResource(@QueryParam("username") String username, 
			@QueryParam("pageuri") String pageUri,
			@QueryParam("ratingvalue") String ratingValue,
			@QueryParam("concepturi") String conceptUri,
			@QueryParam("sessionid") String sessionId
			){
		 
		User user=UserManagament.getInstance().getUserByUsername(username);
	  
		try {
			 if(user!=null){
			 
			 ResourcesServiceManager.getInstance().addResourceRating(user, pageUri,ratingValue,conceptUri, sessionId);
			 } else{
				 LOGGER.info("USER IS NULL:"+username);
			 }
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("processrating called for:"+username+" conceptUri:"+conceptUri+" pageuri:"+pageUri);
		return "Success";
		
	}
}
