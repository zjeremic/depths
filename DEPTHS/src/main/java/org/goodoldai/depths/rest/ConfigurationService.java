package org.goodoldai.depths.rest;

 
import java.text.ParseException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
 
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.rest.json.ConfigurationServiceUtility;
import org.goodoldai.depths.scheduler.CronScheduler;

import org.quartz.SchedulerException;

 

@Path("configure")
public class ConfigurationService {
	private static final Logger LOGGER = Logger.getLogger(ConfigurationService.class);
	@GET
	@Path("getconfiguration")
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getWebServicesConfiguration() {
		String confJson=null;
		try {
			confJson= ConfigurationServiceUtility.getWebServicesConfigurationJson();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("getconfiguration service:"+confJson);
		return confJson;
	}
	@GET
	@Path("checkuser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String checkUserData(@QueryParam("user") String userName,@QueryParam("pass") String password)  {
		 
		String userJson=null;
		try {
			 
			userJson=ConfigurationServiceUtility.checkUserData(userName, password);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("checkuser:"+userJson);
		return userJson;
	}
	@GET
	@Path("startscheduler")
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String startScheduler(){
		if (Settings.getInstance().config.cronSchedulerConfig.activated) {
			CronScheduler cScheduler = new CronScheduler();
			try {
				   
					cScheduler.startScheduler();
				 
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					LOGGER.info("Could not start Cron Scheduler!", e);
				 
			} catch (SchedulerException e) {
				LOGGER.info("Could not start Cron Scheduler!", e);
			}
		}
		 
		return "";
	}

}
