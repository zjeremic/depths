package org.goodoldai.depths.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.rest.json.ContentServiceUtility;
import org.goodoldai.depths.semanticstuff.services.content.ContentManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
 

@Path("content")
public class ContentService {
	private static final Logger LOGGER = Logger.getLogger(ContentService.class);
	@GET
	@Path("getusercourses")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfCoursesForUser(@QueryParam("useruri") String userUri){
		String coursesListJson="";
		try {
			coursesListJson = ContentServiceUtility.getListOfMyCoursesByUriJSON(userUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("content getusercourses service called for user:"+userUri+" returns:"+coursesListJson);
		return coursesListJson;
	}
	@GET
	@Path("getcourseproblems")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfDesignProblemsForCourse(@QueryParam("courseuri") String courseUri){
		String designproblemsListJson="";
		try {
			designproblemsListJson = ContentServiceUtility.getListOfDesignProblemsForCourseJSON(courseUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("content getcourseproblems service called for course:"+courseUri+" returns:"+designproblemsListJson);
		return designproblemsListJson;
	}
	
	@GET
	@Path("getproblemdescription")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getProblemDescriptionForProblem(@QueryParam("problemuri") String problemUri){
		String designproblemsListJson="";
		try {
			designproblemsListJson = ContentServiceUtility.getProblemDescriptionJSON(problemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("content getproblemdescription service called for problem:"+problemUri+" returns:"+designproblemsListJson);
		return designproblemsListJson;
	}
	@GET
	@Path("relatedconcepts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfRelatedConceptsForProblem(@QueryParam("problemuri") String problemUri){
		String relatedConceptsJson="";
		try {
			 relatedConceptsJson = ContentServiceUtility.getListOfRelatedConceptsForProblemJSON(problemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("content relatedconcepts service called for problem:"+problemUri+" returns:"+relatedConceptsJson);
		return relatedConceptsJson;
	}
	@GET
	@Path("recommendedconcepts")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfRecommendedConceptsForProblem(@QueryParam("problemuri") String problemUri){
		String relatedConceptsJson="";
		try {
			 relatedConceptsJson = ContentServiceUtility.getListOfRecommendedConceptsForProblemJSON(problemUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("content recommendedconcepts service called for problem:"+problemUri+" returns:"+relatedConceptsJson);
		return relatedConceptsJson;
	}
	
	@GET
	@Path("deleteresource")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String deleteResourceByUri(@QueryParam("uri") String resourceUri){
		 
		try {
			ContentManagament.getInstance().deleteResourceByUri(resourceUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "Not deleted:"+resourceUri+" because of:"+e.getMessage();
		}
		LOGGER.info("deleteResourceByUri service called for:"+resourceUri);
		return "Deleted resource:"+resourceUri;
	}
	
	@GET
	@Path("deleteuser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String deleteResourceByUsername(@QueryParam("username") String username){
		 if(username.equals("noname")){
			 username="";
		 }
		try {
			UserManagament.getInstance().deleteUserByUsername(username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "Not deleted:"+username+" because of:"+e.getMessage();
		}
		LOGGER.info("deleteResourceByUri service called for:"+username);
		return "Deleted user:"+username;
	}
	
}
