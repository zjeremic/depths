package org.goodoldai.depths.rest;

 
import java.rmi.RemoteException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.annotation.AnnotationManager;
 
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;

  
 

@Path("annotation")
public class AnnotationService {
	private static final Logger LOGGER = Logger.getLogger(AnnotationService.class);
	@GET
	@Path("annotateallrepositories")
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String annotateAllRepositories(){
	 
		AnnotationManager.getInstance().annotateAllRepositories();
		return "";
	}
	
	@GET
	@Path("readAllOntologies")
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String readAllDomainOntologies(){
		try {
			AnnotationManager.getInstance().readAllDomainOntologies();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		return "";
	}
 
	@GET
	@Path("annotatecoursecontent")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String annotateCourseContent(@QueryParam("mdltablename") String mdlTableName,
			@QueryParam("fieldname") String fieldName,
			@QueryParam("fieldvalue") String fieldValue,
			@QueryParam("contentfield") String contentFieldName,
			@QueryParam("content") String content,
			@QueryParam("resourceuri") String resourceuri){
		 
		try {
			Resource resource=RDFDataManagement.getInstance().loadResourceByURI(Resource.class, resourceuri, false);
		 
			AnnotationManager.getInstance().annotateCourseContent(mdlTableName,fieldName,fieldValue,contentFieldName,content,resource);
	 
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			LOGGER.info("RemoteException in annotateCourseContent"+e.getLocalizedMessage());
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			LOGGER.info("Exception in annotateCourseContent"+ex.getLocalizedMessage());
		}
		 
		return ""; 
	}

}
