package org.goodoldai.depths.rest;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.goodoldai.depths.rest.json.RDFDataServiceUtility;
import org.goodoldai.depths.utility.ServicesUtility;

@Path("delete")
public class DeleteRDFDataService {

	private static final Logger LOGGER = Logger.getLogger(InsertRDFDataService.class);
	@POST
	@Path("table")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String deleteMoodleTableFromRDF(InputStream is) {
		 String response="";
		 
		String jsonTable= ServicesUtility.convertInputStreamToString(is);
		LOGGER.info("delete moodle table from RDF called for:"+jsonTable);
	    try {
			response=RDFDataServiceUtility.deleteMoodleTableRow(jsonTable);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	    LOGGER.info("delete moodle table from RDF response:"+response);
		return response;
	}
}

 