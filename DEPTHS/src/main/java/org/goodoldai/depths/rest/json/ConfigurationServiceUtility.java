package org.goodoldai.depths.rest.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.config.Config;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.jsonstuff.ConfigurationJSONExporter;
import org.goodoldai.depths.jsonstuff.UserJSONExporter;
 
public class ConfigurationServiceUtility {
	public static String getWebServicesConfigurationJson() throws JSONException{
		String json=ConfigurationJSONExporter.getInstance().getWebServicesConfigurationJson();
		return json;
	}
	public static String checkUserData(String username, String password) throws JSONException{
		 String json= UserJSONExporter.getInstance().checkUserData(username,password);
		 return json;
	}

}
