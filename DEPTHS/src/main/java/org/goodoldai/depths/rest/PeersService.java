package org.goodoldai.depths.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

//import org.apache.log4j.Logger;
import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.peers.PeersServiceManager;



@Path("peers")
public class PeersService {
 private static final Logger LOGGER=Logger.getLogger(PeersService.class);
	@GET
	@Path("problembased")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getListOfRelevantPeersForDesignProblem(@QueryParam("username") String username, 
			@QueryParam("courseuri") String courseUri,@QueryParam("problemuri") String problemUri){
		User user=UserManagament.getInstance().getUserByUsername(username);
		String relevantPeersJson="";
		try {
			if(user!=null){
			relevantPeersJson=PeersServiceManager.getInstance().processPeersForDesignProblem(user.getUri().toString(),courseUri,problemUri);
 			}
			//projectsJson = ProjectServiceUtility.getListOfMyProjectsByUriJSON(user.getUri().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		 
		 LOGGER.info("getListOfRelevantPeersForDesignProblem:"+relevantPeersJson);
		return relevantPeersJson;
	}
}
