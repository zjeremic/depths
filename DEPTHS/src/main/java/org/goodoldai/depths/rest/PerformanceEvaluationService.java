package org.goodoldai.depths.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.goodoldai.depths.utility.EvaluationUtility;

@Path("evaluation")
public class PerformanceEvaluationService {
	private static final Logger LOGGER = Logger.getLogger(SolutionService.class);
	@GET
	@Path("evaluateclass")
	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String saveNewSuggestedSolution(@QueryParam("class") String classname, @QueryParam("conf") String conf){
		EvaluationUtility.loadEvaluation(classname, conf);
		return "Started";
	}
	@GET
	@Path("getusercommentslog")
	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getUserCommentsLog(){
		EvaluationUtility.getUserCommentsLog();
		return "Started";
	}
}
