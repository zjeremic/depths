package org.goodoldai.depths.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
 

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.goodoldai.depths.rest.json.SolutionServiceUtility;

@Path("solution")
public class SolutionService {
	private static final Logger LOGGER = Logger.getLogger(SolutionService.class);
	@GET
	@Path("savesolution")
	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String saveNewSuggestedSolution(@QueryParam("json") String projectJson){
		String projectUri=null;
		try {
			LOGGER.info("savesolution service called for:"+projectJson);
			projectUri=SolutionServiceUtility.saveNewSuggestedSolution(projectJson);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		 LOGGER.info("savesolution service saved project:"+projectUri);
		return projectUri;
	}
	@GET
	@Path("savediagrams")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String saveDiagrams(@QueryParam("json") String diagramsJson){
		LOGGER.info("savediagrams service called for:"+diagramsJson);
		String jsonoutput=null;
		try {
		 
			jsonoutput=SolutionServiceUtility.saveDiagrams(diagramsJson);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}catch(Exception e){
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		LOGGER.info("savediagrams service output:"+jsonoutput);
		return jsonoutput;
	}
	
	@GET
	@Path("getproject")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String getProjectJsonByUri(@QueryParam("projecturi") String projectUri){
	 LOGGER.info("getproject service called for:"+projectUri);
		 String projectjson=null;
		try {
			projectjson = SolutionServiceUtility.getProjectSolutionJsonByUri(projectUri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
		LOGGER.error("Error in SolutionService getproject for projectUri:"+projectUri);
		projectjson="[]";
		}
		LOGGER.info("getproject service called for:"+projectUri+" and returns:"+projectjson);
		 return projectjson;
	}
}
