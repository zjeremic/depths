package org.goodoldai.depths.rest;



import java.io.InputStream;

import javax.ws.rs.Consumes;
 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.goodoldai.depths.rest.json.RDFDataServiceUtility;
import org.goodoldai.depths.utility.ServicesUtility;

 

@Path("insert")
public class InsertRDFDataService {
	
	private static final Logger LOGGER = Logger.getLogger(InsertRDFDataService.class);
	@POST
	@Path("table")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
	public String addMoodleTableToRDF(InputStream is) {
		 String response="";
		 
		String jsonTable= ServicesUtility.convertInputStreamToString(is);
		LOGGER.info("addMoodleTableToRDF service called for:"+jsonTable);
	    try {
			response=RDFDataServiceUtility.addMoodleTableToRDF(jsonTable);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	    LOGGER.info("addMoodleTableToRDF service called response:"+response);
		return response;
	}
	 
		@GET
		@Path("tabletest")
		@Consumes({ MediaType.APPLICATION_JSON })
		@Produces({ "application/x-javascript", MediaType.APPLICATION_JSON })
		public String test(@QueryParam("json") String json){
		 
			return "testdataOK";
		}


}
