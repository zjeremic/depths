package org.goodoldai.depths.rest.json;

import java.util.Collection;

import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.services.datalayer.RecommendationDataLayer;
import org.goodoldai.depths.services.datalayer.UserModelData;
import org.goodoldai.depths.services.peers.UserModel;


public class PeersServiceUtility {
	private static final Logger LOGGER = Logger.getLogger(PeersServiceUtility.class.getName());
	public static String getRecommendedPeersListAsJSON(String currUserUri,UserModelData uModelData, String courseUri, String dProblemUri)throws JSONException{
			 
			//List<String> sortedList,HashMap<String,UserModel> recommendedPeers 
		 
		JSONStringer js = new JSONStringer();
		js.array();
	 
		try {
			Collection<UserModel> userModels=RecommendationDataLayer.getDataLayerInstance().getPeersForRecommendationForProblemAsCollection(dProblemUri);
			for(UserModel um:userModels){
				String pUri=um.getUser().getUri().toString();
				if(!pUri.equals(currUserUri)){
					//UserModel um = uModelData.getRelevantPeersForDesignProblem(dProblemUri).get(pUri.toString());
				//	UserModel um=RecommendationDataLayer.getDataLayerInstance().getPeersForRecommendationForProblemAsCollection(dProblemUri);
					User user = um.getUser();
					js.object();
					js.key("peeruri").value(pUri);
					js.key("username").value(user.getUsername());
					js.key("relevance").value(um.getTotalCompetence());
					js.endObject();
				} 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	 	js.endArray();
	 	
	  
	 	return js.toString();
	}

	public static String getRecommendedPeersListAsJSON(String currUserUri,
			List<String> sortedListOfPeers, String dProblemUri) throws JSONException {
		JSONStringer js = new JSONStringer();
		js.array();
	 
		try {
			Collection<UserModel> userModels=RecommendationDataLayer.getDataLayerInstance().getPeersForRecommendationForProblemAsCollection(dProblemUri);
			for(UserModel um:userModels){
				String pUri=um.getUser().getUri().toString();
				LOGGER.info("checking peers:"+pUri+" curr:"+currUserUri);
				if(!pUri.equals(currUserUri)){
					//UserModel um = uModelData.getRelevantPeersForDesignProblem(dProblemUri).get(pUri.toString());
				//	UserModel um=RecommendationDataLayer.getDataLayerInstance().getPeersForRecommendationForProblemAsCollection(dProblemUri);
					User user = um.getUser();
			 
					js.object();
					js.key("peeruri").value(pUri);
					js.key("username").value(user.getUsername());
					js.key("relevance").value(um.getTotalCompetence());
					js.endObject();
				} 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
	 	js.endArray();
	 	
	  
	 	return js.toString();
	}
}
