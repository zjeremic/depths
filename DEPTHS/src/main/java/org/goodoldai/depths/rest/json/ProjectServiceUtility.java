package org.goodoldai.depths.rest.json;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONStringer;
import org.goodoldai.depths.domain.activity.Submitting;
import org.goodoldai.depths.domain.activity.UploadingSolution;
import org.goodoldai.depths.domain.content.Submission;
import org.goodoldai.depths.domain.general.LearningContext;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.User;

import org.goodoldai.depths.jsonstuff.ProjectJSONExporter;
import org.goodoldai.depths.semanticstuff.rdfpersistance.urigenerator.URIBuilder;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;
import org.goodoldai.depths.semanticstuff.services.user.UserManagament;
import org.goodoldai.depths.services.datalayer.SolutionsDataLayer;
import org.goodoldai.depths.services.peers.PeersProcessing;
import org.goodoldai.depths.utility.OntologyClassesUtility;

public class ProjectServiceUtility {
	private static final Logger LOGGER = Logger.getLogger(ProjectServiceUtility.class);
	
	public static String getListOfMyProjectsJSON(User user) throws Exception{
		Collection<Project> myProjects=ProjectManagament.getInstance().getProjectsForUser(user.getUri().toString());
		 
		String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(myProjects,user);
		//LOGGER.info("user:"+userUri);
		LOGGER.info(projectsJson);
		return projectsJson;
	}
	public static String getListOfAllProjectsJSON(User user) throws Exception{
		Collection<Project> allProjects=ProjectManagament.getInstance().getAllProjectsForUser(user.getUri().toString());
		String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(allProjects,user);
		LOGGER.info("user:"+user.getUri());
		LOGGER.info(projectsJson);
		return projectsJson;
	}
	public static String getListOfrojectsForDesignProblemJSON(User user,
			String designProblemUri) throws Exception {
		Collection<Project> allProjects=ProjectManagament.getInstance().getAllProjectsForDesignProblem(user.getUri().toString(),designProblemUri);
		String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(allProjects,user);
		LOGGER.info("user:"+user.getUri());
		LOGGER.info("designproblem:"+designProblemUri);
		LOGGER.info(projectsJson);
		return projectsJson;
	}
	public static String getListOfProjectsForProblemNotSubmitted(User user, String designProblemUri) throws Exception{
		Collection<Project> projectsNotSubmitted=ProjectManagament.getInstance().getAllProjectsForDPNotSubmitted(user.getUri().toString(),designProblemUri);
		String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(projectsNotSubmitted,user);
		LOGGER.info("user:"+user.getUri());
		LOGGER.info("designproblem:"+designProblemUri);
		LOGGER.info(projectsJson);
		return projectsJson;
	}
	public static String setProjectAsSubmitted(String username,
			String projectUri, String submissionUri) throws Exception {
		// TODO Auto-generated method stub
		User user=UserManagament.getInstance().getUserByUsername(username);
		if(user==null){
		 		LOGGER.info("User not found for username:"+username);
		}
	 	Project project=ProjectManagament.getInstance().getProjectByUri(projectUri);
		if(project==null){
		 		LOGGER.info("Project not found for uri:"+projectUri);
		}
	 	Submission submission=ProjectManagament.getInstance().loadResourceByURI(Submission.class, submissionUri,false);
		if(submission==null){
		 		LOGGER.info("Submission not found for uri:"+submissionUri);
		}
	 	JSONStringer js = new JSONStringer();
		js.array();
	 	js.object();
		//Submitting
	 	Submitting submitting=new Submitting();
		String submittingUri=URIBuilder.getInstance().uriGenerator.generateInstanceURI(submitting);
		js.key("operation").value("insertrecord");
		js.key("submittinguri").value(submittingUri);
		//String submittingUri=OntologyClassesUtility.getNamespaceClassName(Submitting.class)+"_"+dataobject.getString("id");
		//Submitting submitting=(Submitting) new ClassFactory().createInstance(Submitting.class, submittingUri);
		submitting.setDateTimeSent(new Date());
		submitting.setUri(submittingUri);
		submitting.setInResponseTo(submission);
		submitting.setProjectRef(project);
		submitting.setSentBy(user);
		
		//UploadingSolution
		UploadingSolution uploadingSolution=ProjectManagament.getInstance().getUploadingSolution(user.getUri().toString(),projectUri);
		if(uploadingSolution!=null){
	 			uploadingSolution.setSubmitted(true);
			RDFDataManagement.getInstance().updateResource(uploadingSolution,false);
		}
		
		//LearningContext
		LearningContext lContext=new LearningContext();
		String lContextUri=URIBuilder.getInstance().uriGenerator.generateInstanceURI(lContext);
		lContext.setUri(lContextUri);
		lContext.setContentRef(submission);
		lContext.setUserRef(user);
		lContext.setActivityRef(submitting);
		RDFDataManagement.getInstance().saveResource(lContext,false);
				
		RDFDataManagement.getInstance().saveResource(submitting,false);
		PeersProcessing.getInstance().processLearningContext(lContext, submission);
		
		
		
	 	js.endObject();
	 	js.object();
		js.key("operation").value("urimapping");
		js.key("domainconcept").value(submitting.getClass().getSimpleName());
	 	js.key("uri").value(submitting.getUri().toString());
		js.endObject();
		 
		js.endArray();
	 
		return js.toString();
	}
	public static String getListOfAllAllowedProjectsJSON(User user) throws Exception {
		Collection<Project> allAllowedProjects=ProjectManagament.getInstance().getAllAllowedProjectsForUser(user.getUri().toString());
		String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(allAllowedProjects,user);
		LOGGER.info("user:"+user.getUri());
		LOGGER.info(projectsJson);
		return projectsJson;
	}
	public static String getListOfProjectsByUris(String jsonString) throws Exception {
		JSONArray jsonProjects=new JSONArray(jsonString);
		 Collection<Project> allProjects=new ArrayList<Project>();
		for(int i=0;i<jsonProjects.length();i++){
			JSONObject jProject=jsonProjects.getJSONObject(i);
			String prUri=jProject.getString("selectedproject");
			Project pr=SolutionsDataLayer.getDataLayerInstance().getProjectByUri(prUri);
			//Project pr=ProjectManagament.getInstance().loadResourceByURI(Project.class, prUri,false);
			allProjects.add(pr);
		}
		 String projectsJson=ProjectJSONExporter.getInstance().getProjectsAsJSON(allProjects);
		return projectsJson;
	}
}
