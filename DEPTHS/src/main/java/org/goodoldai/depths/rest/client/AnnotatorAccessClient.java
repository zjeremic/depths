package org.goodoldai.depths.rest.client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.IOUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class AnnotatorAccessClient {
	
	private static URI getBaseURI() {
		return UriBuilder.fromUri(
				"http://localhost:8080/").build();
		//http://localhost:8080/entityhub
	}

	public void addDomainOntology() throws IOException{
        URL url = new URL("http://localhost:8080/engines");

        HttpURLConnection con = (HttpURLConnection)url.openConnection();

        con.setDoOutput(true);

        con.setRequestMethod("POST");

        con.setRequestProperty("Accept", "application/rdf+xml");

        con.setRequestProperty("Content-type", "text/plain");

        OutputStream out = con.getOutputStream();

        IOUtils.write("http://stackoverflow.com/questions/1083674/java-inputstream-print-to-console-the-content", out);

        IOUtils.closeQuietly(out);

        con.connect(); //send the request

        if(con.getResponseCode() > 299){ //assume an error

            //error response

            InputStream errorStream = con.getErrorStream();

            if(errorStream != null){

                String errorMessage = IOUtils.toString(errorStream);

                IOUtils.closeQuietly(errorStream);

                //write a error message

            } else { //no error data

                //write default error message with the status code

            }

        } else { //get the enhancement results

            InputStream enhancementResults = con.getInputStream();
           
            BufferedReader in = new BufferedReader(new InputStreamReader(enhancementResults));
            String line = null;
            
 

        }

	}

}
