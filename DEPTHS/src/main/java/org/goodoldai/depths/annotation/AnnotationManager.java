package org.goodoldai.depths.annotation;

import java.rmi.RemoteException;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.goodoldai.depths.config.DomainOntologyConfig;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.skos.Concept;
import org.goodoldai.depths.semanticstuff.services.moodle.RDFDataManagement;
import org.goodoldai.depths.utility.ConceptRDFReader;
 
 

public class AnnotationManager {
	private static class AnnotationManagerHolder {
		private static final AnnotationManager INSTANCE = new AnnotationManager();
	}

	public static AnnotationManager getInstance() {
		return AnnotationManagerHolder.INSTANCE;
	}

	public AnnotationManager() {

	}

	public void annotateContent(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,
			String content,Resource resource) throws RemoteException {
	ContentAnnotationThread cat=new ContentAnnotationThread(mdlTableName,
			fieldName, fieldValue, contentFieldName,
			content, resource);
	cat.run();
	}

	public void annotateAllRepositories() {
 
		WebRepositoryAnnotationThread wrat = new WebRepositoryAnnotationThread();
	 
		wrat.run();
	 

	}
	private  Collection<Concept> readAllConceptsFromFile (String inputFile, Resource domain) throws Exception {
		ConceptRDFReader rdfReader = new ConceptRDFReader();
		rdfReader.addData(inputFile, null, null);
		Collection<Concept> readConcepts = rdfReader.getConcepts();
		Iterator<Concept> cIter = readConcepts.iterator();
		
		while (cIter.hasNext()) {
			Concept concept = (Concept) cIter.next();
 
			concept=RDFDataManagement.getInstance().saveResource(concept, false);
 
			domain.addTopic(concept);
		}
		return readConcepts;
	}
	public void readAllDomainOntologies() throws Exception{
		ArrayList<DomainOntologyConfig> dmOnts=Settings.getInstance().config.domainOntologiesConfig.domOntConf;
		for(DomainOntologyConfig dmOntConf:dmOnts){
			addDomainOntology(dmOntConf.uri,dmOntConf.filelocation, dmOntConf.title);
		}
	}
	private  void addDomainOntology(String domainUri,String fileLocation, String title) throws Exception {
		Resource domain1 = new Resource();
		domain1.setTitle(title);
		domain1.setUri(domainUri);
		readAllConceptsFromFile(fileLocation, domain1);
		RDFDataManagement.getInstance().saveResource(domain1, false);
	}

	public void annotateCourseContent(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,
			String content, Resource resource) throws RemoteException {
		ContentAnnotationThread contAnnThread = new ContentAnnotationThread(mdlTableName,
				fieldName, fieldValue, contentFieldName,
				content, resource);

		contAnnThread.start();
		
	}
}
