package org.goodoldai.depths.annotation;

import java.io.Serializable;
import java.util.ArrayList;

import org.goodoldai.depths.config.DomainOntologyConfig;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.config.WebRepositoryConfig;

class WebRepositoryAnnotationThread extends Thread implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3783377163979910345L;
	
	public void run()
	{
		 
		ArrayList<WebRepositoryConfig> webRepConfs=Settings.getInstance().config.webRepsConfig.webRepConf;
		ArrayList<DomainOntologyConfig> domOntConfs=Settings.getInstance().config.domainOntologiesConfig.domOntConf;
		SearchCrawler sc=new SearchCrawler();
		for(WebRepositoryConfig wrf:webRepConfs){
			if(wrf.annotate){
			 
		 	String url=wrf.url;
		 	String name=wrf.name;
			String ontId=wrf.ontologyId;
			String ontologyUri="";
			for(DomainOntologyConfig domOntConf:domOntConfs){
				if(domOntConf.id.equals(ontId)){
					ontologyUri=domOntConf.uri;
				}
			}
			int maxUrlNumber=wrf.numbOfLinks;
			sc.startsearching(url, name, maxUrlNumber, ontologyUri);
		}
		} 
			
	 
	}

}
