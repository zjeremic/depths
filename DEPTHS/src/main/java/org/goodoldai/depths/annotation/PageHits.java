package org.goodoldai.depths.annotation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

 

import org.apache.log4j.Logger;
import org.goodoldai.depths.utility.NamespacesHandler;
import org.goodoldai.depths.utility.SemanticAnnotationUtility;

public class PageHits {
	private static final Logger LOGGER = Logger.getLogger(PageHits.class);
	/**
	 * Keeps the value of the number of each pattern appearance (hits) on the page
	 */
	private //the number of occurrences of the considered term in document
	HashMap<String,Integer> patternHits=new HashMap<String,Integer>();
	private String dominantPattern=null;
	private int dominantPatternHits=0;
	private double dominantPattern_TFIDF=0.00;
	
	private String pageUrl=null;
	private String pageTitle="";
	private String titledPattern=null;
	private double pageRelevance=0.00;
	//denominator is the number of occurrences of all terms in document
	private int denominator=0;
	private HashMap<String,Double> documentTFSpaceModel=new HashMap<String,Double>();
	private HashMap<String,Double> documentTFIDFSpaceModel=new HashMap<String,Double>();
	// Euclidean normalized tf values for document
	private HashMap<String,Double> documentEuclNormTFSpaceModel=new HashMap<String,Double>();
	private HashMap<String, Double> patternTFNorm=new HashMap<String,Double>();
	
	 
	PageHits(String url){
		pageUrl=url;
	}
	PageHits(String url,String pageT){
		//LOGGER.info("new PageHits for url:"+url);
		pageUrl=url;
		pageTitle=pageT;
		 
	}
	/**
	 * Add value for each pattern found on the page
	 * @param patternURI
	 */
	 void addPatternHit(OnlineRepositoryManager onlRepManager,String patternURI){
		 
		  denominator=denominator+1;
		 if (patternHits.containsKey(patternURI)){
			 
			 Integer hits=patternHits.get(patternURI);
			 hits=hits+1;
			 patternHits.remove(patternURI);
			 patternHits.put(patternURI, hits);
			 
		 }else{
			 
			 Integer hits=new Integer(1);
			 patternHits.put(patternURI, hits);
			 onlRepManager.addDocContinuingTerm(patternURI);
		 }
		 
	 }
	 /**
	  * Founds the pattern that has the most appereance on the page
	  */
	 boolean analyseHits(OnlineRepositoryManager onlRepManager) {
		 boolean keepThisPage = true;
 
		 Set<String> set = patternHits.keySet();
		 Iterator<String> iter = set.iterator();
		 double max = 0.0;
		 double pageDominator = getDenominator();
		 double allDocNumber = onlRepManager.getNumberOfAllDocuments();
		// LOGGER.info("allDocNumber:" + allDocNumber);
		 LOGGER.info("----------------------------------------");
		 LOGGER.info("----------------------------------------");
		 LOGGER.info("PAGE URL:" + pageUrl );
		  
		 // quantity of tf values
		 double quantitySum = 0;
		 double quantity = 0;
		 while (iter.hasNext()) {
			 String pattern = (String) iter.next();
			 Integer hits = (Integer) patternHits.get(pattern);
			 double patternHits = hits.doubleValue();
			 double termFrequency = patternHits / pageDominator;
			 this.patternTFNorm.put(pattern, termFrequency);
			 LOGGER.info("page:" + pageUrl + " pattern:" + pattern
				 	 + " patternHits:" + patternHits + " pageDominator:"
				 	 + pageDominator + " termFrequency:" + termFrequency );
			 double numbOfDocsContTerm = onlRepManager.getDocNumbContTerm(pattern);
			 double inverseDocumentFrequency =0;
			 double tfidf=1;
			 if (numbOfDocsContTerm>0){
				 double idf_base=allDocNumber / numbOfDocsContTerm;
					 inverseDocumentFrequency= Math.log(idf_base);
			 
				 tfidf = termFrequency * inverseDocumentFrequency;
			 
			 }
					
			 LOGGER.info("allDocNumber:"+allDocNumber+ " numbOfDocsContTerm:" + numbOfDocsContTerm );
			   LOGGER.info("inverseDocumentFrequency:" + inverseDocumentFrequency );
			 
			  
			 LOGGER.info("---"+NamespacesHandler
					 .getLocalNameFromFullName(pattern)
					 + " pageDom:"+ pageDominator
					 + " TF:" + termFrequency
					 + " docContTerm:"+ numbOfDocsContTerm
					 + " IDF:"+ inverseDocumentFrequency
					 + " hits:"+ hits
					 + " tfidf:"+ tfidf );
					 
			 
			 documentTFSpaceModel.put(pattern, new Double(termFrequency));
			 quantitySum = quantitySum + termFrequency * termFrequency;
			 documentTFIDFSpaceModel.put(pattern, new Double(tfidf));
			 if (tfidf > max) {
				 dominantPattern = pattern;
				 max = tfidf;
				 dominantPattern_TFIDF = max;
			 }
		 }
		 quantity = Math.sqrt(quantitySum);
		// LOGGER.info("addPageOnRightPosition quantitySum:" + quantitySum + " quantity:" + quantity );
		 Set<String> set2 = documentTFSpaceModel.keySet();
		 Iterator<String> iter2 = set2.iterator();
		 while (iter2.hasNext()) {
			 String pattern2 = (String) iter2.next();
			 Double tf = documentTFSpaceModel.get(pattern2);
			 double tfVal = tf.doubleValue();
			 // Euclidean normalized tf values
			 double euclNormVal = tfVal / quantity;
			// LOGGER.info("addPageOnRightPosition pattern:" + pattern2 + " Euclidean normalized tf value:" + euclNormVal );
			 documentEuclNormTFSpaceModel.put(pattern2, euclNormVal);
		 }
		 if (dominantPattern_TFIDF < SemanticAnnotationUtility
				 .getTFIDFMinAcceptableValue()) {
			 keepThisPage = false;
			 LOGGER.info("NOT ACCEPTED PAGE:" + pageUrl + " dominant:" + dominantPattern + " TFIDF:" + dominantPattern_TFIDF );
		 }
		 LOGGER.info("FOUND dominantPattern:" +  dominantPattern  +" dominantPattern_TFIDF:"+dominantPattern_TFIDF+" page:"+this.getPageUrl());
		 if(dominantPattern==null){
			 
			 keepThisPage=false;
		 }
		 return keepThisPage;
	 }
	/**
	 * @return the patternHits
	 */
	public HashMap<String, Integer> getPatternHits() {
		return patternHits;
	}
	/**
	 * @return the dominantPattern
	 */
	public String getDominantPattern() {
		return dominantPattern;
	}
	/**
	 * @return the dominantPatternHits
	 */
	public int getDominantPatternHits() {
		return dominantPatternHits;
	}
	/**
	 * @return the pageUrl
	 */
	public String getPageUrl() {
		return pageUrl;
	}
	/**
	 * @return the pageTitle
	 */
	public String getPageTitle() {
		return pageTitle;
	}
	/**
	 * @param pageTitle the pageTitle to set
	 */
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	/**
	 * @return the titledPattern
	 */
	public String getTitledPattern() {
		return titledPattern;
	}
	/**
	 * @param titledPattern the titledPattern to set
	 */
	public void setTitledPattern(String titledPattern) {
 
		this.titledPattern = titledPattern;
	}
	/**
	 * @return the pageRelevance
	 */
	public double getPageRelevance() {
		return pageRelevance;
	}
	/**
	 * @param pageRelevance the pageRelevance to set
	 */
	public void setPageRelevance(double pageRelevance) {
		this.pageRelevance = pageRelevance;
	}
	/**
	 * @return the denominator
	 */
	public int getDenominator() {
		return denominator;
	}
	/**
	 * @param denominator the denominator to set
	 */
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	void setPatternRelevance(OnlineRepositoryManager onlRepManager, String patternUri) {
		double allDocNumber = onlRepManager.getNumberOfAllDocuments();
		double numbOfDocsContTerm = onlRepManager.getDocNumbContTerm(patternUri);
		double inverseDocumentFrequency = Math.log(allDocNumber
				/ numbOfDocsContTerm);

		//double patternTF = documentEuclNormTFSpaceModel.get(patternUri).doubleValue();
		double patternTF = patternTFNorm.get(patternUri).doubleValue();
		double idf_square=Math.sqrt(inverseDocumentFrequency);
		pageRelevance = patternTF * idf_square;
		if(pageRelevance>1){
			pageRelevance=1.00;
		}
		

		double pTF = documentTFIDFSpaceModel.get(patternUri).doubleValue();
		double pr = pTF * inverseDocumentFrequency;
		 
		LOGGER.info(NamespacesHandler
				.getLocalNameFromFullName(patternUri)
				+ " idf:"
				+ inverseDocumentFrequency
				+" idf_square:"
				+idf_square
				+ " tf:"
				+ patternTF
				+ " pageRel:"
				+ pageRelevance
				+ " TFIDF:"
				+ pTF
				+ " ptfidfRel:"
				+ pr +" pageUrl:"+this.pageUrl);
	}
	/**
	 * @return the dominantPattern_relevance
	 */
	 
	
	

}
