package org.goodoldai.depths.annotation;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Settings;
import com.ontotext.kim.client.GetService;
import com.ontotext.kim.client.KIMService;

 

public class KIMFacility {
	private static final Logger LOGGER = Logger.getLogger(KIMFacility.class);
	private static KIMService serviceKim=null;
	private static class KIMFacilityHolder {
		static final KIMFacility INSTANCE = new KIMFacility();
	}

	 
	static KIMFacility getInstance() {
		return KIMFacilityHolder.INSTANCE;
	}
	public KIMFacility(){
		
	}
	public KIMService getKIMServiceInstance(){
		if(serviceKim==null){
			try {
				connectToKIM();
				return serviceKim;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error:"+e.getLocalizedMessage());
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
		}else{
			return serviceKim;
		}
		 return null;
	}
	 void connectToKIM() throws RemoteException, NotBoundException{
		// connect to KIMService (deployed on specific host)
		int port=Settings.getInstance().config.kimConfig.port;
		String host=Settings.getInstance().config.kimConfig.server;
		serviceKim = GetService.from(host,port);
		LOGGER.info("KIM Server connected."); 
		LOGGER.info("KIM Platform : " + serviceKim.getVersion()); 
			 		
	}
}
