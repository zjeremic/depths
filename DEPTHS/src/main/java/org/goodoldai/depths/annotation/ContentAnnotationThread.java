package org.goodoldai.depths.annotation;

import java.io.Serializable;

import org.goodoldai.depths.domain.general.Resource;


public class ContentAnnotationThread  extends Thread implements Serializable{
	 
	private static final long serialVersionUID = 1L;
	private String contentToAnnotate=null;
	private  String mdlTable=null;
	private  String mdlRefFieldName=null;
	private  String mdlRefFieldValue=null;
	private  String contentToAnnotateFieldName=null;
	private Resource resourceToAnnotate=null;
	 
	 
 	  ContentAnnotationThread(String mdlTableName,
			String fieldName, String fieldValue, String contentFieldName,
			String content, Resource resource){
		contentToAnnotate=content;
		 
		mdlTable=mdlTableName;
		 
		mdlRefFieldName=fieldName;
		mdlRefFieldValue=fieldValue;
		contentToAnnotateFieldName=contentFieldName;
		resourceToAnnotate=resource;
		 
	} 
 
	public void run()
	{
	 Annotate a=new Annotate();

	 a.getContentAnnotation(mdlTable,
			 mdlRefFieldName, mdlRefFieldValue, contentToAnnotateFieldName,
			 contentToAnnotate, resourceToAnnotate);
		 
	}
 
}

 