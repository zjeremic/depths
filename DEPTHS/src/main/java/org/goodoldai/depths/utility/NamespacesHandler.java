package org.goodoldai.depths.utility;

import java.io.Serializable;
import java.util.Vector;

import org.apache.log4j.Logger;
//import org.goodoldai.depths.rest.json.SolutionServiceUtilityTest;



public class NamespacesHandler implements Serializable{
	private static final Logger LOGGER = Logger.getLogger(NamespacesHandler.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Vector<Namespace> namespaces=new Vector<Namespace>();
	
	/**
	 * Returns a fully qualified namespace based on its prefix
	 * Each prefix and corresponding URI should be defined in Vector namespaces
	 * that is loaded from the list of namespaces.
	 * @param prefix prefix
	 * @return uri fully qualified namespace URI
	 */
	public static String getFullyQualifiedNamespace(String prefix){
		String uri="";
		int vSize=namespaces.size();
		for (int i=0;i<vSize;i++){
			Namespace namespace=namespaces.get(i);
			if (prefix.equals(namespace.getPrefix())){
				uri=namespace.getNamespaceURI();
				 
			}
		}
		if (uri.equals("")){
			LOGGER.error("ERROR: namespace not found for prefix: "+prefix);
		}
	 return uri;
	}
	public static String getPrefixForNamespace(String nmspc){
		String prefix="";
		int vSize=namespaces.size();
		for (int i=0;i<vSize;i++){
			Namespace namespace=namespaces.get(i);
			if (nmspc.equals(namespace.getNamespaceURI())){
				prefix=namespace.getPrefix();
				 
			}
		}
		if (prefix.equals("")){
			LOGGER.error("ERROR: prefix not found for namespace: "+nmspc);
		}
	 return prefix;
	}
	/**
	 * 
	 * This method add definition for new namespace that will be used
	 * @param prefix defines prefix used for namespace
	 * @param namespaceURI qualified namespace
	 */
	public static void addNamespace(String prefix, String namespaceURI)
	{
		Namespace nmsp=new Namespace(prefix,namespaceURI);
		namespaces.add(nmsp);
	}
	/**
	 * @return the namespaces
	 */
	public static Vector<Namespace> getNamespaces() {
		return namespaces;
	}
	/**
	 * @param namespaces the namespaces to set
	 */
	public static void setNamespaces(Vector<Namespace> namespaces) {
		NamespacesHandler.namespaces = namespaces;
	}
	public static void initializeNamespaces()
	{
		 
		if (namespaces.size()==0){
		String namespace_prefixes[][]={
					{"rdfs","http://www.w3.org/2000/01/rdf-schema#" },
				    {"rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#"},
				    {"owl", "http://www.w3.org/2002/07/owl#"},
					{"xsd", "http://www.w3.org/2001/XMLSchema#"},
					{"d2rq", "http://www.wiwiss.fu-berlin.de/suhl/bizer/D2RQ/0.1#"},
					{"dc", "http://purl.org/dc/elements/1.1/"},
					{"dcterms", "http://purl.org/dc/terms/"},
					{"foaf", "http://xmlns.com/foaf/0.1/"},
					{"skos", "http://www.w3.org/2004/02/skos/core#"},
					{"loco", "http://www.lornet.org/loco-cite/2007/.owl#"},
					{"vcard", "http://www.w3.org/2001/vcard-rdf/3.0#"},
					{"dc", "http://purl.org/dc/elements/1.1/"},
					{"quiz", "http://www.lornet.org/loco-cite/quiz.owl#"},
					{"depths", "http://www.depths.org/zjeremic/software-solution/2008/depths.owl#"},
					//kim namespaces
					{"PLForm","http://cse.unl.edu/~scotth/SWont/Patterns/PLForm.owl#"},
					{"GoFForm","http://cse.unl.edu/~scotth/SWont/Patterns/GoFForm.owl#"},
					{"foaf","http://xmlns.com/foaf/0.1/"},
					{"wkb","http://www.ontotext.com/kim/2006/05/wkb#"},
					{"protont","http://proton.semanticweb.org/2006/05/protont#"},
					{"PFOWLForm","http://cse.unl.edu/~scotth/SWont/Patterns/PFOWLForm.owl#"},
					{"PLForm","http://cse.unl.edu/~scotth/SWont/Patterns/PLForm.owl#"},
					{"POSAForm","http://cse.unl.edu/~scotth/SWont/Patterns/POSAForm.owl#"},
					{"PloPForm","http://cse.unl.edu/~scotth/SWont/Patterns/PLoPForm.owl#"},
					{"protonkm","http://proton.semanticweb.org/2006/05/protonkm#"},
					{"protons","http://proton.semanticweb.org/2006/05/protons#"} ,
					{"kimdepths","http://www.depths.org.zjeremic/software-solution/2008/8/kimdepths.owl#"}
					};
		
			String prefix, namespaceURI;
		for (int i=0; i < namespace_prefixes.length; i++) {
			prefix=namespace_prefixes[i][0];
			namespaceURI=namespace_prefixes[i][1];
			addNamespace(prefix, namespaceURI);
	    }
		}else{
			 
		}
}
	public static String getFullyQualifiedNameFromPrefix_Name(String uri){
		if (uri.contains("http:")){
			uri=uri.replace("http:", "");
		}
		int sI=uri.indexOf(":");
		String localName=uri.substring(sI+1);
		String prefix=uri.subSequence(0, sI).toString();
		String fullNamespace=getFullyQualifiedNamespace(prefix);
		String val=fullNamespace+localName;
		 
		return val;
	}
	/**
	 * Get local name of the uri with prefix
	 * It substrings url with prefix and returns name without prefix
	 * @param uri
	 * @return val local name of the qualified name with prefix
	 */
	public static String getLocalName(String uri){
		int sI=uri.indexOf(":")+1;
		String val=uri.substring(sI);
		return val;
	}
	public static String getLocalNameFromFullName(String fullUri){
		int sI=fullUri.indexOf("#")+1;
		String val=fullUri.substring(sI);
		return val;
	}
	public static String getNamespaceFromFullName(String fullUri){
	 
		int sI=fullUri.indexOf("#");
		 
		String val=fullUri.subSequence(0,sI).toString();
		 
		return val;
	}
	/**
	 * Return prefix of the uri with prefix
	 * @param uri uri that the prefix has to be resolved
	 * @return val prefix
	 */
	public static String getPrefix(String uri){
		int sI=uri.indexOf(":");
		String val=uri.subSequence(0, sI).toString();
		return val;
	}
	public static String getNameWithPrefix(String fullURI){
		
		int sI=fullURI.indexOf("#");
		String namespace=fullURI.subSequence(0,sI+1).toString();
		 String prefix=getPrefixForNamespace(namespace);
		 String name=fullURI.substring(sI+1);
		  
		 return prefix+":"+name;
		
	}

}
