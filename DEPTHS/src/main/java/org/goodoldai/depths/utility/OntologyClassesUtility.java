package org.goodoldai.depths.utility;

import org.apache.log4j.Logger;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

public class OntologyClassesUtility {
	private static final Logger LOGGER = Logger.getLogger(OntologyClassesUtility.class);
	static String getDomainClassNamespace(Class clazz) throws ClassNotFoundException{
		String domainClassUri="";
		Class rdfTypeClass;
		 
			rdfTypeClass = Class.forName( "thewebsemantic.RdfType" );
		 
	    RdfType rdfType = ( RdfType ) clazz.getAnnotation( rdfTypeClass );


	     
	    Class namespaceClass = Class.forName( "thewebsemantic.Namespace" );
	    Namespace namespace = ( Namespace ) clazz.getAnnotation( namespaceClass );


	     
	    domainClassUri=namespace.value()+rdfType.value();
	    
	    return domainClassUri;
	}
	 public static String getNamespaceClassName(Class clazz){
		 String namespace="";
		 try {
			 namespace=getDomainClassNamespace(clazz);
			//namespaceClassName=namespace+clazz.getSimpleName();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		return namespace;
		 
	 }
	 public static String getLocalName(String uri){
		 
			int sI=uri.indexOf("#")+1;
			String val=uri.substring(sI);
			 
			return val;
		}
}
