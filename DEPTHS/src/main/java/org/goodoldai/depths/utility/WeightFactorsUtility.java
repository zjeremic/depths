package org.goodoldai.depths.utility;

public class WeightFactorsUtility {
	private static double brainstormingActivityWeightPeers;
	private static double assessingActivityWeightPeers;
	private static double submittingActivityWeightPeers;
	
	private static double brainstormingCourseActivityWeightPeers;
	private static double assessingCourseActivityWeightPeers;
	private static double submittingCourseActivityWeightPeers;
	
	private static double activityCompetenceWeightPeers;
	
	private static double brainstormingSkillWeightPeers;
	private static double submittingSkillWeightPeers;
	
	private static double brainstormingCourseSkillWeightPeers;
	private static double submittingCourseSkillWeightPeers;
	
	
	private static double skillCompetenceWeightPeers;
	
	private static double patternRelevancePercent;
	private static double ratingRelevancePercent;
	/**
	 * @return the patternRelevancePercent
	 */
	public static double getPatternRelevancePercent() {
		return patternRelevancePercent;
	}
	/**
	 * @param patternRelevancePercent the patternRelevancePercent to set
	 */
	public static void setPatternRelevancePercent(double patternRelevancePercent) {
		WeightFactorsUtility.patternRelevancePercent = patternRelevancePercent;
	}
	/**
	 * @return the ratingRelevancePercent
	 */
	public static double getRatingRelevancePercent() {
		return ratingRelevancePercent;
	}
	/**
	 * @param ratingRelevancePercent the ratingRelevancePercent to set
	 */
	public static void setRatingRelevancePercent(double ratingRelevancePercent) {
		WeightFactorsUtility.ratingRelevancePercent = ratingRelevancePercent;
	}
	/**
	 * @return the brainstormActivityWeightPeers
	 */
	public static double getBrainstormingActivityWeightPeers() {
		return brainstormingActivityWeightPeers;
	}
	/**
	 * @param brainstormActivityWeightPeers the brainstormActivityWeightPeers to set
	 */
	public static void setBrainstormingActivityWeightPeers(
			double brainstormActivityWeightPeers) {
		WeightFactorsUtility.brainstormingActivityWeightPeers = brainstormActivityWeightPeers;
	}
	/**
	 * @return the assessingActivityWeightPeers
	 */
	public static double getAssessingActivityWeightPeers() {
		return assessingActivityWeightPeers;
	}
	/**
	 * @param assessingActivityWeightPeers the assessingActivityWeightPeers to set
	 */
	public static void setAssessingActivityWeightPeers(
			double assessingActivityWeightPeers) {
		WeightFactorsUtility.assessingActivityWeightPeers = assessingActivityWeightPeers;
	}
	/**
	 * @return the submittingActivityWeightPeers
	 */
	public static double getSubmittingActivityWeightPeers() {
		return submittingActivityWeightPeers;
	}
	/**
	 * @param submittingActivityWeightPeers the submittingActivityWeightPeers to set
	 */
	public static void setSubmittingActivityWeightPeers(
			double submittingActivityWeightPeers) {
		WeightFactorsUtility.submittingActivityWeightPeers = submittingActivityWeightPeers;
	}
	/**
	 * @return the successCompetenceWeightPeers
	 */
	public static double getSkillCompetenceWeightPeers() {
		return skillCompetenceWeightPeers;
	}
	/**
	 * @param successCompetenceWeightPeers the successCompetenceWeightPeers to set
	 */
	public static void setSkillCompetenceWeightPeers(
			double skillCompetenceWeightPeers) {
		WeightFactorsUtility.skillCompetenceWeightPeers = skillCompetenceWeightPeers;
	}
	/**
	 * @return the activityCompetenceWeightPeers
	 */
	public static double getActivityCompetenceWeightPeers() {
		return activityCompetenceWeightPeers;
	}
	/**
	 * @param activityCompetenceWeightPeers the activityCompetenceWeightPeers to set
	 */
	public static void setActivityCompetenceWeightPeers(
			double activityCompetenceWeightPeers) {
		WeightFactorsUtility.activityCompetenceWeightPeers = activityCompetenceWeightPeers;
	}
	/**
	 * @return the brainstormingCourseActivityWeightPeers
	 */
	public static double getBrainstormingCourseActivityWeightPeers() {
		return brainstormingCourseActivityWeightPeers;
	}
	/**
	 * @param brainstormingCourseActivityWeightPeers the brainstormingCourseActivityWeightPeers to set
	 */
	public static void setBrainstormingCourseActivityWeightPeers(
			double brainstormingCourseActivityWeightPeers) {
		WeightFactorsUtility.brainstormingCourseActivityWeightPeers = brainstormingCourseActivityWeightPeers;
	}
	/**
	 * @return the assessingCourseActivityWeightPeers
	 */
	public static double getAssessingCourseActivityWeightPeers() {
		return assessingCourseActivityWeightPeers;
	}
	/**
	 * @param assessingCourseActivityWeightPeers the assessingCourseActivityWeightPeers to set
	 */
	public static void setAssessingCourseActivityWeightPeers(
			double assessingCourseActivityWeightPeers) {
		WeightFactorsUtility.assessingCourseActivityWeightPeers = assessingCourseActivityWeightPeers;
	}
	/**
	 * @return the submittingCourseActivityWeightPeers
	 */
	public static double getSubmittingCourseActivityWeightPeers() {
		return submittingCourseActivityWeightPeers;
	}
	/**
	 * @param submittingCourseActivityWeightPeers the submittingCourseActivityWeightPeers to set
	 */
	public static void setSubmittingCourseActivityWeightPeers(
			double submittingCourseActivityWeightPeers) {
		WeightFactorsUtility.submittingCourseActivityWeightPeers = submittingCourseActivityWeightPeers;
	}
	/**
	 * @return the brainstormingSkillWeightPeers
	 */
	public static double getBrainstormingSkillWeightPeers() {
		return brainstormingSkillWeightPeers;
	}
	/**
	 * @param brainstormingSkillWeightPeers the brainstormingSkillWeightPeers to set
	 */
	public static void setBrainstormingSkillWeightPeers(
			double brainstormingSkillWeightPeers) {
		WeightFactorsUtility.brainstormingSkillWeightPeers = brainstormingSkillWeightPeers;
	}
	/**
	 * @return the submittingSkillWeightPeers
	 */
	public static double getSubmittingSkillWeightPeers() {
		return submittingSkillWeightPeers;
	}
	/**
	 * @param submittingSkillWeightPeers the submittingSkillWeightPeers to set
	 */
	public static void setSubmittingSkillWeightPeers(
			double submittingSkillWeightPeers) {
		WeightFactorsUtility.submittingSkillWeightPeers = submittingSkillWeightPeers;
	}
	/**
	 * @return the brainstormingCourseSkillWeightPeers
	 */
	public static double getBrainstormingCourseSkillWeightPeers() {
		return brainstormingCourseSkillWeightPeers;
	}
	/**
	 * @param brainstormingCourseSkillWeightPeers the brainstormingCourseSkillWeightPeers to set
	 */
	public static void setBrainstormingCourseSkillWeightPeers(
			double brainstormingCourseSkillWeightPeers) {
		WeightFactorsUtility.brainstormingCourseSkillWeightPeers = brainstormingCourseSkillWeightPeers;
	}
	/**
	 * @return the submittingCourseSkillWeightPeers
	 */
	public static double getSubmittingCourseSkillWeightPeers() {
		return submittingCourseSkillWeightPeers;
	}
	/**
	 * @param submittingCourseSkillWeightPeers the submittingCourseSkillWeightPeers to set
	 */
	public static void setSubmittingCourseSkillWeightPeers(
			double submittingCourseSkillWeightPeers) {
		WeightFactorsUtility.submittingCourseSkillWeightPeers = submittingCourseSkillWeightPeers;
	}
	 

}
