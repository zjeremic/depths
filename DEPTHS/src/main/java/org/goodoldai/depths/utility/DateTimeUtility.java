package org.goodoldai.depths.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtility {

	public static long convertDateTimeToEpoch(Date date)
	{
		if(date!=null){
	    return date.getTime();
		}else{
			return 0;
		}
	}
	public static String getTime(){
		String dateFormat="yyyy.MMMMM.dd GGG hh:mm:sss aaa";
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
	    return sdf.format(cal.getTime());


	}
	public static String getCurrentDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = new Date();
	    return dateFormat.format(date);
		}

}
