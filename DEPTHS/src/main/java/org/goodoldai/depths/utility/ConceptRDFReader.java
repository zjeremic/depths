package org.goodoldai.depths.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.skos.Concept;


 

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.vocabulary.RDF;

public class ConceptRDFReader {
private OntModel model;
	
	private Resource conceptCls;
	private Property prefLabelProp;
	private static final Logger LOGGER = Logger.getLogger(ConceptRDFReader.class);	
	public ConceptRDFReader() {
		
		model = ModelFactory.createOntologyModel();
		
		conceptCls = model.createResource( Constants.SKOS_NS + "Concept");
		prefLabelProp = model.createProperty( Constants.SKOS_NS+ "prefLabel");
		
	}
	
	public void addData(String dataSrc, String defaultNS, String rdfLang) {
		if ( defaultNS == null )
			defaultNS = Constants.SKOS_NS;
		if ( rdfLang == null )
			rdfLang = "RDF/XML";
		
		try {
			model.read(new FileInputStream(dataSrc), defaultNS, rdfLang);
		} catch (FileNotFoundException e) {
			LOGGER.error( "Could not find file: " + dataSrc );
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (NullPointerException e) {
			LOGGER.error( e.getMessage() );
		}
	}
	
	public Collection<Concept> getConcepts() {
		Collection<Concept> concepts = extractConcepts();
		return concepts;
	}
	
	private Collection<Concept> extractConcepts() {
		Collection<Concept> concepts = new LinkedList<Concept>();
		SimpleSelector selector = new SimpleSelector(null, RDF.type, conceptCls );
		StmtIterator stmtIter = model.listStatements( selector );
		while (stmtIter.hasNext()) {
			Statement statement = (Statement) stmtIter.next();
			Resource conceptRes = statement.getSubject();			
			Concept c = new Concept();
			 
			c.setUri( conceptRes.getURI() );		
			StmtIterator stmtTopicIter = conceptRes.listProperties(prefLabelProp);
			while (stmtTopicIter.hasNext()) {
				Statement prefLabelStmt = (Statement) stmtTopicIter.next();
				String conceptLabel = prefLabelStmt.getLiteral().getString();
				c.setTitle(conceptLabel);
			}
			concepts.add(c);
			
		}
		return concepts;
	}
	
}
