package org.goodoldai.depths.utility;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.apache.log4j.Logger;
//import org.goodoldai.depths.rest.json.SolutionServiceUtilityTest;

import com.ontotext.kim.client.KIMService;



public class MySecurityManager {
	private static final Logger LOGGER = Logger.getLogger(MySecurityManager.class);
	public static KIMService getKIMServiceSecurityPolicy(){
		 
	//	System.setProperty("java.security.policy", "myClient.policy");
		//if (System.getSecurityManager() == null) {
		 
          //  System.setSecurityManager(new SecurityManager());
          

       // }
		KIMService serv=null;
		try {
			String rmihost=SemanticAnnotationUtility.getRMI_HOST();
			 int rmiport=SemanticAnnotationUtility.getRMI_PORT();
			 LOGGER.info("rmihost:"+rmihost+"rmiport"+rmiport);
		    Registry registry = LocateRegistry.getRegistry(rmihost,rmiport);
		  //  String[] names = registry.list();
		    
		    serv = (KIMService) registry.lookup("KIMService");
		    
		    
		} catch (Exception e) {
		    LOGGER.error("Remoteservice exception:");
		    
		}    
		    	 
return serv;
	}
}
