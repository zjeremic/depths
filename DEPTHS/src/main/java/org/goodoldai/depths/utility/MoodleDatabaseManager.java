package org.goodoldai.depths.utility;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
 
 
import org.goodoldai.depths.config.MoodleConfig;
import org.goodoldai.depths.config.Settings;



public class MoodleDatabaseManager implements Serializable{
	private static final Logger LOGGER = Logger.getLogger(MoodleDatabaseManager.class);
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	 
	private static Connection con;
	private static class MoodleDatabaseManagerHolder {
		private static final MoodleDatabaseManager INSTANCE = new MoodleDatabaseManager();
	}

	public static MoodleDatabaseManager getInstance() {
		return MoodleDatabaseManagerHolder.INSTANCE;
	}
	
	 
	 
	
	public  boolean checkDatabaseData(String hostname, String port, String dbname, String username, String password)
	{
		String jdbcUrl="jdbc:mysql://"+hostname+":"+port+"/"+dbname;
		String jdbcDriver="com.mysql.jdbc.Driver";
		boolean accessible=checkConnection(jdbcUrl, jdbcDriver, username, password);
	 
		return accessible;
	}
	 @SuppressWarnings("unused")
	private boolean checkConnection(String jdbcURL, String jdbcDriver, String username, String password)
	{
		boolean accessible=true;
	 
		try{
			Driver driverName=(Driver)Class.forName(jdbcDriver).newInstance();
			DriverManager.registerDriver(driverName);
		 
		}catch(ClassNotFoundException e){
			LOGGER.error("ClassNotFoundException in checkConnection "+e.getMessage());
 		}catch(InstantiationException ie){
 			LOGGER.error("InstantiationException in checkConnection "+ie.getMessage());
 	}catch(IllegalAccessException iae){
 		LOGGER.error("IllegalAccessException in checkConnection "+iae.getMessage());
 	}catch(SQLException e){
		LOGGER.error("SQLException 1 in checkConnection "+e.getMessage());
		 
	}
		try{
			 
		 	 con=DriverManager.getConnection(jdbcURL,username,password);
		 	}catch(SQLException e){
			LOGGER.error("SQLException in checkConnection "+e.getMessage());
			accessible=false;
		}catch(Exception ex){
			LOGGER.error("Exception in checkConnection "+ex.getMessage()+" getCause:"+ex.getCause()+" getStack:"+ex.getStackTrace());
			accessible=false;
		}
	 
		return accessible;
	}
	 
	 private boolean getDatabaseConnection(){
		MoodleConfig moodleConfig= Settings.getInstance().config.moodleConfig;
		 
		String hostname=moodleConfig.databaseHost;
		int port=moodleConfig.port;
		String dbname=moodleConfig.databaseName;
		String username=moodleConfig.databaseUser;
		String password=moodleConfig.databasePassword;
		
		String jdbcUrl="jdbc:mysql://"+hostname+":"+port+"/"+dbname;
		String jdbcDriver="com.mysql.jdbc.Driver";
		boolean accessible=checkConnection(jdbcUrl, jdbcDriver, username, password);
	 
		return accessible;
		
	 }
	 
	public void performUpdateTransaction(String sql){
		try{
			if(getDatabaseConnection()){
		 
		con.setAutoCommit(true);
	 
		final Statement stmt=con.createStatement();
	 
		stmt.executeUpdate(sql);
	 
		 
		
			}
		}catch(SQLException se){
			LOGGER.error("SQLException has happend during the SQL transaction");
			LOGGER.error("SQL:"+sql);
			LOGGER.error("Error cause:"+se.getLocalizedMessage());
		}
	}
 
	 
}
