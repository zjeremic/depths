package org.goodoldai.depths.utility;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.config.Settings;

import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.semanticstuff.rdfpersistance.DataModelManager;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryService;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.ontmodel.OntModelQueryServiceImpl;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.results.QueryResult;
import org.goodoldai.depths.semanticstuff.rdfpersistance.query.results.ResultsCollection;
import org.goodoldai.depths.semanticstuff.services.project.ProjectManagament;


public class EvaluationUtility {
	private static OntModelQueryService queryService = new OntModelQueryServiceImpl();
	 
	private static final Logger LOGGER = Logger.getLogger(EvaluationUtility.class);
	public static void printToFile(String s,String filePart){
		try {
			filePart=filePart.replace("/", "_");
			
	        BufferedWriter out = new BufferedWriter(new FileWriter(Settings.getInstance().config.loggingConfig.evalConfigDir+"/"+filePart+".log", true));
	        out.write(s);
	        out.write("\r\n");
	        out.close();
	    } catch (IOException e) {
	    }
}
	public static void printToFileWithTiming(String s,String filePart){
		long time=System.currentTimeMillis();
		printToFile("["+time+"]-"+s,filePart);
	}
	public static void getUserCommentsLog(){
		Thread thread = new Thread(new Runnable() {
			 
		    public void run() {
		       	String filename="usercomments_logs__";
		     	String queryString = 
		    				"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
			    			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
			    			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
			    			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
			    			"PREFIX dc: <"+Constants.DC_TERMS_NS+"> \n" + 
			    			"SELECT ?dProblem ?task (str(?dprobTitle) as ?dpTitle) ?brainstorming ?annotation (str(?cont) as ?content) " +
			    			"(str(?nType) as ?noteType) (str(?uname) as ?username) (str(?id) as ?identifier) \n" + 
			    			"		WHERE {\n" + 
			    			"		?dProblem rdf:type depths:DesignProblem.\n" + 
			    			"		?dProblem depths:taskRef ?task.\n" + 
			    			"		?dProblem dc:title ?dprobTitle.\n"+
			    			"		?task rdf:type depths:Brainstorm.\n" + 
			    			"		?brainstorming depths:inResponseTo ?task.\n" + 
			    			"		?brainstorming loco:hasAnnotation ?annotation.\n" + 
			    			"		?annotation rdf:type loco:UserNote.\n" + 
			    			"		?annotation loco:hasContent ?cont.\n" + 
			    			"		?annotation depths:hasNoteType ?nType.\n" + 
			    			"		?annotation loco:madeBy ?user.\n" + 
			    			"		?user loco:username ?uname.\n"+
			    			"		?user dc:identifier ?id.\n" + 
			    			"		}\n" ;
		    	 
		    	 
		    	ResultsCollection results = queryService
				.executeSelectSparqlQuery(queryString, 
						DataModelManager.getInstance().getDataModel());
		    	if (results != null && !results.getResultsCollection().isEmpty()){
		    		 
		    		HashMap<String,String> titles=new HashMap<String,String>();
		    		for (QueryResult result : results.getResultsCollection()) {
		    		 
		    			String dProblem=OntologyClassesUtility.getLocalName(result.getVariableValue("dProblem"));
		    			String dpTitle=result.getVariableValue("dpTitle");
		    			String task=OntologyClassesUtility.getLocalName(result.getVariableValue("task"));
		    			String brainstorming=OntologyClassesUtility.getLocalName(result.getVariableValue("brainstorming"));
		    			String annotation=OntologyClassesUtility.getLocalName(result.getVariableValue("annotation"));
		    			String content=result.getVariableValue("content");
		    			String noteType=result.getVariableValue("noteType");
		    			String username=result.getVariableValue("username");
		    			String identifier=result.getVariableValue("identifier");
		    			String message=dProblem+","+task+","+brainstorming+","+annotation+","+content+","+noteType+","+username+","+identifier;
		    			printToFile(message,filename+"_"+dProblem+"_"+DateTimeUtility.getCurrentDate());
		    			if(!titles.containsKey(dProblem)){
		    				titles.put(dProblem, dpTitle);
		    			}
		    			 
		    		}
		    		 Iterator it = titles.entrySet().iterator();
		    		    while (it.hasNext()) {
		    		        Map.Entry pairs = (Map.Entry)it.next();
		    		        printToFile(pairs.getKey()+","+pairs.getValue(),"usercomments_logs_titles");
		    		        it.remove(); // avoids a ConcurrentModificationException
		    		    }

		    	}
		    }
	 
		 
		 
		  
		    });
	 thread.start();
	
	}
	public static void loadEvaluation(final String classToLoad,final String conf){
				 Thread thread = new Thread(new Runnable() {
				 
				    public void run() {
			 
				    	String filename=classToLoad+"_"+conf+"_"+DateTimeUtility.getCurrentDate();
				    	String message=","+classToLoad+" STARTED,conf:"+conf+",,"+DateTimeUtility.getTime();
				    	printToFile(message,filename);
				    	String queryString ="";
				    	if(classToLoad.equals("ALL")){
				    		queryString = 
					    			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
					    			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
					    			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
					    			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
					    			"SELECT DISTINCT ?resource \n" + 
					    			"WHERE  {\n" +
					    						"?resource rdf:type ?class.\n"+
					    					"}";
				    	}else{
				    	  queryString = 
				    			"PREFIX rdf: <"+Constants.RDF_NS+"> \n" + 
				    			"PREFIX sioc: <"+Constants.SIOC_NS+"> \n" + 
				    			"PREFIX loco: <"+Constants.LOCO_NS+"> \n" + 
				    			"PREFIX depths: <"+Constants.DEPTHS_NS+"> \n" + 
				    			"SELECT DISTINCT ?resource \n" + 
				    			"WHERE  {\n" +
				    			 
				    						"{?resource rdf:type loco:"+classToLoad+".\n}UNION"+
				    						"{?resource rdf:type depths:"+classToLoad+".\n}"+
				    					"}";
				    	}
				    	long queryStartTime=System.currentTimeMillis();
				Collection<String>uris = queryService
						.executeOneVariableSelectSparqlQuery(queryString, "resource",
								DataModelManager.getInstance().getDataModel());
				long queryEndTime=System.currentTimeMillis();
				long queryTime=queryEndTime-queryStartTime;
				printToFile(","+classToLoad+" QUERY FINISHED,conf:"+conf+", "+queryTime,filename);
				int i=0;
				if (uris != null && !uris.isEmpty()) {
					for(String uri:uris){
						try {
							i++;
							String localname=OntologyClassesUtility.getLocalName(uri);
							long starttime=System.currentTimeMillis();
							Resource resource=ProjectManagament.getInstance().loadResourceByURI(Resource.class,uri,false);
							long endtime=System.currentTimeMillis();
							long overaltime=endtime-starttime;
							printToFile(i+","+localname+","+resource.getClass().getSimpleName()+","+overaltime,filename);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							LOGGER.error("Error:"+e.getLocalizedMessage());
						}
					//Project project=ProjectManagament.getInstance().loadResourceByURI(Project.class, uri, false);
					
					}
					
				}
				String messageEnd=","+classToLoad+" FINISHED,conf:"+conf+",,"+DateTimeUtility.getTime();
				printToFile(messageEnd,filename);
			 
				    }
				  
				    });
			 thread.start();
			
		}
}
