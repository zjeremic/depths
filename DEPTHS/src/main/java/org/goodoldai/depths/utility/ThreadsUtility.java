package org.goodoldai.depths.utility;

public class ThreadsUtility {
	private static int setFieldThreadDelayTime;
	private static int deleteRecordThreadDelayTime;
	private static int findPeersThreadDelayTime;
	private static int findWebPagesThreadDelayTime;
	private static int rateWebPagesThreadDelayTime;
	private static int rateContentThreadDelayTime;
	private static int findContentThreadDelayTime;

	/**
	 * @return the setFieldThreadDelayTime
	 */
	public static int getSetFieldThreadDelayTime() {
		return setFieldThreadDelayTime;
	}

	/**
	 * @param setFieldThreadDelayTime the setFieldThreadDelayTime to set
	 */
	public static void setSetFieldThreadDelayTime(int setFieldThreadDelayTime) {
		ThreadsUtility.setFieldThreadDelayTime = setFieldThreadDelayTime;
	}

	/**
	 * @return the deleteRecordThreadDelayTime
	 */
	public static int getDeleteRecordThreadDelayTime() {
		return deleteRecordThreadDelayTime;
	}

	/**
	 * @param deleteRecordThreadDelayTime the deleteRecordThreadDelayTime to set
	 */
	public static void setDeleteRecordThreadDelayTime(
			int deleteRecordThreadDelayTime) {
		ThreadsUtility.deleteRecordThreadDelayTime = deleteRecordThreadDelayTime;
	}

	/**
	 * @return the findPeersThreadDelayTime
	 */
	public static int getFindPeersThreadDelayTime() {
		return findPeersThreadDelayTime;
	}

	/**
	 * @param findPeersThreadDelayTime the findPeersThreadDelayTime to set
	 */
	public static void setFindPeersThreadDelayTime(int findPeersThreadDelayTime) {
		ThreadsUtility.findPeersThreadDelayTime = findPeersThreadDelayTime;
	}

	/**
	 * @return the findWebPagesThreadDelayTime
	 */
	public static int getFindWebPagesThreadDelayTime() {
		return findWebPagesThreadDelayTime;
	}

	/**
	 * @param findWebPagesThreadDelayTime the findWebPagesThreadDelayTime to set
	 */
	public static void setFindWebPagesThreadDelayTime(
			int findWebPagesThreadDelayTime) {
		ThreadsUtility.findWebPagesThreadDelayTime = findWebPagesThreadDelayTime;
	}
	public static void setRateWebPagesThreadDelayTime(
			int rateWebPagesThreadDelayTime) {
		ThreadsUtility.rateWebPagesThreadDelayTime = rateWebPagesThreadDelayTime;
	}

	/**
	 * @return the rateWebPagesThreadDelayTime
	 */
	public static int getRateWebPagesThreadDelayTime() {
		return rateWebPagesThreadDelayTime;
	}

	/**
	 * @return the rateContentThreadDelayTime
	 */
	public static int getRateContentThreadDelayTime() {
		return rateContentThreadDelayTime;
	}

	/**
	 * @param rateContentThreadDelayTime the rateContentThreadDelayTime to set
	 */
	public static void setRateContentThreadDelayTime(int rateContentThreadDelayTime) {
		ThreadsUtility.rateContentThreadDelayTime = rateContentThreadDelayTime;
	}

	/**
	 * @return the findContentThreadDelayTime
	 */
	public static int getFindContentThreadDelayTime() {
		return findContentThreadDelayTime;
	}

	/**
	 * @param findContentThreadDelayTime the findContentThreadDelayTime to set
	 */
	public static void setFindContentThreadDelayTime(int findContentThreadDelayTime) {
		ThreadsUtility.findContentThreadDelayTime = findContentThreadDelayTime;
	}

}
