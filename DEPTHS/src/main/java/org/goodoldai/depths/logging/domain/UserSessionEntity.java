package org.goodoldai.depths.logging.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "UserSession")
public class UserSessionEntity  implements java.io.Serializable  {
	
	private static final long serialVersionUID = 2575494548064870723L;
	
	private long id;
	protected String sessionId;
	protected Date sessionstarted;
	private UserEntity userEntity;
	
	public UserSessionEntity(){ }
	
	public UserSessionEntity(UserEntity userEnt,String sessionId, Date datetime){
		this.sessionId=sessionId;
		this.sessionstarted=datetime;
		this.userEntity=userEnt;
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	/*
	@Column(name = "userUri", nullable = false)
	public String getUserUri() {
		return userUri;
	}
	public void setUserUri(String userUri) {
		this.userUri = userUri;
	}
	*/
	
	@Column(name = "sessionId", nullable = true)
	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sessionstarted", length = 19)
	public Date getSessionStarted() {
		return this.sessionstarted;
	}

	public void setSessionStarted(Date datetime) {
		this.sessionstarted = datetime;
	}
	/*
	@OneToOne(cascade = CascadeType.ALL)
	public ResourceEntity getUser() {
		return user;
	}
	public void setUser(ResourceEntity user) {
		this.user = user;
	}
	*/
	@OneToOne(cascade = CascadeType.ALL)
	public UserEntity getUserEntity() {
		return userEntity;
	}
	
	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
}
