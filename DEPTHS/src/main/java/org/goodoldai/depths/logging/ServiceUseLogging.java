package org.goodoldai.depths.logging;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.LoggingConfig;
import org.goodoldai.depths.config.Settings;
import org.goodoldai.depths.domain.user.User;
import org.goodoldai.depths.logging.domain.PropertyEntity;
import org.goodoldai.depths.logging.domain.ServiceEntity;
import org.goodoldai.depths.logging.domain.ServiceTypeEntity;
import org.goodoldai.depths.logging.domain.UserEntity;
import org.goodoldai.depths.logging.domain.UserSessionEntity;
 

public class ServiceUseLogging extends AbstractQueryService{
	private static final Logger LOGGER = Logger.getLogger(ServiceUseLogging.class);
	@SuppressWarnings("unused")
	private LoggingConfig loggConfig = Settings.getInstance().config.loggingConfig;

	public static class ServiceUseLoggingHolder {
		private static final ServiceUseLogging INSTANCE = new ServiceUseLogging();
	}

	public static ServiceUseLogging getInstance() {
		return ServiceUseLoggingHolder.INSTANCE;
	}
	public UserEntity getUserEntity(String userUri, String name) {
		String stringQuery = "from UserEntity where uri =:uri";
	 
		try {
			synchronized (userUri) {
				UserEntity userEntity = (UserEntity) this
						.getUniqueResultForOneParamQuery(stringQuery, "uri", userUri);
				
				if (userEntity == null) {
					userEntity = new UserEntity(userUri, name);
					this.saveEntity(userEntity);
				}

				return userEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("User entity is null");
			return null;
		}
	}
	public UserSessionEntity getUserSessionEntity(UserEntity user,
			String sessionId) {

		String stringQuery = "from UserSessionEntity where userEntity_id=:userId and sessionId=:sessionId";

		try {
			synchronized (sessionId) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("userId", user.getId());
				params.put("sessionId", sessionId);

				UserSessionEntity usSessEntity = (UserSessionEntity) this
						.getUniqueResultForMoreParamQuery(stringQuery, params);

				if (usSessEntity == null) {
						usSessEntity = new UserSessionEntity(user, sessionId,
							new Date());
					this.saveEntity(usSessEntity);
				}
				return usSessEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("UserSession entity is null:" + npe.getMessage());
			return null;
		}
	}
	private ServiceTypeEntity getServiceType(String serviceType) {
		String stringQuery = "from ServiceTypeEntity where type =:type";
		try {
			synchronized (serviceType) {

				ServiceTypeEntity serviceTypeEntity = (ServiceTypeEntity) this
						.getUniqueResultForOneParamQuery(stringQuery, "type",
								serviceType);
				if (serviceTypeEntity == null) {
					serviceTypeEntity = new ServiceTypeEntity(serviceType);
					this.saveEntity(serviceTypeEntity);
				}
				return serviceTypeEntity;
			}
		} catch (NullPointerException npe) {
			LOGGER.info("Service type is null");
			return null;
		}
	}
	public void storeServiceUseRelatedLogData(String serviceType, String sessionId,
			Date eventDate, User user,  
			Map<String, String> properties) {
	 
		UserEntity userEntity = getUserEntity(user.getUri().toString(),
				user.getUsername());		
		if(userEntity==null)
			LOGGER.info("userEntity is null");
		UserSessionEntity userSession = getUserSessionEntity(userEntity,
				sessionId);

		 

		ServiceTypeEntity serviceTypeEnt = getServiceType(serviceType);
		ServiceEntity serviceEnt = new ServiceEntity(eventDate, userSession,
				  serviceTypeEnt);
	

		serviceEnt.setUsersession(userSession);
		this.saveEntity(serviceEnt);
		if (properties!=null){
		Iterator<Entry<String, String>> it = properties.entrySet().iterator();
		Set<PropertyEntity> propertiesSet = new HashSet<PropertyEntity>();

		while (it.hasNext()) {
			Entry<String, String> pairs = it.next();
		 if (pairs.getValue()==null){
			 pairs.setValue("");
		 }
			PropertyEntity pEntity = new PropertyEntity(pairs.getKey(),
					pairs.getValue());
			pEntity.setService(serviceEnt);
			this.saveEntity(pEntity);
			serviceEnt.addProperty(pEntity);
			propertiesSet.add(pEntity);

		}
		}
		this.updateEntity(serviceEnt);

	}
	
}
