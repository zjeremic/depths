package org.goodoldai.depths.logging.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "User")
public class UserEntity  implements java.io.Serializable {
	
	private static final long serialVersionUID = -1172001752363138397L;
	
	private long id;
	protected String userUri;
	protected String name;
	
	
	public UserEntity(){ }
	


	/**
	 * @param userUri
	 * @param name
	 */
	public UserEntity(String userUri, String name) {
		super();
		this.userUri = userUri;
		this.name = name;
		
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "uri", nullable = false, unique = true)
	@Index(name="uri")
	public String getUserUri() {
		return userUri;
	}
	
	public void setUserUri(String userUri) {
		this.userUri = userUri;
	}
	
	@Column(name = "name", nullable = true)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}



}
