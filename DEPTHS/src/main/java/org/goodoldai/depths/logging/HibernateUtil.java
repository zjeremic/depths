package org.goodoldai.depths.logging;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Settings;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static SessionFactory sessionFactory;
	private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class);
	static {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
//			sessionFactory = new Configuration().configure()
//					.buildSessionFactory();

			// Create session factory with values from config.xml
			Properties config = new Properties();
			 config.put("hibernate.dialect", Settings.getInstance().config.loggingConfig.dbDialect);
		 	config.put("hibernate.connection.driver_class", Settings.getInstance().config.loggingConfig.dbDriver);
			 config.put("hibernate.connection.url",  Settings.getInstance().config.loggingConfig.dbURL);
			 config.put("hibernate.connection.username", Settings.getInstance().config.loggingConfig.dbUser);
		 	config.put("hibernate.connection.password", Settings.getInstance().config.loggingConfig.dbPassword);
		 	config.put("hibernate.hbm2ddl.auto", Settings.getInstance().config.loggingConfig.dbDDLAuto);
			config.put("hibernate.connection.pool_size", 1);
			config.put("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
			 
			config.put("hibernate.current_session_context_class", "thread");
			config.put("hibernate.show_sql", false);
			//config.put("log4j.logger.org.hibernate", "ERROR, stdout");
			
			Configuration c = new Configuration().setProperties(config);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.PropertyEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.UserEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.UserSessionEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ServiceTypeEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ServiceEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ActivityEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ActivityPropertyEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ActivityTypeEntity.class);
			c.addAnnotatedClass(org.goodoldai.depths.logging.domain.ResourceEntity.class);
			sessionFactory = c.buildSessionFactory();
			
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			LOGGER.error("Initial SessionFactory creation failed." + ex);
			 
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session getSession() {
		
		return sessionFactory.openSession();
	}

	public static void closeSession(Session session) {
		if (session != null) {
			if (session.isOpen()) {
				session.close();
			}
		}
	}

}
