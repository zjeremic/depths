package org.goodoldai.depths.logging;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.LoggingConfig;
import org.goodoldai.depths.config.Settings;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
 

 

public abstract class AbstractQueryService {
	private static final Logger LOGGER = Logger.getLogger(AbstractQueryService.class);
	LoggingConfig loggConfig = Settings.getInstance().config.loggingConfig;

	public void saveEntityInstance(Object entity) {
		if (loggConfig.enable) {
			Session session = HibernateUtil.getSessionFactory().openSession();

			Transaction transaction = null;
			try {
				transaction = session.beginTransaction();
				session.save(entity);
				transaction.commit();
			} catch (HibernateException e) {
				transaction.rollback();
				LOGGER.info("Save entity instance was not successful:"
						+ e.getMessage());
			} finally {
				HibernateUtil.closeSession(session);
			}
		}
	}

	public void saveEntity(Object entity) {
		if (loggConfig.enable) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			try {
				transaction = session.beginTransaction();
				session.save(entity);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				transaction.rollback();
				LOGGER.info("Save entity was not successful:" + e.getMessage());
			} finally {
				HibernateUtil.closeSession(session);
			}
		}
	}

	public void updateEntity(Object entity) {
		if (loggConfig.enable) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transaction = null;
			try {
				transaction = session.beginTransaction();
				session.saveOrUpdate(entity);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				transaction.rollback();
				LOGGER.info("Update entity was not successful:"
						+ e.getMessage());
			} finally {
				HibernateUtil.closeSession(session);
			}
		}
	}

	public Object getUniqueResultForOneParamQuery(String stringQuery,
			String paramName, String paramValue) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Object result = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery(stringQuery);
			query.setParameter(paramName, paramValue);
			query.setMaxResults(1);
			result = query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			LOGGER.info("Update entity was not successful:" + e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return result;
	}

	public Object getUniqueResultForMoreParamQuery(String stringQuery,
			Map<String, Object> keyValue) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Object result = null;
		try {
			transaction = session.beginTransaction();
			Query query = null;
			query = session.createQuery(stringQuery);
			for (Entry<String, Object> entry : keyValue.entrySet()) {
				LOGGER.debug(entry.getKey() + " - " + entry.getValue());
				query.setParameter(entry.getKey(), entry.getValue());
			}

			query.setMaxResults(1);

			result = query.uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			LOGGER.info("Update entity was not successful:" + e.getMessage());
		} finally {
			HibernateUtil.closeSession(session);
		}
		return result;
	}

}
