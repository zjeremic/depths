package org.goodoldai.depths.logging.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "Resource")
public class ResourceEntity implements java.io.Serializable  {
	private static final long serialVersionUID = 589568079706221766L;
	
	private long id;
	protected String resourceUri;
	protected String resourceType;
	protected String resourceTitle;
	
	public ResourceEntity(){ }
	
	public ResourceEntity(String uri,String type, String title){
		this.resourceTitle=title;
		this.resourceType=type;
		this.resourceUri=uri;
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "uri", nullable = false)
	@Index(name="uri")
	public String getResourceUri() {
		return resourceUri;
	}

	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}
	
	@Column(name = "type", nullable = false, length = 1000)
	//@Index(name="type")
	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	@Column(name = "title", nullable = true, length = 1000)
	public String getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(String resourceTitle) {
		this.resourceTitle = resourceTitle;
	}
	 
}
