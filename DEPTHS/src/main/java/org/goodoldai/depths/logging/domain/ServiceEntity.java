package org.goodoldai.depths.logging.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

 

 

@Entity
@Table(name = "Service")
public class ServiceEntity {

	private long id;
	protected Date datetime;
	private Set<PropertyEntity> properties =new HashSet<PropertyEntity>();
	private UserSessionEntity usersession;
	private ServiceTypeEntity servicetype;
	
	public ServiceEntity(Date date, UserSessionEntity userSession2,
			ServiceTypeEntity serviceTypeEnt) {
		datetime=date;
		usersession=userSession2;
		servicetype=serviceTypeEnt;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, insertable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetime", length = 19)
	public Date getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	@OneToMany(cascade=CascadeType.ALL, mappedBy="service")
	public Set<PropertyEntity> getProperties() {
		return properties;
	}

	public void setProperties(Set<PropertyEntity> properties) {
		this.properties = properties;
	}
	
	public void addProperty(PropertyEntity property){
		this.properties.add(property);
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	public UserSessionEntity getUsersession() {
		return usersession;
	}

	public void setUsersession(UserSessionEntity usersession) {
		this.usersession = usersession;
	}
	@OneToOne(cascade = CascadeType.ALL)
	public ServiceTypeEntity getServicetype() {
		return servicetype;
	}
	
	public void setServicetype(ServiceTypeEntity servicetype) {
		this.servicetype = servicetype;
	}
}
