package org.goodoldai.depths.domain.general;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList; 


import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.annotation.Annotation;
import org.goodoldai.depths.domain.foaf.Agent;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Id;
import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.RDF_NS)
@RdfType("Resource")
public class Resource implements Serializable {

 
	private static final long serialVersionUID = -9040908565811200216L;
	private static final Logger LOGGER = Logger.getLogger(Resource.class);
	protected URI uri;
 
	private Collection<Resource> topics;
	
	private Collection<Annotation> hasAnnotation;
 
	private String description;
 
	protected String title;
 
	private String alternativeTitle;
 
	private String identifier;
 
	private Collection<Resource> relatedResources;
 
	private Date extent;
 
	private Date dateIssued;
 
	private Date dateCreated;
 
	private String language;
 
	private Agent holder;
 
	private Agent maker;
	
	private String visibility;
	protected Collection<Relevance> relevances;
	
	@RdfProperty(Constants.DEPTHS_NS + "hasVisibility")
	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public Resource() {
		topics = new LinkedList<Resource>();
		relatedResources = new LinkedList<Resource>();
		relevances=new LinkedList<Relevance>();
		hasAnnotation=new LinkedList<Annotation>();
		
	}

	public Resource(URI uri) {
		this();
		this.uri = uri;
	}

	public Resource(String uri) {
		this.uri = URI.create(uri);
	}

	/**
	 * @return the uri
	 */
	@Id
	public URI getUri() {
		return uri;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(URI uri) {
		this.uri = uri;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(String uri) {
		this.uri = URI.create(uri);
	}

	/**
	 * @return the description
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "description")
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@RdfProperty(Constants.DC_TERMS_NS + "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the alternativeTitle
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "alternative")
	public String getAlternativeTitle() {
		return alternativeTitle;
	}

	/**
	 * @param alternativeTitle
	 *            the alternativeTitle to set
	 */
	public void setAlternativeTitle(String alternativeTitle) {
		this.alternativeTitle = alternativeTitle;
	}

	/**
	 * @param identifier
	 *            the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the identifier
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "identifier")
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @return the extent
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "extent")
	public Date getExtent() {
		return extent;
	}

	/**
	 * @param extent
	 *            the extent to set
	 */
	public void setExtent(Date extent) {
		this.extent = extent;
	}

	/**
	 * @return the dateIssued
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "issued")
	public Date getDateIssued() {
		return dateIssued;
	}

	/**
	 * @param dateIssued
	 *            the dateIssued to set
	 */
	public void setDateIssued(Date dateIssued) {
		this.dateIssued = dateIssued;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateCreated
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "created")
	public Date getDateCreated() {

		return dateCreated;
	}

	/**
	 * @return the language
	 */
	@RdfProperty(Constants.DC_TERMS_NS + "language")
	public String getLanguage() {
		return language;
	}

	/**
	 * Should use predefined instances of Language class
	 * 
	 * @see org.goodoldai.depths.domainmodel.general.Language
	 * 
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	// @Override
	// public String toString() {
	// return "Resource [" + (uri != null ? "uri=" + uri : "") + "]";
	// }

	@RdfProperty(Constants.DC_TERMS_NS + "subject")
	public Collection<Resource> getTopics() {
		return topics;
	}

	/**
	 * @param topics
	 *            the topics to set
	 */
	public void setTopics(Collection<Resource> topics) {
		this.topics = topics;
	}

	/**
	 * 
	 * @param topic
	 */
	public void addTopic(Resource topic) {
		if (null != topic) {
			if (!getTopics().contains(topic))
				getTopics().add(topic);
		}
	}

	/**
	 * 
	 * @param topic
	 * @return
	 */
	public boolean hasTopic(Resource topic) {
		for (Resource currentTopic : topics) {
			if (currentTopic.equals(topic)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return short uri of an instance (part of the uri without namespace)
	 */
	public String getSimpleUri() {
		try {
			String value = "";
			String uri_s = uri.toString();
			Field[] fields = Class.forName("org.goodoldai.depths.config.Constants")
					.getDeclaredFields();
			for (Field field : fields) {
				if (!field.getName().contains("NS"))
					continue;
				value = field.get(value).toString();
				if (uri_s.contains(value)) {
					return uri_s.substring(value.length(), uri_s.length());
				}
			}

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error:"+e.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * @return the holder
	 */
	@RdfProperty(Constants.PARTICIPATION_NS + "holder")
	public Agent getHolder() {
		return holder;
	}

	/**
	 * @param holder
	 *            the holder to set
	 */
	public void setHolder(Agent holder) {
		if (null != holder) {
			this.holder = holder;
		} else {
			LOGGER.error("Error: holder can not be null");
			 
		}
	}

	/**
	 * @return the maker
	 */
	@RdfProperty(Constants.FOAF_NS + "maker")
	public Agent getMaker() {
		return maker;
	}

	/**
	 * @param maker
	 *            the maker to set
	 */
	public void setMaker(Agent maker) {
		if (null != maker) {
			this.maker = maker;
		} else {
			LOGGER.error("Error: maker can not be null");
			 
		}
	}

	/**
	 * @param relatedResources the relatedResources to set
	 */
	public void setRelatedResources(Collection<Resource> relatedResources) {
		this.relatedResources = relatedResources;
	}

	/**
	 * @return the relatedResources
	 */
	@RdfProperty(Constants.RDFS_NS + "seeAlso")
	public Collection<Resource> getRelatedResources() {
		return relatedResources;
	}
	
	public void addRelatedResource(Resource res) {
		if ( res != null && !getRelatedResources().contains(res) )
			getRelatedResources().add(res);
		else
			LOGGER.error("Did not succeed in adding the related resource: " + res);
	}

	public String toString() {
		return this.uri == null ? super.toString() : this.uri.toString();
	}
	@RdfProperty(Constants.LOCO_NS + "hasAnnotation")
	public Collection<Annotation> getHasAnnotation() {
		return hasAnnotation;
	}

	public void setHasAnnotation(Collection<Annotation> hasAnnotation) {
		this.hasAnnotation = hasAnnotation;
	}
	public void addHasAnnotation(Annotation ann) {
		if ( ann != null && !getHasAnnotation().contains(ann) )
			getHasAnnotation().add(ann);
		else
			LOGGER.error("Did not succeed in adding the annotation: " + ann);
	}
	@RdfProperty(Constants.DEPTHS_NS+"hasRelevance")
	public Collection<Relevance> getRelevances() {
		return relevances;
	}
	public void setRelevances(Collection<Relevance> relevances) {
		this.relevances = relevances;
	}
	public void addRelevance(Relevance relevance) {
		if (null != relevance) {
			if (!getRelevances().contains(relevance)) {
				getRelevances().add(relevance);
			}
		}
	}
}
