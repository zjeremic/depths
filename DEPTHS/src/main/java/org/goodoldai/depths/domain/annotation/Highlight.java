package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Highlight")
public class Highlight extends Annotation {

}
