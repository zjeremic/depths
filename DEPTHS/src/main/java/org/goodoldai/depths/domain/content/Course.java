package org.goodoldai.depths.domain.content;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Course")
public class Course extends ContentItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5692750198912724560L;
	private Collection<User> hasSubscriber;
	private Collection<DesignProblem> containsDesignProblem;
	
	public Course(){
		hasSubscriber=new ArrayList<User>();
		containsDesignProblem=new ArrayList<DesignProblem>();
	}
	@RdfProperty(Constants.DEPTHS_NS + "containsDesignProblem")
	public Collection<DesignProblem> getContainsDesignProblem() {
		return containsDesignProblem;
	}
	public void setContainsDesignProblem(
			Collection<DesignProblem> containsDesignProblem) {
		this.containsDesignProblem = containsDesignProblem;
	}
	public void addDesignProblem(DesignProblem dProblem) {
		if (null != dProblem) {
			if (!containsDesignProblem.contains(dProblem)) {
				containsDesignProblem.add(dProblem);
			}
		}
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasSubscriber")
	public Collection<User> getHasSubscriber() {
		return hasSubscriber;
	}

	public void setHasSubscriber(Collection<User> hasSubscriber) {
		this.hasSubscriber = hasSubscriber;
	}

	
	public void addUser(User user) {
		if (null != user) {
			if (!hasSubscriber.contains(user)) {
				hasSubscriber.add(user);
			}
		}
	}

}
