package org.goodoldai.depths.domain.resources;

import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Diagram;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("OnlineRepository")
public class OnlineRepository extends WebResource {

	protected Collection<WebResource> containsWebResource;

	@RdfProperty(Constants.DEPTHS_NS + "containsWebResource")
	public Collection<WebResource> getContainsWebResource() {
		return containsWebResource;
	}

	public void setContainsWebResource(Collection<WebResource> containsWebResource) {
		this.containsWebResource = containsWebResource;
	}
	public void addWebResource(WebResource wResource) {
		if (null != wResource) {
			if (!getContainsWebResource().contains(wResource)) {
				getContainsWebResource().add(wResource);
			}
		}
	}
}
