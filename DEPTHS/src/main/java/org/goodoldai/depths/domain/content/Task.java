package org.goodoldai.depths.domain.content;

import java.util.Date;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Task")
public class Task extends ContentItem {
	private Date startDate;
	private Date endDate;
	
	private ContentItem contentRef;
	@RdfProperty(Constants.LOCO_NS + "contentRef")
	public ContentItem getContentRef() {
		return contentRef;
	}
	public void setContentRef(ContentItem contentRef) {
		this.contentRef = contentRef;
	}
	@RdfProperty(Constants.DEPTHS_NS+"startDate")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@RdfProperty(Constants.DEPTHS_NS+"endDate")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
