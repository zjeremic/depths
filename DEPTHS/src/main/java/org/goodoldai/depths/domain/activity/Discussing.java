package org.goodoldai.depths.domain.activity;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Discussing")
public class Discussing extends Activity{

}
