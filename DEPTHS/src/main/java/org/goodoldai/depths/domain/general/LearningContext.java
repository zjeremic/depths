package org.goodoldai.depths.domain.general;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.activity.Activity;
import org.goodoldai.depths.domain.content.ContentItem;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("LearningContext")
public class LearningContext extends Resource {
/**
	 * 
	 */
	private static final long serialVersionUID = -619549656143257589L;
private Activity activityRef;
private User userRef;
private ContentItem contentRef;


@RdfProperty(Constants.LOCO_NS + "contentRef")
public ContentItem getContentRef() {
	return contentRef;
}
public void setContentRef(ContentItem contentRef) {
	this.contentRef = contentRef;
}
@RdfProperty(Constants.LOCO_NS + "userRef")
public User getUserRef() {
	return userRef;
}
public void setUserRef(User userRef) {
	this.userRef = userRef;
}
@RdfProperty(Constants.LOCO_NS + "activityRef")
public Activity getActivityRef() {
	return activityRef;
}
public void setActivityRef(Activity activityRef) {
	this.activityRef = activityRef;
}


}
