package org.goodoldai.depths.domain.activity;

import java.util.Collection;
import java.util.LinkedList;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.events.Event;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Activity")
public class Activity extends Resource{
	private  Event  event;
	
	public Activity(){
		 
	}
	@RdfProperty(Constants.LOCO_NS + "hasEvent")
	public  Event  getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event=event;
	}
	 

}
