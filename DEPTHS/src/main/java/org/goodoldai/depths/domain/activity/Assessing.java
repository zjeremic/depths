package org.goodoldai.depths.domain.activity;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Assessment;
import org.goodoldai.depths.domain.general.Scale;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Assessing")
public class Assessing extends Activity{
	private Date dateTimeSent;
	private Assessment inResponseTo;
	private User sentBy;
	private Submitting submittingRef;
	private Scale usingScale;
	
	@RdfProperty(Constants.LOCO_NS + "dateTimeSent")
	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	@RdfProperty(Constants.DEPTHS_NS + "inResponseTo")
	public Assessment getInResponseTo() {
		return inResponseTo;
	}
	public void setInResponseTo(Assessment inResponseTo) {
		this.inResponseTo = inResponseTo;
	}
	@RdfProperty(Constants.LOCO_NS + "sentBy")
	public User getSentBy() {
		return sentBy;
	}
	public void setSentBy(User sentBy) {
		this.sentBy = sentBy;
	}
	@RdfProperty(Constants.DEPTHS_NS + "submittingRef")
	public Submitting getSubmittingRef() {
		return submittingRef;
	}
	public void setSubmittingRef(Submitting submittingRef) {
		this.submittingRef = submittingRef;
	}
	@RdfProperty(Constants.LOCO_NS + "usingScale")
	public Scale getUsingScale() {
		return usingScale;
	}
	public void setUsingScale(Scale usingScale) {
		this.usingScale = usingScale;
	}



	
}
