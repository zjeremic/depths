package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.activity.Assessing;
import org.goodoldai.depths.domain.general.ScaleItem;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("UserRating")
public class UserRating extends Annotation {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3410545646801107565L;
	private Assessing isPartOf;
	private ScaleItem scaleItemRef;
	private Double ratingValue;

	@RdfProperty(Constants.LOCO_NS + "isPartOf")
	public Assessing getIsPartOf() {
		return isPartOf;
	}
	public void setIsPartOf(Assessing isPartOf) {
		 
		this.isPartOf = isPartOf;
	}
	@RdfProperty(Constants.LOCO_NS + "scaleItemRef")
	public ScaleItem getScaleItemRef() {
		return scaleItemRef;
	}
	public void setScaleItemRef(ScaleItem scaleItemRef) {
		this.scaleItemRef = scaleItemRef;
	}
	@RdfProperty(Constants.LOCO_NS + "ratingValue")
	public Double getRatingValue() {
		return ratingValue;
	}
	public void setRatingValue(Double ratingValue) {
		this.ratingValue = ratingValue;
	}
	

	

}
