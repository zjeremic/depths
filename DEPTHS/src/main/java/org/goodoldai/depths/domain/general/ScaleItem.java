package org.goodoldai.depths.domain.general;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("ScaleItem")
public class ScaleItem extends Resource {
	private String hasLabel;
	private double hasValue;
	@RdfProperty(Constants.LOCO_NS+"hasLabel")
	public String getHasLabel() {
		return hasLabel;
	}
	public void setHasLabel(String hasLabel) {
		this.hasLabel = hasLabel;
	}
	@RdfProperty(Constants.LOCO_NS+"hasValue")
	public double getHasValue() {
		return hasValue;
	}
	public void setHasValue(double hasValue) {
		this.hasValue = hasValue;
	}
	

}
