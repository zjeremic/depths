package org.goodoldai.depths.domain.foaf;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.OWL_NS)
@RdfType("Thing")
public class FoafThing extends Resource {

	private static final long serialVersionUID = -5279082386775784648L;
	private static final Logger LOGGER = Logger.getLogger(FoafThing.class);
	private String name;
	private URI homepage;
	private URI isPrimaryTopicOf;
	private URI seeAlso;

	public FoafThing() {
		super();
	}

	public FoafThing(String uri) {
		super(uri);
	}

	@RdfProperty(Constants.FOAF_NS + "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null)
			this.name = name;
	}

	@RdfProperty(Constants.FOAF_NS + "homepage")
	public URI getHomepage() {
		return homepage;
	}

	public void setHomepage(URI homepage) {
		if (homepage != null)
			this.homepage = homepage;
	}

	public void setHomepage(String homepage) {
		if (homepage != null)
			try {
				setHomepage(new URI(homepage));
			} catch (URISyntaxException e) {
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
	}

	@RdfProperty(Constants.FOAF_NS + "isPrimaryTopicOf")
	public URI getIsPrimaryTopicOf() {
		return isPrimaryTopicOf;
	}

	public void setIsPrimaryTopicOf(URI isPrimaryTopicOf) {
		if (isPrimaryTopicOf != null)
			this.isPrimaryTopicOf = isPrimaryTopicOf;
	}

	public void setIsPrimaryTopicOf(String isPrimaryTopicOf) {
		if (isPrimaryTopicOf != null)
			try {
				setIsPrimaryTopicOf(new URI(isPrimaryTopicOf));
			} catch (URISyntaxException e) {
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
	}

	@RdfProperty(Constants.RDFS_NS + "seeAlso")
	public URI getSeeAlso() {
		return seeAlso;
	}

	public void setSeeAlso(URI seeAlso) {
		if (seeAlso != null)
			this.seeAlso = seeAlso;
	}

	public void setSeeAlso(String seeAlso) {
		if (seeAlso != null)
			try {
				setSeeAlso(new URI(seeAlso));
			} catch (URISyntaxException e) {
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
	}

}
