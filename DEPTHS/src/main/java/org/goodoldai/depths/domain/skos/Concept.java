package org.goodoldai.depths.domain.skos;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.config.Constants;
import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.SKOS_NS)
@RdfType("Concept")
public class Concept extends Resource {

	private static final long serialVersionUID = -9182173469213942388L;
	private static final Logger LOGGER = Logger.getLogger(Concept.class);
	private Collection<Concept> narrowerConcepts;
	private Collection<Concept> broaderConcepts;
	
	/**
	 * the skos:ConceptScheme this skos:Concept belongs to
	 */
	private ConceptScheme conceptScheme;
	
	/**
	 * the skos:ConceptScheme in which this skos:Concept is a top level concept
	 */
	private ConceptScheme topConceptInScheme;
	
	public Concept() {
		narrowerConcepts = new LinkedList<Concept>();
		broaderConcepts = new LinkedList<Concept>();
	}

	/**
	 * @return the narrowerConcepts
	 */
	@RdfProperty(Constants.SKOS_NS + "narrower")
	public Collection<Concept> getNarrowerConcepts() {
		return narrowerConcepts;
	}

	/**
	 * @param narrowerConcepts the narrowerConcepts to set
	 */
	public void setNarrowerConcepts(Collection<Concept> narrowerConcepts) {
		this.narrowerConcepts = narrowerConcepts;
	}
	
	public void addNarrowerConcept(Concept concept) {
		if ( concept != null && !getNarrowerConcepts().contains(concept) )
			getNarrowerConcepts().add(concept);
		else
			LOGGER.error("Did not succeed in adding narrower concept: the concept is either null or already exists in the narrower concepts collection");
	}

	/**
	 * @return the broaderConcepts
	 */
	@RdfProperty(Constants.SKOS_NS + "broader")
	public Collection<Concept> getBroaderConcepts() {
		return broaderConcepts;
	}

	/**
	 * @param broaderConcepts the broaderConcepts to set
	 */
	public void setBroaderConcepts(Collection<Concept> broaderConcepts) {
		this.broaderConcepts = broaderConcepts;
	}
	
	public void addBroaderConcept(Concept concept) {
		if ( concept != null && !getBroaderConcepts().contains(concept) )
			getBroaderConcepts().add(concept);
		else
			LOGGER.error("Did not succeed in adding broader concept: the concept is either null or already exists in the broader concepts collection");
	}

	/**
	 * @param conceptScheme the conceptScheme to set
	 */
	public void setConceptScheme(ConceptScheme conceptScheme) {
		this.conceptScheme = conceptScheme;
	}

	/**
	 * @return the conceptScheme
	 */
	@RdfProperty(Constants.SKOS_NS + "inScheme")
	public ConceptScheme getConceptScheme() {
		return conceptScheme;
	}

	/**
	 * @param topConceptInScheme the topConceptInScheme to set
	 */
	public void setTopConceptInScheme(ConceptScheme topConceptInScheme) {
		this.topConceptInScheme = topConceptInScheme;
	}

	/**
	 * @return the topConceptInScheme
	 */
	@RdfProperty(Constants.SKOS_NS + "topConceptOf")
	public ConceptScheme getTopConceptInScheme() {
		return topConceptInScheme;
	}

}
