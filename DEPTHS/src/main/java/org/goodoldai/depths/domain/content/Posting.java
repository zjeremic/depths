package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.DiscussCategory;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Posting")
public class Posting extends Message {
	private Message inReplyTo;
	private DiscussCategory categoryRef;
	private Posting rootPosting;

	
	@RdfProperty(Constants.LOCO_NS + "inReplyTo")
	public Message getInReplyTo() {
		return inReplyTo;
	}
	public void setInReplyTo(Message inReplyTo) {
		this.inReplyTo = inReplyTo;
	}
	@RdfProperty(Constants.LOCO_NS + "categoryRef")
	public DiscussCategory getCategoryRef() {
		return categoryRef;
	}
	public void setCategoryRef(DiscussCategory categoryRef) {
		this.categoryRef = categoryRef;
	}
	@RdfProperty(Constants.LOCO_NS + "rootPosting")
	public Posting getRootPosting() {
		return rootPosting;
	}
	public void setRootPosting(Posting rootPosting) {
		this.rootPosting = rootPosting;
	}
	


}
