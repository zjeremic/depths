package org.goodoldai.depths.domain.skos;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.SKOS_NS)
@RdfType("ConceptScheme")
public class ConceptScheme extends Resource {
	
	private static final long serialVersionUID = -3067633981714949832L;
	private static final Logger LOGGER = Logger.getLogger(ConceptScheme.class);
	private Collection<Concept> topConcepts;
	
	public ConceptScheme() {
		setTopConcepts(new LinkedList<Concept>());
	}

	/**
	 * @param topConcepts the topConcepts to set
	 */
	public void setTopConcepts(Collection<Concept> topConcepts) {
		this.topConcepts = topConcepts;
	}

	/**
	 * @return the topConcepts
	 */
	@RdfProperty(Constants.SKOS_NS + "hasTopConcept")
	public Collection<Concept> getTopConcepts() {
		return topConcepts;
	}
	
	public void addTopConcept(Concept concept) {
		if ( concept != null && !getTopConcepts().contains(concept) )
			getTopConcepts().add(concept);
		else
			LOGGER.error("Did not succeed in adding top-level concept: the concept is either null or already exists in the top concepts collection");
	}
	
	

}
