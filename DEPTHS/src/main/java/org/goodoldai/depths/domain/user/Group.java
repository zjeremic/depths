package org.goodoldai.depths.domain.user;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.Course;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Group")
public class Group extends Resource{
	private String groupname="";
	private String description="";
	private Collection<User> members;
	private Collection<Course> subscribedToCourse;
public Group(){
	members=new ArrayList<User>();
	subscribedToCourse=new ArrayList<Course>();
}
	@RdfProperty(Constants.DEPTHS_NS + "subscribedToCourse")
	public Collection<Course> getSubscribedToCourse() {
		return subscribedToCourse;
	}

	public void setSubscribedToCourse(Collection<Course> subscribedToCourse) {
		this.subscribedToCourse = subscribedToCourse;
	}

	public void addCourse(Course course) {
		if (null != course) {
			if (!getSubscribedToCourse().contains(course)) {
				getSubscribedToCourse().add(course);
			}
		}
	}
	
	@RdfProperty(Constants.DEPTHS_NS + "groupname")
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	@RdfProperty(Constants.DEPTHS_NS + "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@RdfProperty(Constants.DEPTHS_NS + "member")
	public Collection<User> getMembers() {
		return members;
	}
	public void setMembers(Collection<User> members) {
		this.members = members;
	}
	public void addMember(User user) {
		if (null != user) {
			if (!getMembers().contains(user)) {
				getMembers().add(user);
			}
		}
	}
	public boolean containsMember(User user){
		if(getMembers().contains(user)){
			return true;
		}else return false;
	}
}
