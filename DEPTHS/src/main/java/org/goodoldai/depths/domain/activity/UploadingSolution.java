package org.goodoldai.depths.domain.activity;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.project.Project;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("UploadingSolution")
public class UploadingSolution extends Activity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1595544665620608497L;
	private Boolean submitted=Boolean.FALSE;
			//new Boolean(false);
	private Project projectRef;
	private User sentBy;
	private Date dateTimeSent;

	@RdfProperty(Constants.DEPTHS_NS + "submitted")
	public Boolean getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}
	@RdfProperty(Constants.DEPTHS_NS + "projectRef")
	public Project getProjectRef() {
		return projectRef;
	}
	public void setProjectRef(Project projectRef) {
		this.projectRef = projectRef;
	}
	@RdfProperty(Constants.LOCO_NS + "sentBy")
	public User getSentBy() {
		return sentBy;
	}
	public void setSentBy(User sentBy) {
		this.sentBy = sentBy;
	}
	@RdfProperty(Constants.LOCO_NS + "dateTimeSent")
	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}

}
