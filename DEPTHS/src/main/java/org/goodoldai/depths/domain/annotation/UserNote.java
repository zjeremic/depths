package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.activity.Assessing;
import org.goodoldai.depths.semanticstuff.util.StringUtils;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("UserNote")
public class UserNote extends Annotation {

	private Assessing isPartOf;
	private String hasContent;
	private String noteType;

	@RdfProperty(Constants.DEPTHS_NS + "hasNoteType")
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	@RdfProperty(Constants.LOCO_NS + "isPartOf")
	public Assessing getIsPartOf() {
		return isPartOf;
	}
	public void setIsPartOf(Assessing isPartOf) {
		this.isPartOf = isPartOf;
	}
	@RdfProperty(Constants.LOCO_NS + "hasContent")
	public String getHasContent() {
		if(hasContent!=null){
			return StringUtils.checkOutputContent(hasContent);
		}else return null;
	 
	}
	public void setHasContent(String content) {
		//content=StringUtils.checkInputContentSize(content, 430);
		//this.hasContent =StringUtils.checkInputContent(content);
		this.hasContent=content;
	}
}
