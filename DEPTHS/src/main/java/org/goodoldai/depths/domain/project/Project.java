package org.goodoldai.depths.domain.project;

 

import java.util.ArrayList;
import java.util.Collection;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;
import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.content.DesignProblem;
import org.goodoldai.depths.domain.content.Diagram;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;
 

@Namespace(Constants.DEPTHS_NS)
@RdfType("Project")
public class Project  extends Resource{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5369845256958123593L;
	protected String fileUrl="";
	protected String projectId="";


	protected User modifiedBy;
	protected User createdBy;
	protected Project parentProject;
	protected Solution hasSolution;
	protected Collection<Diagram> containsDiagrams;
	protected Collection<DesignProblem> hasDesignProblemRef;
	protected Collection<DesignProblem> isDescriptionOfDesignProblem;
	
	
	public Project(){
		containsDiagrams=new ArrayList<Diagram>();
		hasDesignProblemRef=new ArrayList<DesignProblem>();
		isDescriptionOfDesignProblem=new ArrayList<DesignProblem>();
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasDesignProblemRef")
	public Collection<DesignProblem> getHasDesignProblemRef() {
		return hasDesignProblemRef;
	}

	public void setHasDesignProblemRef(Collection<DesignProblem> hasDesignProblemRef) {
		this.hasDesignProblemRef = hasDesignProblemRef;
	}
	@RdfProperty(Constants.DEPTHS_NS + "isDescriptionOfDesignProblem")
	public Collection<DesignProblem> getIsDescriptionOfDesignProblem() {
		return isDescriptionOfDesignProblem;
	}
	public void setIsDescriptionOfDesignProblem(
			Collection<DesignProblem> isDescriptionOfDesignProblem) {
		this.isDescriptionOfDesignProblem = isDescriptionOfDesignProblem;
	}
	public void addIsDescriptionOfDesignProblem(DesignProblem dProblem) {
		if (null != dProblem) {
			if (!getIsDescriptionOfDesignProblem().contains(dProblem)) {
				getIsDescriptionOfDesignProblem().add(dProblem);
			}
		}
	}
	@RdfProperty(Constants.DEPTHS_NS + "projectid")
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public void addHasDesignProblemRef(DesignProblem dProblem) {
		if (null != dProblem) {
			if (!getHasDesignProblemRef().contains(dProblem)) {
				getHasDesignProblemRef().add(dProblem);
			}
		}
	}

	
	@RdfProperty(Constants.LOCO_NS + "fileUrl")
	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String url) {
		this.fileUrl = url;
	}
	
	@RdfProperty(Constants.DEPTHS_NS + "modifiedBy")
	public User getModifiedBy(){
		return this.modifiedBy;
	}
	public void setModifiedBy(User user){
		this.modifiedBy=user;
	}
	@RdfProperty(Constants.DEPTHS_NS + "createdBy")
	public User getCreatedBy(){
		return this.createdBy;
	}
	public void setCreatedBy(User user){
		this.createdBy=user;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasParentProject")
	public Project getParentProject(){
		return this.parentProject;
	}
	public void setParentProject(Project pProject){
		this.parentProject=pProject;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasSolution")
	public Solution getHasSolution(){
		return this.hasSolution;
	}
	public void setHasSolution(Solution cSolution){
		this.hasSolution=cSolution;
	}
	@RdfProperty(Constants.DEPTHS_NS + "containsDiagram")
	public Collection<Diagram> getContainsDiagrams() {
		return this.containsDiagrams;
	}

	public void setContainsDiagrams(Collection<Diagram> diagrams) {
		this.containsDiagrams = diagrams;
	}

	public void addDiagram(Diagram diagram) {
		if (null != diagram) {
			if (!getContainsDiagrams().contains(diagram)) {
				getContainsDiagrams().add(diagram);
			}
		}
	}


}
