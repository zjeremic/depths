package org.goodoldai.depths.domain.foaf;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.FOAF_NS)
@RdfType("Image")
public class Image extends FoafThing {

	private static final long serialVersionUID = 4342897875754208982L;

	public Image() {
		super();
	}

	public Image(String uri) {
		super(uri);
	}

}
