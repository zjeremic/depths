package org.goodoldai.depths.domain.resources;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Relevance;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.general.ScaleItem;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("WebResource")
public class WebResource extends Resource{
	protected String href;
	protected String title;
	
	
	public WebResource(){
		relevances=new ArrayList<Relevance>();
	}
	@RdfProperty(Constants.LOCO_NS + "href")
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	@RdfProperty(Constants.LOCO_NS + "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	 
	
	
	 

}
