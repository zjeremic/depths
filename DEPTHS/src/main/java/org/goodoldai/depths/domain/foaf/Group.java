package org.goodoldai.depths.domain.foaf;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.FOAF_NS)
@RdfType("Group")
public class Group extends FoafThing {

	private static final long serialVersionUID = -7585741783716396033L;

	public Group() {
		super();
	}

	public Group(String uri) {
		super(uri);
	}

}
