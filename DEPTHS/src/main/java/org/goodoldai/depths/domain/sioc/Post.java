package org.goodoldai.depths.domain.sioc;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.SIOC_NS)
@RdfType("Post")
public class Post extends Item {

	private static final long serialVersionUID = 4857297652417006369L;
	private static final Logger LOGGER = Logger.getLogger(Post.class);
	private Post replyOf;
	private URI primaryTopicOf;
	
	public Post() {
		super();
	}

	public Post(String uri) {
		super(uri);
	}

	@RdfProperty(Constants.SIOC_NS + "reply_of")
	public Post getReplyOf() {
		return replyOf;
	}

	public void setReplyOf(Post replyOf) {
		if(replyOf != null){
			replyOf.setUri(replyOf.getUri().toString().replaceFirst("Post", "ReplyPost"));
			this.replyOf = replyOf;
		}
	}

	@RdfProperty(Constants.FOAF_NS + "isPrimaryTopicOf")
	public URI getPrimaryTopicOf() {
		return primaryTopicOf;
	}

	public void setPrimaryTopicOf(URI primaryTopicOf) {
		this.primaryTopicOf = primaryTopicOf;
	}
	
	public void setPrimaryTopicOf(String primaryTopicOf) {
		if(primaryTopicOf != null)
			try {
				setPrimaryTopicOf(new URI(primaryTopicOf));
			} catch (URISyntaxException e) {
				LOGGER.error("Error:"+e.getLocalizedMessage());
			}
	}

}
