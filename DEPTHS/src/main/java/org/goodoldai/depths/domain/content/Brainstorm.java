package org.goodoldai.depths.domain.content;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Brainstorm")
public class Brainstorm extends Task {

}
