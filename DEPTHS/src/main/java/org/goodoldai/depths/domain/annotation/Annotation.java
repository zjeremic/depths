package org.goodoldai.depths.domain.annotation;

import java.util.Date;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;
import org.goodoldai.depths.domain.user.User;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Annotation")
public class Annotation extends Resource{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String format;
	private String description;
	private User madeBy;
	 

	@RdfProperty(Constants.DC_TERMS_NS + "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@RdfProperty(Constants.DC_TERMS_NS + "format")
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	@RdfProperty(Constants.LOCO_NS + "madeBy")
	public User getMadeBy() {
		return madeBy;
	}
	public void setMadeBy(User madeBy) {
		this.madeBy = madeBy;
	}
	 

}
