package org.goodoldai.depths.domain.annotation;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Metadata")
public class Metadata extends Annotation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 584105023811255699L;

}
