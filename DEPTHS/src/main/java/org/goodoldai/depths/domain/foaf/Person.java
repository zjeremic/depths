package org.goodoldai.depths.domain.foaf;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.FOAF_NS)
@RdfType("Person")
public class Person extends Agent {

	private static final long serialVersionUID = 1862465439415539L;
	private Image img;

	public Person() {
		super();
	}

	public Person(String uri) {
		super(uri);
	}

	@RdfProperty(Constants.FOAF_NS + "img")
	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		if (img != null)
			this.img = img;
	}

}
