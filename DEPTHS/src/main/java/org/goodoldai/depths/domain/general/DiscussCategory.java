package org.goodoldai.depths.domain.general;

import org.goodoldai.depths.config.Constants;

import thewebsemantic.Namespace;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("DiscussCategory")
public class DiscussCategory extends Resource{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5272550479594267439L;

}
