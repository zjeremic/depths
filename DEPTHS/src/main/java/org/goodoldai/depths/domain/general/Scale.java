package org.goodoldai.depths.domain.general;

import java.util.ArrayList;
import java.util.Collection;

import org.goodoldai.depths.config.Constants;


import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.LOCO_NS)
@RdfType("Scale")
public class Scale extends Resource {
	/**
	 * 
	 */
	private static final long serialVersionUID = 100894045620398672L;
	//private String description;
	private int maxValue;
	private int minValue;
	//private String title;
	private Collection<ScaleItem> hasScaleItem;
	
	public Scale(){
		hasScaleItem=new ArrayList<ScaleItem>();
	}
	/*
	@RdfProperty(Constants.DC_TERMS_NS+"description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	*/
	
	@RdfProperty(Constants.LOCO_NS+"maxValue")
	public int getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}
	@RdfProperty(Constants.LOCO_NS+"minValue")
	public int getMinValue() {
		return minValue;
	}
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	
	/*
	@RdfProperty(Constants.LOCO_NS+"title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	*/
	
	@RdfProperty(Constants.LOCO_NS+"hasScaleItem")
	public Collection<ScaleItem> getHasScaleItem() {
		return hasScaleItem;
	}
	
	public void setHasScaleItem(Collection<ScaleItem> hasScaleItem) {
		this.hasScaleItem = hasScaleItem;
	}
	public void addScaleItem(ScaleItem sItem) {
		if (null != sItem) {
			if (!getHasScaleItem().contains(sItem)) {
				getHasScaleItem().add(sItem);
			}
		}
	}
	
}
