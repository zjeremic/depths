package org.goodoldai.depths.domain.project;

import org.goodoldai.depths.config.Constants;
import org.goodoldai.depths.domain.general.Resource;

import thewebsemantic.Namespace;
import thewebsemantic.RdfProperty;
import thewebsemantic.RdfType;

@Namespace(Constants.DEPTHS_NS)
@RdfType("Solution")
public class Solution extends Resource{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2177906118469629180L;
	
	String description;
	String hasAdditionalRequirements;
	String hasCons;
	String hasConsequences;
	String hasDesignConstraints;
	String hasDesignRules;
	String hasPros;
	
	@RdfProperty(Constants.DEPTHS_NS + "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasAdditionalRequirements")
	public String getHasAdditionalRequirements() {
		return hasAdditionalRequirements;
	}
	public void setHasAdditionalRequirements(String hasAdditionalRequirements) {
		this.hasAdditionalRequirements = hasAdditionalRequirements;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasCons")
	public String getHasCons() {
		return hasCons;
	}
	public void setHasCons(String hasCons) {
		this.hasCons = hasCons;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasConsequences")
	public String getHasConsequences() {
		return hasConsequences;
	}
	public void setHasConsequences(String hasConsequences) {
		this.hasConsequences = hasConsequences;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasDesignConstraints")
	public String getHasDesignConstraints() {
		return hasDesignConstraints;
	}
	public void setHasDesignConstraints(String hasDesignConstraints) {
		this.hasDesignConstraints = hasDesignConstraints;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasDesignRules")
	public String getHasDesignRules() {
		return hasDesignRules;
	}
	public void setHasDesignRules(String hasDesignRules) {
		this.hasDesignRules = hasDesignRules;
	}
	@RdfProperty(Constants.DEPTHS_NS + "hasPros")
	public String getHasPros() {
		return hasPros;
	}
	public void setHasPros(String hasPros) {
		this.hasPros = hasPros;
	}
	

}
