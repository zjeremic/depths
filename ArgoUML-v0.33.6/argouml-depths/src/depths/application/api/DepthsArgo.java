// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.application.api;

import org.argouml.configuration.Configuration;
import org.argouml.configuration.ConfigurationKey;

public final class DepthsArgo {
	

	/**
	 * Key for user uri.
	 */
	public static final ConfigurationKey USER_URI = Configuration.makeKey(
			"depths_useruri", "useruri");
	
	public static final ConfigurationKey SSL = Configuration.makeKey(
			"depths_ssl", "ssl");
	
	/**
	 * Key for user email address.
	 */
	public static final ConfigurationKey KEY_USER_PASSWORD = Configuration
			.makeKey("depths_user", "password");

	/**
	 * Key for link WS.
	 */
	public static final ConfigurationKey KEY_WS = Configuration.makeKey(
			"depths_ws", "link");

	/**
	 * Key remember.
	 */
	public static final ConfigurationKey KEY_USER_REMEMBER = Configuration
			.makeKey("depths_remember", "remember");

	/**
	 * Key forftp port.
	 */
	public static final ConfigurationKey KEY_FTP_PORT = Configuration.makeKey(
			"depths_ftp_port", "link");
	/**
	 * Key for projects folder.
	 */

	public static final ConfigurationKey KEY_FTP_PROJECTS_FOLDER = Configuration
			.makeKey("depths_ftp_projects", "link");
	/**
	 * Key for images folder.
	 */
	public static final ConfigurationKey KEY_FTP_IMAGES_FOLDER = Configuration
			.makeKey("depths_ftp_images", "link");

	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_USER_USERNAME = Configuration
			.makeKey("depths_user", "username");
	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_USER_ID = Configuration.makeKey(
			"depths_user", "ID");
	/**
	 * Key for user email address.
	 */
	public static final ConfigurationKey KEY_FTP_HOST = Configuration.makeKey(
			"depths_moodle", "ftp_host");

	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_REPOSITORY_USERNAME = Configuration
			.makeKey("depths_moodle", "repository_username");
	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_REPOSITORY_PASSWORD = Configuration
			.makeKey("depths_moodle", "repository_password");
	
	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_MOODLE_HOSTNAME = Configuration
			.makeKey("depths_moodle", "hostname");
	/**
	 * Key for user full name.
	 */
	public static final ConfigurationKey KEY_MOODLE_REPOSITORY = Configuration
			.makeKey("depths_moodle", "repository");

	/**
	 * Name of the TagDefinition for tagged values containing "description"
	 * reference.
	 */
	public static final String DESCRIPTION_TAG = "description";
	/**
	 * Name of the TagDefinition for tagged values containing "design rules"
	 * reference.
	 */
	public static final String DESIGN_RULES_TAG = "design rules";
	/**
	 * Name of the TagDefinition for tagged values containing
	 * "design constraints" reference.
	 */
	public static final String DESIGN_CONSTRAINTS_TAG = "design constraints";
	/**
	 * Name of the TagDefinition for tagged values containing
	 * "additional requirements" reference.
	 */
	public static final String ADDITIONAL_REQUIREMENTS_TAG = "additional requirements";

	/**
	 * Name of the TagDefinition for tagged values containing "consequences"
	 * reference.
	 */
	public static final String CONSEQUENCES_TAG = "consequences";

	/**
	 * Name of the TagDefinition for tagged values containing "pros" reference.
	 */
	public static final String PROS_TAG = "pros";

	/**
	 * Name of the TagDefinition for tagged values containing "cons" reference.
	 */
	public static final String CONS_TAG = "cons";
	/**
	 * Name of the TagDefinition for tagged values containing "dsc_problem"
	 * reference.
	 */
	public static final String DSC_PROBLEM_TAG = "dsc_problem";
	/**
	 * Name of the TagDefinition for tagged values containing "Problem title"
	 * reference.
	 */
	public static final String DSC_PROBLEM_TITLE_TAG = "dsc_problem_title";
	/**
	 * Name of the TagDefinition for tagged values containing "author name"
	 * reference.
	 */
	public static final String DSC_PROBLEM_AUTHOR_NAME_TAG = "dsc_problem_author_name";
	/**
	 * Name of the TagDefinition for tagged values containing "related links"
	 * reference.
	 */
	public static final String DSC_PROBLEM_RELETED_LINKS_TAG = "dsc_problem_releted_links";
}
