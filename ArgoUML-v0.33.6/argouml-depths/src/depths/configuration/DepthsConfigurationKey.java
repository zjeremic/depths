// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.configuration;

import java.beans.PropertyChangeEvent;

public interface DepthsConfigurationKey {
	/**
	 * Return the actual key used to access the configuration.
	 * 
	 * @return the key.
	 */
	String getKey();

	/**
	 * Tells if this configuration key is the one changed in the
	 * <code>PropertyChangeEvent</code>.
	 * 
	 * @param pce
	 *            PropertyChangeEvent to check
	 * @return true if the changed property is for the key.
	 */
	boolean isChangedProperty(PropertyChangeEvent pce);
}
