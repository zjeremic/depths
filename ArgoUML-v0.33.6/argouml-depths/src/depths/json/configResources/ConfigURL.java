// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.configResources;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;

public class ConfigURL {

	public  static String home = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
	public  static String getconfiguration = home
			+ "/depths/rest/configure/getconfiguration/";
	public  static String checkuser = home + "/depths/rest/configure/checkuser";
	public static String getproject = home + "/depths/rest/solution/getproject";
	public static String saveProject = home + "/depths/rest/solution/savesolution";
	public static String saveDiagrams = home + "/depths/rest/solution/savediagrams";
	public static String listallprojects = home
			+ "/depths/rest/projects/listavailableprojects";
	public static String listbyusername = home
			+ "/depths/rest/projects/listbyusername";
	public static String getusercourses = home + "/depths/rest/content/getusercourses";
	public static String getcourseproblems = home
			+ "/depths/rest/content/getcourseproblems";
	public static String listprojectsforproblem = home
			+ "/depths/rest/projects/listprojectsforproblem";
	public static String getProblemDescription = "/depths/rest/content/getproblemdescription";
	
	
	public static  void update(String h) {

		ConfigURL.home = h;
		ConfigURL.getconfiguration = h
				+ "/depths/rest/configure/getconfiguration/";
		ConfigURL.checkuser = h + "/depths/rest/configure/checkuser";
		ConfigURL.getproject = h + "/depths/rest/solution/getproject";
		ConfigURL.saveProject = h + "/depths/rest/solution/savesolution";
		ConfigURL.saveDiagrams = h + "/depths/rest/solution/savediagrams";
		ConfigURL.listallprojects = h
				+ "/depths/rest/projects/listallprojects";
		ConfigURL.listbyusername = h
				+ "/depths/rest/projects/listbyusername";
		ConfigURL.getusercourses = h + "/depths/rest/content/getusercourses";
		ConfigURL.getcourseproblems = h
				+ "/depths/rest/content/getcourseproblems";
		ConfigURL.listprojectsforproblem = h
				+ "/depths/rest/projects/listprojectsforproblem";
		ConfigURL.getProblemDescription = "/depths/rest/content/getproblemdescription";
		
	}
	
}
