// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.json.project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.argouml.kernel.ProjectManager;
import org.argouml.model.Model;

import org.argouml.sequence2.diagram.UMLSequenceDiagram;
import org.argouml.ui.DetailsPane;
import org.argouml.ui.ProjectBrowser;
import org.argouml.uml.diagram.ArgoDiagram;
import org.argouml.uml.diagram.activity.ui.UMLActivityDiagram;
import org.argouml.uml.diagram.collaboration.ui.UMLCollaborationDiagram;
import org.argouml.uml.diagram.deployment.ui.UMLDeploymentDiagram;
import org.argouml.uml.diagram.state.ui.UMLStateDiagram;
import org.argouml.uml.diagram.static_structure.ui.UMLClassDiagram;
import org.argouml.uml.diagram.use_case.ui.UMLUseCaseDiagram;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.service.DepthsJSONUser;

import depths.uml.diagram.DepthsUMLActivityDiagram;
import depths.uml.diagram.DepthsUMLClassDiagram;
import depths.uml.diagram.DepthsUMLCollaborationDiagram;
import depths.uml.diagram.DepthsUMLDeploymentDiagram;
import depths.uml.diagram.DepthsUMLDiagram;
import depths.uml.diagram.DepthsUMLSequenceDiagram;
import depths.uml.diagram.DepthsUMLStatechartDiagram;
import depths.uml.diagram.DepthsUMLUseCaseDiagram;
import depths.uml.ui.tab.description.DescriptionTab;
import depths.uml.ui.tab.solution.SolutionTab;

public class Project {
	private static final Logger LOG = Logger.getLogger(Project.class);
	private String projectId = null;
	private String projecturi = null;
	private String projectName = null;
	private String fileurl = null;
	private String datetimecreated = null;
	private String createdBy = null;
	private String creatorname = null;
	private String modifiedByName = null;
	private String parentProject = null;
	private String designProblem = null;
	private String solutionuri = null;
	private String description = null;
	private String hasAdditionalRequirements = null;
	private String hasCons = null;
	private String hasConsequences = null;
	private String hasDesignConstraints = null;
	private String hasDesignRules = null;
	private String hasPros = null;

	private String dscProblem = null;
	private String dscProblemTitle = null;
	private String dscProblemAuthorName = null;
	// key is url, value is title url
	private HashMap<String, String> dscProblemReletedLinks = new HashMap<String, String>();

	private String modifiedBy = null;
	private ArrayList<DepthsUMLClassDiagram> classDiagrams = new ArrayList<DepthsUMLClassDiagram>();
	private ArrayList<DepthsUMLUseCaseDiagram> useCaseDiagrams = new ArrayList<DepthsUMLUseCaseDiagram>();
	private ArrayList<DepthsUMLActivityDiagram> activityDiagrams = new ArrayList<DepthsUMLActivityDiagram>();
	private ArrayList<DepthsUMLCollaborationDiagram> collaborationDiagrams = new ArrayList<DepthsUMLCollaborationDiagram>();
	private ArrayList<DepthsUMLDeploymentDiagram> deploymentDiagrams = new ArrayList<DepthsUMLDeploymentDiagram>();
	private ArrayList<DepthsUMLSequenceDiagram> sequenceDiagrams = new ArrayList<DepthsUMLSequenceDiagram>();
	private ArrayList<DepthsUMLStatechartDiagram> statechartDiagrams = new ArrayList<DepthsUMLStatechartDiagram>();

	private ArrayList<DepthsUMLDiagram> oldDiagrams = new ArrayList<DepthsUMLDiagram>();
	private ArrayList<DepthsUMLDiagram> deletedDiagrams = new ArrayList<DepthsUMLDiagram>();

	public Project() {
		setProjectId(createUniqueId());
		setFileurl("Project_" + getProjectId() + ".zargo");
	}

	public ArrayList<DepthsUMLDiagram> getDeletedDiagrams() {
		return deletedDiagrams;
	}

	public void setDeletedDiagrams(ArrayList<DepthsUMLDiagram> deletedDiagrams) {
		this.deletedDiagrams = deletedDiagrams;
	}

	public ArrayList<DepthsUMLDiagram> getOldDiagrams() {
		return oldDiagrams;
	}

	public void setOldDiagrams(ArrayList<DepthsUMLDiagram> oldDiagrams) {
		this.oldDiagrams = oldDiagrams;
	}

	public String getDesignProblem() {
		return designProblem;
	}

	public void setDesignProblem(String designProblem) {
		this.designProblem = designProblem;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public String getLastModifedBy() {
		return DepthsJSONUser.getUser().getUri();
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public static String createUniqueId() {
		String filename = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		String dateString = dateFormat.format(date);
		String moodle_username = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_USERNAME);
		filename = moodle_username + "__" + dateString;
		return filename;
	}

	public String getUMLDiagramUniqueId(String type) {
		String id = null;
		String projectId = ProjectUtility.getCurrentProject().getProjectId();
		int number = getNumberOfDiagrams(type) + 1;
		id = type + "_" + number + "_" + projectId;
		return id;
	}

	public boolean isOldDiagram(String title) {
		if (ProjectUtility.getLastOpenProject() != null) {

			ArrayList<DepthsUMLDiagram> old = ProjectUtility
					.getLastOpenProject().getOldDiagrams();
			for (DepthsUMLDiagram tmp : old) {
				if (tmp.getDiagramName().equalsIgnoreCase(title)) {
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public String getOldDiagramUri(String title) {
		if (ProjectUtility.getLastOpenProject() != null) {
			ArrayList<DepthsUMLDiagram> old = ProjectUtility
					.getLastOpenProject().getOldDiagrams();
			for (DepthsUMLDiagram tmp : old) {
				if (tmp.getDiagramName().equalsIgnoreCase(title)) {
					return tmp.getDiagramUri();
				}
			}

			return null;
		}
		return null;
	}

	public boolean deleteOldDiagrams(String title) {
		if (ProjectUtility.getLastOpenProject() != null) {
			ArrayList<DepthsUMLDiagram> old = ProjectUtility
					.getLastOpenProject().getOldDiagrams();
			for (int i = 0; i < old.size(); i++) {
				if (old.get(i).getDiagramName().equalsIgnoreCase(title)) {
					ProjectUtility.getLastOpenProject().getOldDiagrams()
							.remove(i);
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public void markedDeletedDiagrams() {
		for (int i = 0; i < deletedDiagrams.size(); i++) {
			deletedDiagrams.get(i).setMode("delete");
		}
	}

	public void setProjectValues() {
		LOG.debug("METHOD SET VALUES");
		Collection root = Model.getFacade().getRootElements();
		String projectN = null;
		if (!root.isEmpty()) {
			Object child = null;
			for (Object t : root) {
				child = t;
				projectN = Model.getFacade().getName(child);
			}
			setProjectName(projectN);
			SolutionTab solutionTab = (SolutionTab) ((DetailsPane) ProjectBrowser
					.getInstance().getDetailsPane()).getTab(SolutionTab.class);

			this.setDescription(solutionTab.getDescription());
			this.setHasDesignRules(solutionTab.getDesignRules());
			this.setHasAdditionalRequirements(solutionTab
					.getAdditionalRequirements());
			this.setHasCons(solutionTab.getCons());
			this.setHasConsequences(solutionTab.getConsequences());
			this.setHasPros(solutionTab.getPros());
			this.setHasDesignConstraints(solutionTab.getDesignConstraints());

			DescriptionTab descriptionProblemTab = (DescriptionTab) ((DetailsPane) ProjectBrowser
					.getInstance().getDetailsPane())
					.getTab(DescriptionTab.class);
			this.setDscProblem(descriptionProblemTab.getDscProblemDescription());
			this.setDscProblemAuthorName(descriptionProblemTab
					.getDscProblemAuthorName());
//			this.setDscProblemReletedLinks(descriptionProblemTab
//					.getDscProblemReletedLinks());
			this.setDscProblemTitle(descriptionProblemTab.getDscProblemTitle());

			Project old = ProjectUtility.getLastOpenProject();

			String user = DepthsConfiguration.getString(DepthsArgo.USER_URI);
			if (old != null) {
				// ovde provjera null za old parametre
				this.setParentProject(old.getProjecturi());
				this.setCreatedBy(old.getCreatedBy());
				this.setSolutionuri(old.getSolutionuri());
				this.setProjecturi(old.getProjecturi());
				this.setCreatedBy(old.getCreatedBy());
				this.setDesignProblem(old.getDesignProblem());
				this.setModifiedBy(user);
			} else {
				this.setCreatedBy(user);
				this.setModifiedBy(null);
			}

			if (ProjectUtility.getSelectedDesignProblemURI() != null) {
				this.setDesignProblem(ProjectUtility
						.getSelectedDesignProblemURI());
			}

			
			this.setDatetimecreated(new Date().toString());

			if (Model.getFacade().isAModel(child)) {

				List<ArgoDiagram> children = null;
				children = ProjectManager.getManager().getCurrentProject()
						.getDiagramList();
				setUMLDiagramsValues(children);
			}
			if (ProjectUtility.getLastOpenProject() != null) {
				deletedDiagrams = ProjectUtility.getLastOpenProject()
						.getOldDiagrams();
				markedDeletedDiagrams();
			}
			ProjectUtility.setCurrentProject(this);
		}

	}

	public void setUMLDiagramsValues(List<ArgoDiagram> children) {
		if (!children.isEmpty()) {
			for (ArgoDiagram obj : children) {
				if (obj instanceof UMLClassDiagram) {
					UMLClassDiagram diagram = (UMLClassDiagram) obj;
					String name = diagram.getName();
					DepthsUMLClassDiagram myDiagram = new DepthsUMLClassDiagram();
					String id = this.getUMLDiagramUniqueId("ClassDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("ClassDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}

					addClassDiagram(myDiagram);
				}

				if (obj instanceof UMLUseCaseDiagram) {

					UMLUseCaseDiagram diagram = (UMLUseCaseDiagram) obj;
					String name = diagram.getName();
					DepthsUMLUseCaseDiagram myDiagram = new DepthsUMLUseCaseDiagram();
					String id = this.getUMLDiagramUniqueId("UseCaseDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("UseCaseDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addUseCaseDiagram(myDiagram);
				}

				if (obj instanceof UMLActivityDiagram) {

					UMLActivityDiagram diagram = (UMLActivityDiagram) obj;
					String name = diagram.getName();

					DepthsUMLActivityDiagram myDiagram = new DepthsUMLActivityDiagram();
					String id = this.getUMLDiagramUniqueId("ActivityDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("ActivityDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addActivityDiagram(myDiagram);
				}

				if (obj instanceof UMLCollaborationDiagram) {

					UMLCollaborationDiagram diagram = (UMLCollaborationDiagram) obj;
					String name = diagram.getName();

					DepthsUMLCollaborationDiagram myDiagram = new DepthsUMLCollaborationDiagram();
					String id = this
							.getUMLDiagramUniqueId("CollaborationDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("CollaborationDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addCollaborationDiagram(myDiagram);
				}

				if (obj instanceof UMLDeploymentDiagram) {

					UMLDeploymentDiagram diagram = (UMLDeploymentDiagram) obj;
					String name = diagram.getName();

					DepthsUMLDeploymentDiagram myDiagram = new DepthsUMLDeploymentDiagram();
					String id = this.getUMLDiagramUniqueId("DeploymentDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("DeploymentDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addDeploymentDiagram(myDiagram);
				}

				if (obj instanceof UMLSequenceDiagram) {

					UMLSequenceDiagram diagram = (UMLSequenceDiagram) obj;
					String name = diagram.getName();

					DepthsUMLSequenceDiagram myDiagram = new DepthsUMLSequenceDiagram();
					String id = this.getUMLDiagramUniqueId("SequenceDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("SequenceDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addSequenceDiagram(myDiagram);
				}

				if (obj instanceof UMLStateDiagram) {

					UMLStateDiagram diagram = (UMLStateDiagram) obj;
					String name = diagram.getName();

					DepthsUMLStatechartDiagram myDiagram = new DepthsUMLStatechartDiagram();
					String id = this.getUMLDiagramUniqueId("StatechartDiagram");
					myDiagram.setDiagramId(id);
					myDiagram.setDiagramName(name);
					myDiagram.setFileUrl(id + ".png");
					myDiagram.setUMLDiagram(diagram);
					myDiagram.setDiagramType("StatechartDiagram");
					oldDiagrams.add(myDiagram);
					if (isOldDiagram(myDiagram.getDiagramName())) {
						myDiagram.setDiagramUri(getOldDiagramUri(myDiagram
								.getDiagramName()));
						myDiagram.setMode("edit");
						deleteOldDiagrams(myDiagram.getDiagramName());
					} else {
						myDiagram.setMode("create");
					}
					addStatechartDiagram(myDiagram);
				}
			}
		}

	}

	public int getSumOfDiagrams() {
		int size = 0;
		size = classDiagrams.size() + useCaseDiagrams.size()
				+ +activityDiagrams.size() + collaborationDiagrams.size()
				+ deploymentDiagrams.size() + sequenceDiagrams.size()
				+ statechartDiagrams.size();
		return size;

	}

	public int getNumberOfDiagrams(String type) {
		int size = 0;
		if (type.equals("ClassDiagram")) {
			size = classDiagrams.size();
		} else if (type.equals("UseCaseDiagram")) {
			size = useCaseDiagrams.size();
		} else if (type.equals("ActivityDiagram")) {
			size = activityDiagrams.size();
		} else if (type.equals("CollaborationDiagram")) {
			size = collaborationDiagrams.size();
		} else if (type.equals("DeploymentDiagram")) {
			size = deploymentDiagrams.size();
		} else if (type.equals("SequenceDiagram")) {
			size = sequenceDiagrams.size();
		} else if (type.equals("StatechartDiagram")) {
			size = statechartDiagrams.size();
		}
		return size;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {

		this.projectId = projectId;
	}

	public ArrayList<DepthsUMLStatechartDiagram> getStatechartDiagrams() {
		return statechartDiagrams;
	}

	public void setStatechartDiagrams(
			ArrayList<DepthsUMLStatechartDiagram> statechartDiagrams) {
		this.statechartDiagrams = statechartDiagrams;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {

		this.projectName = projectName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProjecturi() {
		return projecturi;
	}

	public void setProjecturi(String projecturi) {
		this.projecturi = projecturi;
	}

	public String getFileurl() {
		return fileurl;
	}

	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}

	public String getDatetimecreated() {
		return datetimecreated;
	}

	public void setDatetimecreated(String datetimecreated) {
		this.datetimecreated = datetimecreated;
	}

	public String getCreatorname() {
		return creatorname;
	}

	public void setCreatorname(String creatorname) {
		this.creatorname = creatorname;
	}

	public String getParentProject() {
		return parentProject;
	}

	public void setParentProject(String parentProject) {
		this.parentProject = parentProject;
	}

	public String getSolutionuri() {
		return solutionuri;
	}

	public void setSolutionuri(String solutionuri) {
		this.solutionuri = solutionuri;
	}

	public String getHasAdditionalRequirements() {
		return hasAdditionalRequirements;
	}

	public void setHasAdditionalRequirements(String hasAdditionalRequirements) {
		this.hasAdditionalRequirements = hasAdditionalRequirements;
	}

	public String getHasCons() {
		return hasCons;
	}

	public void setHasCons(String hasCons) {
		this.hasCons = hasCons;
	}

	public String getHasConsequences() {
		return hasConsequences;
	}

	public void setHasConsequences(String hasConsequences) {
		this.hasConsequences = hasConsequences;
	}

	public String getHasDesignConstraints() {
		return hasDesignConstraints;
	}

	public void setHasDesignConstraints(String hasDesignConstraints) {
		this.hasDesignConstraints = hasDesignConstraints;
	}

	public String getHasDesignRules() {
		return hasDesignRules;
	}

	public void setHasDesignRules(String hasDesignRules) {
		this.hasDesignRules = hasDesignRules;
	}

	public String getHasPros() {
		return hasPros;
	}

	public void setHasPros(String hasPros) {
		this.hasPros = hasPros;
	}

	public ArrayList<DepthsUMLClassDiagram> getClassDiagrams() {
		return classDiagrams;
	}

	public void setClassDiagrams(ArrayList<DepthsUMLClassDiagram> classDiagrams) {
		this.classDiagrams = classDiagrams;
	}

	public ArrayList<DepthsUMLUseCaseDiagram> getUseCaseDiagrams() {
		return useCaseDiagrams;
	}

	public void setUseCaseDiagrams(
			ArrayList<DepthsUMLUseCaseDiagram> useCaseDiagrams) {
		this.useCaseDiagrams = useCaseDiagrams;
	}

	public ArrayList<DepthsUMLActivityDiagram> getActivityDiagrams() {
		return activityDiagrams;
	}

	public void setActivityDiagrams(
			ArrayList<DepthsUMLActivityDiagram> activityDiagrams) {
		this.activityDiagrams = activityDiagrams;
	}

	public ArrayList<DepthsUMLCollaborationDiagram> getCollaborationDiagrams() {
		return collaborationDiagrams;
	}

	public void setCollaborationDiagrams(
			ArrayList<DepthsUMLCollaborationDiagram> collaborationDiagrams) {
		this.collaborationDiagrams = collaborationDiagrams;
	}

	public ArrayList<DepthsUMLDeploymentDiagram> getDeploymentDiagrams() {
		return deploymentDiagrams;
	}

	public void setDeploymentDiagrams(
			ArrayList<DepthsUMLDeploymentDiagram> deploymentDiagrams) {
		this.deploymentDiagrams = deploymentDiagrams;
	}

	public ArrayList<DepthsUMLSequenceDiagram> getSequenceDiagrams() {
		return sequenceDiagrams;
	}

	public void setSequenceDiagrams(
			ArrayList<DepthsUMLSequenceDiagram> sequenceDiagrams) {
		this.sequenceDiagrams = sequenceDiagrams;
	}

	public void addClassDiagram(DepthsUMLClassDiagram classDiagram) {
		classDiagrams.add(classDiagram);

	}

	public void addUseCaseDiagram(DepthsUMLUseCaseDiagram useCaseDiagram) {
		useCaseDiagrams.add(useCaseDiagram);
	}

	public void addActivityDiagram(DepthsUMLActivityDiagram activityDiagram) {
		activityDiagrams.add(activityDiagram);
	}

	public void addCollaborationDiagram(
			DepthsUMLCollaborationDiagram collaborationDiagram) {
		collaborationDiagrams.add(collaborationDiagram);
	}

	public void addDeploymentDiagram(
			DepthsUMLDeploymentDiagram deploymentDiagram) {
		deploymentDiagrams.add(deploymentDiagram);
	}

	public void addSequenceDiagram(DepthsUMLSequenceDiagram sequenceDiagram) {
		sequenceDiagrams.add(sequenceDiagram);
	}

	public void addStatechartDiagram(DepthsUMLStatechartDiagram stateDiagram) {
		statechartDiagrams.add(stateDiagram);
	}

	public String getCreatedBy() {
		// if (createdBy == null) {
		// createdBy = DepthsJSONUser.getUser().getUri();
		// }
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDscProblem() {
		return dscProblem;
	}

	public void setDscProblem(String dscProblem) {
		this.dscProblem = dscProblem;
	}

	public String getDscProblemTitle() {
		return dscProblemTitle;
	}

	public void setDscProblemTitle(String dscProblemTitle) {
		this.dscProblemTitle = dscProblemTitle;
	}

	public String getDscProblemAuthorName() {
		return dscProblemAuthorName;
	}

	public void setDscProblemAuthorName(String dscProblemAuthorName) {
		this.dscProblemAuthorName = dscProblemAuthorName;
	}

	public HashMap<String, String> getDscProblemReletedLinks() {
		return dscProblemReletedLinks;
	}

	public void setDscProblemReletedLinks(
			HashMap<String, String> dscProblemReletedLinks) {
		this.dscProblemReletedLinks = dscProblemReletedLinks;
	}

	public String getModifiedByName() {
		return modifiedByName;
	}

	public void setModifiedByName(String modifiedByName) {
		this.modifiedByName = modifiedByName;
	}

	@Override
	public String toString() {
		return projectName;
	}

}
