package depths.json.service;

import org.argouml.ui.DetailsPane;
import org.argouml.ui.ProjectBrowser;

import depths.json.designProblem.DesignProblem;
import depths.json.project.Project;
import depths.uml.ui.tab.description.DescriptionTab;
import depths.uml.ui.tab.solution.SolutionTab;

public class DepthsJSONTabService {

	public static boolean setValueSolution(Project p) {
		if (p == null) {
			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class))
					.setDescription("");
			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class))
					.setDesignRules("");

			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class))
					.setDesignConstraints("");
			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class))
					.setAdditionalRequirements("");

			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class))
					.setConsequences("");

			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class)).setPros("");

			((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(SolutionTab.class)).setCons("");

			return false;
		} else {
			if (p.getDescription() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class))
						.setDescription(p.getDescription());
			}
			if (p.getHasDesignRules() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class))
						.setDesignRules(p.getHasDesignRules());
			}

			if (p.getHasDesignConstraints() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class))
						.setDesignConstraints(p.getHasDesignConstraints());
			}

			if (p.getHasAdditionalRequirements() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class))
						.setAdditionalRequirements(p
								.getHasAdditionalRequirements());
			}

			if (p.getHasConsequences() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class))
						.setConsequences(p.getHasConsequences());
			}

			if (p.getHasPros() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class)).setPros(p
						.getHasPros());
			}
			if (p.getHasCons() != null) {
				((SolutionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(SolutionTab.class)).setCons(p
						.getHasCons());
			}

			return true;
		}

	}

	public static boolean setValueDescription(DesignProblem p) {
		if (p == null) {
			((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(DescriptionTab.class))
					.setDscProblemAuthorName("");
			((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(DescriptionTab.class))
					.setDscProblemDescription("");
			((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(DescriptionTab.class))
					.setDscProblemReletedLinks("");
			((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
					.getDetailsPane()).getTab(DescriptionTab.class))
					.setDscProblemTitle("");
			return false;
		} else {
			if (p.getAuthor() != null) {
				((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(DescriptionTab.class))
						.setDscProblemAuthorName(p.getAuthor());
			}
			if (p.getDescription() != null) {
				((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(DescriptionTab.class))
						.setDscProblemDescription(p.getDescription());
			}

			if (p.getRelatedLinkHref() != null) {
				if (p.getRelatedLinkTitle() != null) {

					((DescriptionTab) ((DetailsPane) ProjectBrowser
							.getInstance().getDetailsPane())
							.getTab(DescriptionTab.class)).setReletedLinksHtml(
							p.getRelatedLinkTitle(), p.getRelatedLinkHref());

				}
			}

			if (p.getTitle() != null) {
				((DescriptionTab) ((DetailsPane) ProjectBrowser.getInstance()
						.getDetailsPane()).getTab(DescriptionTab.class))
						.setDscProblemTitle(p.getTitle());
			}

			return true;
		}

	}
}
