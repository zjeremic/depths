// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import depths.json.configResources.ConfigURL;
import depths.json.ftp.FtpParameter;

public class DepthsJSONFTP {

	private static final Logger LOG = Logger.getLogger(DepthsJSONFTP.class);

	public static FtpParameter readFtpParameter() {
		String wsURL = ConfigURL.getconfiguration;

		if (ValidURL.valide(wsURL)) {
			try {
				LOG.debug("read readFtpParameter.");
				Client c = Client.create();
				WebResource r = c.resource(wsURL);
				String jsonRes = r.accept(MediaType.APPLICATION_JSON).get(
						String.class);
				System.out.println(jsonRes);
				
				JSONArray json = new JSONArray(jsonRes);
				JSONArray ftpRepositoryObject = json.getJSONObject(0)
						.getJSONArray("ftp-repository");
				JSONObject parameterFTP = ftpRepositoryObject.getJSONObject(0);
				FtpParameter ftp = new FtpParameter();
				ftp.setFtpUsername(parameterFTP.getString("ftp-username"));
				ftp.setFtpPassword(parameterFTP.getString("ftp-password"));
				ftp.setFtpUrl(parameterFTP.getString("ftp-url"));
				ftp.setFtpPort(parameterFTP.getString("ftp-port"));
				ftp.setSecurityssl(parameterFTP.getString("security-ssl"));
				ftp.setFtpLocationProjects(parameterFTP
						.getString("ftp-location-projects"));
				ftp.setFtpLocationImages(parameterFTP
						.getString("ftp-location-images"));
				return ftp;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

			return null;
		}
		return null;

	}

}
