// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ValidURL {

	public static boolean valide(String url) {
		if (url.startsWith("http://")) {
				return true;
		} else {
				return false;
		}
		 
	}

	public static boolean existWebService(String URLName) {
		URL url;
		InputStream is = null;
		DataInputStream dis;
		String line;

		try {
			url = new URL(URLName);
			is = url.openStream(); // throws an IOException
			dis = new DataInputStream(new BufferedInputStream(is));
			while ((line = dis.readLine()) != null) {
				if (line.equalsIgnoreCase("<h2>Depths Web Service!</h2>")) {
					return true;
				}
			}

		} catch (MalformedURLException mue) {
			return false;
		} catch (IOException ioe) {
			return false;
		}
		return false;
	}

	public static boolean exists(String URLName) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URLName)
					.openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			return false;
		}
	}

	public static int getResponseCode(String urlString)
			throws MalformedURLException, IOException {
		urlString = urlString + "/depths/";
		URL u = new URL(urlString);
		HttpURLConnection huc = (HttpURLConnection) u.openConnection();
		huc.setRequestMethod("GET");
		huc.connect();
		return huc.getResponseCode();
	}

}
