// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import java.util.ArrayList;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.configResources.ConfigURL;
import depths.json.course.Course;

public class DepthsJSONCourse {

	private static final Logger LOG = Logger.getLogger(DepthsJSONCourse.class);

	public static ArrayList<Course> listAllCourse() {
		String wsURL = ConfigURL.getusercourses;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method DepthsJSONCourse listAllCourse()");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);
			String userURI = DepthsConfiguration.getString(DepthsArgo.USER_URI);
			String jsonRes = r.queryParam("useruri", userURI)
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);
						ArrayList<Course> listCourse = new ArrayList<Course>();
						for (int i = 0; i < arr.length(); i++) {
							JSONObject course = arr.getJSONObject(i);
							Course tmp = new Course();
							tmp.setUri(course.getString("courseuri"));
							tmp.setTitle(course.getString("title"));
							tmp.setDescription(course
									.getString("coursedescription"));
							tmp.setNumberOfDesignProblem(Integer
									.parseInt(course
											.getString("designproblemsnumber")));
							listCourse.add(tmp);
						}
						return listCourse;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
}
