// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import java.util.ArrayList;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.configResources.ConfigURL;
import depths.json.designProblem.DesignProblem;

public class DepthsJSONDesignProblem {

	private static final Logger LOG = Logger
			.getLogger(DepthsJSONDesignProblem.class);

	public static ArrayList<DesignProblem> listAllDesignProblem(String courseuri) {
		String wsURL = ConfigURL.getcourseproblems;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method DepthsJSONDesignProblem listAllDesignProblem(String courseuri)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);
			String jsonRes = r.queryParam("courseuri", courseuri)
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);
						ArrayList<DesignProblem> listProblems = new ArrayList<DesignProblem>();
						for (int i = 0; i < arr.length(); i++) {
							JSONObject course = arr.getJSONObject(i);
							DesignProblem tmp = new DesignProblem();

							tmp.setUri(course.getString("problemuri"));
							tmp.setTitle(course.getString("title"));
							tmp.setDescription(course
									.getString("problemdescription"));
							tmp.setDateCreated(course.getString("datecreated"));

							listProblems.add(tmp);
						}
						return listProblems;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
	
	
	public static DesignProblem getDescriptionProblem(String problemuri) {
		String wsURL = ConfigURL.getProblemDescription;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method DepthsJSONDesignProblem getDescriptionProblem(String problemuri)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);
			String jsonRes = r.queryParam("problemuri", problemuri)
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);
							JSONObject dsc = arr.getJSONObject(0);
							DesignProblem tmp = new DesignProblem();

							tmp.setUri(dsc.getString("problemuri"));
							tmp.setTitle(dsc.getString("title"));
							tmp.setDescription(dsc
									.getString("problemdescription"));
							tmp.setDateCreated(dsc.getString("datecreated"));
						if (dsc.has("creatorfirstname")) {
							tmp.setAuthor(dsc.getString("creatorfirstname"));
						}
						if (dsc.has("creatorlastname")) {
							tmp.setAuthor(tmp.getAuthor() + " " +dsc.getString("creatorlastname"));
						}
						
						JSONArray arrL = dsc.getJSONArray("links");
						for (int i = 0; i < arrL.length(); i++) {
							JSONObject j = arrL.getJSONObject(i);
							tmp.getRelatedLinkTitle().add(j.getString("pagetitle"));
							tmp.getRelatedLinkHref().add(j.getString("href"));
						}
						
						return tmp;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
	
	
	
	

}
