// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.service;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONStringer;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.configResources.ConfigURL;
import depths.json.project.Project;
import depths.json.project.ProjectUtility;
import depths.uml.diagram.DepthsUMLDiagram;

public class DepthsJSONProject {

	private static final Logger LOG = Logger.getLogger(DepthsJSONProject.class);

	public static Project getProject(String projectURI) {
		Project p = searchProject(projectURI);
		ProjectUtility.setLastOpenProject(p);
		ProjectUtility.setLastUserUri(p.getModifiedBy());
		ProjectUtility.setSelectedDesignProblemURI(null);
		return p;
	}

	private static Project searchProject(String projectURI) {

		String wsURL = ConfigURL.getproject;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method ProjectParameter getProject(String projectURI)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);

			try {
				String jsonRes = r.queryParam("projecturi", projectURI)
						.accept(MediaType.APPLICATION_JSON).get(String.class);

				System.out.println("Dobijeni json za open: " + jsonRes);
				Project projectParameter = new Project();
				JSONArray projectJson = new JSONArray(jsonRes);
				JSONObject projectJsonURI = projectJson.getJSONObject(0);
				projectParameter.setProjecturi(projectJsonURI
						.getString("projecturi"));

				JSONObject projectJsonObject = projectJsonURI
						.getJSONObject("project");
				projectParameter.setProjectName(projectJsonObject
						.getString("title"));
				projectParameter.setFileurl(projectJsonObject
						.getString("fileurl"));

				projectParameter.setDatetimecreated(projectJsonObject
						.getString("datetimecreated"));
				projectParameter.setCreatedBy(projectJsonObject
						.getString("createdBy"));
				projectParameter.setCreatorname(projectJsonObject
						.getString("creatorname"));

				if (projectJsonURI.has("diagrams")) {
					JSONArray diagrams = projectJsonURI
							.getJSONArray("diagrams");
					ArrayList<DepthsUMLDiagram> oldDiagrams = new ArrayList<DepthsUMLDiagram>();

					for (int i = 0; i < diagrams.length(); i++) {
						JSONObject objectDiagram = diagrams.getJSONObject(i);
						String type = objectDiagram.getString("type");
						String diagID = objectDiagram.getString("id");
						String diagURI = objectDiagram.getString("diagramuri");
						String title = objectDiagram.getString("title");
						String fileUrl = objectDiagram.getString("fileUrl");
						DepthsUMLDiagram diag = new DepthsUMLDiagram();
						diag.setDiagramId(diagID);
						diag.setDiagramName(title);
						diag.setDiagramType(type);
						diag.setDiagramUri(diagURI);
						diag.setFileUrl(fileUrl);
						oldDiagrams.add(diag);
					}

					projectParameter.setOldDiagrams(oldDiagrams);

				}

				if (projectJsonObject.has("parentProject")) {
					projectParameter.setParentProject(projectJsonObject
							.getString("parentProject"));
				}
				if (projectJsonURI.has("solution")) {
					JSONObject solution = projectJsonURI
							.getJSONObject("solution");
					projectParameter.setSolutionuri(solution
							.getString("solutionuri"));
					projectParameter.setDescription(solution
							.getString("description"));
					projectParameter.setHasAdditionalRequirements(solution
							.getString("hasAdditionalRequirements"));
					projectParameter.setHasCons(solution.getString("hasCons"));
					projectParameter.setHasConsequences(solution
							.getString("hasConsequences"));
					projectParameter.setHasDesignConstraints(solution
							.getString("hasDesignConstraints"));
					projectParameter.setHasDesignRules(solution
							.getString("hasDesignRules"));
					projectParameter.setHasPros(solution.getString("hasPros"));
				}

				
				
				// added json parameter for problem description

				return projectParameter;
			} catch (JSONException e) {
				LOG.debug("Error", e);
			} catch (UniformInterfaceException e) {
				LOG.debug("Error", e);
			}

			return null;
		}
		return null;
	}

	
	
	public static String saveProject(Project proCurrent, String mode) {

		String wsURL = ConfigURL.saveProject;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method ProjectParameter save project (project project)");

			JSONStringer js = new JSONStringer();
			try {

				js.object();
				String user = DepthsConfiguration
						.getString(DepthsArgo.USER_URI);
				js.key("useruri").value(user);

				if (proCurrent.getDesignProblem() != null) {
					js.key("designproblem")
							.value(proCurrent.getDesignProblem());
				} else {
					js.key("designproblem").value("");
				}

				if (mode.equals("create")) {
					js.key("mode").value("create");
				} else if (mode.equals("edit")) {
					js.key("mode").value("edit");
				} else if (mode.equals("reuse")) {
					js.key("mode").value("reuse");
				}

				js.key("project");

				js.object();

				if (mode.equals("edit")) {
					js.key("projecturi").value(proCurrent.getProjecturi());
				}

				js.key("projectid").value(proCurrent.getProjectId());
				if (mode.equals("reuse")) {
					js.key("parentprojecturi").value(
							proCurrent.getParentProject());
				}

				js.key("title").value(proCurrent.getProjectName());
				js.key("dateTimeSent").value(new Date().toString());
				js.key("fileUrl").value(proCurrent.getFileurl());
				js.key("createdBy").value(proCurrent.getCreatedBy());
				if (mode.equals("edit")) {
					js.key("modifiedBy").value(proCurrent.getModifiedBy());
				} else {
					js.key("modifiedBy").value("");
				}

				js.endObject();

				js.key("solution");
				js.object();
				js.key("description").value(proCurrent.getDescription());
				js.key("hasAdditionalRequirements").value(
						proCurrent.getHasAdditionalRequirements());
				js.key("hasCons").value(proCurrent.getHasCons());
				js.key("hasConsequences")
						.value(proCurrent.getHasConsequences());
				js.key("hasDesignConstraints").value(
						proCurrent.getHasDesignConstraints());
				js.key("hasDesignRules").value(proCurrent.getHasDesignRules());
				js.key("hasPros").value(proCurrent.getHasPros());
				js.endObject();

				js.endObject();
				System.out.println("SAVE: " + js.toString());
				return js.toString();

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String saveDiagrams(Project project, String projectURI) {
		JSONStringer js = new JSONStringer();
		try {
			js.object();
			String user = DepthsConfiguration.getString(DepthsArgo.USER_URI);
			js.key("useruri").value(user);

			js.key("projecturi").value(projectURI);

			js.key("diagrams");
			js.array();
			ArrayList<DepthsUMLDiagram> allDiagrams = new ArrayList<DepthsUMLDiagram>();
			allDiagrams.addAll(project.getActivityDiagrams());
			allDiagrams.addAll(project.getClassDiagrams());
			allDiagrams.addAll(project.getCollaborationDiagrams());
			allDiagrams.addAll(project.getDeploymentDiagrams());
			allDiagrams.addAll(project.getSequenceDiagrams());
			allDiagrams.addAll(project.getStatechartDiagrams());
			allDiagrams.addAll(project.getUseCaseDiagrams());
			allDiagrams.addAll(project.getDeletedDiagrams());

			for (int i = 0; i < allDiagrams.size(); i++) {
				DepthsUMLDiagram diag = allDiagrams.get(i);
				String diagID = diag.getDiagramId();
				String type = diag.getDiagramType();
				String file = diag.getFileUrl();
				String name = diag.getDiagramName();
				String mode = diag.getMode();
				js.object();
				if (mode.equals("create")) {
					js.key("mode").value("create");
					js.key("type").value(type);
					js.key("fileUrl").value(file);
					js.key("title").value(name);
					js.key("id").value(diagID);
				} else if (mode.equals("edit")) {
					js.key("mode").value("edit");
					js.key("type").value(type);
					js.key("fileUrl").value(file);
					js.key("title").value(name);
					String uri = diag.getDiagramUri();
					js.key("uri").value(uri);
					js.key("id").value(diagID);
				} else if (mode.equals("delete")) {
					js.key("mode").value("delete");
					js.key("type").value(type);
					js.key("fileUrl").value(file);
					js.key("title").value(name);
					String uri = diag.getDiagramUri();
					js.key("uri").value(uri);
					js.key("id").value(diagID);
				}
				js.endObject();

			}

			js.endArray();
			js.endObject();
			return js.toString();
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	public static ArrayList<Project> listAllProjects(String userName) {
		String wsURL = ConfigURL.listallprojects;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method ProjectParameter listAllProjects(String projectURI)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);

			String jsonRes = r.queryParam("username", userName)
					.accept(MediaType.APPLICATION_JSON).get(String.class);

			System.out.println("AVIB : " + jsonRes);
			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);
						
						ArrayList<Project> listProj = new ArrayList<Project>();
						for (int i = 0; i < arr.length(); i++) {
							JSONArray arrP = arr.getJSONArray(i);
							for (int j = 0; j < arrP.length(); j++) {
								JSONObject o = arrP.getJSONObject(j);
								Project pro = new Project();
								pro.setProjecturi(o.getString("projecturi"));
								JSONObject pt = o.getJSONObject("project");
								pro.setProjectName(pt.getString("title"));
								pro.setFileurl(pt.getString("fileurl"));
								pro.setDatetimecreated(pt
										.getString("datetimecreated"));
								pro.setCreatedBy(pt.getString("createdBy"));
								pro.setCreatorname(pt.getString("creatorname"));
								listProj.add(pro);
							}

						}
						return listProj;
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public static ArrayList<Project> listMyProjects(String userName) {
		String wsURL = ConfigURL.listbyusername;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method ProjectParameter listMyProjects(String projectURI)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);

			String jsonRes = r.queryParam("username", userName)
					.accept(MediaType.APPLICATION_JSON).get(String.class);

			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);

						ArrayList<Project> listProj = new ArrayList<Project>();
						for (int i = 0; i < arr.length(); i++) {
							JSONArray arrP = arr.getJSONArray(i);
							for (int j = 0; j < arrP.length(); j++) {
								JSONObject o = arrP.getJSONObject(j);
								Project pro = new Project();
								pro.setProjecturi(o.getString("projecturi"));
								JSONObject pt = o.getJSONObject("project");
								pro.setProjectName(pt.getString("title"));
								pro.setFileurl(pt.getString("fileurl"));
								pro.setDatetimecreated(pt
										.getString("datetimecreated"));
								pro.setCreatedBy(pt.getString("createdBy"));
								pro.setCreatorname(pt.getString("creatorname"));
								listProj.add(pro);
							}

						}
						return listProj;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	public static ArrayList<Project> listAllProjectForDesignProblem(
			String designProblemURI) {
		String wsURL = ConfigURL.listprojectsforproblem;

		if (ValidURL.valide(wsURL)) {
			LOG.debug("call method ProjectParameter listAllProjectForDesignProblem(String uriProblem)");
			Client c = Client.create();
			WebResource r = c.resource(wsURL);
			String userURI = DepthsConfiguration.getString(DepthsArgo.USER_URI);

			MultivaluedMapImpl queryParams = new MultivaluedMapImpl();
			Object of = userURI;
			queryParams.add("useruri", of);
			of = designProblemURI;
			queryParams.add("designproblemuri", of);

			String jsonRes = r.queryParams(queryParams)
					.accept(MediaType.APPLICATION_JSON).get(String.class);
			try {
				if (jsonRes != null) {
					if (!jsonRes.equals("")) {
						JSONArray arr = new JSONArray(jsonRes);

						ArrayList<Project> listProj = new ArrayList<Project>();
						for (int i = 0; i < arr.length(); i++) {
							JSONArray arrP = arr.getJSONArray(i);
							JSONObject obj = arrP.getJSONObject(0);
							Project pro = new Project();
							pro.setProjecturi(obj.getString("projecturi"));
							JSONObject proJson = obj.getJSONObject("project");
							pro.setProjectName(proJson.getString("title"));
							pro.setFileurl(proJson.getString("fileurl"));
							pro.setDatetimecreated(proJson
									.getString("datetimecreated"));
							pro.setCreatedBy(proJson.getString("createdBy"));
							pro.setCreatorname(proJson.getString("creatorname"));
							pro.setModifiedBy(proJson.getString("modifiedBy"));
							pro.setModifiedByName(proJson
									.getString("modifiedByName"));
							listProj.add(pro);
						}

						return listProj;
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
}
