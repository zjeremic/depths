// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.json.ftp;

public class FtpParameter {

	private String ftpUsername;
	private String ftpPassword;
	private String ftpUrl;
	private String ftpPort;
	private String ftpLocationProjects;
	private String ftpLocationImages;
	private String securityssl;
	
	public String getFtpUsername() {
		return ftpUsername;
	}

	public void setFtpUsername(String ftpUsername) {
		this.ftpUsername = ftpUsername;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public String getFtpUrl() {
		return ftpUrl;
	}

	public void setFtpUrl(String ftpUrl) {
		this.ftpUrl = ftpUrl;
	}

	public String getFtpPort() {
		return ftpPort;
	}

	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}

	public String getFtpLocationProjects() {
		return ftpLocationProjects;
	}

	public void setFtpLocationProjects(String ftpLocationProjects) {
		this.ftpLocationProjects = ftpLocationProjects;
	}

	public String getFtpLocationImages() {
		return ftpLocationImages;
	}

	public void setFtpLocationImages(String ftpLocationImages) {
		this.ftpLocationImages = ftpLocationImages;
	}

	public String getSecurityssl() {
		return securityssl;
	}

	public void setSecurityssl(String securityssl) {
		this.securityssl = securityssl;
	}

	@Override
	public String toString() {
		return "FtpParameter [ftpUsername=" + ftpUsername + ", ftpPassword="
				+ ftpPassword + ", ftpUrl=" + ftpUrl + ", ftpPort=" + ftpPort
				+ ", ftpLocationProjects=" + ftpLocationProjects
				+ ", ftpLocationImages=" + ftpLocationImages + ", securityssl="
				+ securityssl + "]";
	}

	

}
