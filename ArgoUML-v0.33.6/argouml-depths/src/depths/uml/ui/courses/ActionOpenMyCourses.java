// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.uml.ui.courses;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.argouml.application.api.CommandLineInterface;
import org.argouml.application.helpers.ResourceLoaderWrapper;
import org.argouml.configuration.Configuration;
import org.argouml.kernel.ProjectManager;
import org.argouml.persistence.PersistenceManager;
import org.argouml.ui.ProjectBrowser;
import org.argouml.ui.UndoableAction;
import org.argouml.util.ArgoDialog;

import depths.i18n.DepthsTranslator;
import depths.json.project.Project;
import depths.json.project.ProjectUtility;
import depths.json.service.DepthsJSONCourse;
import depths.json.service.DepthsJSONProject;
import depths.json.service.DepthsJSONTabService;
import depths.ui.DepthsModule;
import depths.ui.ErrorOpenMenu;
import depths.uml.ui.FTPConnectionManager;

public class ActionOpenMyCourses extends UndoableAction implements
		CommandLineInterface {

	private static final Logger LOG = Logger
			.getLogger(ActionOpenMyCourses.class);
	private ArgoDialog frame = null;
	private CoursePanel coursePanel;

	public ActionOpenMyCourses() {

		super(DepthsTranslator.localize("action.my-courses"),
				ResourceLoaderWrapper.lookupIcon("action.settings"));

		// Set the tooltip string:
		putValue(Action.SHORT_DESCRIPTION,
				DepthsTranslator.localize("action.my-courses-tooltip"));
	}

	ActionListener myCoursesButton = new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
			frame.setVisible(false);
		}
	};

	ActionListener myCoursesButtonOpenProject = new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
			PersistenceManager pm = PersistenceManager.getInstance();

			LOG.info("before ftp connection x1");
			FTPConnectionManager.createConnectionToFTPServer();

			String uri = coursePanel.getSelProjectUri();
			Project pro = DepthsJSONProject.getProject(uri);
			ProjectUtility.setSelectedDesignProblemURI(pro.getDesignProblem());

			String f = pro.getFileurl();

			if (f.endsWith(".zargo")) {
				f = f.substring(0, f.length() - 6);
			}

			File theFile = FTPConnectionManager.getFileFromRepository(f);
			FTPConnectionManager.closeConnection();

			if (!theFile.canRead()) {
				if (!theFile.canRead()) {
					File n = new File(theFile.getPath() + "."
							+ pm.getDefaultExtension());
					if (n.canRead()) {
						theFile = n;
					}
				}
			}
			if (theFile != null) {

				Configuration.setString(
						PersistenceManager.KEY_OPEN_PROJECT_PATH,
						theFile.getPath());

				ProjectBrowser.getInstance().loadProjectWithProgressMonitor(
						theFile, true);
				DepthsModule.changeMenuStatus(true);
				DepthsJSONTabService.setValueSolution(pro);
//				DepthsJSONTabService.setValueDescription(pro);
			}

			frame.setVisible(false);
			frame = null;
		}
	};

	ActionListener myCoursesButtonNewProject = new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
			ProjectUtility.setLastOpenProject(null);
			ProjectManager.getManager().makeEmptyProject();
			ProjectUtility.setSelectedDesignProblemURI(coursePanel
					.getSelDesignProblemUri());
			frame.setVisible(false);
			frame = null;
		}
	};

	public void actionPerformed(ActionEvent e) {
		if (ErrorOpenMenu.showMessage()) {
			boolean connect = FTPConnectionManager
					.createConnectionToFTPServer();

			if (connect == true) {
				FTPConnectionManager.closeConnection();

				super.actionPerformed(e);

				if (frame == null) {

					coursePanel = null;
					LOG.debug("dialog==null");
					frame = new ArgoDialog("MY COURSES", true);
					frame.addWindowListener(new java.awt.event.WindowAdapter() {
						public void windowClosing(java.awt.event.WindowEvent e) {
							frame.setVisible(false);
							frame = null;
						}
					});
					frame.setSize(1006, 410);
					frame.setResizable(false);
					frame.setTitle("MY COURSES");

					coursePanel = new CoursePanel();

					coursePanel.retrunJButtoOpenProject().addActionListener(
							myCoursesButtonOpenProject);
					coursePanel.retrunJButtoNewProject().addActionListener(
							myCoursesButtonNewProject);

					frame.getContentPane()
							.add(coursePanel, BorderLayout.CENTER);
					frame.setLocation(300, 180);
					frame.setVisible(true);

				} else {
					frame.setVisible(true);
				}
			}else{
			JOptionPane.showMessageDialog(null, "FTP server is offline!");
			}
		}
	}

	public boolean doCommand(String argument) {
		// TODO Auto-generated method stub
		return false;
	}

}
