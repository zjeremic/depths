// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade 

package depths.uml.ui.openproject;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.argouml.application.api.CommandLineInterface;
import org.argouml.application.helpers.ResourceLoaderWrapper;
import org.argouml.configuration.Configuration;
import org.argouml.persistence.PersistenceManager;
import org.argouml.ui.ProjectBrowser;
import org.argouml.ui.UndoableAction;
import org.argouml.util.ArgoDialog;

import depths.i18n.DepthsTranslator;
import depths.json.project.Project;
import depths.json.service.DepthsJSONProject;
import depths.json.service.DepthsJSONTabService;
import depths.ui.DepthsModule;
import depths.ui.ErrorOpenMenu;
import depths.uml.ui.FTPConnectionManager;

public class ActionOpenDepthsProject extends UndoableAction implements
		CommandLineInterface {
	private static final long serialVersionUID = 3884996525438151141L;
	private static final Logger LOG = Logger
			.getLogger(ActionOpenDepthsProject.class);
	// private OpenDepthsPaneDialog dialog;
	private ArgoDialog frame = null;
	private OpenPanel d;

	// //////////////////////////////////////////////////////////////
	// constructors

	/**
	 * Constructor for this action.
	 */
	public ActionOpenDepthsProject() {

		super(DepthsTranslator.localize("action.open-depths-project"),
				ResourceLoaderWrapper.lookupIcon("action.open-project"));

		// Set the tooltip string:
		putValue(Action.SHORT_DESCRIPTION,
				DepthsTranslator.localize("action.open-depths-project-tooltip"));
	}

	// //////////////////////////////////////////////////////////////
	// main methods
	ActionListener openProjectSendButton = new ActionListener() {
		public void actionPerformed(ActionEvent ev) {

			frame.setVisible(false);

			PersistenceManager pm = PersistenceManager.getInstance();

			LOG.info("before ftp connection x1");
			FTPConnectionManager.createConnectionToFTPServer();

			String f = d.returnProjectFile().getText();

			if (f.endsWith(".zargo")) {
				f = f.substring(0, f.length() - 6);
			}

			String uri = d.getProjectURI();

			System.out.println("ime fajla je: " + f);
			Project proj = DepthsJSONProject.getProject(uri);

			
			File theFile = FTPConnectionManager.getFileFromRepository(f);
			FTPConnectionManager.closeConnection();

			if (!theFile.canRead()) {

				/* Try adding the extension from the chosen filter. */
				// FileFilter ffilter = chooser.getFileFilter();
				/*
				 * if (ffilter instanceof AbstractFilePersister) {
				 * AbstractFilePersister afp = (AbstractFilePersister) ffilter;
				 * File m = new File(theFile.getPath() + "." +
				 * afp.getExtension()); if (m.canRead()) { theFile = m; } }
				 */
				if (!theFile.canRead()) {

					/* Try adding the default extension. */
					File n = new File(theFile.getPath() + "."
							+ pm.getDefaultExtension());
					if (n.canRead()) {
						theFile = n;
					}
				}
			}
			if (theFile != null) {

				Configuration.setString(
						PersistenceManager.KEY_OPEN_PROJECT_PATH,
						theFile.getPath());

				ProjectBrowser.getInstance().loadProjectWithProgressMonitor(
						theFile, true);
				DepthsModule.changeMenuStatus(true);
				DepthsJSONTabService.setValueSolution(proj);
//				DepthsJSONTabService.setValueDescription(proj);


			}

			d.setDefaultValue();
			frame.setVisible(false);
			frame = null;
		}

	};

	/**
	 * Performs the action of opening a project.
	 * 
	 * @param e
	 *            an event
	 */
	public void actionPerformed(ActionEvent e) {
		if (ErrorOpenMenu.showMessage()) {
			boolean connect = FTPConnectionManager
					.createConnectionToFTPServer();

			if (connect == true) {
				FTPConnectionManager.closeConnection();
				super.actionPerformed(e);

				if (frame == null) {

					LOG.debug("dialog==null");
					frame = new ArgoDialog(
							"Open Project from remote repository...", true);
					frame.addWindowListener(new java.awt.event.WindowAdapter() {
						public void windowClosing(java.awt.event.WindowEvent e) {
							frame.setVisible(false);
							frame = null;
						}
					});
					frame.setResizable(false);
					frame.setSize(858, 255);
					frame.setTitle("Open Project from remote repository...");


					d = new OpenPanel();


					d.returnOpenButton().addActionListener(
							openProjectSendButton);

					frame.getContentPane().add(d, BorderLayout.CENTER);
					frame.setLocation(300, 180);
					frame.setVisible(true);

				} else {
					frame.setVisible(true);
				}
			} else {
				JOptionPane.showMessageDialog(null, "FTP server is offline!");
			}
		}
	}

	/**
	 * Execute this action from the command line.
	 * 
	 * @see org.argouml.application.api.CommandLineInterface#doCommand(String)
	 * @param argument
	 *            is the url of the project we load.
	 * @return true if it is OK.
	 */
	public boolean doCommand(String argument) {
		return ProjectBrowser.getInstance().loadProject(new File(argument),
				false, null);
	}

}
