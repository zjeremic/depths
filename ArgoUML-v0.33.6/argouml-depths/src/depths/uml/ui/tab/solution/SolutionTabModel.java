package depths.uml.ui.tab.solution;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.argouml.configuration.Configuration;
import org.argouml.swingext.UpArrowIcon;
import org.argouml.uml.ui.PropPanel;
import org.argouml.uml.ui.UMLModelElementTaggedValueDocument;
import org.argouml.uml.ui.UMLTextArea2;
import org.tigris.swidgets.Horizontal;
import org.tigris.swidgets.Vertical;

import depths.application.api.DepthsArgo;
import depths.i18n.DepthsTranslator;

public class SolutionTabModel extends PropPanel {

	private static String orientation = Configuration.getString(Configuration
			.makeKey("layout", "tabdocumentation"));

	JTextArea description, designRules, designConstraints,
			additionalRequirements, consequences, pros, cons;

	/**
	 * Construct a property panel with a given name and icon.
	 * 
	 * @param diagramName
	 *            the diagram name to use as the title of the panel
	 * @param icon
	 *            an icon to display on the panel
	 */
	protected SolutionTabModel(String diagramName, ImageIcon icon) {
		super(diagramName, icon);
		setOrientation((orientation.equals("West") || orientation
				.equals("East")) ? Vertical.getInstance() : Horizontal
				.getInstance());
		setIcon(new UpArrowIcon());

		description = new JTextArea();
		description.setRows(2);
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		JScrollPane spDescription = new JScrollPane();
		spDescription.getViewport().add(description);
		addField(DepthsTranslator.localize("depths_label.description"),
				spDescription);

		designRules = new JTextArea();
		designRules.setRows(2);
		designRules.setLineWrap(true);
		designRules.setWrapStyleWord(true);
		JScrollPane spDesignRules = new JScrollPane();
		spDesignRules.getViewport().add(designRules);
		addField(DepthsTranslator.localize("depths_label.design_rules"),
				spDesignRules);

		designConstraints = new JTextArea();
		designConstraints.setRows(2);
		designConstraints.setLineWrap(true);
		designConstraints.setWrapStyleWord(true);
		JScrollPane spDesignConstraints = new JScrollPane();
		spDesignConstraints.getViewport().add(designConstraints);
		addField(DepthsTranslator.localize("depths_label.design_constraints"),
				spDesignConstraints);

		additionalRequirements = new JTextArea();
		additionalRequirements.setRows(2);
		additionalRequirements.setLineWrap(true);
		additionalRequirements.setWrapStyleWord(true);
		JScrollPane spAdditionalRequirements = new JScrollPane();
		spAdditionalRequirements.getViewport().add(additionalRequirements);
		addField(
				DepthsTranslator
						.localize("depths_label.additional_requirements"),
				spAdditionalRequirements);

		// make new column with LabelledLayout
		// add(LabelledLayout.getSeperator());
		addSeparator();

		consequences = new JTextArea();
		consequences.setRows(2);
		consequences.setLineWrap(true);
		consequences.setWrapStyleWord(true);
		JScrollPane spConsequences = new JScrollPane();
		spConsequences.getViewport().add(consequences);
		addField(DepthsTranslator.localize("depths_label.consequences"),
				spConsequences);

		pros = new JTextArea();
		pros.setRows(2);
		pros.setLineWrap(true);
		pros.setWrapStyleWord(true);
		JScrollPane spPros = new JScrollPane();
		spPros.getViewport().add(pros);
		addField(DepthsTranslator.localize("depths_label.pros"), spPros);

		cons = new JTextArea();
		cons.setRows(2);
		cons.setLineWrap(true);
		cons.setWrapStyleWord(true);
		JScrollPane spCons = new JScrollPane();
		spCons.getViewport().add(cons);
		addField(DepthsTranslator.localize("depths_label.cons"), spCons);

		setButtonPanelSize(18);
	}

}
