// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.uml.ui.tab.description;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import depths.i18n.DepthsTranslator;

public class DescriptionTab extends DescriptionTabModel {

	private static final Logger LOG = Logger.getLogger(DescriptionTab.class);

	/**
	 * Constructor for PropPanelUMLClassDiagram.
	 */
	public DescriptionTab() {
		super(DepthsTranslator.localize("depths_problem_description.title"),
				lookupIcon("ClassDiagram"));

	}

	public void setDscProblemTitle(String text) {
		dscProblemTitle.setText(text);
	}

	public void setDscProblemDescription(String text) {
		dscProblem.setText(text);
	}

	public void setDscProblemAuthorName(String text) {
		dscProblemAuthorName.setText(text);
	}

	public void setDscProblemReletedLinks(String text) {
		dscProblemReletedLinks.setText(text);
	}

	public String getDscProblemTitle() {
		return dscProblemTitle.getText();
	}

	public String getDscProblemDescription() {
		return dscProblem.getText();
	}

	public String getDscProblemAuthorName() {
		return dscProblemAuthorName.getText();
	}

	public String getDscProblemReletedLinks() {
		return dscProblemReletedLinks.getText();
	}

	public static DescriptionTab getIstance() {
		return new DescriptionTab();
	}

	public void setReletedLinksHtml(ArrayList<String> title,
			ArrayList<String> href) {

		setDscProblemReletedLinks("<html><body>");
		for (int i = 0; i < title.size(); i++) {
			setDscProblemReletedLinks(getDscProblemReletedLinks() + "<a href='"
					+ href.get(i) + "'>" + title.get(i) + "</a><br/>");
		}
		setDscProblemReletedLinks(getDscProblemReletedLinks()
				+ "</body></html>");
	}

}
