// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.uml.ui;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import it.sauronsoftware.ftp4j.FTPListParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.log4j.Logger;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.project.ProjectUtility;
import depths.utility.CheckPort;

/**
 * 
 * 
 * @author Zoran
 */
public class FTPConnectionManager {

	/**
	 * Logger.
	 */
	private static final Logger LOG = Logger
			.getLogger(FTPConnectionManager.class);
	static FTPClient ftp = new FTPClient();

	/**
	 * 
	 * @param host
	 * @param username
	 * @param password
	 */
	public static boolean createConnectionToFTPServer() {

		if (ftp.isConnected()) {
			closeConnection();
		}

		String ssl = DepthsConfiguration.getString(DepthsArgo.SSL);
		String host = DepthsConfiguration.getString(DepthsArgo.KEY_FTP_HOST);

		String username = DepthsConfiguration

		.getString(DepthsArgo.KEY_REPOSITORY_USERNAME);
		String password = DepthsConfiguration
				.getString(DepthsArgo.KEY_REPOSITORY_PASSWORD);
		String port = DepthsConfiguration.getString(DepthsArgo.KEY_FTP_PORT);
		if (CheckPort.check(host, Integer.parseInt(port))==false) {
			JOptionPane.showMessageDialog(null, "Error, your internet provider not support port 21.");
			return false;
		}
		try {

			if (ssl.equals("yes")) {
				TrustManager[] trustManager = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(X509Certificate[] certs,
							String authType) {
					}

					public void checkServerTrusted(X509Certificate[] certs,
							String authType) {
					}
				} };

				SSLContext sslContext = null;
				try {
					sslContext = SSLContext.getInstance("SSL");
					sslContext.init(null, trustManager, new SecureRandom());
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (KeyManagementException e) {
					e.printStackTrace();
				}
				SSLSocketFactory sslSocketFactory = sslContext
						.getSocketFactory();
				ftp.setSSLSocketFactory(sslSocketFactory);

				ftp.setSecurity(FTPClient.SECURITY_FTPES);
			}

			ftp.connect(host, Integer.parseInt(port));
			if (ftp.isConnected()==false) {
				return false;
			}
			ftp.login(username, password);

			if (ftp.isConnected()) {
				LOG.info("Connected Successfully to the FTP server");
				return true;
			} else {
				LOG.info("Connection to the FTP server Failed");
				return false;
			}

		} catch (SocketException ex) {
			LOG.debug("Error connecting to the ftp server SocketException:"
					+ ex.getMessage());
		} catch (IOException ioe) {
			LOG.debug("Error connection ftp server: " + ioe.getCause());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
		} catch (FTPException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * @param file
	 * @param fName
	 */
	public static void uploadFileToRepository(File file, String fName) {
		String fileName = null;

		String pathImg = DepthsConfiguration
				.getString(DepthsArgo.KEY_FTP_IMAGES_FOLDER);
		String pathProj = DepthsConfiguration
				.getString(DepthsArgo.KEY_FTP_PROJECTS_FOLDER);

		try {
			if (fName.equals(".png")) {

				ftp.setType(FTPClient.TYPE_AUTO);
				LOG.info(".png");
				fileName = file.getName();
				if (ftp.currentDirectory().equals("/")) {
					ftp.changeDirectory(ftp.currentDirectory() + pathImg);
				} else {
					ftp.changeDirectory(ftp.currentDirectory() + "/" + pathImg);
				}

			} else if (fName.equals(".zargo")) {
				ftp.setType(FTPClient.TYPE_AUTO);
				try {
					fileName = ProjectUtility.getCurrentProject().getFileurl();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Problem to create project" + e.getMessage()
									+ "\n\n" + e.getCause());
				}
				if (ftp.currentDirectory().equals("/")) {
					ftp.changeDirectory(ftp.currentDirectory() + pathProj);
				} else {
					ftp.changeDirectory(ftp.currentDirectory() + "/" + pathProj);
				}

			}

			ftp.upload(file);
			if (fName.equals(".zargo")) {
				ftp.rename(file.getName(), fileName);
			}
			ftp.changeDirectory("/");

		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null,
					ioe.getMessage() + "\n\n" + ioe.getCause());
		} catch (Exception e) {
			LOG.debug("Exception:" + e.getClass().getName());
			JOptionPane.showMessageDialog(null,
					e.getMessage() + "\n\n" + e.getCause() + "\n\n");
		}

	}

	public static File getFileFromRepository(String projectName) {
		String projectURL = "";
		if (projectName.endsWith(".zargo")) {
			projectURL = projectName + "";
		} else {
			projectURL = projectName + ".zargo";
		}

		// File projectFile = new File(projectURL);
		File projectFile = null;
		try {
			projectFile = File.createTempFile("tmp_depths", ".zargo");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		String pathProj = DepthsConfiguration
				.getString(DepthsArgo.KEY_FTP_PROJECTS_FOLDER);
		try {

			if (ftp.currentDirectory().equals("/")) {
				ftp.changeDirectory(ftp.currentDirectory() + pathProj);
			} else {
				ftp.changeDirectory(ftp.currentDirectory() + "/" + pathProj);
			}
			ftp.download(projectURL, projectFile);

			ftp.changeDirectory("/");

		} catch (FileNotFoundException fnfe) {
			LOG.debug("File not found exception");
			JOptionPane.showMessageDialog(null, fnfe.getMessage() + "\n\n"
					+ fnfe.getCause());
		} catch (IOException ioe) {
			LOG.debug("IOException");
			JOptionPane.showMessageDialog(null,
					ioe.getMessage() + "\n\n" + ioe.getCause());
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
		} catch (FTPException e) {
			e.printStackTrace();
		} catch (FTPDataTransferException e) {
			e.printStackTrace();
		} catch (FTPAbortedException e) {
			e.printStackTrace();
		}

		return projectFile;
	}

	public static String createUniqueFileName(String fName) {
		String filename = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		String dateString = dateFormat.format(date);
		String moodle_username = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_USERNAME);
		filename = moodle_username + "__" + dateString + fName;

		return filename;
	}

	public static void closeConnection() {
		try {
			ftp.disconnect(true);
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null,
					ioe.getMessage() + "\n\n" + ioe.getCause());
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FTPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void createStructureDirectory(String path) {
		createConnectionToFTPServer();
		try {
			String pathDir[] = path.split("/");
			String contactPath = "";

			for (int i = 0; i < pathDir.length; i++) {
				String pathChange = "";
				if (contactPath.equals("")) {
					pathChange = "/";
				} else {
					pathChange = contactPath;
				}
				FTPFile[] listDirFTP = ftp.list(pathChange);
				boolean alredy = false;
				for (int k = 0; k < listDirFTP.length; k++) {
					if (listDirFTP[k].getName().equals(pathDir[i])) {
						alredy = true;
					}
				}
				if (alredy == false) {
					ftp.changeDirectory("/");
					ftp.changeDirectory(ftp.currentDirectory() + "/"
							+ pathChange);
					ftp.createDirectory(pathDir[i]);
					ftp.changeDirectory("/");
				}
				contactPath = contactPath + "/" + pathDir[i];
			}
			closeConnection();

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FTPIllegalReplyException e) {
			e.printStackTrace();
		} catch (FTPException e) {
			e.printStackTrace();
		} catch (FTPDataTransferException e) {
			e.printStackTrace();
		} catch (FTPAbortedException e) {
			e.printStackTrace();
		} catch (FTPListParseException e) {
			e.printStackTrace();
		}
	}

}