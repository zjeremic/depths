// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.ui;

import java.awt.HeadlessException;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JOptionPane;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.json.service.DepthsJSONUser;
import depths.json.service.ValidURL;

public class ErrorOpenMenu {

	private static boolean checkUserName() {
		if (DepthsConfiguration.getString(DepthsArgo.KEY_USER_USERNAME).equals(
				"")) {
			JOptionPane.showMessageDialog(null,
					"Please enter user name (DEPTHS->Depths Settings)!");
			return false;
		} else {
			return true;
		}

	}

	private static boolean checkUserPassword() {
		if (DepthsConfiguration.getString(DepthsArgo.KEY_USER_PASSWORD).equals(
				"")) {
			JOptionPane.showMessageDialog(null,
					"Please enter password (DEPTHS->Depths Settings)!");
			return false;
		} else {
			return true;
		}
	}

	private static boolean checkWS() {
		if (DepthsConfiguration.getString(DepthsArgo.KEY_WS).equals("")) {
			JOptionPane.showMessageDialog(null,
					"Please enter URL Web Service (DEPTHS->Depths Settings)!");
			return false;
		} else {
			return true;
		}
	}

	private static boolean checkAccessUser() {
		if (DepthsJSONUser.checkAccessUser(
				DepthsConfiguration.getString(DepthsArgo.KEY_USER_USERNAME),
				DepthsConfiguration.getString(DepthsArgo.KEY_USER_PASSWORD))) {
			return true;
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"Please enter valid user name, password and URL web service (DEPTHS->Depths Settings)!");
			return false;
		}
	}

	public static boolean checkOnlineWS() {
		try {
			if (ValidURL.exists(DepthsConfiguration
					.getString(DepthsArgo.KEY_WS)) == false) {

				JOptionPane.showMessageDialog(null,
						"DEPTHS Web Service is offline!");

				return false;

			} else {
				return true;
			}

		} catch (HeadlessException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean showMessage() {
		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
		String url = wsURL + "/depths/";
		if (ValidURL.exists(url)) {
			if (checkWS()) {
				if (checkUserName()) {
					if (checkUserPassword()) {
						if (checkAccessUser()) {
							return true;
						}

					}
				}
			}
		}

		JOptionPane.showMessageDialog(null, "Web service is offline!");
		// if (checkOnlineWS()) {

		// }
		return false;
	}
}
