// Copyright (c) 2011 Dzenan Strujic, University "Mediterranean" , Podgorica Montenegro

package depths.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.argouml.application.api.GUISettingsTabInterface;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.i18n.DepthsTranslator;
import depths.json.configResources.ConfigURL;
import depths.json.ftp.FtpParameter;
import depths.json.service.DepthsJSONFTP;
import depths.json.service.ValidURL;
import depths.uml.ui.FTPConnectionManager;

public class DepthsSettingsTabWS extends JPanel implements
		GUISettingsTabInterface {

	/**
	 * This is where the user enters full name in settings tab. This information
	 * is stored in the argo.user.properties file.
	 */

	private JTextField link_ws;
	private JButton check;
	private JLabel status;

	DepthsSettingsTabWS() {
		setLayout(new BorderLayout());
		JPanel top = new JPanel();
		top.setLayout(new GridBagLayout());

		GridBagConstraints labelConstraints = new GridBagConstraints();
		labelConstraints.anchor = GridBagConstraints.WEST;
		labelConstraints.gridy = 0;
		labelConstraints.gridx = 0;
		labelConstraints.gridwidth = 1;
		labelConstraints.gridheight = 1;
		labelConstraints.insets = new Insets(2, 20, 2, 4);

		GridBagConstraints fieldConstraints = new GridBagConstraints();
		fieldConstraints.anchor = GridBagConstraints.EAST;
		fieldConstraints.fill = GridBagConstraints.HORIZONTAL;
		fieldConstraints.gridy = 0;
		fieldConstraints.gridx = 1;
		fieldConstraints.gridwidth = 3;
		fieldConstraints.gridheight = 1;
		fieldConstraints.weightx = 1.0;
		fieldConstraints.insets = new Insets(2, 4, 2, 20);

		labelConstraints.gridy = 0;
		fieldConstraints.gridy = 0;
		top.add(new JLabel(DepthsTranslator.localize("tab.label_depths_ws")),
				labelConstraints);
		JTextField j = new JTextField();
		link_ws = j;
		top.add(link_ws, fieldConstraints);

		labelConstraints.gridy = 2;
		fieldConstraints.gridy = 2;
		top.add(new JLabel(""), labelConstraints);
		JLabel jl = new JLabel("");
		status = jl;
		top.add(status, fieldConstraints);

		labelConstraints.gridy = 1;
		fieldConstraints.gridy = 1;
		top.add(new JLabel(""), labelConstraints);
		JButton jb = new JButton(
				DepthsTranslator.localize("tab.label_depths_ws_check"));
		check = jb;

		check.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleSettingsTabSave();
				String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
				// add validate web service url
				wsURL = wsURL.replaceAll(" ", "");
				if (wsURL.endsWith("/")) {
					wsURL = wsURL.substring(0, wsURL.length() - 1);
					link_ws.setText(wsURL);
					DepthsConfiguration.setString(DepthsArgo.KEY_WS,
							link_ws.getText());
					
				}
				String url = wsURL + "/depths/";

				if (ValidURL.existWebService(url)) {
					status.setText("Successfully connected!");
					Initialize.initializeFtpParameters();
					DepthsConfiguration.setString(DepthsArgo.KEY_WS, link_ws.getText());
					ConfigURL.update(DepthsConfiguration.getString(DepthsArgo.KEY_WS));
					System.out.println("SAD JE KEY WS: " + DepthsConfiguration.getString(DepthsArgo.KEY_WS) );
//					FtpParameter ftp = DepthsJSONFTP.readFtpParameter();
					// FTPConnectionManager.createStructureDirectory(ftp.getFtpLocationImages());
					// FTPConnectionManager.createStructureDirectory(ftp.getFtpLocationProjects());

				} else {
					status.setText("Not available!");
				}
			}
		});
		top.add(check, fieldConstraints);

		add(top, BorderLayout.NORTH);

	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabRefresh()
	 */
	public void handleSettingsTabRefresh() {
		link_ws.setText(DepthsConfiguration.getString(DepthsArgo.KEY_WS));

	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabSave()
	 */
	public void handleSettingsTabSave() {
		DepthsConfiguration.setString(DepthsArgo.KEY_WS, link_ws.getText());
		
	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabCancel()
	 */
	public void handleSettingsTabCancel() {
		handleSettingsTabRefresh();
	}

	/**
	 * @see org.argouml.ui.GUISettingsTabInterface#handleResetToDefault()
	 */
	public void handleResetToDefault() {
		// Do nothing - these buttons are not shown.
	}

	/**
	 * @see GUISettingsTabInterface#getTabKey()
	 */
	public String getTabKey() {
		return "tab.ws";
	}

	/**
	 * @see GUISettingsTabInterface#getTabPanel()
	 */
	public JPanel getTabPanel() {
		return this;
	}

	/**
	 * The UID.
	 */

}
