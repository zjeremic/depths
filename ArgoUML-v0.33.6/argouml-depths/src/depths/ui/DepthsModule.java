// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.ui;

import java.awt.BorderLayout;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JMenuItem;
import javax.swing.JToolBar;

import org.apache.log4j.Logger;

import org.argouml.application.helpers.ResourceLoaderWrapper;

import org.argouml.moduleloader.ModuleInterface;
import org.argouml.ui.ArgoToolbarManager;
import org.argouml.ui.DetailsPane;

import org.argouml.ui.ProjectBrowser;
import org.argouml.ui.cmd.GenericArgoMenuBar;
import org.argouml.uml.ui.ActionOpenProject;
import org.tigris.toolbar.ToolBarFactory;
import org.tigris.toolbar.layouts.DockBorderLayout;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import depths.i18n.DepthsTranslator;
import depths.json.configResources.ConfigURL;
import depths.json.ftp.FtpParameter;
import depths.json.service.DepthsJSONFTP;
import depths.json.service.DepthsJSONUser;
import depths.json.service.ValidURL;
import depths.json.user.User;
import depths.uml.ui.FTPConnectionManager;
import depths.uml.ui.courses.ActionOpenMyCourses;
import depths.uml.ui.openproject.ActionOpenDepthsProject;
import depths.uml.ui.saveproject.ActionSaveDepthsGraphicsInRepository;
import depths.uml.ui.tab.description.DescriptionTab;
import depths.uml.ui.tab.description.DescriptionTabModel;
import depths.uml.ui.tab.solution.SolutionTab;
import depths.uml.ui.tab.solution.SolutionTabModel;
import depths.uml.ui.update.ActionUpdateProject;

/**
 * 
 * 
 * @author Zoran
 */
public class DepthsModule implements ModuleInterface {
	private static final Logger LOG = Logger.getLogger(DepthsModule.class);
	private JToolBar depthsToolbar;
	private SolutionTab solutionTab;
	private DescriptionTab problemDescriptionTab;
	private static JMenuItem menuItemUpdate;

	/**
	 * 
	 * @param type
	 * @return string
	 * @see org.argouml.moduleloader.ModuleInterface#getInfo(int)
	 */
	public String getInfo(int type) {
		switch (type) {
		case DESCRIPTION:
			return "This is the DEPTHS module.";
		case AUTHOR:
			return "Jeremic Zoran";
		case VERSION:
			return "0.0.1";
		}
		return null;
	}

	/**
	 * 
	 * @return boolean
	 * @see org.argouml.moduleloader.ModuleInterface#disable()
	 */
	public boolean disable() {
		return false;
	}

	/**
	 * 
	 * @return boolean
	 * @see org.argouml.moduleloader.ModuleInterface#enable()
	 */
	public boolean enable() {
		ResourceLoaderWrapper.addResourceLocation("/depths/images");
		createDepthsDetailsPane();
		createDepthsMenu();
		createDepthsToolbar();

		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
		String url = wsURL + "/depths/";
		if (ValidURL.exists(url)) {
			initializeFtpParameters();
			initializeUsersParameter();
		}

		return true;
	}

	public void initializeUsersParameter() {
		if (!DepthsConfiguration.getString(DepthsArgo.KEY_USER_REMEMBER)
				.equals("true")) {
			DepthsConfiguration.setString(DepthsArgo.KEY_USER_PASSWORD, "");
			JOptionPane.showMessageDialog(null,
					"Please enter username and password moodle depths!");
		} else {
			if (initializeUserData() == false) {
				DepthsConfiguration.setString(DepthsArgo.KEY_USER_PASSWORD, "");
			}
		}
	}

	public boolean initializeUserData() {
		
		if (Initialize.initializeUserData()==true) {
			return true;
		}
		return false;
		
//		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
//		String userName = DepthsConfiguration
//				.getString(DepthsArgo.KEY_USER_USERNAME);
//		String password = DepthsConfiguration
//				.getString(DepthsArgo.KEY_USER_PASSWORD);
//		if (userName.equals("") || password.equals("")) {
//			return false;
//		}
//		if (wsURL.equals("")) {
//			JOptionPane
//					.showMessageDialog(
//							null,
//							"Web Service is not initialize, please enter link WS.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
//			return false;
//		} else {
//
//			if (DepthsJSONUser.checkAccessUser(userName, password)) {
//
//				User user = DepthsJSONUser.readUser(userName, password);
//				user.setPassword(password);
//
//				if (user != null) {
//
//					if (user.getAccess().equals("true")) {
//						DepthsConfiguration.setString(
//								DepthsArgo.KEY_USER_PASSWORD,
//								user.getPassword());
//						DepthsConfiguration.setString(
//								DepthsArgo.KEY_USER_USERNAME,
//								user.getUserName());
//						DepthsConfiguration.setString(DepthsArgo.USER_URI,
//								user.getUri());
//						return true;
//					} else {
//						LOG.info("enter valid user name and password");
//						JOptionPane.showMessageDialog(null,
//								"Please enter valid user name and password!");
//						return false;
//					}
//
//				} else {
//					JOptionPane
//							.showMessageDialog(
//									null,
//									"Please enter valid WS URL.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
//					return false;
//				}
//
//			}
//			return false;
//		}
	}

	public void initializeFtpParameters() {
		Initialize.initializeFtpParameters();
//		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
//		if (wsURL.equals("")) {
//			JOptionPane
//					.showMessageDialog(
//							null,
//							"Web Service is not initialize, please enter link WS.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
//		} else {
//
//			FtpParameter ftp = DepthsJSONFTP.readFtpParameter();
//			if (ftp != null) {
//				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_HOST,
//						ftp.getFtpUrl());
//
//				DepthsConfiguration.setString(
//						DepthsArgo.KEY_REPOSITORY_USERNAME,
//						ftp.getFtpUsername());
//				DepthsConfiguration.setString(
//						DepthsArgo.KEY_REPOSITORY_PASSWORD,
//						ftp.getFtpPassword());
//				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_PORT,
//						ftp.getFtpPort());
//				DepthsConfiguration.setString(DepthsArgo.KEY_FTP_IMAGES_FOLDER,
//						ftp.getFtpLocationImages());
//				DepthsConfiguration.setString(
//						DepthsArgo.KEY_FTP_PROJECTS_FOLDER,
//						ftp.getFtpLocationProjects());
//
//				FTPConnectionManager.createStructureDirectory(ftp
//						.getFtpLocationImages());
//				FTPConnectionManager.createStructureDirectory(ftp
//						.getFtpLocationProjects());
//
//			} else {
//				JOptionPane
//						.showMessageDialog(
//								null,
//								"Please enter valid WS URL.\nYou should set it in order to use DEPTHS (DEPTHS menu->DEPTHS settings).");
//
//			}
//
//		}
	}

	public SolutionTab getSolutionTab() {
		return solutionTab;
	}

	public DescriptionTab getProblemDescriptionTab() {
		return problemDescriptionTab;
	}

	private void createDepthsDetailsPane() {
		solutionTab = new SolutionTab();
		solutionTab.setEnabled(true);
		((DetailsPane) ProjectBrowser.getInstance().getDetailsPane()).addTab(
				solutionTab, false);
		problemDescriptionTab = new DescriptionTab();
		((DetailsPane) ProjectBrowser.getInstance().getDetailsPane()).addTab(
				problemDescriptionTab, false);

	}

	private void createDepthsMenu() {
		GenericArgoMenuBar mainMenu = (GenericArgoMenuBar) ProjectBrowser
				.getInstance().getJMenuBar();
		JMenu depthsMenu = new JMenu("Depths");
		mainMenu.add(depthsMenu);
		JMenuItem menuItemDepthsSettings = depthsMenu
				.add(new ActionDepthsSettings());
		depthsMenu.add(menuItemDepthsSettings);
		depthsMenu.addSeparator();
		JMenuItem menuItemCourses = depthsMenu.add(new ActionOpenMyCourses());
		depthsMenu.add(menuItemCourses);

		depthsMenu.addSeparator();
		JMenuItem menuItemOpenProject = depthsMenu
				.add(new ActionOpenDepthsProject());
		depthsMenu.add(menuItemOpenProject);
		depthsMenu.addSeparator();
		menuItemUpdate = depthsMenu.add(new ActionUpdateProject());
		menuItemUpdate.setEnabled(false);
		depthsMenu.add(menuItemUpdate);

		JMenuItem menuItemSavePicture = depthsMenu
				.add(new ActionSaveDepthsGraphicsInRepository());
		depthsMenu.add(menuItemSavePicture);

	}

	public static void changeMenuStatus(boolean status) {
		menuItemUpdate.setEnabled(status);
	}

	private void createDepthsToolbar() {
		Collection<Action> toolbarTools = new ArrayList<Action>();
		toolbarTools.add(new ActionOpenProject());
		toolbarTools.add((ProjectBrowser.getInstance().getSaveAction()));
		depthsToolbar = new ToolBarFactory(toolbarTools).createToolBar();
		depthsToolbar.setName(DepthsTranslator.localize("depths.toolbar"));
		depthsToolbar.setFloatable(true);
		final JPanel toolbarBoundary = new JPanel();
		toolbarBoundary.setLayout(new DockBorderLayout());
		// TODO: - should save and restore the last positions of the toolbars
		final String toolbarPosition = BorderLayout.NORTH;
		toolbarBoundary.add(depthsToolbar, toolbarPosition);
		ProjectBrowser.getInstance().getContentPane()
				.add(toolbarBoundary, BorderLayout.NORTH);

		ArgoToolbarManager.getInstance().registerToolbar(depthsToolbar,
				depthsToolbar, 4);
	}

	/**
	 * 
	 * @return boolean
	 * @see org.argouml.moduleloader.ModuleInterface#getName()
	 */
	public String getName() {
		return "DEPTHS Module";
	}

	/**
	 * for test purposes
	 * 
	 * @param s
	 */
	public void printToFile(String s) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(
					"C://_test/ArgoUML_DepthsModule.txt", true));
			out.write(s);
			out.write("\r\n");
			out.close();
		} catch (IOException e) {

		}
	}

	/**
	 * 
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO: Auto-generated method stub

	}

}
