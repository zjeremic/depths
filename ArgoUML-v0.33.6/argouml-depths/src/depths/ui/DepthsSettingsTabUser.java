// $Id: TabDepthsProps.java 50003 2008-04-13 21:58:15Z Zoran $ 
// Copyright (c) 2007-2008 Jeremic Zoran, University of Belgrade

package depths.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import depths.application.api.DepthsArgo;
import depths.configuration.DepthsConfiguration;
import org.argouml.application.api.GUISettingsTabInterface;

import depths.i18n.DepthsTranslator;
import depths.json.service.DepthsJSONUser;
import depths.json.service.ValidURL;
import depths.json.user.User;

/**
 * 
 * 
 * @author Zoran
 */
public class DepthsSettingsTabUser extends JPanel implements
		GUISettingsTabInterface {
	/**
	 * This is where the user enters full name in settings tab. This information
	 * is stored in the argo.user.properties file.
	 */
	private JTextField username;

	/**
	 * This is where the user enters email in settings tab. This information is
	 * stored in the argo.user.properties file.
	 */
	private JPasswordField userPassword;

	private JCheckBox remember;

	private JButton test;

	/**
	 * The constructor.
	 * 
	 */
	DepthsSettingsTabUser() {
		setLayout(new BorderLayout());
		JPanel top = new JPanel();
		top.setLayout(new GridBagLayout());

		GridBagConstraints labelConstraints = new GridBagConstraints();
		labelConstraints.anchor = GridBagConstraints.WEST;
		labelConstraints.gridy = 0;
		labelConstraints.gridx = 0;
		labelConstraints.gridwidth = 1;
		labelConstraints.gridheight = 1;
		labelConstraints.insets = new Insets(2, 20, 2, 4);

		GridBagConstraints fieldConstraints = new GridBagConstraints();
		fieldConstraints.anchor = GridBagConstraints.EAST;
		fieldConstraints.fill = GridBagConstraints.HORIZONTAL;
		fieldConstraints.gridy = 0;
		fieldConstraints.gridx = 1;
		fieldConstraints.gridwidth = 3;
		fieldConstraints.gridheight = 1;
		fieldConstraints.weightx = 1.0;
		fieldConstraints.insets = new Insets(2, 4, 2, 20);

		labelConstraints.gridy = 0;
		fieldConstraints.gridy = 0;
		top.add(new JLabel(DepthsTranslator.localize("tab.label_username")),
				labelConstraints);
		JTextField j = new JTextField();
		username = j;
		top.add(username, fieldConstraints);

		labelConstraints.gridy = 1;
		fieldConstraints.gridy = 1;
		top.add(new JLabel(DepthsTranslator.localize("tab.label_password")),
				labelConstraints);
		JPasswordField j1 = new JPasswordField();
		userPassword = j1;
		top.add(userPassword, fieldConstraints);

		labelConstraints.gridy = 2;
		fieldConstraints.gridy = 2;
		top.add(new JLabel(DepthsTranslator.localize("tab.label_user_remember")),
				labelConstraints);
		JCheckBox c1 = new JCheckBox();
		remember = c1;
		top.add(remember, fieldConstraints);

		labelConstraints.gridy = 3;
		fieldConstraints.gridy = 3;
		top.add(new JLabel(""), labelConstraints);
		JButton b1 = new JButton();
		test = b1;
		test.setText(DepthsTranslator.localize("tab.label_user_test"));
		final JLabel status = new JLabel();
		test.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				handleSettingsTabSave();

				if (test() == true) {
					status.setText("The user has access!");
				} else {
					status.setText("The user does not have access!");
				}

			}
		});
		top.add(test, fieldConstraints);
		fieldConstraints.gridy = 4;
		top.add(status, fieldConstraints);
		add(top, BorderLayout.NORTH);
	}

	// Copyright (c) 2011 Dzenan Strujic, University Mediteranian, Podgorica
	// Montenegro
	public boolean test() {
		String wsURL = DepthsConfiguration.getString(DepthsArgo.KEY_WS);
		// if (ValidURL.exists(wsURL)) {

		String userName = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_USERNAME);
		String password = DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_PASSWORD);
		if (userName.equals("") || password.equals("")) {
			return false;

		}

		String url = wsURL + "/depths/";
		System.out.println("URL NA TEST JE: " + url);
		if (ValidURL.exists(url)) {

			if (DepthsJSONUser.checkAccessUser(userName, password)) {

				User user = DepthsJSONUser.readUser(userName, password);
				user.setPassword(password);

				if (user != null) {

					// DepthsConfiguration.setString(DepthsArgo.KEY_REPOSITORY_USERNAME,
					// user.getFtpUsername());
					//
					if (user.getAccess().equals("true")) {
						return true;
					} else {
						return false;
					}

				}
			}
		}
		// }
		return false;
	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabRefresh()
	 */

	public void handleSettingsTabRefresh() {
		username.setText(DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_USERNAME));
		userPassword.setText(DepthsConfiguration
				.getString(DepthsArgo.KEY_USER_PASSWORD));

		if (DepthsConfiguration.getString(DepthsArgo.KEY_USER_REMEMBER).equals(
				"true")) {
			remember.setSelected(true);
		} else {
			remember.setSelected(false);
		}

	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabSave()
	 */
	public void handleSettingsTabSave() {
		DepthsConfiguration.setString(DepthsArgo.KEY_USER_USERNAME,
				username.getText());
		// UserModelHandler.getUserDataFromRepository();
		DepthsConfiguration.setString(DepthsArgo.KEY_USER_PASSWORD,
				userPassword.getText());

		if (test() == true) {
			User user = DepthsJSONUser.getUser();
			Initialize.initializeUserData();
			DepthsConfiguration.setString(DepthsArgo.USER_URI, user.getUri());
		}

		if (remember.isSelected()) {
			DepthsConfiguration.setString(DepthsArgo.KEY_USER_REMEMBER, "true");
		} else {
			DepthsConfiguration
					.setString(DepthsArgo.KEY_USER_REMEMBER, "false");
		}
	}

	/**
	 * @see GUISettingsTabInterface#handleSettingsTabCancel()
	 */
	public void handleSettingsTabCancel() {
		handleSettingsTabRefresh();
	}

	/**
	 * @see org.argouml.ui.GUISettingsTabInterface#handleResetToDefault()
	 */
	public void handleResetToDefault() {
		// Do nothing - these buttons are not shown.
	}

	/**
	 * @see GUISettingsTabInterface#getTabKey()
	 */
	public String getTabKey() {
		return "tab.user";
	}

	/**
	 * @see GUISettingsTabInterface#getTabPanel()
	 */
	public JPanel getTabPanel() {
		return this;
	}

	/**
	 * The UID.
	 */
	private static final long serialVersionUID = -742258688091914619L;

}
