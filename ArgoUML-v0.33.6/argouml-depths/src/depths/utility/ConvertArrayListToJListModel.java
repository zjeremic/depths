package depths.utility;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;

public class ConvertArrayListToJListModel {

	public static DefaultListModel convert(List elements) {
		if (elements == null) {
			return new DefaultListModel();
		} else {
			DefaultListModel model = new DefaultListModel();
			for (int i = 0; i < elements.size(); i++) {
				model.addElement(elements.get(i));
			}
			return model;
		}
	}
}
