package depths.utility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class CheckPort {

	public static boolean check(String host, int port) {
		try {
			InetAddress iAddr = InetAddress.getByName(host);
			SocketAddress sockAddr = new InetSocketAddress(iAddr, port);
			Socket sock = new Socket();
			sock.connect(sockAddr, 3000);
		} catch (UnknownHostException e) {
			return false;
		} catch (SocketTimeoutException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
}
