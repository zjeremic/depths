<?php  /// Moodle Configuration File 
 
unset($CFG);
 
$CFG = new stdClass();
 
$CFG->dbtype    = 'mysql';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodledepths';
$CFG->dbuser    = 'zoran';
$CFG->dbpass    = 'zoran';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';
 
$CFG->wwwroot   = 'http://localhost/moodleDEPTHS';
$CFG->dirroot   = '/var/www/moodleDEPTHS';
$CFG->dataroot  = '/home/zoran/moodledata';
$CFG->admin     = 'admin';
 
$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

//$CFG->passwordsaltmain = '+tW2a#MgG{JW1e?xTk<X_^rVCQYQc%';

$CFG->resturl='http://localhost:9010/depths/rest/';
$CFG->imagerepository='http://localhost/depths_repository/images';
 $CFG->oposurl='http://147.91.128.71:9090/opos/';
 
// $CFG->oposurl='http://localhost:9091/OPOS/';
//$CFG->oposurl='http://147.91.128.71:9090/opos/';
require_once("$CFG->dirroot/lib/setup.php");
 
// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>
