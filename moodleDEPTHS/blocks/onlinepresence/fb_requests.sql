/*
SQLyog Community Edition- MySQL GUI v7.11 
MySQL - 5.0.67-community : Database - moodle
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`moodle` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `moodle`;

/*Table structure for table `fb_requests` */

CREATE TABLE `fb_requests` (
  `fb_user_id` bigint(20) unsigned NOT NULL,
  `request_id` bigint(20) NOT NULL,
  `outstanding` tinyint(1) NOT NULL default '1',
  UNIQUE KEY `unique_pair` (`fb_user_id`,`request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;