<?php 
$options = array('0'=>get_string('never'), '30000'=>'30', '60000'=>'60', '120000'=>'120', '180000'=>'180', '2400000'=>'240',
				 '300000'=>'300');

$settings->add(new admin_setting_configselect('block_onlinepresence_refresh', get_string('refreshevery', 'block_onlinepresence'),
                   get_string('refresheveryseconds', 'block_onlinepresence'), 'all', $options));
?>