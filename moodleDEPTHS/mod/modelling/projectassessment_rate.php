
<head>

<script type="text/javascript" src="../../depths/lib/yoxview/yoxview-init.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery.noConflict();
	jQuery(".yoxview").yoxview( {
		backgroundColor: '#808080'}	);
});
</script>
</head>
<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

require_once("../../config.php");
require_once("lib.php");
require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
require_once($CFG->dirroot.'/depths/setup/depths_config.php');
require_once($CFG->dirroot."/depths/rest/curl_client.php");
require_once($CFG->dirroot.'/depths/mapper/utility.php');

$id = required_param('id');
$t  = required_param('t');
$selectedprojecturi = required_param('selectedproject');

$submissionid=required_param('submissionid');

$criteria  = optional_param('criteria');
$comment  = optional_param('comment');
$overcomm = optional_param('overcomm');
$editmy = optional_param('editmy');

if ($id) {
	if (! $cm = get_record("course_modules", "id", $id)) {
		error("Course Module ID was incorrect");
	}
	if (! $course = get_record("course", "id", $cm->course)) {
		error("Course is misconfigured");
	}
	if (! $project = get_record("modelling", "id", $cm->instance)) {
		error("Course module is incorrect");
	}
} else {
	if (! $project = get_record("modelling", "id", $submissionid)) {
		error("Course module is incorrect");
	}
	if (! $course = get_record("course", "id", $project->course)) {
		error("Course is misconfigured");
	}
	if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
		error("Course Module ID was incorrect");
	}
}

$projectdata = get_record ("modelling_projects", "id", $submissionid);
require_login($course->id);

add_to_log($course->id, "modelling", "Assessment rate", "view.php?id=$id", "$cm->instance");

/// Print the page header

$navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";


print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
navmenu($course));


$taskdata = get_record ("modelling_tasks", "id", $t);

//--------Add Criteria-------------//

echo '<form  method="post" action="projectassessment_rate.php?id='.$id.'&t='.$t.'"> ';

echo '<input type="hidden" name="submissionid" value="'.$submissionid.'" />';
 
if ($criteria) {
	if ($editmy) {

		set_field ("modelling_as_rating", "overallcomments", $overcomm, "submissionid", $submissionid, "userid", $USER->id, "assessmentid", $t);
			
		$oldratingsids = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE selectedproject='".$selectedprojecturi."' and submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");

		foreach ($criteria as $criteriakey => $criteriavalue) {


			foreach ($oldratingsids as $oldratingsid) {

				if ($oldratingsid->criteriaid == $criteriakey) {

					set_field ("modelling_as_crit_rating", "rate", $criteriavalue, "id", $oldratingsid->id);
					set_field ("modelling_as_crit_rating", "comment", $comment[$criteriakey], "id", $oldratingsid->id);

				}

			}

		}

			
	}
	else
	{
		$dataArray=array();
			$dataArray['action']="insert";
		$rating = new object;
		$rating->selectedproject = $selectedprojecturi;
		$rating->assessmentid = $t;
		$task=get_record("modelling_tasks","id",$rating->assessmentid);
		$rating->assessmenturi=$task->taskuri;
	 
		$rating->userid = $USER->id;
		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$USER->id,'domainconcept','User');
		$rating->useruri=$useruri;
		$rating->overallcomments = $overcomm;
		$rating->time = time();
		$rating->submissionid=$submissionid;
	 
		$submissionuri=get_condition_value("modelling_projects",'submittinguri','id', $submissionid);
	 
 		$rating->submissionuri=$submissionuri;
 				
		$ratingid=insert_record ("modelling_as_rating", $rating);
		$rating->id=$ratingid;
		 

		$dataArray['rating']=json_encode($rating);
		$dataArray['tablename']="modelling_as_rating";

		foreach ($criteria as $criteriakey => $criteriavalue) {
	 
			$ordNumb=$ordNumb+1;
				
			$criteria = new object;
			$criteria->selectedproject = $selectedprojecturi;
			$criteria->assessmentid = $t;
			$task=get_record("modelling_tasks","id",$criteria->assessmentid);
				 
			$assessmenturi=$task->taskuri;
			$criteria->assessmenturi=$assessmenturi;
			$criteria->userid = $USER->id;
			
		//	$citeria->useruri=urlencode($useruri);
			$criteria->criteriaid = $criteriakey;
			$criteriauri=get_condition_value("modelling_crit_name",'scaleuri','id',$criteria->criteriaid);
			$criteria->criteriauri=$criteriauri;
			$criteria->rate = $criteriavalue;
			$ratinguri=get_condition_value("modelling_crit_rating",'scaleitemuri','id',$criteria->rate,'criteriaid',$criteria->criteriaid);
			 $criteria->ratinguri= $ratinguri;
			//$criteria->ratinguri=urlencode($ratinguri);
			$criteria->comment = $comment[$criteriakey];
			$criteria->time = time();
			$criteria->submissionid=$submissionid;
			
			$criteria->submissionuri= $submissionuri;
			$criteriaid=insert_record ("modelling_as_crit_rating", $criteria);
			$criteria->id=$criteriaid;
				
			$criterias[$ordNumb-1]=json_encode($criteria);
	 
		}
	 
		$dataArray['criterias']=json_encode($criterias);
		$url=$CFG->resturl."insert/table";

		$data=json_encode($dataArray);
		$response=curl_call($url,'POST', $data);
 
		$datatochange=json_decode($response, true);
		foreach($datatochange as $key=>$value){
			print_message("KT-2","assessment_timing");
			$operation=$value["operation"];
			if($operation=="urimapping"){
			$urimapping=new object;
			$urimapping->domainconcept=$value['domainconcept'];
			$urimapping->instanceid=$value['instanceid'];
			$urimapping->uri=$value['uri'];
			print_message("***insert_table_to_rdf table:".$urimapping->domainconcept." instance:".$urimapping->instanceid.
					" uri:".$urimapping->uri,"projectassessment");
 
			insert_record("modelling_urimapping", $urimapping);
 
			}else if($operation=="setfield"){
			 
			foreach($value as $k=>$v){
			 
					if($k!="operation"){
						print_message("set field: modelling_as_rating "." k:".$k." v:".$v." for id=".$rating->id,"projectassessment");
						set_field("modelling_as_rating", $k, $v, "id", $rating->id);
					}
				}
	 
			}else if ($operation=="setotherfield"){
		 
			foreach($value as $k=>$v){
				 
					if($k!="operation"){
						$crit_ratingid=$value["criteriaratingid"];
						print_message("set other field: modelling_as_crit_rating "." k:".$k." v:".$v." for id=".$crit_ratingid,"projectassessment");
						set_field("modelling_as_crit_rating", $k, $v, "id", $crit_ratingid);
					}
				}
 
			}
		}
	}
 
	redirect ("projectassessment.php?id=".$id."&t=".$t);
 
}

echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div><hr /><br />';

echo '<table >';

echo '<tr>
    <td>';
echo "
<script type=\"text/javascript\">
function getBodyScrollTop()
{
  return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}
</script>

<style type=\"text/css\">
    #videolaer
    {
        left: 20px;
        top: 200px;
        height: 360px;
        width: 400px;
        position: fixed;
        //position: absolute;
        overflow-y: auto;
        top: expression(
                    getBodyScrollTop() + 200 +\"px\"
                );
    }
</style>
        ";

$project = get_record ("modelling_projects", "user_groupid", $projectdata->user_groupid, "taskid", $taskdata->tasktoassess,"submissionid",$submissionid);

$problem= get_record("modelling", "id", $cm->instance);
print_box_start();
echo '<h3>'.get_string ('projectdetails', 'modelling').'</h3>';

echo '<table cellpadding="5" cellspacing="0" >';
echo '<tr><td><b>'.get_string ('projectdescription', 'modelling').'</b></td>';
echo '<td>'.$problem->description.'</td></tr>';

if ($problem->useprojectdates == "true") {

	echo '<tr><td><b>'.get_string ('projectstartdate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $problem->timeopen).'</td></tr>';
	echo '<tr><td><b>'.get_string ('projectenddate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $problem->timeclose).'</td></tr>';
	echo '<tr><td><b>'.get_string ('lateenddate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $problem->timelate).'</td></tr>';
}

if ($problem->filename != "none") {

	echo '<tr><td><b>'.get_string ('projectfile', 'modelling').'</b></td>';
	echo '<td><a href="'.$CFG->wwwroot.'/file.php/1/'.str_replace ("__", "_", str_replace (" ", "_", $problem->filename)).'">view file</a></td></tr>';
}
echo '</table>';

if($selectedprojecturi!="none"){
	if ($selectedprojecturi!=""){

		$response=getProjectByUri($selectedprojecturi);
		$proj=$response[0];

		$projectData=$proj['project'];
		$solutionData=$proj['solution'];
		$diagrams=$proj['diagrams'];
		$projectName=$projectData['title'];
		$createdBy=$projectData['creatorname'];
		$dateC=$projectData['datetimecreatedE'];
		$dateCreated=modelling_dateformat ($dateC/1000);
		$projectDescription=$solutionData['description'];

		if ($projectDescription==""){
			$projectDescription=get_string("notextprovided","depths");
		}
		$designRules=$solution["hasDesignRules"];
		if($designRules==""){
			$designRules=get_string("notextprovided","depths");
		}

		$designConstraints=$solution["hasDesignConstraints"];
		if($designConstraints==""){
			$designConstraints=get_string("notextprovided","depths");
		}

		$additional_requirements=$solution["hasAdditionalRequirements"];
		if($additional_requirements==""){
			$additional_requirements=get_string("notextprovided","depths");
		}
			
		$consequences=$solution["hasConsequences"];
		if($consequences==""){
			$consequences=get_string("notextprovided","depths");
		}
			
		$pros=$solution["hasPros"];
		if($pros==""){
			$pros=get_string("notextprovided","depths");
		}

		$cons=$solution["hasCons"];
		if($cons==""){
			$cons=get_string("notextprovided","depths");
		}
	 
		include("showProject.php");
	}
}

print_box_end();

echo'</td>
    <td rowspan="2" valign="top" >';

echo '<b>'.get_string("rating", "modelling").'</b><br />';

if ($criterias = get_records("modelling_crit_name", "assessmentid", $t, 'id')) {

	echo '<input type="hidden" name="selectedproject" value="'.$selectedprojecturi.'" />';


	if ($editmy) {
		$oldratings = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE selectedproject='".$selectedprojecturi."' and submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");
		foreach ($oldratings as $oldrating) {
			$oldratings2[$oldrating->criteriaid] = $oldrating;
		}
		echo '<input type="hidden" name="editmy" value="1" />';
	}

	foreach ($criterias as $criteria) {

		echo '<table cellpadding="0" cellspacing="0" width="220px" ><tr><td><br /><b>'.$criteria->name.'</b></td></tr>';
		if ($ratings = get_records("modelling_crit_rating", "criteriaid", $criteria->id, 'id')) {
			foreach ($ratings as $rating) {
				if ($bgcolor == "#ffffcc" || empty($bgcolor)) {
					$bgcolor = "#ffffff";
				}
				else
				{
					$bgcolor = "#ffffcc";
				}
				echo '<tr><td bgcolor="'.$bgcolor.'"><input type="radio" name="criteria['.$criteria->id.']" value="'.$rating->id.'" ';
				if ($editmy && ($rating->id == $oldratings2[$criteria->id]->rate)) {
					echo 'checked';
				}
				echo ' /> <small>'.$rating->name.'</small></td></tr>';
			}
		}

		if ($taskdata->peercommenting == "yes") {
			echo '<tr><td> <small>'.get_string("criterianamecomments", "modelling").'</small><br /><textarea name="comment['.$criteria->id.']" rows="2" cols="40">';
			if ($editmy) {
				echo $oldratings2[$criteria->id]->comment;
			}
			echo '</textarea><br /> </td></tr>';
		}
		else
		{
			echo '<tr><td> </td></tr>';
		}

		echo '</table>';

	}
	if ($taskdata->peercommenting == "yes") {
		echo '<tr> <td> <br /><small><b>'.get_string("overallcomments", "modelling").'</b></small><br /><textarea name="overcomm" rows="5" cols="80">';
		if ($editmy) {
			$oldratings = get_record_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_rating WHERE selectedproject='".$selectedprojecturi."' and submissionid='".$submissionid."' and userid='".$USER->id."' and assessmentid='".$t."'");
			echo $oldratings->overallcomments;
		}
		echo '</textarea> </td></tr>';
	}

	echo '<tr><td align="center"><br/><input type="submit" name="submit" value="Save" /></td></tr>';


}

echo '</form>';

echo '</td></tr></table>';

echo '</td>
  </tr>
  <tr>
   
    <td>';




echo '</td><td valign="top" width="220">';


echo '</td>
  </tr>';
echo '</table>';



print_footer($course);
?>