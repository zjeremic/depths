<?php
require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_modelling_mod_form extends moodleform_mod {

    function definition() {

        global $CFG, $course, $form;
        $update = optional_param('update', 0, PARAM_INT);
        
        $cm = get_record("course_modules", "id", $update);
        $project = get_record("modelling", "id", $cm->instance);
        $mform    =& $this->_form;
 
//-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));
        
        $mform->addElement('html', "<script language=\"javascript\">
function show_add_params() {
	document.getElementById('add_params').style.display='block';
}

function hide_add_params() {
	document.getElementById('add_params').style.display='none';
}
</script>");
     $mform->addElement('text', 'name', get_string('act_projecttitle', 'modelling'), array('size'=>'60'));
        $mform->setType('name', PARAM_TEXT);
        $mform->setHelpButton('name', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
        $mform->addRule('name', null, 'required', null, 'client');

        $mform->addElement('htmleditor', 'description', get_string('act_projectdescr', 'modelling'));
        $mform->setType('description', PARAM_RAW);
        $mform->setHelpButton('description', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
        $mform->addRule('description', get_string('required'), 'required', null, 'client');
        
        
        $mform->addElement('checkbox', 'useprojectdates', get_string('act_usedates', 'modelling'), '', array('onclick' => "if (this.checked) {show_add_params();} else {hide_add_params();}"));
      
        if ($form->useprojectdates == "false") {
        
            $mform->setDefault('useprojectdates', 0); 
        }

        $mform->addElement('html', '<div id="add_params">');
        
        $mform->addElement('date_selector', 'timeopen', get_string('act_startdate', 'modelling'));
        $mform->setHelpButton('timeopen', array('projstartdate', '', 'modelling', true, false, '', true));
        
        $mform->addElement('date_selector', 'timeclose', get_string('act_enddate', 'modelling'));
        $mform->setHelpButton('timeclose', array('projenddate', '', 'modelling', true, false, '', true));
        $mform->setDefault('timeclose', mktime(0,0,0,date("m") + 3,date("d"),date("Y"))); 
        
        $mform->addElement('date_selector', 'timelate', get_string('act_laterenddate', 'modelling'));
        $mform->setDefault('timelate', mktime(0,0,0,date("m") + 3,date("d"),date("Y"))); 
        $mform->setHelpButton('timelate', array('projlateenddate', '', 'modelling', true, false, '', true));
        
        $mform->addElement('html', '</div>');
        
        $options=array();
        for ($i=100; $i >=0; $i--) {
            $options[$i] = $i;
        }
        $mform->addElement('select', 'totalpoints', get_string('act_totalpoints', 'modelling'), $options);
        $mform->setHelpButton('totalpoints', array('points', '', 'modelling', true, false, '', true));
        
        
        
        $teachersgroupstext = "Group projects (teacher-created groups course-wide)";

        if (function_exists('groups_get_all_groups')) {
            if (!groups_get_all_groups($course->id)) {
                $teachersgroupstext = $teachersgroupstext.' !You have not added course groups yet!';
            }
        }
        
        //$mform->addElement('select', 'projecttype', get_string('act_projecttype', 'modelling'), Array ("individual" => "Individual Projects", "teacher groups" => $teachersgroupstext, "teacher groups project" => "Group projects (teacher-created groups in one project only)", "student groups" => "Group projects (student-created groups)"));
        $mform->addElement('select', 'projecttype', get_string('act_projecttype', 'modelling'), Array ("individual" => "Individual Projects", "teacher_groups" => $teachersgroupstext, "no_groups" => "All students in one group"));
        
        $mform->addElement('file', 'attachment', get_string('act_uploadfile', 'modelling'));
        $mform->addElement('text', 'selectedproject',get_string('selectedproject', 'modelling'), array('size'=>'60'));
         $selectModelButton=$mform->addElement('button','modelSelect',get_string('modelSelectButtonTitle', 'modelling'));
        
         $options = 'menubar=1,toolbar=1,  location=1,scrollbars,resizable,width=1000,height=800';
         $calledFrom='selectModel';
         $url="/depths/student/projects/projects_view.php?reload=1&calledFrom=".$calledFrom;
                $buttonattributes = array('title'=>get_string('modelSelectButtonTitleToolTip', 'modelling'), 'onClick'=>"openpopup('$url', '"
                              . $selectModelButton->getName()."', '$options', 0);");
            $selectModelButton->updateAttributes($buttonattributes);
            $mform->addElement('text', 'selectedconcepts');
            $selectRelatedConcepts=$mform->addElement('button','modelSelect',get_string('selectRelatedConceptsButtonTitle', 'modelling'));
            
            $options = 'menubar=1,toolbar=1,  location=1,scrollbars,resizable,width=1000,height=800';
            $calledFrom='selectModel';
            $url="/depths/student/projects/concepts_select.php?reload=1&calledFrom=".$calledFrom."&problemuri=".urlencode($project->designproblemuri);
            //$url=$CFG->wwwroot.'/depths/tag/add_tags.php?ideaUri='.urlencode($idea->ideauri);
                  /// $buttonattributes = array('title'=>get_string('selectRelatedConceptsButtonToolTip', 'modelling'), 'onClick'=>"return showDEPTHSModalDialog(\''.$url.'\', $(this)); return false;");
            $buttonattributes = array('title'=>get_string('modelSelectButtonTitleToolTip', 'modelling'), 'onClick'=>"openpopup('$url', '"
            . $selectModelButton->getName()."', '$options', 0);");
            $selectRelatedConcepts->updateAttributes($buttonattributes);
            
                
        if ($form->useprojectdates == "false" || empty($form->useprojectdates)) {
            $form->useprojectdates = 0;
            $mform->addElement('html', "<script language=\"javascript\">hide_add_params();</script>");
        }
        else
        {
            $mform->addElement('html', "<script language=\"javascript\">show_add_params();</script>");
        }
       
        //-------------------------------------------------------------------------------
        //ova polja treba dodati u tabelu modelling
        $mform->addElement('header', '', get_string('grade'));
        $mform->addElement('checkbox', 'userating', get_string('allowratings', 'modelling') , get_string('ratingsuse', 'modelling'));
     
        if ($form->userating == "false") {
        	 
            $mform->setDefault('userating', 0); 
        }
        $options=array();
        $options[2] = get_string('ratingonlyteachers', 'modelling', moodle_strtolower($COURSE->teachers));
        $options[1] = get_string('ratingeveryone', 'modelling');
        $mform->addElement('select', 'assessed', get_string('users'), $options);
        $mform->disabledIf('assessed', 'userating');

        $mform->addElement('modgrade', 'scale', get_string('grade'), false);
        $mform->disabledIf('scale', 'userating');

        $mform->addElement('checkbox', 'ratingtime', get_string('ratingtime', 'modelling'));
        $mform->disabledIf('ratingtime', 'userating');
        
        $mform->addElement('date_time_selector', 'assesstimestart', get_string('from'));
        $mform->disabledIf('assesstimestart', 'userating');
        $mform->disabledIf('assesstimestart', 'ratingtime');

        $mform->addElement('date_time_selector', 'assesstimefinish', get_string('to'));
        $mform->disabledIf('assesstimefinish', 'userating');
        $mform->disabledIf('assesstimefinish', 'ratingtime');

//-------------------------------------------------------------------------------

        $this->standard_hidden_coursemodule_elements();
        
        $mform->addElement('hidden', 'visible', 1);
        $mform->setType('visible', PARAM_INT);
        
        //grouping 
        //-------------------------------------------------------------------------------
        $features = new stdClass;
        $features->groups = true;
        $features->groupings = true;
        $features->groupmembersonly = true;
        $this->standard_coursemodule_elements($features);
        //-------------------------------------------------------------------------------
        
        
        $this->add_action_buttons();
    }

 function data_preprocessing(&$default_values){
        if (empty($default_values['scale'])){
            $default_values['assessed'] = 0;
        }        

        if (empty($default_values['assessed'])){
            $default_values['userating'] = 0;
            $default_values['ratingtime'] = 0;
        } else {
            $default_values['userating'] = 1;
            $default_values['ratingtime']=
                ($default_values['assesstimestart'] && $default_values['assesstimefinish']) ? 1 : 0;
        }
    }


}
?>