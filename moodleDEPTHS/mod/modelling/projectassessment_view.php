<head>

<script type="text/javascript" src="../../depths/lib/yoxview/yoxview-init.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery.noConflict();
	jQuery(".yoxview").yoxview( {
		backgroundColor: '#808080'}	);
});
</script>
</head>
<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');
    require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
    require_once($CFG->dirroot.'/depths/setup/depths_config.php');
require_once($CFG->dirroot."/depths/rest/curl_client.php");

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $submissionid=required_param('submissionid');
    $sort = optional_param('sort', 'criteria', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 
    
    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    if (! $submission = get_record("modelling_projects", "id", $submissionid)) {
            error("Submission id is incorrect");
        }
    } else {
        if (! $submission = get_record("modelling_projects", "id", $submissionid)) {
            error("Submission id is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "Assessment viewing", "projectassessment_view.php?id=".$id."&t=".$t."$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  

    $taskdata = get_record ("modelling_tasks", "id", $t);
    $submissiondata = get_record ("modelling_projects", "id", $submissionid);
    
    $fullpath = $CFG->wwwroot . "/file.php/" . modelling_get_file_path_www ($cm->course) . "/" . modelling_return_file_name ($file->file);
    $filename = modelling_return_file_name ($file->file);
    
    
 
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div><hr /><br />';
    
       //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);

    $row[] = new tabobject('return', "projectassessment.php?id=".$id."&t=".$t, get_string('returntoassessment', 'modelling'));
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    
    echo "<hr /><br />";
    
    print_box_start();
    echo '<table cellpadding="5" cellspacing="0" >';
    echo '<tr><td><b>'.get_string ('suggestedby', 'modelling').'</b></td>';
    echo '<td>'.modelling_return_user_groupid_name ($project, $submission->user_groupid).'</td></tr>';
    echo "</table>";
       if ($taskdata->peerviewing == "yes") {  
 if($submission->selectedproject !="none"){
 
 $selectedprojecturi=$submission->selectedproject;

		$response=getProjectByUri($selectedprojecturi);
		$proj=$response[0];

		$projectData=$proj['project'];
		$solutionData=$proj['solution'];
		$diagrams=$proj['diagrams'];
		$projectName=$projectData['title'];
		$projectDescription=$solutionData['description'];

		if ($projectDescription==""){
			$projectDescription=get_string("notextprovided","depths");
		}
		$designRules=$solution["hasDesignRules"];
		if($designRules==""){
			$designRules=get_string("notextprovided","depths");
		}

		$designConstraints=$solution["hasDesignConstraints"];
		if($designConstraints==""){
			$designConstraints=get_string("notextprovided","depths");
		}

		$additional_requirements=$solution["hasAdditionalRequirements"];
		if($additional_requirements==""){
			$additional_requirements=get_string("notextprovided","depths");
		}
			
		$consequences=$solution["hasConsequences"];
		if($consequences==""){
			$consequences=get_string("notextprovided","depths");
		}
			
		$pros=$solution["hasPros"];
		if($pros==""){
			$pros=get_string("notextprovided","depths");
		}

		$cons=$solution["hasCons"];
		if($cons==""){
			$cons=get_string("notextprovided","depths");
		}
			
		foreach($diagrams as $k=>$diagram){
			$cDiagramFileUrl=$diagram['fileUrl'];
			$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
			list($width, $height, $type, $attr) = getimagesize($imageLink);
			if($width>100){
			$imageHref=depths_diagram_popup_window_link($imageLink,"submited","View");
			}
		}
	  
		include("showProject.php");
	}
    }
    
   
    print_box_end();
    
   
    echo "<hr /><br />";
    
    $titlesarray = Array ('Criteria'=>'criteria', 'Teacher Rating'=>'ratingbyteacher', 'Students\' Rating (average)'=>'averrating', 'Self Rating'=>'selfrating');

    $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'projectassessment_view.php?id='.$id.'&t='.$t);
    $table->align = array ("left", "center", "center", "center");
    $table->width = "800";
    
    $data = modelling_get_assessment_criterias ($t,  $project, $submissiondata);
    foreach ($data as $datakey => $datavalue) {
        $table->data[] = array ($datakey, $datavalue['averteacher'], $datavalue['averstudent'], $datavalue['self']);
    }
    
    $table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);
    
    if ($table) {
        print_table($table);
    }
    
    if ($taskdata->peercommenting == "yes") { 
        $overcomms = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_rating WHERE  assessmentid='".$t."' and submissionid='".$submissionid."'");
        if ($overcomms) {
            echo '<br /><hr /><br />';
            print_simple_box_start('center', '100%', '#ffffff', 10);
            echo '<b>'.get_string ('overallcomments', 'modelling').'</b><br /><br />';
            foreach ($overcomms as $overcomm) {
                echo "\"" . $overcomm->overallcomments . "\"<br /><br />";
            }
            print_simple_box_end();
        }
    
        $criterias = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE assessmentid='".$t."' and submissionid='".$submissionid."' ORDER BY id");
    
        if ($criterias) {
            echo '<br /><hr /><br />';
            print_simple_box_start('center', '100%', '#ffffff', 10);
            $criterianames = get_records ("modelling_crit_name", "assessmentid", $t);
    
            foreach ($criterianames as $criterianame) {
                echo '<b>'.$criterianame->name.'</b><br /><br />';
                $ratings = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE criteriaid='".$criterianame->id."' and assessmentid='".$t."' and submissionid='".$submissionid."' ORDER BY id");
                foreach ($ratings as $rating) {
                    echo "\"" . $rating->comment . "\"<br /><br />";
                }
            }
            print_simple_box_end();
        }
    }
    
    print_footer($course);
    
?>