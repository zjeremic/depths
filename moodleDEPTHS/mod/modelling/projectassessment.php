<head>
		
	<script type="text/javascript" src="../../depths/lib/yoxview/yoxview-init.js"></script>
		<script type="text/javascript">

			$(document).ready(function(){ 
			    $(".yoxview").yoxview( {backgroundColor: '#808080'});
			    $("#yoxviewText").yoxview({ textLinksSelector: "" });
			});
		</script>
	</head>
<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

require_once("../../config.php");
require_once("lib.php");
require_once("dplib.php");
require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot."/depths/rest/curl_client.php");
//$depths_config=$CFG->dirroot.'/depths/setup/depths_config.php';
//require_once($depths_config);
require_once($CFG->dirroot.'/depths/mapper/utility.php');
 
// require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
//  require_once($CFG->dirroot.'/depths/setup/depths_config.php');

$id = required_param('id');
$t  = required_param('t');
$v  = optional_param('v');
$criterianame  = optional_param('criterianame');
$ratingvalue  = optional_param('ratingvalue');
$templatename  = optional_param('templatename');
$fromtemplate  = optional_param('fromtemplate');
$submit  = optional_param('submit');
$delcriteria  = optional_param('delcriteria');
$upcriteria  = optional_param('upcriteria');
$upcriteriaid  = optional_param('upcriteriaid');
$deletemy  = optional_param('deletemy');

$sort = optional_param('sort', 'studentsgroups', PARAM_ALPHA);
$orderby = optional_param('orderby', 'ASC', PARAM_ALPHA);

 
if ($id) {
	 
	if (! $cm = get_record("course_modules", "id", $id)) {
		error("Course Module ID was incorrect");
	}
	if (! $course = get_record("course", "id", $cm->course)) {
		error("Course is misconfigured");
	}
	if (! $modelling = get_record("modelling", "id", $cm->instance)) {
		error("Course module is incorrect");
	}
} else {
	if (! $modelling = get_record("modelling", "id", $a)) {
		error("Course module is incorrect");
	}
	if (! $course = get_record("course", "id", $modelling->course)) {
		error("Course is misconfigured");
	}
	if (! $cm = get_coursemodule_from_instance("modelling", $modelling->id, $course->id)) {
		error("Course Module ID was incorrect");
	}
}

require_login($course->id);

add_to_log($course->id, "modelling", "Assessment viewing", "view.php?id=$id", "$cm->instance");

/// Print the page header

$navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";




print_header("$course->shortname: $modelling->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $modelling->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
navmenu($course));
 
//For teacher, view select button
if (isteacher($cm->course)) {

	if (empty($_SESSION['SESSION']->modelling_teacherview)) {
		$_SESSION['SESSION']->modelling_teacherview = "teacherview";
	}
	if ($v == "teacher") {
		$_SESSION['SESSION']->modelling_teacherview = "teacherview";
	}
	if ($v == "student") {
		$_SESSION['SESSION']->modelling_teacherview = "studentview";
	}
	if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
		echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
	}
	if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
		echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
	}
}

$taskdata = get_record ("modelling_tasks", "id", $t);


//---------Delete Rating------------//
 
if ($deletemy) {

	delete_records ("modelling_as_crit_rating", "topicid", $deletemy, "userid", $USER->id, "assessmentid", $t);
	delete_records ("modelling_as_rating", "topicid", $deletemy, "userid", $USER->id, "assessmentid", $t);

}
 
//--------Add Criteria-------------//

if ($criterianame && isteacher($cm->course)) {
	if ($upcriteriaid) {
		set_field ("modelling_crit_name", "name", $criterianame, "id", $upcriteriaid);
		foreach ($ratingvalue as $ratingkey_ => $ratingvalue_) {
			set_field ("modelling_crit_rating", "name", $ratingvalue_, "id", $ratingkey_);
		}
	}
	else
	{
		$dataArray=array();
		$dataArray['action']="insert";
		$criteria = new object;
		$criteria->instance = $id;
		$criteria->assessmentid = $t;
		$criteria->name = $criterianame;
		$criteria->time = time();

		$criteriaid = insert_record("modelling_crit_name", $criteria);
		$criteria->id=$criteriaid;
		$task=get_record("modelling_tasks","id",$criteria->assessmentid);
		$criteria->assessmenturi=$task->taskuri;
		$dataArray['criteria']=json_encode($criteria);
		$dataArray['tablename']="modelling_crit_name";
		$ordNumb=0;
		$ratings=array();
		foreach ($ratingvalue as $ratingvalue_) {
			$ordNumb=$ordNumb+1;
			$ratingnumvalue=get_project_assessment_rating_number_value($taskdata->numberofitems,$ordNumb);

			$rating = new object;
			$rating->criteriaid = $criteriaid;
			$rating->name = $ratingvalue_;
			$rating->time = time();
			$rating->value=$ratingnumvalue;
			$ratingId= insert_record("modelling_crit_rating", $rating);
			$rating->id=$ratingId;
			$ratings[$ordNumb-1]=json_encode($rating);
			 
		}
		$dataArray['ratings']=json_encode($ratings);
		$url=$CFG->resturl."insert/table";

		$data=json_encode($dataArray);
		$response=curl_call($url,'POST', $data);
		require_once($CFG->dirroot.'/depths/mapper/utility.php');
		 
		$datatochange=json_decode($response, true);
		foreach($datatochange as $key=>$value){
			$operation=$value["operation"];
			if($operation=="setfield"){
				 
				foreach($value as $k=>$v){
					if($k!="operation"){
					//	print_message("set field: modelling_crit_name "." k:".$k." v:".$v." for id=".$criteria->id,"projectassessment");
						set_field("modelling_crit_name", $k, $v, "id", $criteria->id);
					}
				}

			}else if($operation=="insert"){
			//	print_message("operation insert","projectassessment");
			}else if($operation="setotherfield"){
				$tableToUpdate=$value['tablename'];
				$scaleitemuri=$value['scaleitemuri'];
				$scaleitemid=$value['scaleitemid'];
			//	print_message("operation setotherfield:".$tableToUpdate." uri:".$scaleitemuri." id:".$scaleitemid,"projectassessment");
				set_field($tableToUpdate,'scaleitemuri',$scaleitemuri,"id",$scaleitemid);
					

			}else if($operation=="urimapping"){
				$urimapping=new object;
				$urimapping->domainconcept=$value['domainconcept'];
				$urimapping->instanceid=$value['instanceid'];
				$urimapping->uri=$value['uri'];
			//	print_message("***insert_table_to_rdf table:".$urimapping->domainconcept." instance:".$urimapping->instance.
				//	" uri:".$urimapping->uri,"projectassessment");
				insert_record("modelling_urimapping", $urimapping);
			}else{
				echo "this operation is not processed";
			}
		}
	}
}
 
//----------------template name-----//
if ($templatename && isteacher($cm->course)) {

	$allcriterias = get_records ("modelling_crit_name", "assessmentid", $t, "id");
	foreach ($allcriterias as $allcriteria) {
		unset ($criteriascalelist1); //, $criteriavaluelist1);
		$criterialist .= $allcriteria->name . "{}";
		$allcriteriasscale = get_records ("modelling_crit_rating", "criteriaid", $allcriteria->id, "id");
		foreach ($allcriteriasscale as $allcriteriasscale_) {
			$criteriascalelist1 .= $allcriteriasscale_->name . "{}";
		}
		$criteriascalelist1 = substr ($criteriascalelist1, 0, -2);

		$criteriascalelist .= $criteriascalelist1 . "||";
	}

	$criterialist = substr ($criterialist, 0, -2);
	$criterialistusers = substr ($criterialistusers, 0, -2);
	$criteriascalelist = substr ($criteriascalelist, 0, -2);

	$assessmenttemplate = new object;
	$assessmenttemplate->title = $templatename;
	$assessmenttemplate->criteria = $criterialist;
	$assessmenttemplate->ratings = $criteriascalelist;

	insert_record ("modelling_crit_template", $assessmenttemplate);

}
 
//-------------from template--------//
if ($fromtemplate && isteacher($cm->course)) {

	if ($submit == "Delete Template") {
		delete_records ("modelling_crit_template", "id", $fromtemplate);
	}
	else
	{
		$assessmenttemplate = get_record ("modelling_crit_template", "id", $fromtemplate);

		$criterialist        = explode ("{}", $assessmenttemplate->criteria);
		$criteriascalelist   = explode ("||", $assessmenttemplate->ratings);

		$criterialist = array_reverse ($criterialist);

		foreach ($criterialist as $criterialistkey => $criterialistvalue) {
			$newcriteriadata = new object;
			$newcriteriadata->assessmentid = $t;
			$newcriteriadata->instance = $id;
			$newcriteriadata->name     = $criterialistvalue;
			$newcriteriadata->time     = time();
			 
			$criteriaidnew = insert_record ("modelling_crit_name", $newcriteriadata);

			$criteria_scale = explode ("{}", $criteriascalelist[$criterialistkey]);
			$ordNumb=0;
			$numberofitems=count($criteria_scale);
			 
			foreach ($criteria_scale as $criteria_scalekey => $criteria_scalevalue) {
				$ordNumb=$ordNumb+1;
				$newscaledata = new object;
				$newscaledata->criteriaid = $criteriaidnew;
				$newscaledata->name      = $criteria_scalevalue;
				$newscaledata->time      = time();
				$ratingvalue=get_project_assessment_rating_number_value($numberofitems,$ordNumb);
				$newscaledata->value=$ratingvalue;

				insert_record ("modelling_crit_rating", $newscaledata);
			}
		}
	}
}
 
//----------Delete Criteria---------//

if ($delcriteria && isteacher($cm->course)) {
	 
	if ($delcriteria == "all") {
		if ($criteria = get_records ("modelling_crit_name", "assessmentid", $t)) {
			foreach ($criteria as $criteria_) {
				delete_records ("modelling_crit_name", "id", $criteria_->id);
				delete_records ("modelling_crit_rating", "criteriaid", $criteria_->id);
			}
		}
	}
	else
	{
		delete_records ("modelling_crit_name", "id", $delcriteria);
		delete_records ("modelling_crit_rating", "criteriaid", $delcriteria);
	}
}
 
//----------------------------------//

echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';

if (isteacher($cm->course)) {

	if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
		echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
	}
	else
	{
		echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
	}
}
 
print_simple_box_start('center', '100%', '#ffffff', 10);

echo '<table cellpadding="5" cellspacing="0">';

echo '<tr><td><b>'.get_string ('assessmenttaskdescription', 'modelling').'</b></td>';
echo '<td>'.$taskdata->description.'</td></tr>';
 
if ($modelling->useprojectdates == "true") {
	echo '<tr><td><b>'.get_string ('assessmenttaskstartdate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
	echo '<tr><td><b>'.get_string ('assessmenttaskenddate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
	echo '<tr><td><b>'.get_string ('assessmenttasklateenddate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $taskdata->laterenddate).'</td></tr>';
}
 
echo "</table>";

print_simple_box_end();

//------------PRINT TABS------------//
$row  = array();

$bar = modelling_gettabbar ($id, $t);
 
if (!empty($bar['prev'])) {
	$row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
}

$row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));


if (!empty($bar['next'])) {
	$row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
}

$tabs[] = $row;

print_tabs($tabs);
//----------------------------------//
 
echo "<hr /><br />";
 
if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {

	echo '<center><table cellpadding="20" cellspacing="0" width="900"><tr><td valign="top">';

	print_simple_box_start('center', '400', '#ffffff', 10);

	echo '<form action="projectassessment.php?id='.$id.'&t='.$t.'" method="post"><center><b>'.get_string ("criteria", "modelling").'</b></center>';

	if ($upcriteria) {

		echo '<input type="hidden" name="upcriteriaid" value="'.$upcriteria.'" />';

		$criteriadata = get_record ("modelling_crit_name", "id", $upcriteria);
		$criterianamevalue = $criteriadata->name;
	}
	else
	{

		$criterianamevalue = "Criteria Name";
	}

	echo '<table cellpadding="6" cellspacing="0" width="400">';

	echo '<tr><td>'.get_string ("criterianame", "modelling").'</td><td><input type="text" name="criterianame" value="'.$criterianamevalue.'" />'.modelling_makelocalhelplinkmain ("criteria", "Help, with Criteria Name (new window)", "modelling").'</td></tr>';

	if ($upcriteria) {

		$ratingsdata = get_records ("modelling_crit_rating", "criteriaid", $upcriteria);
		$i = 0;
		foreach ($ratingsdata as $ratingsdata_) {
			echo '<tr><td>'.get_string ("ratingvalue", "modelling").$i.'</td><td><input type="text" name="ratingvalue['.$ratingsdata_->id.']" value="'.$ratingsdata_->name.'" /></td></tr>';
			$i ++;
		}
	}
	else
	{

		for ($i=0; $i < $taskdata->numberofitems; $i++) {
			echo '<tr><td>'.get_string ("ratingvalue", "modelling").$i.'</td><td><input type="text" name="ratingvalue['.$i.']" value="Label '.$i.'" /></td></tr>';
		}
	}

	echo '</table>';

	if ($upcriteria) {
		echo '<center><input type="submit" value="'.get_string("updatecriteria", "modelling").'"></center></form>';
	}
	else
	{

		echo '<center><input type="submit" value="'.get_string("addnewcriteria", "modelling").'"></center></form>';
	}

	print_simple_box_end();

	echo '</td><td valign="top">';

	print_simple_box_start('center', '400', '#ffffff', 10);

	echo '<center><b>'.get_string ("template", "modelling").'</b></center>';

	echo '<table cellpadding="6" cellspacing="0" width="400">';
	echo '<tr><td><b>'.get_string("useassessmenttemplate", "modelling") . " " . modelling_makelocalhelplinkmain ("useanassessmenttemplate", "Help, with Use Anassessment template (new window)", "modelling").'</b></td><td><b>'.get_string("savecriteriabelowasanassessmenttemplate", "modelling") . " " . modelling_makelocalhelplinkmain ("savecriteriabelowasanassessmenttemplate", "Help, with Save Criteria below Asanassessment template", "modelling").'</b></td></tr>';
	echo '<tr><td><form action="projectassessment.php?id='.$id.'&t='.$t.'" method="post" name="templatepreviewform">';

	if ($templates = get_records("modelling_crit_template")) {
		 
		foreach ($templates as $template) {
			$templatemenu[$template->id] = $template->title;
		}

		choose_from_menu($templatemenu, "fromtemplate", "", "","","");
		echo '<br /><br /><input type="submit" name="submit" value="'.get_string("usethistemplete", "modelling").'" /><br /><input type="submit" name="submit" value="Delete Template" /><br /><input type="submit" name="submit" value="'.get_string("previewtemplete", "modelling").'" onclick="this.target=\'templatepreview\'; return openpopup(\'/mod/modelling/templatepreview.php?id='.$id.'&a=assessment&t=\' + templatepreviewform.fromtemplate.value, \'templatepreview\', \'scrollbars=yes,resizable=yes,width=700,height=540\', 0);" />';

	}
	else
	{
		echo get_string("notemplate", "modelling");
	}

	echo '</form></td>';

	echo '<td><form action="projectassessment.php?id='.$id.'&t='.$t.'" method="post"><input type="text" name="templatename" /><input type="submit" value="'.get_string("savetemplate", "modelling").'"></form></td></tr>';
	echo '</table>';

	print_simple_box_end();

	echo '</td></tr></table></center>';

	if ($criteria = get_records ("modelling_crit_name", "assessmentid", $t)) {

		echo '<br /><hr /><br /><div style="text-align: center"><b>'.get_string ('currentratingcriteria', 'modelling').'</b> <a href="projectassessment.php?id='.$id.'&t='.$t.'&delcriteria=all" onclick="if(confirm(\'Are you sure you want to Delete All criterias?\')) return true; else return false;">Delete all</a></div>';

		foreach ($criteria as $criteria_) {

			print_simple_box_start('center', '400px', '#ffffff', 10);
			echo '<b>' . $criteria_->name . '</b> - <a href="projectassessment.php?id='.$id.'&t='.$t.'&delcriteria='.$criteria_->id.'">Delete</a> '. modelling_makelocalhelplinkmain ("deletecriteria", "Help, with Delete Criteria", "modelling") . '- <a href="assessment.php?id='.$id.'&t='.$t.'&upcriteria='.$criteria_->id.'">Update Criteria</a> ' . modelling_makelocalhelplinkmain ("updatecriteria", "Help, with Update Criteria", "modelling");

			$ratings = get_records ("modelling_crit_rating", "criteriaid", $criteria_->id, "id");
			echo '<table cellpadding="6" cellspacing="0">';
			$c = 0;
			foreach ($ratings as $rating) {
				echo '<tr><td>'.$c.' = '.$rating->name.'</td></tr>';
				$c ++;
			}
			echo '</table>';
			print_simple_box_end();
		}
	}
	 
}
else
{
 
	$mygroupmembers=getmygroupmembers($modelling,$course);
 
	//$criteria = get_records ("modelling_crit_name", "assessmentid", $t);
	
	//$table->width = "1000";

	if ($projects = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_projects WHERE instance='".$id."' and taskid='".$taskdata->tasktoassess."'")) {
		$titlesarray = Array ('Students/Groups'=>'studentsgroups', 'Project'=>'projectid', 'Preview project'=>'','Edit'=>'', 'Rate'=>'', 'View Ratings'=>'', 'Date Added'=>'dateadded');
		
		$table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'projectassessment.php?id='.$id.'&t='.$t);
		$table->align = array ("left", "left", "center", "center", "center", "center");
	 
		//$projectsToAssess= array();
		//foreach($projects as $cproject){
	 
			//if(in_array($cproject->user_groupid,$mygroupmembers)){
		 
		//	$projectsToAssess[]=$cproject;
		 
		//	}else{
			 
		//	}
		
		 
			// $response=getListOfProjectsByUri($projectsToAssess);
	 
		 
		foreach ($projects as $currentproject) {

			if((in_array($currentproject->user_groupid,$mygroupmembers))||($currentproject->user_groupid==$USER->id)){
				 
	 
			   //if($currentproject->user_groupid!=""){
			$response=getProjectByUri($currentproject->selectedproject);
		 
			$proj=$response[0];


			$projectData=$proj['project'];
			$projectTitle=$projectData['title'];
			 
			$solutionData=$proj['solution'];
			 
			$diagrams=$proj['diagrams'];
 $diva= '<div  class="yoxview">';
        		   $divb= '</div>';
        		 $di=0;
			foreach($diagrams as $k=>$diagram){
				 
				 
				$cDiagramFileUrl=$diagram['fileUrl'];
				$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
				list($width, $height, $type, $attr) = getimagesize($imageLink);
				$diagramTitle=$diagram['title'];
				if($width>100){
					$di++;
	  	    		 if($di==1){
	  	    		$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'">View</a>';
	  	    		 }else{
	  	    		 	$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'"></a>';
	  	    			
	  	    		 }
	  	    		$alllinks=$alllinks.$imageHref;
				}
			}
			 	$i++; 
			 
			//$topic = get_record ("modelling_topics", "user_groupid", $file->user_groupid, "instance", $id);
			$tabledata['previewproject']=$diva.$alllinks.$divb;
   $alllinks="";


			if (get_record("modelling_as_rating", "userid", $USER->id, "assessmentid", $taskdata->id, "submissionid",$currentproject->id)) {
				 
				$tabledata['edit'] = '<a href="projectassessment_rate.php?id='.$id.'&t='.$t.'&selectedproject='.urlencode($currentproject->selectedproject).'&editmy=1&submissionid='.$currentproject->id.'">'.get_string("editmyrating", "modelling").'</a>';
				$tabledata['rate'] = '<a href="projectassessment_rate.php?id='.$id.'&t='.$t.'&selectedproject='.urlencode($currentproject->selectedproject).'&selectedprojectid='.$currentproject->id.'&editmy=0&submissionid='.$currentproject->id.'">'.get_string("rateprojectsubmission", "modelling") . modelling_makelocalhelplinkmain ("rateprojectsubmission", "Help, Rate", "modelling") . '</a>';
				$tabledata['view'] = '<a href="projectassessment_view.php?id='.$id.'&t='.$t.'&submissionid='.$currentproject->id.'">'.get_string("viewratings", "modelling") . modelling_makelocalhelplinkmain ("viewratings", "Help, View Rating", "modelling") . '</a>';
			}
			else
			{
				 

				$tabledata['edit'] = '';
				if ($taskdata->peerrating == "yes") {

					$tabledata['rate'] = '<a href="projectassessment_rate.php?id='.$id.'&t='.$t.'&selectedproject='.urlencode($currentproject->selectedproject).'&selectedprojectid='.$currentproject->id.'&editmy=0&submissionid='.$currentproject->id.'">'.get_string("rateprojectsubmission", "modelling") . modelling_makelocalhelplinkmain ("rateprojectsubmission", "Help, Rate", "modelling").'</a>';
				}
				else
				{

					$tabledata['rate'] = get_string("notratesubmission", "modelling");
				}
				$tabledata['view'] = '<a href="projectassessment_view.php?id='.$id.'&t='.$t.'&submissionid='.$currentproject->id.'">'.get_string("viewratings", "modelling") . modelling_makelocalhelplinkmain ("viewratings", "Help, View Rating", "modelling").'</a>';
			}
			$table->data[] = array (modelling_return_user_groupid_name ($modelling, $currentproject->user_groupid), $projectTitle,$tabledata['previewproject'], $tabledata['edit'], $tabledata['rate'], $tabledata['view'], array (date("m:i d M Y", $currentproject->time), $currentproject->time));
		}
		}

		$table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);

		if ($table) {
			print_table($table);
		}
		 
		 
	 }
	else
	{
		echo '<br /><div style="text-align: center"><b>'.get_string ('nofiles', 'modelling').'</b></div>';
	}
}

 
print_footer($course);

?>