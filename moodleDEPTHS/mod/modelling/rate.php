<?php   // $Id: rate.php,v 1.1 2008/02/20 10:26:33 cvsadmin Exp $

//  Collect ratings, store them, then return to where we came from

 
    require_once('../../config.php');
    require_once('dplib.php');
 
    $modellingid = required_param('modellingid', PARAM_INT); // The forum the rated posts are from
 
    if (!$modelling = get_record('modelling', 'id', $modellingid)) {
        error("Incorrect design problem id");
    }

    if (!$course = get_record('course', 'id', $modelling->course)) {
        error("Course ID was incorrect");
    }

    if (!$cm = get_coursemodule_from_instance('modelling', $modelling->id)) {
        error("Course Module ID was incorrect");
    }
 
    require_login($course, false, $cm);

    if (isguestuser()) {
        error("Guests are not allowed to rate entries.");
    }

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    if (!$modelling->assessed) {
        error("Rating of items not allowed!");
    }
 
    if ($modelling->assessed == 2) {
    	//ovo treba srediti
        require_capability('mod/glossary:rate', $context);
    }

    if (!empty($_SERVER['HTTP_REFERER'])) {
        $returnurl = $_SERVER['HTTP_REFERER'];
    } else {
        $returnurl = $CFG->wwwroot.'/mod/modelling/view.php?id='.$cm->id;
    }
 
    if ($data = data_submitted()) {    // form submitted
  
    	 
     
        foreach ((array)$data as $entryid => $rating) {
         
            if (!is_numeric($entryid)) {
            	  
                continue;
            }
            
            if (!$entry = get_record('modelling_ideas', 'id', $entryid)) {
             
                continue;
            }
		 
            /*if ($entry->glossaryid != $glossary->id) {
                error("This is not valid entry!");
            }*/

            if ($modelling->assesstimestart and $modelling->assesstimefinish) {
            
            	$currenttime=time();
                if ($currenttime < $modelling->assesstimestart or $currenttime > $modelling->assesstimefinish) {
                	 
                    // we can not rate this, ignore it - this should not happen anyway unless teacher changes setting
                    continue;
                }
              
            }
 
            if ($entry->userid == $USER->id) {
            
       
                //can not rate own entry
                continue;
            }
   
            if ($oldrating = get_record("modelling_idea_ratings", "userid", $USER->id, "entryid", $entry->id)) {
           
                //Check if we must delete the rate
                 
                if ($rating == -999) {
                	 
                    delete_records('modelling_idea_ratings','userid',$oldrating->userid, 'entryid',$oldrating->entryid);
                    modelling_update_grades($modelling, $entry->userid);

                } else if ($rating != $oldrating->rating) {
                	 
                    $oldrating->rating = $rating;
                    $oldrating->time = time();
                    
                    if (! update_record("modelling_idea_ratings", $oldrating)) {
                    	 
                        error("Could not update an old rating ($entry = $rating)");
                    }
                    
                    modelling_update_grades($modelling, $entry->userid);
                   
                }
 
            } else if ($rating >= 0) {
             
                $newrating = new object();
                $newrating->userid  = $USER->id;
                $newrating->time    = time();
                $newrating->entryid = $entry->id;
                $newrating->rating  = $rating;
  	 
                if (! insert_record("modelling_idea_ratings", $newrating)) {
          
                    error("Could not insert a new rating ($entry->id = $rating)");
                    
                }
       
                modelling_update_grades($modelling, $entry->userid);
                
            }
           
        }
  
        redirect($returnurl, get_string("ratingssaved", "modelling"));
 
    } else {
     
        error("This page was not accessed correctly");
    }
 
?>
