<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $v  = optional_param('v'); 
    
    $useringroup = optional_param('useringroup'); 
    $groupname   = optional_param('groupname');
    $unassign    = optional_param('unassign');
    $jointogroup = optional_param('jointogroup');
    $delfromgr   = optional_param('delfromgr');
    $grid        = optional_param('grid');
    

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "make group viewing", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
    
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    if (isteacher($cm->course)) {
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }

    $taskdata = get_record ("modelling_tasks", "id", $t);
    
    //-------Add users to Group---------//
    
    if ($groupname) {
        if (!modelling_checkuseringroup ($id)) {
            if ((count ($useringroup) + 1) >= $taskdata->minsize && (count ($useringroup) + 1) <= $taskdata->maxsize) {
                $group = new object;
                $group->name = $groupname;
                $group->instance = $id;
                
                $groupid = insert_record ("modelling_groups", $group);
                
                if (!isteacher($cm->course) || $_SESSION['SESSION']->modelling_teacherview == "studentview") {
                    $useringroup[$USER->id] = "on";
                }
                
                foreach ($useringroup as $useringroupkey => $useringroupvalue) {
                    $groupuser = new object;
                    $groupuser->groupid = $groupid;
                    $groupuser->userid = $useringroupkey;
                    
                    insert_record ("modelling_gr_students", $groupuser);
                }
            }
        }
    }
    
    //----------UnAssign from group-----//
    
    if ($unassign) {
        delete_records ("modelling_gr_students", "groupid", $unassign, "userid", $USER->id);
    }
    
    //---------join to group------------//
    
    if ($jointogroup) {
        if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            delete_records ("modelling_gr_students", "groupid", $jointogroup);
            delete_records ("modelling_gr", "id", $jointogroup);
        }
        else
        {
            if (count_records("modelling_gr_students", "groupid", $jointogroup) < $taskdata->maxsize) {
                $groupuser = new object;
                $groupuser->groupid = $jointogroup;
                $groupuser->userid = $USER->id;
            
                insert_record ("modelling_gr_students", $groupuser);
            }
        }
    }
    
    //----------Delete from Group-------//
    
    if ($delfromgr && $grid && isteacher($cm->course)) {
        delete_records ("modelling_gr_students", "groupid", $grid, "userid", $delfromgr);
    }
    
    //----------------------------------//
    
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
    
    if (isteacher($cm->course)) {
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
    
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';

    echo '<tr><td><b>'.get_string ('grouptaskdescription', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->description.'</td></tr>';
    
    if ($project->useprojectdates == "true") {
        echo '<tr><td><b>'.get_string ('grouptaskstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('grouptaskenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
    }
    echo '<tr><td><b>'.get_string ('minimumgroupsize', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->minsize.'</td></tr>';
    echo '<tr><td><b>'.get_string ('maximumgroupsize', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->maxsize.'</td></tr>';
    
    echo "</table>";
    
    print_simple_box_end();
    
    //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);
   
    if (!empty($bar['prev'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
    }

    $row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

    
    if (!empty($bar['next'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
    }
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    
    echo "<hr /><br />";
    
    echo '<center><table style="border: 1px solid black;" cellpadding="10"><tr><td valign="top">';
    
    echo '<form action="makegroup.php?id='.$id.'&t='.$t.'" method="post"><table border="0" cellpadding="5" cellspacing"10"><tr><td><b>';
    echo get_string ("grouptaskunassignedusers", "modelling");
    echo '</b></td><td><b>';
    echo get_string ("grouptaskselectusers", "modelling");
    echo '</b></td></tr>';
    
    $students = modelling_unassignedusers ($cm->course, $id);
    
    foreach ($students as $studentkey => $studentvalue) {
        echo '<tr><td>'.$studentvalue.'</td><td><input type="checkbox" name="useringroup['.$studentkey.']" /></td></tr>';
    }
    
    if ((!modelling_checkuseringroup ($id) && $project->projecttype == "student groups") || ($project->projecttype == "teacher groups project" && isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview")) {
        echo '<tr><td colspan="2"><hr /></td></tr><tr><td>'.get_string ("grouptaskgroupname", "modelling").'</td><td><input type="text" name="groupname" style="width:100px;" /></td></tr><tr><td colspan="2"><input type="submit" value="'.get_string ("grouptasksubmit", "modelling").'" /></td></tr>';
    }
    
    echo '</table></form>';
    
    echo '</td><td style="border-left: 1px solid black;"></td><td valign="top">';

    echo '<form action="makegroup.php?id='.$id.'&t='.$t.'" method="post"><table border="0" cellpadding="5" cellspacing"10"><tr><td><b>';
    echo get_string ("grouptaskcurrentgroups", "modelling");
    echo '</b></td><td><b>';
    echo get_string ("grouptaskselectgroups", "modelling");
    echo '</b></td></tr>';
    
    if ($groups = get_records("modelling_groups", "instance", $id)) {
        foreach ($groups as $group) {
            $studentsingroup = get_records ("modelling_gr_students", "groupid", $group->id);
            $studenttext = "";
            foreach ($studentsingroup as $studentsingroup_) {
                $getuserdata = get_record("user", "id", $studentsingroup_->userid);
                if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
                    $studenttext .= $getuserdata->username . " (" . fullname($getuserdata).") <a href=\"makegroup.php?id=".$id."&t=".$t."&delfromgr=".$studentsingroup_->userid."&grid=".$group->id."\">delete</a><br />";
                }
                else
                {
                    $studenttext .= $getuserdata->username . " (" . fullname($getuserdata).")<br />";
                }
            }
            echo '<tr><td><b>'.$group->name.'</b><br />'.$studenttext.'</td><td>';
            
            if (!modelling_checkuseringroup ($id)) {
                echo '<input type="radio" name="jointogroup" value="'.$group->id.'" />';
            }
            else
            {
                if (get_record("modelling_gr_students", "groupid", $group->id, "userid", $USER->id)) {
                    echo '<input type="hidden" name="unassign" value="'.$group->id.'" /><input type="submit" value="unassign from group" />';
                }
                else
                {
                    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
                        echo '<input type="radio" name="jointogroup" value="'.$group->id.'" />';
                    }
                }
            }
            
            echo '</td></tr>';
        }
        
        if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<tr><td colspan="2"><input type="submit" value="'.get_string ("grouptaskdeletegroup", "modelling").'" /></td></tr>';
        }
        else
        {
            if (!modelling_checkuseringroup ($id) && $project->projecttype != "teacher groups project") {
                echo '<tr><td colspan="2"><input type="submit" value="'.get_string ("grouptaskjoingroup", "modelling").'" /></td></tr>';
            }
        }
    }
    else
    {
        echo '<tr><td colspan="2">'.get_string ("grouptasknogroupsexist", "modelling").'</td></tr>';
    }
    
    echo '</table></form>';
    
    echo '</td></tr>';
    
    echo '</table>';
    
    echo '<br /><br /><a href="view.php?id='.$id.'">'.get_string("returntomainprojectpage", "modelling").'</a>';
    
    echo '</center>';
    
    
    print_footer($course);

?>