<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov
echo '<style type="text/css">
 
.userlink {font-size: 12px}
.boxcenter {
  margin-left:auto;
  margin-right:auto;
  text-align:center;
}
</style>';

echo '
<script type="text/javascript" src="../../depths/lib/scripts/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../depths/lib/scripts/jquery-ui-1.8.16.custom.min.js"></script>
  <script type="text/javascript" src="../../depths/lib/scripts/jquery.validate.min.js"></script>
  <script type="text/javascript" src="../../depths/lib/scripts/jcarousellite_1.0.1c4.js"></script>
    <script type="text/javascript" src="../../depths/lib/tags/dialogsInit.js"></script>
    <script type="text/javascript" src="../../depths/lib/recommendationDialog.js"></script>
     <script type="text/javascript" src="../../depths/lib/tags/expand.js"></script>
<link rel="stylesheet" type="text/css" href="../../depths/lib/styles/smoothness/jquery-ui-1.8.16.custom.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../depths/lib/styles/tagclouds.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../depths/lib/styles/ticker.css" media="screen" />
<script type="text/javascript">
$(function() {    
 
$("a.expand").toggler({method: "slideFadeToggle"});
 })
</script>
';
require_once("../../config.php");
require_once("lib.php");
require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once("dplib.php");
require_once($CFG->dirroot.'/depths/mapper/utility.php');
require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');

$id = required_param('id');
$t  = required_param('t');
$v  = optional_param('v');
$release  = optional_param('release');
$selIdea=optional_param('selIdea');
 
$name = optional_param('name');
$description = optional_param('description');
$choosedtopic = optional_param('choosedtopic');
$teacherassignedtopic = optional_param('teacherassignedtopic');
$teacherdeletetopic = optional_param('teacherdeletetopic');
$teacherdeletetopictotal = optional_param('teacherdeletetopictotal');
 

$templatetitle = optional_param('templatetitle');
$fromtemplate = optional_param('fromtemplate');
$submit = optional_param('submit');

$sort = optional_param('sort', 'studentsgroups', PARAM_ALPHA);
$orderby = optional_param('orderby', 'ASC', PARAM_ALPHA);

if ($id) {
	 
	if (! $cm = get_record("course_modules", "id", $id)) {
		error("Course ID was incorrect");
	}
	if (! $course = get_record("course", "id", $cm->course)) {
		error("Course is misconfigured");
	}
	if (! $modelling = get_record("modelling", "id", $cm->instance)) {
		error("Course module is incorrect");
	}
} else {

	if (! $modelling = get_record("modelling", "id", $a)) {
		error("Course module is incorrect");
	}
	if (! $course = get_record("course", "id", $project->course)) {
		error("Course is misconfigured");
	}
	if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
		error("Course Module ID was incorrect");
	}
}

require_login($course->id);

add_to_log($course->id, "modelling", "brainstorm viewing", "view.php?id=$id", "$cm->instance");

/// Print the page header

$navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
navmenu($course));

//For teacher, view select button
if (isteacher($cm->course)) {

	if (empty($_SESSION['SESSION']->modelling_teacherview)) {

		$_SESSION['SESSION']->modelling_teacherview = "teacherview";
	}
	if ($v == "teacher") {

		$_SESSION['SESSION']->modelling_teacherview = "teacherview";
	}
	if ($v == "student") {

		$_SESSION['SESSION']->modelling_teacherview = "studentview";
	}
	if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
		 
		echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
	}
	if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {

		echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
	}
}

$taskdata = get_record ("modelling_tasks", "id", $t);
 
echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
//ovo ostaje
if (isteacher($cm->course)) {
	 
	if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
		echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
	}
	else
	{
		echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
	}
}

print_simple_box_start('center', '100%', '#ffffff', 10);

echo '<table cellpadding="5" cellspacing="0">';

echo '<tr><td><b>'.get_string ('brainstormtaskdescription', 'modelling').'</b>&nbsp;</td>';
echo '<td>'.$taskdata->description.'</td></tr>';

if ($modelling->useprojectdates == "true") {
	 
	echo '<tr><td><b>'.get_string ('brainstormtaskstartdate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
	echo '<tr><td><b>'.get_string ('brainstormtaskenddate', 'modelling').'</b></td>';
	echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
}

echo "</table>";

print_simple_box_end();

//------------PRINT TABS------------//
$row  = array();

$bar = modelling_gettabbar ($id, $t);
 
if (!empty($bar['prev'])) {
	 
	$row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
}

$row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

if (!empty($bar['next'])) {

	$row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
}

$tabs[] = $row;

print_tabs($tabs);

echo "<hr /><br />";
$returnedjson=getRelatedConceptsForDesignProblem($modelling->designproblemuri);
$conceptsjson=$returnedjson[1];
$conceptsjson2=$returnedjson[2];
$recommendedconcepts=$conceptsjson['recommendedconcepts'];
$relatedconcepts=$conceptsjson2['relatedconcepts'];	 
  
print_box_start('boxwidthwide boxaligncenter generalbox');
echo "Recommended reading:&nbsp;&nbsp;";
foreach($recommendedconcepts as $k1=>$concept){
	 
	$conceptname=$concept['conceptname'];
	$concepturi=$concept['concepturi'];
	echo '<a onclick="return showDEPTHSRecommendationDialog(\''.$CFG->wwwroot.'/depths/services/online_resources/view_suggestions.php?conceptUri='.urlencode($concepturi).'\',\''.$conceptname.'\'); return false;" href="#" title="View recommended resources">'.$conceptname.'</a>&nbsp;&nbsp;';

}
echo "<br>";
echo "Related resources:&nbsp;&nbsp;";
foreach($relatedconcepts as $k1=>$concept){

	$conceptname=$concept['conceptname'];
	$concepturi=$concept['concepturi'];
	echo '<a onclick="return showDEPTHSRecommendationDialog(\''.$CFG->wwwroot.'/depths/services/online_resources/view_suggestions.php?conceptUri='.urlencode($concepturi).'\',\''.$conceptname.'\'); return false;" href="#" title="View recommended resources">'.$conceptname.'</a>&nbsp;&nbsp;';

}
print_box_end();
 
print_simple_box_start('center', '100%', '#ffffff', 10);
echo '<form id="editing" method="post" action="brainstorm_process.php?id='.$id.'&t='.$t.'">';
 print_textarea(true, 15, 34, 100, 100, 'ideacontent', null);
 use_html_editor('ideacontent', 'formatblock  subscript superscript copy cut paste clean undo redo   insertorderedlist insertunorderedlist  inserthorizontalrule createanchor nolink inserttable');
// echo '<input type="hidden" name="format" value="'.FORMAT_HTML.'" />';
 echo '<center><input type="submit" value="'.get_string('addidea', 'modelling').'" />&nbsp;</center>';
echo '</form>';
print_simple_box_end();
echo "<hr /><br />";
 
$strftimedatetime = get_string("strftimerecent");
$strftimetime = get_string("strftimetime").":%S";
$currentUserId=$USER->id;
 
//get all ideas from database
$ideas=get_records("modelling_ideas","instance",$t);
$mygroupmembers=getmygroupmembers($modelling,$course);
 
$desproblid=get_field("course_modules","instance","id",$id);
$modelling=get_record("modelling","id",$desproblid);
 
$formsent = 1;
echo "<form method=\"post\" action=\"rate.php\">";
echo "<div>";
echo "<input type=\"hidden\" name=\"modellingid\" value=\"$desproblid\" />";
 
foreach($ideas as $idea){
 
 
if(in_array($idea->userid,$mygroupmembers)){
 
	print_box_start('boxwidthwide boxaligncenter generalbox');
	if ($idea->id==$selIdea){
		echo '<div style="background-color:#FEC0B1">';
	}
	$ideaCont=str_replace("[[[wwwroot]]]",$CFG->wwwroot,$idea->content);
	echo $ideaCont.'<br/>';
	$ideacreator=get_record("user","id",$idea->userid);
	$fullname = fullname($ideacreator, true);
	$realuserinfo = " <a $CFG->frametarget
            href=\"$CFG->wwwroot/user/view.php?id=$idea->userid&amp;course=$course->id\">$fullname</a> ";

	echo '<div class="userlink">';
	echo $realuserinfo.' | '.userdate($idea->time,$strftimedatetime);
		
	$ratings = NULL;
	$ratingsmenuused = false;
	if ($modelling->assessed and isloggedin() and !isguestuser()) {
			
			
		$ratings = new object();
		if ($ratings->scale = make_grades_menu($modelling->scale)) {
			 
			$ratings->ratingtime=$modelling->ratingtime;
			if($modelling->ratingtime==1){
				 

				$ratings->assesstimestart = $modelling->assesstimestart;
				$ratings->assesstimefinish = $modelling->assesstimefinish;
			}
		}
		if ($modelling->assessed == 2) {
			$ratings->allow = false;
		} else {
			if ($USER->id!=$idea->userid){
				$ratings->allow = true;
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				if (modelling_print_entry_ratings($course, $desproblid,$idea, $ratings ) ){
					$ratingsmenuused = true;
				}
			}
		}
	}
	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	echo '<a title="Add tags for this idea" href="#"
 	onclick="return showDEPTHSModalDialog(\''.$CFG->wwwroot.'/depths/tag/add_tags.php?ideaUri='.urlencode($idea->ideauri).'\', $(this)); return false;">Add Tags</a>';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<a title="Add comment for this idea" href="#"
 	 	onclick="return showDEPTHSAddCommentDialog(\''.$CFG->wwwroot.'/depths/tag/add_comment.php?ideaUri='.urlencode($idea->ideauri).'\', $(this)); return false;">Add comment</a>';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	  echo '<a class="expand" title="See comments for this idea" href="#" onclick="getAllCommentsForIdea(\''.$idea->id.'\',\''.urlencode($idea->ideauri).'\',\''.$CFG->wwwroot.'\'); return false;">See all comments</a>';
	 
	echo '
	<div class="collapse" id='.$idea->id.'>';
	
	
	
	echo '</div>';
	echo '<div id="loading'.$idea->id.'" style="display:none; text-align:left">
			  			    		<img src="../../depths/lib/images/ajax-loader.gif" alt="Loader" />
			  					</div>';
	echo '</div>';

	$ratingsmenuused = true;

	if ($idea->id==$selIdea){
		echo '</div>';
	}
	print_box_end();
}
}

if ($ratingsmenuused) {

	echo "<div class=\"boxcenter\"><input type=\"submit\" value=\"".get_string("sendinratings", "modelling")."\" />";
	if ($modelling->scale < 0) {
		 
		if ($scale = get_record("scale", "id", abs($modelling->scale))) {
			print_scale_menu_helpbutton($course->id, $scale );
		}
	}
	echo "</div>";
}


if (!empty($formsent)) {
	// close the form properly if used
	echo "</div>";
	echo "</form>";
}


print_footer($course);
 

?>