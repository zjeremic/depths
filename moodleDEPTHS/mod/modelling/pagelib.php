<?php // $Id: pagelib.php,v 1.1 2008/02/20 10:25:18 cvsadmin Exp $

require_once($CFG->libdir.'/pagelib.php');
require_once($CFG->dirroot.'/course/lib.php'); // needed for some blocks

define('PAGE_DESIGNPROBLEM_VIEW',   'mod-modelling-view');

page_map_class(PAGE_DESIGNPROBLEM_VIEW, 'page_modelling');

$DEFINEDPAGES = array(PAGE_DESIGNPROBLEM_VIEW);
/*
*/

/**
 * Class that models the behavior of a modelling
 *
 * @author Jon Papaioannou
 * @package pages
 */

class page_modelling extends page_generic_activity {

    function init_quick($data) {
        if(empty($data->pageid)) {
            error('Cannot quickly initialize page: empty course id');
        }
        $this->activityname = 'modelling';
        parent::init_quick($data);
    }

    function print_header($title, $morenavlinks = NULL, $meta) {
        parent::print_header($title, $morenavlinks, '', $meta);
    }

    function get_type() {
        return PAGE_DESIGNPROBLEM_VIEW;
    }
}

?>
