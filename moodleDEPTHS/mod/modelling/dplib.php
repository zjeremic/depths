<?php
function  modelling_print_entry_ratings($course, $modellingid, $entry, $ratings = NULL) {

    global $USER, $CFG;
 
 
    $modelling = get_record('modelling', 'id', $desginproblemid);
    $modellingmod = get_record('modules','name','modelling');
    
    
    $cm = get_record_sql("select * from {$CFG->prefix}course_modules where course = $course->id
                          and module = $modellingmod->id and instance = $modellingid");
 
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
 
    $ratingsmenuused = false;
     
    if (!empty($ratings) and !empty($USER->id)) {
         $useratings = true;
        if ($ratings->assesstimestart and $ratings->assesstimefinish) {
        	 
        	$currenttime=time();
         
        	  if ($currenttime < $ratings->assesstimestart or $currenttime > $ratings->assesstimefinish) {
            	   
            	$useratings = false;
            }
        }
        if ($useratings) {
        	 
        	 
        	//proveriti ova ovlascenja
        	 
            if (has_capability('mod/glossary:viewrating', $context)) {
            	 
            	 
                modelling_print_ratings_mean($entry->id, $ratings->scale);
                
                
                if ($USER->id != $entry->userid) {
                 
                     modelling_print_rating_menu($entry->id, $USER->id, $ratings->scale);
                     
                     $ratingsmenuused = true;
                }
            } else if ($USER->id == $entry->userid) {
             
            	 
                modelling_print_ratings_mean($entry->id, $ratings->scale);
                
            } else if (!empty($ratings->allow) ) {
            	 
             
                modelling_print_rating_menu($entry->id, $USER->id, $ratings->scale);
                 
                $ratingsmenuused = true;
                
            }
        }
          
    }
   
    return $ratingsmenuused;
}
function modelling_print_ratings_mean($entryid, $scale) {
/// Print the multiple ratings on a entry given to the current user by others.
/// Scale is an array of ratings
 
    static $strrate;
 
    $mean = modelling_get_ratings_mean($entryid, $scale);
 
    if ($mean !== "") {
 
        if (empty($strratings)) {
        	 
            $strratings = get_string("ratings", "modelling");
             
        }
 
        echo "$strratings: ";
       
        //ovo treba odraditi
        link_to_popup_window ("/mod/modelling/report.php?id=$entryid", "ratings", $mean, 400, 600);
    }
    
}
function modelling_get_ratings_mean($entryid, $scale, $ratings=NULL) {
/// Return the mean rating of a entry given to the current user by others.
/// Scale is an array of possible ratings in the scale
/// Ratings is an optional simple array of actual ratings (just integers)
 
    if (!$ratings) {
    	 
        $ratings = array();
         
        if ($rates = get_records("modelling_ratings", "entryid", $entryid)) {
        	 
            foreach ($rates as $rate) {
            	 
                $ratings[] = $rate->rating;
               
            }
        }
    }
 
    $count = count($ratings);

    if ($count == 0) {
    	 
        return "";

    } else if ($count == 1) {
    	 
        return $scale[$ratings[0]];

    } else {
    	 
        $total = 0;
        foreach ($ratings as $rating) {
        	 
            $total += $rating;
        }
        
        $mean = round( ((float)$total/(float)$count) + 0.001);  // Little fudge factor so that 0.5 goes UP

        if (isset($scale[$mean])) {
         
            return $scale[$mean]." ($count)";
        } else {
        	 
            return "$mean ($count)";    // Should never happen, hopefully
        }
    }
    
}
function modelling_print_rating_menu($entryid, $userid, $scale) {
/// Print the menu of ratings as part of a larger form.
/// If the entry has already been - set that value.
/// Scale is an array of ratings

    static $strrate;
  
    if (!$rating = get_record("modelling_idea_ratings", "userid", $userid, "entryid", $entryid)) {
        $rating->rating = -999;
    }
 
    if (empty($strrate)) {
        $strrate = get_string("rate", "modelling");
    }
 
    choose_from_menu($scale, $entryid, $rating->rating, "$strrate...",'',-999);
   
}
function modelling_update_grades($modelling=null, $userid=0, $nullifnone=true) {
    global $CFG;
    
    require_once($CFG->libdir.'/gradelib.php');
 
    if ($modelling != null) {
     
        if ($grades = modelling_get_user_grades($modelling, $userid)) {
        	 
            grade_update('mod/modelling', $modelling->course, 'mod', 'modelling', $modelling->id, 0, $grades);
 
        } else if ($userid and $nullifnone) {
         
            $grade = new object();
            $grade->userid   = $userid;
            $grade->rawgrade = NULL;
            
            grade_update('mod/modelling', $modelling->course, 'mod', 'modelling', $modelling->id, 0, $grade);
        
        }

    } else {
     
        $sql = "SELECT g.*, cm.idnumber as cmidnumber
                  FROM {$CFG->prefix}modelling g, {$CFG->prefix}course_modules cm, {$CFG->prefix}modules m
                 WHERE m.name='designrpoblem' AND m.id=cm.module AND cm.instance=g.id";
        if ($rs = get_recordset_sql($sql)) {
            if ($rs->RecordCount() > 0) {
                while ($modelling = rs_fetch_next_record($rs)) {
                    modelling_grade_item_update($modelling);
                    if ($modelling->assessed) {
                        modelling_update_grades($modelling, 0, false);
                    }
                }
            }
            rs_close($rs);
        }
    }
     
}
function modelling_get_user_grades($modelling, $userid=0) {
    global $CFG;
 
    $user = $userid ? "AND u.id = $userid" : "";

    $sql = "SELECT u.id, u.id AS userid, avg(gr.rating) AS rawgrade
              FROM {$CFG->prefix}user u, {$CFG->prefix}modelling_ideas ge,
                   {$CFG->prefix}modelling_ratings gr
             WHERE u.id = ge.userid AND ge.id = gr.entryid
                   AND gr.userid != u.id AND ge.instance = course_modules.id AND course_modules.instance=$modelling->id
                   $user
          GROUP BY u.id";
 
    return get_records_sql($sql);
}

function get_project_assessment_rating_number_value($numberofitems,$ordnumb){
	$value=$ordnumb/$numberofitems;
	 
	
	return $value;
}
?>