<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');
    require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
    require_once($CFG->dirroot.'/depths/setup/depths_config.php');
require_once($CFG->dirroot."/depths/rest/curl_client.php");
require_once($CFG->dirroot.'/depths/lib/depths_lib.php');

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $submissionid=required_param('submissionid');
    $sort = optional_param('sort', 'criteria', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 
    
    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    if (! $submission = get_record("modelling_files", "id", $submissionid)) {
            error("Submission id is incorrect");
        }
    } else {
        if (! $submission = get_record("modelling_files", "id", $submissionid)) {
            error("Submission id is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "Assessment viewing", "assessment_view.php?id=".$id."&t=".$t."$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  

    $taskdata = get_record ("modelling_tasks", "id", $t);
    $submissiondata = get_record ("modelling_projects", "id", $submissionid);
    $file = get_record ("modelling_files", "user_groupid", $submission->user_groupid, "taskid", $taskdata->tasktoassess);
    
    $fullpath = $CFG->wwwroot . "/file.php/" . modelling_get_file_path_www ($cm->course) . "/" . modelling_return_file_name ($file->file);
    $filename = modelling_return_file_name ($file->file);
    $fullpath=depthsConvertUrlForFile($fullpath);
    //------------PRINT TABS------------//
    $row  = array();
    
    $bar = modelling_gettabbar ($id, $t);
    
    $row[] = new tabobject('return', "assessment.php?id=".$id."&t=".$t, get_string('returntoassessment', 'modelling'));
    
    $tabs[] = $row;
    
    print_tabs($tabs);
    
 
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div><hr /><br />';
    
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';
    
    echo '<tr><td colspan="2"><b>'.get_string ('theratings', 'modelling').'</b></td>';
    echo '</tr>';
    echo '<tr><td><b>'.get_string ('presentedby', 'modelling').'</b></td>';
    echo '<td>'.modelling_return_user_groupid_name ($project, $file->user_groupid).'</td></tr>';
    
    if ($taskdata->peerviewing == "yes") {
    	echo '<tr><td><b> &nbsp;'.get_string ('submittedfile', 'modelling').'</b></td>';
    	echo '<td><a href="'.$fullpath.'">'.$filename.'</a></td></tr>';
    }
    
    echo "</table>";
    
    print_simple_box_end();
    

    //----------------------------------//
   
    echo "<hr /><br />";

    $titlesarray = Array ('Criteria'=>'criteria', 'Teacher Rating'=>'ratingbyteacher', 'Students\' Rating (average)'=>'averrating', 'Self Rating'=>'selfrating');

    $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'projectassessment_view.php?id='.$id.'&t='.$t);
    $table->align = array ("left", "center", "center", "center");
    $table->width = "800";
    
    $data = modelling_get_assessment_criterias ($t,  $project, $submissiondata);
    foreach ($data as $datakey => $datavalue) {
        $table->data[] = array ($datakey, $datavalue['averteacher'], $datavalue['averstudent'], $datavalue['self']);
    }
    
    $table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);
    
    if ($table) {
        print_table($table);
    }
    
    if ($taskdata->peercommenting == "yes") { 
        $overcomms = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_rating WHERE  assessmentid='".$t."' and submissionid='".$submissionid."'");
        if ($overcomms) {
            echo '<br /><hr /><br />';
            print_simple_box_start('center', '100%', '#ffffff', 10);
            echo '<b>'.get_string ('overallcomments', 'modelling').'</b><br /><br />';
            foreach ($overcomms as $overcomm) {
                echo "\"" . $overcomm->overallcomments . "\"<br /><br />";
            }
            print_simple_box_end();
        }
    
        $criterias = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE assessmentid='".$t."' and submissionid='".$submissionid."' ORDER BY id");
    
        if ($criterias) {
            echo '<br /><hr /><br />';
            print_simple_box_start('center', '100%', '#ffffff', 10);
            $criterianames = get_records ("modelling_crit_name", "assessmentid", $t);
    
            foreach ($criterianames as $criterianame) {
                echo '<b>'.$criterianame->name.'</b><br /><br />';
                $ratings = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_as_crit_rating WHERE criteriaid='".$criterianame->id."' and assessmentid='".$t."' and submissionid='".$submissionid."' ORDER BY id");
                foreach ($ratings as $rating) {
                    echo "\"" . $rating->comment . "\"<br /><br />";
                }
            }
            print_simple_box_end();
        }
    }
         
    print_footer($course);
     
?>