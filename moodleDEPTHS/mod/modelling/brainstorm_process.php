<?php
require_once("../../config.php");
require_once("lib.php");
$id = required_param('id');
$t  = required_param('t');
$ideacontent = optional_param('ideacontent');
if ($ideacontent) {
	$idea = new object;
	$idea->content = $ideacontent;
	$idea->instance = $t;
	$idea->time = time();
	$idea->userid=$USER->id;
	 
	insert_record ("modelling_ideas", $idea);
	 
	 header('Location: brainstorm.php?id='.$id.'&t='.$t.'');

}

?>