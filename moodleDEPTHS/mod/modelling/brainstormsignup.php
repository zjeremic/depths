<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov
echo '<style type="text/css">
 
.userlink {font-size: 12px}
.boxcenter {
  margin-left:auto;
  margin-right:auto;
  text-align:center;
}
</style>';

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');
    require_once("dplib.php");
 
    $id = required_param('id'); 
    $t  = required_param('t'); 
    $v  = optional_param('v'); 
    $release  = optional_param('release'); 
    
    $name = optional_param('name'); 
    $description = optional_param('description'); 
    $choosedtopic = optional_param('choosedtopic'); 
    $teacherassignedtopic = optional_param('teacherassignedtopic'); 
    $teacherdeletetopic = optional_param('teacherdeletetopic'); 
    $teacherdeletetopictotal = optional_param('teacherdeletetopictotal'); 
    
    $templatetitle = optional_param('templatetitle'); 
    $fromtemplate = optional_param('fromtemplate'); 
    $submit = optional_param('submit'); 
    
    $sort = optional_param('sort', 'studentsgroups', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 
    
     $ideacontent = optional_param('ideacontent'); 
  
    if ($id) {
     
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $modelling = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
    	 
        if (! $modelling = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }
 
    require_login($course->id);

    add_to_log($course->id, "modelling", "brainstorm/signup viewing", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
     
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    if (isteacher($cm->course)) {
    	 
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
        	 
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
        	 
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
        	 
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
         
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
        	 
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }

    $taskdata = get_record ("modelling_tasks", "id", $t);
    
    
    
if ($ideacontent) {
     
        $idea = new object;
        $idea->content = $ideacontent;
         
        $idea->instance = $id;
        $idea->time = time();
        
        $idea->userid=$USER->id;
        
        
        insert_record ("modelling_ideas", $idea);
    
    }
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
   //ovo ostaje 
    if (isteacher($cm->course)) {
   
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
    
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';
    
    echo '<tr><td><b>'.get_string ('brainstormsignuptaskdescription', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->description.'</td></tr>';
        
    if ($modelling->useprojectdates == "true") {
     
        echo '<tr><td><b>'.get_string ('brainstormsignuptaskstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('brainstormsignuptaskenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
    }
    
    echo "</table>";
  
    print_simple_box_end();
    
    //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);
   
    if (!empty($bar['prev'])) {
     
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
    }

    $row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

    
    if (!empty($bar['next'])) {
    	 
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
    }
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    
    echo "<hr /><br />";
    
    
class mod_modelling_addidea_form extends moodleform {
    	

        function definition() {
 
            global $CFG, $cm, $USER, $project;

            $mform    =& $this->_form;
             
            $mform->addElement('header', 'general', get_string('addnewidea', 'modelling').modelling_makelocalhelplinkmain ("addideas", "Help with Add a new idea", "modelling"));
  
          //  $mform->addElement('text', 'idea', get_string('brainstormidea', 'modelling'), array('size'=>'64'));
            // $mform->setType('idea', PARAM_TEXT);
           //  $mform->addRule('idea', null, 'required', null, 'client');
 
       
            
            $mform->addElement('htmleditor', 'ideacontent', get_string('brainstormidea', 'modelling'), array('rows'=>'15', 'cols'=>'50'));
            $mform->setType('ideacontent', PARAM_RAW);
            $mform->addRule('ideacontent', null, 'required');
             //$mform->addRule('ideacontent', '', 'required', null, 'client');
            $mform->setHelpButton('ideacontent', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');
           
 

            $this->add_action_buttons($cancel = false, $submitlabel = get_string('addidea', 'modelling'));
 
        }
    }
   		//videti da li treba postaviti neki uslov kada se pojavljuje opcija
    	$mform = new mod_modelling_addidea_form('brainstormsignup.php?id=' . $id . '&t=' . $t);
        $mform->display(); 
     
    
    echo "<hr /><br />";
    $strftimedatetime = get_string("strftimerecent");
    $strftimetime = get_string("strftimetime").":%S";
 //get all ideas from database
 $ideas=get_records("modelling_ideas","instance",$id);
 $desproblid=get_field("course_modules","instance","id",$id);
 $modelling=get_record("modelling","id",$desproblid);
 
  
 
 
  $formsent = 1;
             echo "<form method=\"post\" action=\"rate.php\">";
            echo "<div>";
           // echo "<input type=\"hidden\" name=\"modellingideaid\" value=\"$idea->id\" />";
             echo "<input type=\"hidden\" name=\"modellingid\" value=\"$desproblid\" />";
 foreach($ideas as $idea){
  
 	print_box_start('boxwidthwide boxaligncenter generalbox');
 	 
 	 
 	echo $idea->content.'<br/>';
 	$ideacreator=get_record("user","id",$idea->userid);
 	$fullname = fullname($ideacreator, true);
           $realuserinfo = " <a $CFG->frametarget
            href=\"$CFG->wwwroot/user/view.php?id=$idea->userid&amp;course=$course->id\">$fullname</a> ";
            
 	echo '<div class="userlink">';
 	echo $realuserinfo.' | '.userdate($idea->time,$strftimedatetime);
 	 
 		$ratings = NULL;
        $ratingsmenuused = false;
 	if ($modelling->assessed and isloggedin() and !isguestuser()) {
 		
 	 
 	$ratings = new object();
            if ($ratings->scale = make_grades_menu($modelling->scale)) {
             
            	$ratings->ratingtime=$modelling->ratingtime;
            	if($modelling->ratingtime==1){
             
            	 
                $ratings->assesstimestart = $modelling->assesstimestart;
                $ratings->assesstimefinish = $modelling->assesstimefinish;
            	}
                
               
            }
            
            if ($modelling->assessed == 2) {
             
                $ratings->allow = false;
            	} else {
           		 	if ($USER->id!=$idea->userid){
            		$ratings->allow = true;
 					echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
           		 	//if ( glossary_print_entry($course, $cm, $idea, $entry, $mode, $hook,1,$displayformat,$ratings) ) {
           		 	if (modelling_print_entry_ratings($course, $desproblid,$idea, $ratings ) ){
                $ratingsmenuused = true; 
            }
 				} 
            }      
 	}
        echo '<br/>';
 	echo '</div>';
  
 //if ( glossary_print_entry($course, $cm, $glossary, $entry, $mode, $hook,1,$displayformat,$ratings) ) {
                $ratingsmenuused = true;
          //  }
  		print_box_end();
 		 
  }
 
 	if ($ratingsmenuused) {

        echo "<div class=\"boxcenter\"><input type=\"submit\" value=\"".get_string("sendinratings", "modelling")."\" />";
        if ($modelling->scale < 0) {
        	
            if ($scale = get_record("scale", "id", abs($modelling->scale))) {
                print_scale_menu_helpbutton($course->id, $scale );
            }
        }
        echo "</div>";
    }
 	
 	if (!empty($formsent)) {
        // close the form properly if used
        echo "</div>";
        echo "</form>";
    }
    
    //end
    print_footer($course);
     

?>