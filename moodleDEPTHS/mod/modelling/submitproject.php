<head>
		
	 
		<script type="text/javascript" src="../../depths/lib/yoxview/yoxview-init.js"></script>
		<script type="text/javascript">

			$(document).ready(function(){ 
			    $(".yoxview").yoxview( {backgroundColor: '#808080',defaultDimensions: { iframe: { width: 500 }}});
			    $("#yoxviewText").yoxview({ textLinksSelector: "" ,defaultDimensions: { iframe: { width: 500 }}});
			});
		</script>
	</head>
<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
     require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');
     require_once($CFG->dirroot.'/depths/setup/depths_config.php');
     require_once($CFG->dirroot.'/depths/mapper/utility.php');
  

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $v  = optional_param('v'); 
    $delproject  = optional_param('delproject'); 
    $sel=optional_param('selproj');
    $submitprojectUri=urldecode(optional_param('submitproject'));
    $sort = optional_param('sort', 'studentsgroups', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 
 
    if($submitprojectUri){
    
    	if($submitprojectUri!=$_SESSION['lastsubmitted']){
    		 
   		 $_SESSION['projectsubmitted']=false;
   		 
    	}
    	 
    }
    
    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }
    
    if($submitprojectUri){
    	if(($_SESSION['lastsubmitted']!=$submitprojectUri)&&($_SESSION['projectsubmitted']!=true)){
    	 
    	$taskuri=get_condition_value("modelling_tasks",'taskuri','id', $t);
     
    	$sResponse=setProjectAsSubmitted($submitprojectUri,$taskuri);
    	$_SESSION['projectsubmitted']=true;
    	$_SESSION['lastsubmitted']=$submitprojectUri;
    	unset($_GET['submitproject']);
    	$sR=$sResponse[0];
    	print_message("***insert_table_to_rdf data 4:".$submitprojectUri,"submitProject");
    	print_message("response:".$sResponse,"submitProject");
  
    	foreach($sR as $sRkey=>$sRvalue){
    		 
    		 
    		$operation=$sR["operation"];
    		 
    		if($operation=="insertrecord"){
 
    				if($sRkey!="operation"){
    					$submittinguri=$sR["submittinguri"];
 
    					$currenttime=time();
    					$sql="INSERT INTO mdl_modelling_projects (submittinguri, instance, taskid, user_groupid,selectedproject,time) VALUES ('".$submittinguri."', '".$id."', '".$t."', '".$USER->id."','".$submitprojectUri."','".$currenttime."') ";
    					
    					execute_sql($sql);
    					 
    				}
    	 
    		
    		}else if($operation=="urimapping"){
    		
    			$urimapping=new object;
    			$urimapping->domainconcept=$value['domainconcept'];
    			$urimapping->instanceid=$value['instanceid'];
    			$urimapping->uri=$value['uri'];
    			insert_record("modelling_urimapping", $urimapping);
    		}
    
    	}
    } 
    }
 
    require_login($course->id);

    add_to_log($course->id, "modelling", "make group viewing", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
     print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
               
    //For teacher, view select button
    if (isteacher($cm->course)) {
     
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }

    $taskdata = get_record ("modelling_tasks", "id", $t);
 
			 
     global $newproject;
   
    	 
    	 if (!$newproject){
    	  
   			 $newproject = new object;
    	 }
    	
            $group = get_record("modelling_gr_students", "userid", modelling_return_user_groupid ($project, $USER->id));
          
            $newproject->user_groupid = modelling_return_user_groupid ($project, $USER->id);

            $newproject->instance = $id;
            $newproject->taskid = $t;
           
            $newproject->time = time();
    
    
    if ($delproject) {
    	 
        if (isteacher($cm->course)) {
        
        if ($oldprojectdata = get_record ("modelling_projects", "user_groupid", modelling_return_user_groupid ($project, $USER->id), "instance", $id, "id", $delproject)) {
                
        	delete_records ("modelling_projects", "id", $delproject);
           
            }
        }
        else
        {
        	
            if ($oldprojectdata = get_record ("modelling_projects", "user_groupid", modelling_return_user_groupid ($project, $USER->id), "instance", $id, "id", $delproject)) {
                //@unlink (modelling_get_file_path ($cm->course)."/".$oldfiledata->file);
                delete_records ("modelling_projects", "id", $delproject);
               
            }
        }
    }

    //----------------------------------//
    
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
    
    if (isteacher($cm->course)) {
    	
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
   
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';

    echo '<tr><td><b>'.get_string ('grouptaskdescription', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->description.'</td></tr>';
    
    if ($project->useprojectdates == "true") {
        echo '<tr><td><b>'.get_string ('grouptaskstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('grouptaskenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('scheduletasklateenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->laterenddate).'</td></tr>';
    }
    
    echo "</table>";
  
    print_simple_box_end();
    
    //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);
   
    if (!empty($bar['prev'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
    }

    $row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

    
    if (!empty($bar['next'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
    }
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    echo "<hr /><br />";
  
    $strftimedatetime = get_string("strftimerecent");
          	$strftimetime = get_string("strftimetime").":%S";   
    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
        //ovde napraviti da nastavnik ima pregled svih projekata studenata
        $sqlForAllProjects="SELECT * FROM ".$CFG->prefix."modelling_projects WHERE instance='".$id."' and taskid='".$t."'";
        	$allprojects=get_records_sql($sqlForAllProjects);
       	 
        if($allprojects){
        	echo "Projects submitted for assessment by students";
        	$titlesarray = Array ('#'=>'', 'Students/Groups'=>'studentsgroups', 'Uploaded Project'=>'uploadedproject', 'View Project'.modelling_makelocalhelplinkmain ("viewproject", "Help, View Project", "modelling")=>'',  'Date Added'=>'dateadded', 'Delete Project'.modelling_makelocalhelplinkmain ("deleteproject", "Help, Delete Project", "modelling")=>'');

        $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'submitproject.php?id='.$id.'&t='.$t);
        $table->align = array ("center", "left", "left", "center", "center", "center", "center");
        $table->width = "1000";
       
        foreach ($allprojects as $currentproject) {  
        	$response=getProjectByUri($currentproject->selectedproject);
        $proj=$response[0];
          		$projectData=$proj['project'];
          		$projectTitle=$projectData['title'];
        		$solutionData=$proj['solution'];
        	 
        		$diagrams=$proj['diagrams'];
        		$diva= '<div  class="yoxview">';
        		$divb= '</div>';
        		$di=0;
        	 
        foreach($diagrams as $k=>$diagram){
        		 	$cDiagramFileUrl=$diagram['fileUrl'];
        		  
        			$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
        			list($width, $height, $type, $attr) = getimagesize($imageLink);
        			$diagramTitle=$diagram['title'];
        			if($width>100){
        				$di++;
	  	    		 if($di==1){
	  	    		$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'">View</a>';
	  	    		 }else{
	  	    		 	$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'"></a>';
	  	    			
	  	    		 }
	  	    		$alllinks=$alllinks.$imageHref;
        			}
        		}
       	    $i=$i+1;
       	   
       	   $thisuserid= modelling_return_user_groupid_name ($project, $currentproject->user_groupid);
           $table->data[] = array ($i,$thisuserid,$projectTitle,$diva.$alllinks.$divb, userdate($currentproject->time,$strftimedatetime) , '<a href="submitproject.php?id='.$id.'&t='.$t.'&delproject='.$currentproject->id.'">Delete</a> '.modelling_makelocalhelplinkmain ("deleteproject", "Help, Delete Project", "modelling"));  
           $alllinks="";
        }
        if ($table) {
        	 echo '<center>';
             print_table($table);
             echo '</center>'; 
            }
        }
     }
    else
    {
    	 
       $currentUserGroupId=modelling_return_user_groupid ($project, $USER->id);
       
             $sqlForProjects="SELECT * FROM ".$CFG->prefix."modelling_projects WHERE instance='".$id."' and user_groupid='".$currentUserGroupId."' and taskid='".$t."'";
             $existingprojects=get_records_sql($sqlForProjects);
        if($existingprojects){
        	echo "Projects submitted for assessment";
       	   $table->head = array ("#","Project","Time sent", "View", "Delete");
            $table->align = array ("center","left", "center","center", "center");
            $table->width = "700";
        	$i=0;
       foreach ($existingprojects as $existingproject) {  
        	if($existingproject->selectedproject!=""){
      			$response=getProjectByUri($existingproject->selectedproject);
       			$proj=$response[0];
          		$projectData=$proj['project'];
          		$projectDate=$projectData['datetimecreatedE'];
          		$projectTitle=$projectData['title'];
        		$solutionData=$proj['solution'];
        		$diagrams=$proj['diagrams'];
        		 $diva= '<div  class="yoxview">';
        		   $divb= '</div>';
        		 $di=0;
        		foreach($diagrams as $k=>$diagram){
        		 	$cDiagramFileUrl=$diagram['fileUrl'];
        			$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
        			list($width, $height, $type, $attr) = getimagesize($imageLink);
        			$diagramTitle=$diagram['title'];
        			if($width>100){
        				$di++;
	  	    		 if($di==1){
	  	    		$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'">View</a>';
	  	    		 }else{
	  	    		 	$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'"></a>';
	  	    			
	  	    		 }
	  	    		$alllinks=$alllinks.$imageHref;
        			}
        		}
        			$i++; 
		
            $table->data[] = array ($i,$projectTitle, date("d M Y, H:m",$projectDate/1000) ,$diva.$alllinks.$divb, '<a href="submitproject.php?id='.$id.'&t='.$t.'&delproject='.$existingproject->id.'">Delete</a> '.modelling_makelocalhelplinkmain ("deleteproject", "Help, Delete Project", "modelling"));  
			 
            $alllinks="";
       	}
 
       }
      
        if ($table) {
        	
        	 echo '<center>';
             print_table($table);
             echo '</center>'; 
            
            }
         
        }
        
  

    
        class mod_modelling_uploadproject_form extends moodleform {
			var $_selProj;
			
			function getselproject(){
				return $this->_selProj;
			}
            function definition() {
                global $CFG, $cm, $USER, $project, $alreadyuploaded, $newproject;
                $mform    =& $this->_form;
//-------------------------------------------------------------------------------
                $mform->addElement('header', 'general', get_string('act_uploadproject', 'modelling'));
//-------------------------------------------------------------------------------
				$mform->addElement('text', 'selectedproject',get_string('selectedproject', 'modelling'), array('size'=>'60'));
		        $selectModelButton=$mform->addElement('button','modelSelect',get_string('modelSelectButtonTitle', 'modelling'));
		        
		        $options = 'menubar=1,toolbar=1,  location=1,scrollbars,resizable,width=1000,height=800';
		        $calledFrom='selectModel';
		        $url=$CFG->httpswwwroot."/depths/student/projects/projects_view.php?reload=1&calledFrom=".$calledFrom."&problemuri=".urlencode($project->designproblemuri);
		        $buttonattributes = array('title'=>get_string('modelSelectButtonTitleToolTip', 'modelling'), 'onClick'=>"depthsopenpopup('$url', '"
		                             . $selectModelButton->getName()."', '$options', 0);");
		        $selectModelButton->updateAttributes($buttonattributes);
//-------------------------------------------------------------------------------
 
		        $this->add_action_buttons($cancel = false, $submitlabel = get_string('submit', 'modelling'));
 
            }
            function definition_after_data(){
	            	global $newproject, $id, $t;
	             
	        		$mform    =& $this->_form;
	        		$this->_selProj=$mform->getElementValue('selectedproject');
            }
        }
 
	        echo '<br /><hr /><br />';
	      
	        
	        
	        $notSubmittedProjects=getProjectsNotSubmitted($project->designproblemuri);
	        if($notSubmittedProjects){
	        	$nstable->head = array ("#","Project","Time sent", "View", "Submit");
	        	$nstable->align = array ("center","left", "center","center", "center");
	        	$nstable->width = "700";
	        	$i=0;
	        	foreach ($notSubmittedProjects as $notsubmittedproject) {
	         
	        			$nsProjectData=$notsubmittedproject[0];
	        		        		 
	        			$projectUri=$nsProjectData['projecturi'];
	        			
	        			$projectData=$nsProjectData['project'];
	        			$projectTitle=$projectData['title'];
	        			$projectTime=$projectData['datetimecreatedE'];
	        		 
	        			$solutionData=$nsProjectData['solution'];
	        			$diagrams=$nsProjectData['diagrams'];
	        			$diva= '<div  class="yoxview">';
	        			$divb= '</div>';
	        			$di=0;
	        		 
	        			foreach($diagrams as $k=>$diagram){
	        				
	        				$cDiagramFileUrl=$diagram['fileUrl'];
	        			 
	        				$imageLink=$CFG->imagerepository."/".$cDiagramFileUrl;
	        			 
	        				list($width, $height, $type, $attr) = getimagesize($imageLink);
	        				$diagramTitle=$diagram['title'];
	        				if($width>100){
	        					$di++;
	        				if($di==1){
	        					$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'">View</a>';
	        				}else{
	        					$imageHref='<a class="yoxviewLink" href="'.$imageLink.'" title="'.$diagramTitle.'"></a>';
	        	  	  
	        				}
	        				$alllinks=$alllinks.$imageHref;
	        				 
	        				}
	        			}
	        
	        				
	        
	        			$i++;
	        			//date("d M Y, H:m",$projectDate/1000)
	        			$nstable->data[] = array ($i,$projectTitle, date("d M Y, H:m",$projectTime/1000) ,$diva.$alllinks.$divb, '<a href="submitproject.php?id='.$id.'&t='.$t.'&submitproject='.urlencode($projectUri).'">Submit</a> '.modelling_makelocalhelplinkmain ("submituploadedproject", "Help, Submit Project", "modelling"));
	        
	        			$alllinks="";
	        	//	}
	        
	        	}
	        
	        	if ($nstable) {
	        		 
	        		echo '<center>';
	        		//echo 'Select uploaded project as solution';
	        		echo get_string('selectuploadedproject', 'modelling');
	        		print_table($nstable);
	        		echo '</center>';
	        
	        	}
	        	 
	        }       
	        $mform = new mod_modelling_uploadproject_form('submitproject.php?id=' . $id . '&t=' . $t );
 
	        echo '<center>';
	       
	        $mform->display(); 
	        echo '</center>'; 
 		
	        $selP=$mform->getselproject();
	        $newproject->selectedproject = $selP;
	       
	        insert_record ("modelling_projects", $newproject);
        if ($mform->_selProj){
        	
   			redirect ("submitproject.php?id=".$id."&t=".$t);
        	}
  }   	
    print_footer($course);  

?>