<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $name = optional_param('name'); 
    $description = optional_param('description'); 
    $timeopen = optional_param('timeopen'); 
    $timeclose = optional_param('timeclose'); 
    $laterenddate = optional_param('laterenddate'); 
    //$allowstudentstoaddtopics = optional_param('allowstudentstoaddtopics'); 
    
    $update = optional_param('update'); 
 

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "brainstorm", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
     
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    
    if (!isteacher($cm->course)) {
        error("Only for teachers");
    }
    
    
    ///////////////////////////////////
   
    //---------------------------
    
    if ($name) {
        $task = new object;
        $task->instance = $id;
        $task->name = $name;
        if ($description) {
            $task->description = $description;
        }
        else
        {
            $task->description = "";
        }
        
        if ($timeopen && $timeclose) {
            $task->startdate = mktime (0,0,0,$timeopen['month'],$timeopen['day'],$timeopen['year']);
            $task->enddate = mktime (0,0,0,$timeclose['month'],$timeclose['day'],$timeclose['year']);
            $task->laterenddate = mktime (0,0,0,$laterenddate['month'],$laterenddate['day'],$laterenddate['year']);
        }
        /*if ($allowstudentstoaddtopics) {
            $task->allowstudentstoaddtopics = $allowstudentstoaddtopics;
        }
        else
        {
            $task->allowstudentstoaddtopics = 0;
        }*/
        
        $task->type = "brainstorm";
        
        if ($update) {
            $task->id = $update;
            if (update_record("modelling_tasks", $task)) {
                redirect ("view.php?id=".$id, "Brainstorm Task updated");
            }
        }
        else
        {
            $positiontasks = get_records ("modelling_tasks", "instance", $id, "position desc");
            
            if ($positiontasks) {
                $positiontasks = current($positiontasks);
                $task->position = $positiontasks->position + 100;
            }
            else
            {
                $task->position = 100;
            }
            if (insert_record("modelling_tasks", $task)) {
                redirect ("view.php?id=".$id, "Brainstorm Task added");
            }
        }
    }
    
    //---------------------------
    
    class mod_modelling_brainstorm_form extends moodleform {

        function definition() {

            global $CFG, $cm, $project, $USER, $update;

            if ($update) {
                $data = get_record ("modelling_tasks", "id", $update);
            }
            
            $mform    =& $this->_form;
            
            $mform->addElement('header', 'general', get_string('brainstorm', 'modelling'));
              
            $mform->addElement('text', 'name', get_string('brainstormtaskname', 'modelling'), array('size'=>'64'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addRule('name', null, 'required', null, 'client');

            $mform->addElement('textarea', 'description', get_string('brainstormtaskdescription', 'modelling'), 'rows="3" cols="48"');
            
            if ($project->useprojectdates == "true") {
                $mform->addElement('date_selector', 'timeopen', get_string('assessmenttaskstartdate', 'modelling'));
                $mform->addElement('date_selector', 'timeclose', get_string('assessmenttaskenddate', 'modelling'));
                $mform->setDefault('timeclose', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
                $mform->addElement('date_selector', 'laterenddate', get_string('assessmenttasklateenddate', 'modelling'));
                $mform->setDefault('laterenddate', mktime(0,0,0,date("m") + 1,date("d"),date("Y"))); 
            }
            
            /*$mform->addElement('checkbox', 'allowstudentstoaddtopics', get_string('brainstormallowstudentstoaddtopics', 'modelling')); 
            $mform->setDefault('allowstudentstoaddtopics', 1); */
            
            if ($update) {
                $mform->setDefault('name', $data->name);
                $mform->setDefault('description', $data->description);
                $mform->setDefault('timeopen', $data->startdate);
                $mform->setDefault('timeclose', $data->enddate);
                $mform->setDefault('laterenddate', $data->laterenddate);
            // $mform->setDefault('allowstudentstoaddtopics', $data->allowstudentstoaddtopics);
            }
             
        
            $this->add_action_buttons(false); 
           
        }
    }
    
    if (!$update) {
        $mform = new mod_modelling_brainstorm_form('task_brainstorm.php?id=' . $id);
    }
    else
    {
        $mform = new mod_modelling_brainstorm_form('task_brainstorm.php?id=' . $id . '&update=' . $update);
    }
   
    $mform->display();
    
if ($project->scale < 0) {
	print_box_start('boxwidthwide boxaligncenter generalbox');
        	echo '<br/><center>'.get_string('checkscaleordermessage', 'modelling').'</center> <br/>';
            if ($scale = get_record("scale", "id", abs($project->scale))) {
               
                echo '<table cellpadding="6" cellspacing="0" width="400" align="center">';
        echo '<tr><td colspan=2>Scale name:<b>'.$scale->name.'</b></td></tr>';
       // echo '<tr><td><form action="projectassessment.php?id='.$id.'&t='.$t.'" method="post" name="templatepreviewform">';
         
//                $mform->addElement('html','<br/><center>Scale name:<strong>'.$scale->name.'</strong></center>');
                 $scalearray=make_scalearray_from_list_not_reversed($scale->scale);
 	 			 $scaleitemid=0;
 	  			foreach ($scalearray as $scaleitem){
//	   		 
//	   			$mform->addElement('html','<br/><center>value:<strong>'.$scaleitemid.'</strong> label:<strong>'.$scaleitem.'</strong></center>');
//   
 echo '<tr><td colspan=2>'.$scaleitemid.' = '.$scaleitem.'</td></tr>';           
	   			$scaleitemid=$scaleitemid+1;
             }
            	  echo '</table>';
        }
         print_box_end();
}
 //test
    

    print_footer($course);

?>