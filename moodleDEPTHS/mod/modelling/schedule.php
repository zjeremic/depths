<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov

    require_once("../../config.php");
    require_once("lib.php");
    require_once ($CFG->dirroot.'/course/moodleform_mod.php');

    $id = required_param('id'); 
    $t  = required_param('t'); 
    $v  = optional_param('v'); 
    
    $dayavailable  = optional_param('dayavailable'); 
    $beginhour  = optional_param('beginhour'); 
    $beginminute  = optional_param('beginminute'); 
    $endhour  = optional_param('endhour'); 
    $endminute  = optional_param('endminute'); 
    
    $sort = optional_param('sort', 'time', PARAM_ALPHA); 
    $orderby = optional_param('orderby', 'ASC', PARAM_ALPHA); 
    
    $removetime = optional_param('removetime'); 
    $removeappointment = optional_param('removeappointment'); 
    $sh = optional_param('sh'); 
    $shdel = optional_param('shdel'); 
    
    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "modelling", "schedule viewing", "view.php?id=$id", "$cm->instance");
    
/// Print the page header

    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";
    
     
    

    print_header("$course->shortname: $project->name", "$course->fullname",
                 "$navigation <a href=\"index.php?id=$course->id\">Project</a> -> $project->name", 
                  "", "", true, update_module_button($id, $course->id, $strproject), 
                  navmenu($course));
                  
    //For teacher, view select button
    if (isteacher($cm->course)) {
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&t='.$t.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }

    $taskdata = get_record ("modelling_tasks", "id", $t);
    
    //-------Add users to Group---------//
    if ($dayavailable && count_records ("modelling_schedule", "day", mktime (0,0,0,$dayavailable['month'],$dayavailable['day'],$dayavailable['year'])) == 0) {
        $starttime = $beginhour * 60 + $beginminute;
        $endtime = $endhour * 60 + $endminute + 1;
        
        $cicls = round(($endtime - $starttime) / $taskdata->duration);
        
        $alltime = 0;
    
        for ($i=1; $i <= $cicls; $i++) {
            $shedule = new object;
            $shedule->instance = $id;
            $shedule->day = mktime (0,0,0,$dayavailable['month'],$dayavailable['day'],$dayavailable['year']);
            $shedule->begin = $beginhour . ":" . $beginminute;
            $shedule->end = $endhour . ":" . $endminute;
            $alltime += $taskdata->duration;
            $shedule->timeperiod = $alltime;
            
            insert_record ("modelling_schedule", $shedule);
        }
    }
    //-------------Remove Time---------//
    if (isteacher($cm->course) && $removetime) {
        foreach ($removetime as $removetimekey => $removetimevalue) {
            delete_records ("modelling_schedule", "id", $removetimekey);
        }
    }
    //-------------Remove appointment---//
    if (isteacher($cm->course) && $removeappointment) {
        foreach ($removeappointment as $removeappointmentkey => $removeappointmentvalue) {
            set_field ("modelling_schedule", "student_groupid", "", "id", $removeappointmentkey);
        }
    }
    //-----Remove student appointment---//
    if ($shdel) {
        set_field ("modelling_schedule", "student_groupid", "", "id", $shdel, "student_groupid", modelling_return_user_groupid ($project, $USER->id));
    }
    //----------------------------------//
    
    if ($sh) {
        set_field ("modelling_schedule", "student_groupid", modelling_return_user_groupid ($project, $USER->id), "id", $sh);
        set_field ("modelling_schedule", "time", time(), "id", $sh);
    }
    
    //----------------------------------//
    
    echo '<div style="text-align: center"><h1>'.$taskdata->name.'</h1></div>';
    
    if (isteacher($cm->course)) {
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
    
    print_simple_box_start('center', '100%', '#ffffff', 10);
    
    echo '<table cellpadding="5" cellspacing="0">';

    echo '<tr><td><b>'.get_string ('scheduletaskdescription', 'modelling').'</b></td>';
    echo '<td>'.$taskdata->description.'</td></tr>';
    
    if ($project->useprojectdates == "true") {
        echo '<tr><td><b>'.get_string ('scheduletaskstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->startdate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('scheduletaskenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->enddate).'</td></tr>';
        echo '<tr><td><b>'.get_string ('scheduletasklateenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $taskdata->laterenddate).'</td></tr>';
    }
    
    echo "</table>";
    
    print_simple_box_end();
    
    //------------PRINT TABS------------//
    $row  = array();

    $bar = modelling_gettabbar ($id, $t);
   
    if (!empty($bar['prev'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['prev'], "<< Previous Task");
    }

    $row[] = new tabobject('return', "view.php?id=$id", get_string('returntomainprojectpage', 'modelling'));

    
    if (!empty($bar['next'])) {
        $row[] = new tabobject('return', $CFG->wwwroot . "/mod/modelling/" . $bar['next'], "Next Task >>");
    }
    
    $tabs[] = $row;

    print_tabs($tabs);
    //----------------------------------//
    
    echo "<hr /><br />";
    
    class mod_modelling_addschedule_form extends moodleform {

        function definition() {

            global $CFG, $cm, $USER, $project;

            $mform    =& $this->_form;
        
//-------------------------------------------------------------------------------
            $mform->addElement('header', 'general', get_string('addinganewsession', 'modelling'));

            $mform->addElement('date_selector', 'dayavailable', get_string('dayavailable', 'modelling'));
            
            $hours = array();
            
            for ($i = 0; $i <24; $i++) {
                if ($i < 10) {
                    $hours[$i] = "0".$i;
                }
                else
                {
                    $hours[$i] = $i;
                }
            }
            
            $minutes = array();
            
            for ($i = 0; $i <60; $i=$i+5) {
                if ($i < 10) {
                    $minutes[$i] = "0".$i;
                }
                else
                {
                    $minutes[$i] = $i;
                }
            }
            
            $standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('select', 'beginhour', get_string('hour', 'form'), $hours);
            $standardfilename[] = &MoodleQuickForm::createElement('select', 'beginminute', get_string('minute', 'form'), $minutes);
            $mform->addGroup($standardfilename, 'begin', get_string('timepresentationswillbegin', 'modelling'), ' ', false);
            $mform->setDefault('beginhour', '9'); 

            $standardfilename = array();
            $standardfilename[] = &MoodleQuickForm::createElement('select', 'endhour', get_string('hour', 'form'), $hours);
            $standardfilename[] = &MoodleQuickForm::createElement('select', 'endminute', get_string('minute', 'form'), $minutes);
            $mform->addGroup($standardfilename, 'end', get_string('timepresentationswillend', 'modelling'), ' ', false);
            $mform->setDefault('endhour', '10'); 
//-------------------------------------------------------------------------------

            $this->add_action_buttons($cancel = false, $submitlabel = get_string('add', 'modelling'));

        }
    }

    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
        $mform = new mod_modelling_addschedule_form('schedule.php?id=' . $id . '&t=' . $t);
        $mform->display(); 
        
        echo "<hr /><br />";
        
        if (count_records ("modelling_schedule", "instance", $id) > 0) {
            $titlesarray = Array ('Time'=>'time', 'Students/Groups'=>'studentsgroups', 'Remove Time Period'=>'', 'Remove Appointment'=>'', 'Date Added'=>'dateadded');

            $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'schedule.php?id='.$id.'&t='.$t);
            $table->align = array ("left", "center", "center", "center", "center");
            $table->width = "800";
            
            if ($schedule = get_records ("modelling_schedule", "instance", $id, "id")) {
                foreach ($schedule as $schedule_) {
                    $printabledata = modelling_shedule_colculate ($schedule_, $taskdata, $project, $id, $t);
                    
                    $table->data[] = array ($printabledata['time'], $printabledata['student_groupid'], $printabledata['removetime'], $printabledata['removeappointment'], $printabledata['dateadded']);
                }
            }
            
            $table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);
            
            if ($table) {
                echo '<br /><form action="schedule.php?id='.$id.'&t='.$t.'" method="post">';
                print_table($table);
                echo '<br /><center><input type="submit" value="'.get_string("savechanges", "modelling").'" /></center></form>';
            }
        }
    }
    else
    {
        $titlesarray = Array ('Time'=>'time', 'Students/Groups'=>'studentsgroups', 'Assigned Topic'=>'assignedtopi', 'Date Added'=>'dateadded');

        $table->head = modelling_make_table_headers ($titlesarray, $orderby, $sort, 'schedule.php?id='.$id.'&t='.$t);
        $table->align = array ("left", "center", "center", "center");
        $table->width = "700";
        
        if ($schedule = get_records ("modelling_schedule", "instance", $id, "id")) {
            foreach ($schedule as $schedule_) {
                $printabledata = modelling_shedule_colculate ($schedule_, $taskdata, $project, $id, $t);
                
                $table->data[] = array ($printabledata['time'], $printabledata['addedlink'], $printabledata['topic'], $printabledata['dateadded']);
            }
            
            $table->data = modelling_sort_table_data ($table->data, $titlesarray, $orderby, $sort);
            
            if ($table) {
                print_table($table);
            }
        }
    }
        
    print_footer($course);

?>