  <?php echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">\n";
        echo "<html $direction>\n";
        echo "<head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n";
    ?>    
  
  <script type="text/javascript" src="../../depths/lib/scripts/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../depths/lib/scripts/jquery-ui-1.8.16.custom.min.js"></script>
 <script type="text/javascript" src="../../depths/lib/yoxview/yoxview-init.js"></script>
 
  

 <script type="text/javascript">
 jQuery(document).ready(function(){
	var $ = jQuery.noConflict();
	
   });
 </script>
   <script type="text/javascript" src="../../depths/lib/recommendationDialog.js"></script>
     <script type="text/javascript" src="../../depths/lib/tags/dialogsInit.js"></script>
 <link rel="stylesheet" type="text/css" href="../../depths/lib/styles/smoothness/jquery-ui-1.8.16.custom.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../depths/lib/styles/tagclouds.css" media="screen" />
 </head>
<?php // $Id: v 2.0 2007/12/01 12:37:00 serafim panov
echo '

 
 <link rel="stylesheet" type="text/css" href="../../depths/lib/styles/smoothness/jquery-ui-1.8.16.custom.css" media="screen" />


';
 
    require_once("../../config.php");
    require_once("lib.php");
   
      require_once($CFG->dirroot.'/mod/modelling/pagelib.php');
     require_once($CFG->libdir.'/blocklib.php');
     require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php');
     require_once($CFG->dirroot.'/depths/setup/depths_config.php');  
     require_once($CFG->dirroot.'/depths/mapper/utility.php');
    

    $id = required_param('id'); 
    $a  = optional_param('a', 0, PARAM_INT);  
    $v  = optional_param('v'); 
    $uplink  = optional_param('uplink'); 
    $downlink  = optional_param('downlink'); 
    $delete  = optional_param('delete'); 
    $addweighting  = optional_param('addweighting'); 
    $weighting  = optional_param('weighting', 0, PARAM_INT); 
    $t  = optional_param('t', 0, PARAM_INT); 
        $edit = optional_param('edit', -1, PARAM_BOOL);
 

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $project = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    if (! $modelling = get_record("modelling", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
    } else {
        if (! $project = get_record("modelling", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $project->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("modelling", $project->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }
 
    require_login($course->id);
 
 require_once($CFG->dirroot.'/depths/mapper/utility.php');
 
 print_message('modelling id:'.$modelling->id,"getpeersjson");
    $_SESSION["lastaccessedproblem"]=$modelling->id;
    print_message('lastaccessedproblem:'.$_SESSION['lastaccessedproblem'],"getpeersjson");
   
    add_to_log($course->id, "modelling", "view", "view.php?id=$id", "$cm->instance");

/// Print the page header
 
    $navigation = "<a href=\"../../course/view.php?id=$course->id\">$course->shortname</a> ->";

   // Initialize $PAGE, compute blocks
    $PAGE       = page_create_instance($modelling->id);
  
    $pageblocks = blocks_setup($PAGE);
   
    $blocks_preferred_width = bounded_number(180, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]), 210);
  
if (($edit != -1) and $PAGE->user_allowed_editing()) {
        $USER->editing = $edit;
    }
  
 
     $meta = '';
    if (!empty($CFG->enablerssfeeds) && !empty($CFG->data_enablerssfeeds) && $data->rssarticles > 0) {
        $rsspath = rss_get_url($course->id, $USER->id, 'data', $data->id);
        $meta .= '<link rel="alternate" type="application/rss+xml" ';
        $meta .= 'title ="'. format_string($course->shortname) .': %fullname%" href="'.$rsspath.'" />';
    }
    if ($data->csstemplate) {
        $meta .= '<link rel="stylesheet" type="text/css" href="'.$CFG->wwwroot.'/mod/data/css.php?d='.$data->id.'" /> ';
    }
    if ($data->jstemplate) {
        $meta .= '<script type="text/javascript" src="'.$CFG->wwwroot.'/mod/data/js.php?d='.$data->id.'"></script>';
    }
  
/// Print the page header
    $PAGE->print_header($course->shortname.': %fullname%', '', $meta);

     
    //----------------ACTIONS---------------//
/// If we have blocks, then print the left side here
    if (!empty($CFG->showblocksonmodpages)) {
    	 
        echo '<table id="layout-table"><tr>';
        if ((blocks_have_content($pageblocks, BLOCK_POS_LEFT) || $PAGE->user_is_editing())) {
        	 
            echo '<td style="width: '.$blocks_preferred_width.'px;" id="left-column">';
            
            if (!empty($THEME->customcorners)) print_custom_corners_start();
          
            blocks_print_group($PAGE, $pageblocks, BLOCK_POS_LEFT);
           
            if (!empty($THEME->customcorners)) print_custom_corners_end();
           
            echo '</td>';
        }
        echo '<td id="middle-column">';
        if (!empty($THEME->customcorners)) print_custom_corners_start();
    }
  
    if ($uplink && isteacher($cm->course)) {
    	 
        $tasks = get_records ("modelling_tasks", "instance", $id, "position");
        foreach ($tasks as $task) {
            if ($task->id == $uplink && $beforeid) {
                $positionbefore  = $tasks[$beforeid]->position;
                $positioncurrent = $tasks[$uplink]->position;
                set_field ("modelling_tasks", "position", $positionbefore, "id", $uplink);
                set_field ("modelling_tasks", "position", $positioncurrent, "id", $beforeid);
            }
            else
            {
                $beforeid = $task->id;
            }
        }
    }
 
    //--------------------------------------//
  
    if ($downlink && isteacher($cm->course)) {
    	 
        $tasks = get_records ("modelling_tasks", "instance", $id, "position desc");
        foreach ($tasks as $task) {
            if ($task->id == $downlink && $beforeid) {
                $positionbefore  = $tasks[$beforeid]->position;
                $positioncurrent = $tasks[$downlink]->position;
                set_field ("modelling_tasks", "position", $positionbefore, "id", $downlink);
                set_field ("modelling_tasks", "position", $positioncurrent, "id", $beforeid);
            }
            else
            {
                $beforeid = $task->id;
            }
        }
    }
 
    //--------------------------------------//
    
    if ($delete && isteacher($cm->course)) {
    	
        $tasks = get_records ("modelling_tasks", "instance", $id, "position");
        unset ($tasks[$delete]);
        //delete_records ("modelling_tasks", "id", $delete);
       
        modelling_delete_task("modelling_tasks", "id", $delete);
        
        $position = 100;
        foreach ($tasks as $task) {
            set_field ("modelling_tasks", "position", $position, "id", $task->id);
            $position += 100;
        }
    }
  
    //--------------------------------------//
  
    if ($weighting && $t && isteacher($cm->course)) {
    	 
        if (count_records("modelling_tasks", "instance", $id, "type", "assessment") > 1) {
            set_field ("modelling_tasks", "weighting", $weighting, "id", $t);
            if ($otherassessmentdata = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_tasks WHERE instance='".$id."' and type='assessment'")) {
                $totalweighting = 0;
                foreach ($otherassessmentdata as $otherassessmentdata_) {
                    $totalweighting += $otherassessmentdata_->weighting;
                }
                if ($totalweighting > 100) {
                    $needde = 100 - $totalweighting;
                    foreach ($otherassessmentdata as $otherassessmentdata_) {
                        if ($otherassessmentdata_->id != $t) {
                            if (($otherassessmentdata_->weighting + $needde) >= 0) {
                                set_field ("modelling_tasks", "weighting", ($otherassessmentdata_->weighting + $needde), "id", $otherassessmentdata_->id);
                            }
                        }
                    }
                }
                else if ($totalweighting < 100) {
                    $needde = 100 - $totalweighting;
                    foreach ($otherassessmentdata as $otherassessmentdata_) {
                        if ($otherassessmentdata_->id != $t) {
                            if (($otherassessmentdata_->weighting + $needde) >= 0) {
                                set_field ("modelling_tasks", "weighting", ($otherassessmentdata_->weighting + $needde), "id", $otherassessmentdata_->id);
                            }
                        }
                    }
                }
            }
        }
    }
  
    //--------------------------------------//
    
    if ($addweighting && isteacher($cm->course)) {
    	 
        foreach ($addweighting as $addweightingkey => $addweightingvalue) {
            $totalweighting += $addweightingvalue;
        }
        if ($totalweighting != 100) {
            error ("Please check total weighting, (".$totalweighting."% now)", "view.php?id=".$id);
        }
        else
        {
            foreach ($addweighting as $addweightingkey => $addweightingvalue) {
                set_field ("modelling_tasks", "weighting", $addweightingvalue, "id", $addweightingkey);
            }
        }
    }
 
    //--------------------------------------//
    //-----------------CHECK Assessment-----//
    
    $countofassessement = count_records ("modelling_tasks", "instance", $id, "type", "assessment");
    
    if ($countofassessement == 1) {
    	 
        $dataofassessment = get_record ("modelling_tasks", "instance", $id, "type", "assessment");
        if ($dataofassessment->weighting != 100) {
            set_field ("modelling_tasks", "weighting", 100, "id", $dataofassessment->id);
        }
    }
     //--------------------------------------//
  
    if (isteacher($cm->course)) {
    	 
        if (empty($_SESSION['SESSION']->modelling_teacherview)) {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "teacher") {
            $_SESSION['SESSION']->modelling_teacherview = "teacherview";
        }
        if ($v == "student") {
            $_SESSION['SESSION']->modelling_teacherview = "studentview";
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&v=teacher" method="post"><input type="submit" value="'.get_string('teacherview', 'modelling').'"></form></div>';
        }
        if ($_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '<div style="text-align: right"><form action="?id='.$id.'&v=student" method="post"><input type="submit" value="'.get_string('studentview', 'modelling').'"></form></div>';
        }
    }
              
    echo '<div style="text-align: center"><h1>'.print_heading_with_help($project->name, "mods", "modelling").'</h1></div>';
    
    if (isteacher($cm->course)) {
     
        if ($_SESSION['SESSION']->modelling_teacherview == "studentview") {
            echo '<div style="text-align: center">'.get_string('studentview1', 'modelling').'</div><br />';
        }
        else
        {
            echo '<div style="text-align: center">'.get_string('teacherview1', 'modelling').'</div><br />';
        }
    }
 
    print_simple_box_start('center', '100%', '#ffffff', 10);
  
    echo '<div style="text-align: center"><h3>'.get_string ('projectdetails', 'modelling').'</h3></div>';
    
    echo '<table cellpadding="5" cellspacing="0">';
    echo '<tr><td><b>'.get_string ('projectdescription', 'modelling').'</b></td>';
    echo '<td>'.$project->description.'</td></tr>';
   
    if ($project->useprojectdates == "true") {
    	 
        echo '<tr><td><b>'.get_string ('projectstartdate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $project->timeopen).'</td></tr>';
        echo '<tr><td><b>'.get_string ('projectenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $project->timeclose).'</td></tr>';
        echo '<tr><td><b>'.get_string ('lateenddate', 'modelling').'</b></td>';
        echo '<td>'.date("d M Y", $project->timelate).'</td></tr>';
    }
   
    if ($project->filename != "none") {
        echo '<tr><td><b>'.get_string ('projectfile', 'modelling').'</b></td>';
        echo '<td><a href="'.$CFG->wwwroot.'/file.php/1/'.str_replace ("__", "_", str_replace (" ", "_", $project->filename)).'">view file</a></td></tr>';
    }
    $returnedjson=getRelatedConceptsForDesignProblem($modelling->designproblemuri);
    $conceptsjson=$returnedjson[1];
   
    $recommendedconcepts=$conceptsjson['recommendedconcepts'];
    echo '<tr><td><b>Recommended reading:&nbsp;&nbsp;</b></td>';
    echo '<td>';
    foreach($recommendedconcepts as $k1=>$concept){
    
    	$conceptname=$concept['conceptname'];
    	$concepturi=$concept['concepturi'];
    	echo '<a onclick="return showDEPTHSRecommendationDialog(\''.$CFG->wwwroot.'/depths/services/online_resources/view_suggestions.php?conceptUri='.urlencode($concepturi).'\',\''.$conceptname.'\'); return false;" href="#" title="View recommended resources">'.$conceptname.'</a>&nbsp;&nbsp;';
    
    }
    echo '</td></tr>';
   
    if($project->selectedproject !=""){
    	
        $selectedprojecturi=$project->selectedproject;
    	$response=getProjectByUri($selectedprojecturi);
		$proj=$response[0];
		$projectData=$proj['project'];
		$solutionData=$proj['solution'];
		$createdBy=$projectData['creatorname'];
		$modifiedBy=$projectData['modifiedByName'];
		$diagrams=$proj['diagrams'];
		$projectName=$projectData['title'];
		$dateC=$projectData['datetimecreatedE'];
	 
		$dateCreated=modelling_dateformat ($dateC/1000);
		$projectDescription=$solutionData['description'];
		
		if ($projectDescription==""){
			$projectDescription=get_string("notextprovided","depths");
		}
		$designRules=$solutionData["hasDesignRules"];
		if($designRules==""){
			$designRules=get_string("notextprovided","depths");
		}

		$designConstraints=$solutionData["hasDesignConstraints"];
		if($designConstraints==""){
			$designConstraints=get_string("notextprovided","depths");
		}

		$additional_requirements=$solutionData["hasAdditionalRequirements"];
		if($additional_requirements==""){
			$additional_requirements=get_string("notextprovided","depths");
		}
			
		$consequences=$solutionData["hasConsequences"];
		if($consequences==""){
			$consequences=get_string("notextprovided","depths");
		}
			
		$pros=$solutionData["hasPros"];
		if($pros==""){
			$pros=get_string("notextprovided","depths");
		}

		$cons=$solutionData["hasCons"];
		if($cons==""){
			$cons=get_string("notextprovided","depths");
		}
	
	include("showProject.php");	
	 
 
   } 

  

     echo "</table>";
   
   print_simple_box_end();
      
    echo "<hr /><br />";
    
    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
   
        if ($project->useprojectdates == "true") {
        	 
            $table->head = array ("Task Name ".modelling_makelocalhelplinkmain ("taskname", "Help, Task Name", "modelling"), "Assessment<br />Weighting".modelling_makelocalhelplinkmain ("assessmentweighting", "Help, Assessment Weighting", "modelling"), "Start&nbsp;Date", "End&nbsp;Date", "Late&nbsp;End&nbsp;Date", "<table><tr><td nowrap>Teacher&nbsp;Mark ".modelling_makelocalhelplinkmain ("teachermark", "Help, Teacher Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Students'&nbsp;Mark".modelling_makelocalhelplinkmain ("studentsmark", "Help, Students Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Self&nbsp;Mark".modelling_makelocalhelplinkmain ("selfmark", "Help, Self Mark", "modelling")."</td></tr></table>");
            $table->align = array ("left", "center", "center", "center", "center", "center", "center", "center");
            $table->size = array ("300px", "100px", "100px", "100px", "100px", "100px", "100px", "100px");
            $table->wrap = array ("1");
            $table->width = "1000px";
            $width = 1000;
        }
        else
        {
      	  
            $table->head = array ("Task Name".modelling_makelocalhelplinkmain ("taskname", "Help, Task Name", "modelling"), "Assessment<br />Weighting".modelling_makelocalhelplinkmain ("assessmentweighting", "Help, Assessment Weighting", "modelling"), "<table><tr><td nowrap>Teacher&nbsp;Mark".modelling_makelocalhelplinkmain ("teachermark", "Help, Teacher Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Students'&nbsp;Mark".modelling_makelocalhelplinkmain ("studentsmark", "Help, Students Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Self&nbsp;Mark".modelling_makelocalhelplinkmain ("selfmark", "Help, Self Mark", "modelling")."</td></tr></table>");
            $table->align = array ("left", "center", "center", "center", "center");
            $table->size = array ("300px", "100px", "100px", "100px", "100px");
            $table->wrap = array ("1");
            $table->width = "700px";
            $width = 700;
              
        }
        
    }
    else
    {
   
        if ($project->useprojectdates == "true") {
            $table->head = array ("Task Name".modelling_makelocalhelplinkmain ("taskname", "Help, Task Name", "modelling"), "Start&nbsp;Date", "End&nbsp;Date", "Late&nbsp;End&nbsp;Date", "<table><tr><td nowrap>Teacher&nbsp;Mark".modelling_makelocalhelplinkmain ("teachermark", "Help, Teacher Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Students'&nbsp;Mark".modelling_makelocalhelplinkmain ("studentsmark", "Help, Students Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Self&nbsp;Mark".modelling_makelocalhelplinkmain ("selfmark", "Help, Self Mark", "modelling")."</td></tr></table>", "Progress");
            $table->align = array ("left", "center", "center", "center", "center", "center", "center", "center");
            $table->size = array ("300px", "100px", "100px", "100px", "100px", "100px", "100px", "100px");
            $table->wrap = array ("1");
            $table->width = "1000px";
        }
        else
        {
            $table->head = array ("Task Name".modelling_makelocalhelplinkmain ("taskname", "Help, Task Name", "modelling"), "<table><tr><td nowrap>Teacher&nbsp;Mark".modelling_makelocalhelplinkmain ("teachermark", "Help, Teacher Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Students'&nbsp;Mark".modelling_makelocalhelplinkmain ("studentsmark", "Help, Students Mark", "modelling")."</td></tr></table>", "<table><tr><td nowrap>Self&nbsp;Mark".modelling_makelocalhelplinkmain ("selfmark", "Help, Self Mark", "modelling")."</td></tr></table>", "Progress");
            $table->align = array ("left", "left", "center", "center", "center");
            $table->size = array ("300px", "100px", "100px", "100px", "100px");
            $table->wrap = array ("1");
            $table->width = "700px";
        }
    }
   
    $needshow = 0;
    $noteforstudent = "";
  
    $tasks = get_records("modelling_tasks", "instance", $id, "position");
    
    
    $tc = 0;
      
    foreach ($tasks as $task) {
  
        $startdate = modelling_dateformat ($task->startdate);
        $enddate = modelling_dateformat ($task->enddate);
        $laterdate = modelling_dateformat ($task->laterenddate);
        
        $needlink = false;
        
        if ($task->type == "brainstorm") {
         
        	 
            //$brainstormallowstudentstoaddtopics = $task->allowstudentstoaddtopics;
        }
        
        if ($task->startdate > time()) {
        	 
            $ico = '<span style="background: #ccf;border: 1px solid black;width: 10px;">&nbsp;&nbsp;&nbsp;</span>';
        }
        else if ($task->enddate > time()) {
        	 
            $ico = '<span style="background: #cfc;border: 1px solid black;width: 10px;">&nbsp;&nbsp;&nbsp;</span>';
            $needlink = true;
        }
        else if ($task->laterenddate > time() && !empty($task->laterenddate)) {
        	 
            $ico = '<span style="background: #ffc;border: 1px solid black;width: 10px;">&nbsp;&nbsp;&nbsp;</span>';
            $needlink = true;
        }
        else
        {
        	 
            $ico = '<span style="background: #fcc;border: 1px solid black;width: 10px;">&nbsp;&nbsp;&nbsp;</span>';
        }
        
        if ($project->useprojectdates == "false") {
        	 
            $ico = '<span style="background: #cfc;border: 1px solid black;width: 10px;">&nbsp;&nbsp;&nbsp;</span>';
            $needlink = true;
        }
        
        if ($needlink) {
        	 
            $link_1 = '<a href="'.$task->type.'.php?id='.$id.'&t='.$task->id.'">';
            $link_2 = '</a>';
        }
        else
        {
        	 
            $link_1 = '';
            $link_2 = '';
        }
    
        $assessementweighting = "";
        
        //---------------Create Marks-----------------//
        
        unset ($teachermark, $studentsmark, $selfmark);
        
        if ($task->type == "assessment") {
         
        
            unset($data, $countpr, $datares);
        
            if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
                $files = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_files WHERE instance='".$id."' and taskid='".$task->tasktoassess."'");
            }
            else
            {
                $files = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_files WHERE instance='".$id."' and taskid='".$task->tasktoassess."' and user_groupid='".modelling_return_user_groupid ($project, $USER->id)."'");
            }
        
            if ($files) {
                foreach ($files as $file) {
                    $topic = get_record ("modelling_topics", "user_groupid", $file->user_groupid, "instance", $id);
                    $data[] = modelling_get_assessment_criterias ($task->id, $topic->id, $project, $topic);
                }
            }
            
            foreach ($data as $datacriteria) {
                foreach ($datacriteria as $dataratings) {
                    if (is_array($dataratings['averstudent'])) {
                        $countpr = explode ("/", $dataratings['averstudent'][0]);
                        $datares['averstudent']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['averstudent']['num'] += 1;
                    }
                    else
                    {
                        $datares['averstudent']['count'] += 0;
                        $datares['averstudent']['num'] += 1;
                    }
                    //-------------------------------------------------------------------------//
                    if (is_array($dataratings['self'])) {
                        $countpr = explode ("/", $dataratings['self'][0]);
                        $datares['self']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['self']['num'] += 1;
                    }
                    else
                    {
                        $datares['self']['count'] += 0;
                        $datares['self']['num'] += 1;
                    }
                    //-------------------------------------------------------------------------//
                    if (is_array($dataratings['averteacher'])) {
                        $countpr = explode ("/", $dataratings['averteacher'][0]);
                        $datares['averteacher']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['averteacher']['num'] += 1;
                    }
                    else
                    {
                        $datares['averteacher']['count'] += 0;
                        $datares['averteacher']['num'] += 1;
                    }
                }
            }
      
            $teachermark  = round($datares['averteacher']['count']/$datares['averteacher']['num'], 2) * 100;
            $studentsmark  = round($datares['averstudent']['count']/$datares['averstudent']['num'], 2) * 100;
            $selfmark  = round($datares['self']['count']/$datares['self']['num'], 2) * 100;
            
            $overalmark[$task->id]['teachermark']['count'] = $teachermark;
            $overalmark[$task->id]['teachermark']['weighting'] = $task->weightingteacher;
            $overalmark[$task->id]['studentsmark']['count'] = $studentsmark;
            $overalmark[$task->id]['studentsmark']['weighting'] = $task->weightingpeer;
            $overalmark[$task->id]['selfmark']['count'] = $selfmark;
            $overalmark[$task->id]['selfmark']['weighting'] = $task->weightingself;
            $overalmark[$task->id]['weighting'] = $task->weighting;
            
            $teachermark  .= "/100"."<br />(".$task->weightingteacher."%)";
            $studentsmark  .= "/100"."<br />(".$task->weightingpeer."%)";
            $selfmark  .= "/100"."<br />(".$task->weightingself."%)";
            
            
            //-----Assessement Weighting---------//
            
            //Number of Assessements
            $tc ++;
            //---
            
            if ($countofassessement < 3) {
                $assessementweighting = '<form action="?id='.$id.'" name="changeweighting'.$task->id.'" id="taskform'.$task->id.'" method="post"><select name="addweighting'.$task->id.'" onchange="document.changeweighting'.$task->id.'.action = document.changeweighting'.$task->id.'.addweighting'.$task->id.'.options[document.changeweighting'.$task->id.'.addweighting'.$task->id.'.selectedIndex].value;document.changeweighting'.$task->id.'.submit(); return true;" >';
            }
            else
            {
                $assessementweighting = '<select name="addweighting['.$task->id.']">';
            }
            
            if (count_records("modelling_tasks", "instance", $id, "type", "assessment") > 1) {
                $otherassessmentdata = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_tasks WHERE instance='".$id."' and type='assessment'");
                for ($i=100; $i>=0; $i--) {
                    $weightingoptions[$i] = $i;
                }
                foreach ($weightingoptions as $optionkey => $optionvalue) {
                    if ($countofassessement < 3) {
                        $assessementweighting .= '<option value ="view.php?id='.$id.'&t='.$task->id.'&weighting='.$optionkey.'" ';
                    }
                    else
                    {
                        $assessementweighting .= '<option value ="'.$optionkey.'" ';
                    }
                    if ($optionkey == $task->weighting) {
                       $assessementweighting .= ' selected="selected" ';
                    }
                    $assessementweighting .= '>'.$optionvalue.'</option>';
                }
            }
            else
            {
                $assessementweighting .= '<option value ="view.php?id='.$id.'&t='.$task->id.'&weighting='.$task->weighting.'">'.$task->weighting.'</option>';
            }
            $assessementweighting .= '</select>';
            
            if (($tc == $countofassessement) && ($countofassessement > 2)) {
                $assessementweighting .= '<br /><input type="submit" value="Change Weightings">';
            }
            
            if ($countofassessement < 3) {
                $assessementweighting .= '</form>';
            }
        }
   
        //--------------------if project assessment------------------------//
         if ($task->type == "projectassessment") {
         
        
            unset($data, $countpr, $datares);
       
            if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            	 
                $submissions = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_projects WHERE instance='".$id."' and taskid='".$task->tasktoassess."'");
            }
            else
            {
            	 
                $submissions = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_projects WHERE instance='".$id."' and taskid='".$task->tasktoassess."' and user_groupid='".modelling_return_user_groupid ($project, $USER->id)."'");
            }
        
            if ($submissions) {
            	 
                foreach ($submissions as $subm) {
                 
                    $submission = get_record ("modelling_projects", "user_groupid", $subm->user_groupid, "instance", $id);
                     
                    $data[] = modelling_get_assessment_criterias ($task->id,  $project, $submission);
                }
            }
            
            foreach ($data as $datacriteria) {
                foreach ($datacriteria as $dataratings) {
                    if (is_array($dataratings['averstudent'])) {
                        $countpr = explode ("/", $dataratings['averstudent'][0]);
                        $datares['averstudent']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['averstudent']['num'] += 1;
                    }
                    else
                    {
                        $datares['averstudent']['count'] += 0;
                        $datares['averstudent']['num'] += 1;
                    }
                    //-------------------------------------------------------------------------//
                    if (is_array($dataratings['self'])) {
                        $countpr = explode ("/", $dataratings['self'][0]);
                        $datares['self']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['self']['num'] += 1;
                    }
                    else
                    {
                        $datares['self']['count'] += 0;
                        $datares['self']['num'] += 1;
                    }
                    //-------------------------------------------------------------------------//
                    if (is_array($dataratings['averteacher'])) {
                        $countpr = explode ("/", $dataratings['averteacher'][0]);
                        $datares['averteacher']['count'] += round(($countpr[0]/$countpr[1]),2);
                        $datares['averteacher']['num'] += 1;
                    }
                    else
                    {
                        $datares['averteacher']['count'] += 0;
                        $datares['averteacher']['num'] += 1;
                    }
                }
            }
      
            $teachermark  = round($datares['averteacher']['count']/$datares['averteacher']['num'], 2) * 100;
            $studentsmark  = round($datares['averstudent']['count']/$datares['averstudent']['num'], 2) * 100;
            $selfmark  = round($datares['self']['count']/$datares['self']['num'], 2) * 100;
            
            $overalmark[$task->id]['teachermark']['count'] = $teachermark;
            $overalmark[$task->id]['teachermark']['weighting'] = $task->weightingteacher;
            $overalmark[$task->id]['studentsmark']['count'] = $studentsmark;
            $overalmark[$task->id]['studentsmark']['weighting'] = $task->weightingpeer;
            $overalmark[$task->id]['selfmark']['count'] = $selfmark;
            $overalmark[$task->id]['selfmark']['weighting'] = $task->weightingself;
            $overalmark[$task->id]['weighting'] = $task->weighting;
            
            $teachermark  .= "/100"."<br />(".$task->weightingteacher."%)";
            $studentsmark  .= "/100"."<br />(".$task->weightingpeer."%)";
            $selfmark  .= "/100"."<br />(".$task->weightingself."%)";
            
            
            //-----Assessement Weighting---------//
            
            //Number of Assessements
            $tc ++;
            //---
            
            if ($countofassessement < 3) {
                $assessementweighting = '<form action="?id='.$id.'" name="changeweighting'.$task->id.'" id="taskform'.$task->id.'" method="post"><select name="addweighting'.$task->id.'" onchange="document.changeweighting'.$task->id.'.action = document.changeweighting'.$task->id.'.addweighting'.$task->id.'.options[document.changeweighting'.$task->id.'.addweighting'.$task->id.'.selectedIndex].value;document.changeweighting'.$task->id.'.submit(); return true;" >';
            }
            else
            {
                $assessementweighting = '<select name="addweighting['.$task->id.']">';
            }
            
            if (count_records("modelling_tasks", "instance", $id, "type", "projectassessment") > 1) {
                $otherassessmentdata = get_records_sql ("SELECT * FROM ".$CFG->prefix."modelling_tasks WHERE instance='".$id."' and type='projectassessment'");
                for ($i=100; $i>=0; $i--) {
                    $weightingoptions[$i] = $i;
                }
                foreach ($weightingoptions as $optionkey => $optionvalue) {
                    if ($countofassessement < 3) {
                        $assessementweighting .= '<option value ="view.php?id='.$id.'&t='.$task->id.'&weighting='.$optionkey.'" ';
                    }
                    else
                    {
                        $assessementweighting .= '<option value ="'.$optionkey.'" ';
                    }
                    if ($optionkey == $task->weighting) {
                       $assessementweighting .= ' selected="selected" ';
                    }
                    $assessementweighting .= '>'.$optionvalue.'</option>';
                }
            }
            else
            {
                $assessementweighting .= '<option value ="view.php?id='.$id.'&t='.$task->id.'&weighting='.$task->weighting.'">'.$task->weighting.'</option>';
            }
            $assessementweighting .= '</select>';
            
            if (($tc == $countofassessement) && ($countofassessement > 2)) {
                $assessementweighting .= '<br /><input type="submit" value="Change Weightings">';
            }
            
            if ($countofassessement < 3) {
                $assessementweighting .= '</form>';
            }
        }
        
        //---------------------end of project assessment-----------------------//
   
        if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
          
            $editlink = '<a href="task_'.$task->type.'.php?id='.$id.'&update='.$task->id.'"><img src="'.$CFG->wwwroot.'/mod/modelling/images/update.gif" border="0" /></a> ';

            if ($task->position != 100) {
                $uplink = '<a href="view.php?id='.$id.'&uplink='.$task->id.'"><img src="'.$CFG->wwwroot.'/mod/modelling/images/up.gif" border="0" /></a>';
            }
            else
            {
                $uplink = '';
            }
            
            if ($task->position != count($tasks) * 100) {
                $downlink = '<a href="view.php?id='.$id.'&downlink='.$task->id.'"><img src="'.$CFG->wwwroot.'/mod/modelling/images/down.gif" border="0" /></a>';
            }
            else
            {
                $downlink = '';
            }
            
            $deletelink = '<a href="view.php?id='.$id.'&delete='.$task->id.'" onclick="if(confirm(\'Delete ?\')) return true; else return false;"><img src="'.$CFG->wwwroot.'/mod/modelling/images/delete.gif" border="0" /></a>';
            
            $editlink = $editlink . $deletelink . $uplink . $downlink .modelling_makelocalhelplinkmain ("updatetask", "Help, Update Details", "modelling");
            
            if ($project->useprojectdates == "true") {
                $table->data[] = array ($ico . " " . $link_1 . $task->name . $link_2 . " " . $editlink, $assessementweighting, $startdate, $enddate, $laterdate, $teachermark, $studentsmark, $selfmark);
            }
            else
            {
                $table->data[] = array ($ico . " " . $link_1 . $task->name . $link_2 . " " . $editlink, $assessementweighting, $teachermark, $studentsmark, $selfmark);
            }
            
            
            if ($task->type == "assessment") {
                if (count_records ("modelling_crit_name", "assessmentid", $task->id) == 0) {
                    $allertmessage['assessment']['name'] = $task->name;
                }
            }
            
            //ovo pogledati
       if ($task->type == "projectassessment") {
               if (count_records ("modelling_crit_name", "assessmentid", $task->id) == 0) {
                   $allertmessage['assessment']['name'] = $task->name;
               }
           }
        }
        else
        {
      
            //Check make group
            $challerts['makegroup'] = "Not done";
            $chgroups = get_records ("modelling_groups", "instance", $id);
            foreach ($chgroups as $chgroup) {
                if (get_record ("modelling_gr_students", "groupid", $chgroup->id, "userid", $USER->id)) {
                    $challerts['makegroup'] = "Done";
                }
            }
            
            //Check brainstorm signup
             
            if ($chbrainstorm = get_record("modelling_ideas", "instance", $task->id, "userid", modelling_return_user_groupid ($project, $USER->id))) {
            	 
                $challerts['brainstorm'] = "Done";
            }
            else
            {
                $challerts['brainstorm'] = "Not done";
            }
             
            //Check Schedule
            if (get_record("modelling_schedule", "instance", $id, "student_groupid", modelling_return_user_groupid ($project, $USER->id))) {
                $challerts['schedule'] = "Done";
            }
            else
            {
                $challerts['schedule'] = "Not done";
            }
            
            //Check Submit
            if (get_record("modelling_files", "instance", $id, "user_groupid", modelling_return_user_groupid ($project, $USER->id), "taskid", $task->id)) {
                $challerts['submit'] = "Done";
            }
            else
            {
                $challerts['submit'] = "Not done";
            }
            
        //Check Submit UML solution
            if (get_record("modelling_projects", "instance", $id, "user_groupid", modelling_return_user_groupid ($project, $USER->id), "taskid", $task->id)) {
                $challerts['submitproject'] = "Done";
            }
            else
            {
                $challerts['submitproject'] = "Not done";
            }
            
            //Check Assessment 
            
            if (get_record("modelling_as_rating", "assessmentid", $task->id, "userid", $USER->id) && !empty($submission->id)) {
                $challerts['assessment'] = "Done";
            }
            else
            {
                $challerts['assessment'] = "Not done";
            }
            
        //Check Project  Assessment 
            
            if (get_record("modelling_as_rating", "assessmentid", $task->id, "userid", $USER->id) && !empty($submission->id)) {
                $challerts['projectassessment'] = "Done";
            }
            else
            {
                $challerts['projectassessment'] = "Not done";
            }
            
            //----------------------------------------//
            //-----Check previous task complited------//
            if ($task->type != "makegroup" || $task->type != "assessment") {
                if ($task->type == "brainstorm" && $challerts['makegroup'] == "Not done" && ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project")) {
                    $link_1 = "";
                    $link_2 = "";
                    if ($project->projecttype != "teacher groups project") {
                        $noteforstudent .= get_string('notejointogroup', 'modelling')."<br />";
                    }
                    else
                    {
                        $noteforstudent .= get_string('noteteachertogroup', 'modelling')."<br />";
                    }
                }
                /*
                if ($task->type == "submit" && $challerts['brainstorm'] == "Not done" && $task->fasttracktosubmit == 0 && $project->projecttype != "individual") {
                    $link_1 = "";
                    $link_2 = "";
                    if (!empty($noteforstudent)) {
                        $noteforstudent .= "After<br />";
                    }
                   if ($brainstormallowstudentstoaddtopics == 1) {
                        $noteforstudent .= get_string('noteaddtopic', 'modelling')."<br />";
                    }
                    else
                    {
                        $noteforstudent .= get_string('noteaddtopicbyteacher', 'modelling')."<br />";
                    }
                }
                */
               
            }
            //----------------------------------------//
            
      
            if ($project->useprojectdates == "true") {
                  $table->data[] = array ($ico . " " . $link_1 . $task->name . $link_2, $startdate, $enddate, $laterdate, $teachermark, $studentsmark, $selfmark, $challerts[$task->type]);
            }
            else
            {
                 $table->data[] = array ($ico . " " . $link_1 . $task->name . $link_2, $teachermark, $studentsmark, $selfmark, $challerts[$task->type]);
            }
        }
    }
  
    if (!empty($noteforstudent)) {
        print_simple_box_start('center', '400px', '#ffffff', 10);
        echo '<div style="text-align: center"><h3>'.get_string ('messagesforstudent', 'modelling').'</h3></div>';
        echo '<br />'.$noteforstudent.'';
        print_simple_box_end();
    }
   
    if (!empty($table) && count_records("modelling_tasks", "instance", $id) > 0) {
    	
        if (($countofassessement > 2) && isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
        	 
            echo '<form action="?id='.$id.'" name="changeweightings" method="post">';
        }
        
        print_table($table);
        
        if (($countofassessement > 2) && isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
            echo '</form>';
        }
    }
    else
    {
        echo '<div style="text-align: center"><b>'.get_string ('notasksyet', 'modelling').'</b></div>';
    }
    
    
    //--OVER MARK-------------------------------------//
  
    foreach ($overalmark as $overalmark_) {
        $totalovermark['count'] = $overalmark_['teachermark']['count'] * ($overalmark_['teachermark']['weighting'] / 100) + $overalmark_['studentsmark']['count'] * ($overalmark_['studentsmark']['weighting'] / 100) + $overalmark_['selfmark']['count'] * ($overalmark_['selfmark']['weighting'] / 100);
        $totalovermark['num'] ++;
        $totalovermark['countfinal'] += $totalovermark['count'] * ($overalmark_['weighting'] / 100);
    }
    
    
    
    $totalovermarkres = round ($totalovermark['countfinal'], 1);
 
    if ($totalovermark['countfinal'] > 0) {
        echo "<center><br />" . get_string("currenttotalfinalmark", "modelling") . " " . $totalovermarkres . "/100<br /></center>";
    }
    else
    {
        echo "<center><br />" . get_string("currenttotalfinalmark", "modelling") . " " . get_string("nomarksmadeyet", "modelling") . "<br /></center>";
    }
    
    //------------------------------------------------//
   
  
    if (isteacher($cm->course) && $_SESSION['SESSION']->modelling_teacherview == "teacherview") {
    	 
    
        $options['view.php?id='.$id] = "Add new task";
    
        echo '<center><br /><table width="'.$width.'"><tr><td><div><form action="?id='.$id.'" name="addtask" id="taskform" method="post"><select name="addtaskvalue" onchange="document.addtask.action = document.addtask.addtaskvalue.options[document.addtask.addtaskvalue.selectedIndex].value;document.addtask.submit(); return true;" >';
        if ($project->projecttype == "student groups" || $project->projecttype == "teacher groups project") {
            $options['task_makegroup.php?id='.$id] = "Make group";
        }
        
        $options['task_brainstorm.php?id='.$id] = "brainstorm";
        $options['task_submit.php?id='.$id] = "submit file";
        $options['task_submitproject.php?id='.$id] = "submit UML model";
        $options['task_schedule.php?id='.$id] = "schedule";
        $options['task_assessment.php?id='.$id] = "file assessment";
        $options['task_projectassessment.php?id='.$id] = "project assessment";
        
        foreach ($options as $optionkey => $optionvalue) {
            echo '<option value ="'.$optionkey.'">'.$optionvalue.'</option>';
        }
        
        echo '</select>'.modelling_makelocalhelplinkmain ("addtask", "Help with Add Task (new window)", "modelling").'</form></div></td></tr></table></center>'; // 
        
        if ($allertmessage['assessment']['name']) {
            echo "<br /><br /><center><b><font color=\"red\">Warning: No criteria have been set for \"".$allertmessage['assessment']['name']."\"</font></b> </center>";
        }
    }
    else
    {
    
    	
        echo '<div style="margin-top: 3em;">
        <div style="text-align:center;"><b>Legend</b></div>
        <center><table width="auto" style="border: 1px solid black;">
        <tr>
        <td style="background: #ccf;border: 1px solid black;width: 10px; padding: 3px;border: 1px solid black;"></td>
        <td style="padding: 3px;border: 1px solid black;">Task has not opened yet&nbsp;</td>
        </tr>
        <tr>
        <td style="background: #cfc;border: 1px solid black;width: 10px; padding: 3px;border: 1px solid black;"></td>
        <td style="padding: 3px;border: 1px solid black;">Task is open&nbsp;</td>
        </tr>
        <tr>
        <td style="background: #ffc;border: 1px solid black;width: 10px; padding: 3px;border: 1px solid black;"></td>
        <td style="padding: 3px;border: 1px solid black;">Task has ended&nbsp;</td>
        </tr>
        <tr>
        <td style="background: #fcc;border: 1px solid black;width: 10px; padding: 3px;border: 1px solid black;"></td>
        <td style="padding: 3px;border: 1px solid black;">Task late submission closed&nbsp;</td>
        </tr>
        </table></center>
        </div>';
    }
/// If we have blocks, then print the left side here
    if (!empty($CFG->showblocksonmodpages)) {
    	 
        if (!empty($THEME->customcorners)) print_custom_corners_end();
        echo '</td>';   // Middle column
        if ((blocks_have_content($pageblocks, BLOCK_POS_RIGHT) || $PAGE->user_is_editing())) {
            echo '<td style="width: '.$blocks_preferred_width.'px;" id="right-column">';
            if (!empty($THEME->customcorners)) print_custom_corners_start();
            blocks_print_group($PAGE, $pageblocks, BLOCK_POS_RIGHT);
            if (!empty($THEME->customcorners)) print_custom_corners_end();
            echo '</td>';
        }
        echo '</tr></table>';
    }
    
    print_footer($course);
  

?>
