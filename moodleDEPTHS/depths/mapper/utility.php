<?php
function print_message($message,$fileType='insert')
{
	global $CFG;
	  
    $myFile=$CFG->moodlelogsdir.$fileType.'.txt';  
 
   $date = date("Y-m-d H:i:s").": ";
     $s= "\r\n".$date;
    $s.=$message;
    $s.="\r\n";    
   $fh=@fopen($myFile,'a');
    fwrite($fh,$s);
    fclose($fh);
    print_in_one_file($message);
    
}
function print_error_message($message,$fileType='insert')
{
	global $CFG;
	$myFile=$CFG->moodlelogsdir.$fileType.'.txt';
   // $myFile='/home/zoran/moodlelogs/_'.$fileType.'.txt';  
     $s= "\r\n";
    $s.=$message;
    $s.="\r\n";    
   $fh=@fopen($myFile,'a');
    fwrite($fh,$s);
    fclose($fh);
}
function print_in_one_file($message){
	//require_once("../config.php");
	global $CFG;
	$myFile=$CFG->moodlelogsdir.'_onefile.txt'; 
	 $s= "\r\n";
    $s.=$message;
    $s.="\r\n";    
   $fh=@fopen($myFile,'a');
    fwrite($fh,$s);
    fclose($fh);
}
function  get_condition_value($table, $valuename, $field1, $value1, $field2='',$value2=''){
	
 	$record=get_record($table,$field1, $value1, $field2,$value2);
 	$myReturnValue=$record->$valuename;
 	 return $myReturnValue;
 }
 function explore_dataobject($dataobject,$table,$fileName){
	print_message("--------------".$table,$fileName);
	foreach ($dataobject as $k=>$v){
		print_message("k: ".$k." v: ".$v,$fileName);
	}
}
function explore_depths_data_fields($table, $field1, $value1, $field2, $value2, $field3, $value3,$fileName){
	print_message("----explore_depths_data_fields----------".$table,$fileName);
	 
		print_message("field1: ".$field1." value1: ".$value1,$fileName);
		print_message("field2: ".$field2." value2: ".$value2,$fileName);
		print_message("field3: ".$field3." value3: ".$value3,$fileName);
	 
}
?>