<?php


class InsertRDFData {


	function InsertRDFData(){
			

	}

	function depths_update_record($table,$dataobject){
		global $CFG;
		require_once($CFG->dirroot.'/depths/mapper/utility.php');
		print_message("update record in table:".$table." id:".$dataobject->id,'update_tables');
		$this->depths_rdf_set_record("update",$table,$dataobject, $dataobject->id);
		print_message("update record in table finished:".$table,'update_tables');
	}
	function depths_set_field($table, $newfield, $newvalue, $field1, $value1, $field2='', $value2='', $field3='', $value3=''){

	 global $CFG;
	 require_once($CFG->dirroot.'/depths/mapper/utility.php');
	 require_once($CFG->dirroot."/depths/rest/curl_client.php");
	 $tables=array("user");
	 if(in_array($table,$tables)){
	 	$dataArray=array();
	 	$dataobject=array();
	 	$dataArray['action']="update";
	 	$dataArray['tablename']=$table;
	 	$url=$CFG->resturl."insert/table";
	 if($table=='user'){
	  		$dataArray['id']=$value1;
	 		$userUri=get_condition_value("modelling_urimapping",'uri','instanceid',$value1,'domainconcept','User');
	 		$dataAddOn['useruri']=$userUri;
	 		$dataobject[$newfield]=$newvalue;
	  
	 }
	 
	 print_message("***set_field_table_to_rdf 5:".$table,"tables_set_field");
	 $dataArray['dataobject']=json_encode($dataobject);
	 $dataArray['addon']=json_encode($dataAddOn);
	 $data=json_encode($dataArray);
	 print_message("***set_field_table_to_rdf table:".$table,"tables_set_field");
	 print_message("***set_field_table_to_rdf service:".$url,"tables_set_field");
	 print_message("***set_field_table_to_rdf data:".$data,"tables_set_field");
	 $response=curl_call($url,'POST', $data );
	 }
	 
	 print_message("depths_set_field finishing".$table." field1:".$value1,"tables_set_field");
	 return '';
	}
	function depths_insert_record($table,$dataobject,$id){
		global $CFG;
		require_once($CFG->dirroot.'/depths/mapper/utility.php');
		print_message("insert record in table:".$table,'tables');

		$this->depths_rdf_set_record("insert",$table,$dataobject, $id);

	}

	function depths_rdf_set_record($action,$table,$dataobject, $id)
	{
			
		global $CFG;
		require_once($CFG->dirroot.'/depths/mapper/utility.php');
		$depths_config=$CFG->dirroot.'/depths/setup/depths_config.php';
		require_once($depths_config);
			
		print_message("depths_insert_record new ".$table." id:".$id,'tables');

		if ($table=='cache_text'){

	 	return '';
	 }else{

	 	explore_dataobject($dataobject, $table,'tables_to_insert');
	 	//check_insert_table_type($table,$dataobject,$id);

	 	$tables=array('user','forum_posts','forum_read','forum_ratings','forum_discussions',
		'forum','course','lesson','resource','workshop',
		'workshop_submissions','workshop_assessments','quiz',
		'question','quiz_attempts','quiz_question_instances','question_answers','question_states',
		'chat','chat_messages','message','message_read','modelling','modelling_tasks','modelling_files',
		'modelling_ideas','scale','modelling_idea_ratings','modelling_projects','role_assignments',
		'groups','groups_members','groupings', 'groupings_groups', 'user'
	 	);

	 	print_message("action".$action."_table_to_rdf 2:".$table,"insert");
	 	if(in_array($table,$tables)){
	 		print_message("table is processing:".$table,"insert");
	 		$url=$CFG->resturl."insert/table";
	 		print_message("table to insert on url:".$CFG->resturl,"insert");
	 		require_once($CFG->dirroot."/depths/rest/curl_client.php");
	 		$dataArray=array();
	 		$dataArray['sessionid']=session_id();
	 		$dataArray['action']=$action;
	 		$dataArray['tablename']=$table;
	 		$dataArray['id']=$id;
	 		$dataAddOn=array();
			 if($table=='course'){
			 	if($action=='update'){
			 		$courseUri=get_condition_value("modelling_urimapping","uri","instanceid",$id,'domainconcept','Course');
			 		$dataAddOn['courseuri']= $courseUri ;
			 	}else{
			 		$dataAddOn["additionaldata"]="no";
			 	}
			
	 	} else if($table=='modelling_files'){
	 		$userUri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->user_groupid,'domainconcept','User');
	 		$dataAddOn['useruri']=$userUri;
	 		$task=get_record("modelling_tasks","id",$dataobject->taskid);
	 		$dataAddOn['taskname']=$task->name;
	 		$dataAddOn['taskuri']=$task->taskuri;
	 	}else if($table=='user'){
	  		$userUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','User');
	  		$dataAddOn['useruri']=$userUri;
	  }else if($table=='role_assignments'){
	 		$context=get_record("context",'id',$dataobject->contextid);
	 		$courseUri=get_condition_value("modelling_urimapping","uri","instanceid",$context->instanceid,'domainconcept','Course');
	 		$dataAddOn['courseuri']= $courseUri ;
	 		$userUri=get_condition_value("modelling_urimapping","uri","instanceid",$dataobject->userid,'domainconcept',"User");
	 		$dataAddOn['useruri']= $userUri;
	 	}else if($table=='modelling_as_crit_rating'){
	 		$scaleitem=get_record("modelling_crit_rating","id",$dataobject->rate);
	 		$dataAddOn['scaleitem']=json_encode($scaleitem);
	 	}else if($table=='modelling_idea_ratings'){
	 		$idea=get_record("modelling_ideas","id",$dataobject->entryid);
	 		$task=get_record("modelling_tasks","id",$idea->instance);
	 		$cm=get_record("course_modules","id",$task->instance);
	 		$modeling=get_record("modelling","id",$cm->instance);

	 		$scaleid=$modeling->scale;
	 		$dataAddOn['scaleid']=$scaleid;
	 		if($scaleid<0){
	 			$scaleitemuri=get_condition_value("modelling_urimapping",'uri','instanceid',$scaleid*(-1),"parameter",$dataobject->rating,'domainconcept','ScaleItem');
	 			//$scaleitemuri=$modeling->scale;
	 			$dataAddOn['scaleitemuri']=$scaleitemuri;
	 		}

	 		$scaleuri=get_condition_value("modelling_urimapping",'uri','instanceid',$scaleid,'domainconcept','Scale');
	 		$dataAddOn['scaleuri']=$scaleuri;

	 		$brainstorminguri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->entryid,'domainconcept','Brainstorming');
	 		$dataAddOn['brainstorminguri']=$brainstorminguri;
	 		print_message("test task uri:".$task->taskuri,"test");
	 		$dataAddOn['brainstormuri']=$task->taskuri;

	 		$userId=$dataobject->userid;
	 		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$userId,'domainconcept','User');
	 		$dataAddOn["useruri"]=$useruri;
	 		if($action=='update'){
	 			$userRatingUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','UserRating');
	 			$dataAddOn['userratinguri']=$userRatingUri;
	 		}


	 	}else if ($table=='modelling_tasks'){
	 		$cm=get_record('course_modules','id',$dataobject->instance);
	 		$dp=get_record('modelling','id',$cm->instance);
	 		$dataAddOn["designproblemuri"]=$dp->designproblemuri;
	 		if($dataobject->tasktoassess>0){
	 			$dataAddOn["taskToAssessUri"]=get_condition_value("modelling_tasks",'taskuri','id',$dataobject->tasktoassess);
	 		}
	 		if($action=='update'){
	 			if($dataobject->type=='brainstorm'){
	 				$dataAddOn["brainstormuri"]=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Brainstorm');
	 			}else if($dataobject->type=='submitproject'){
	 				$dataAddOn["submissionuri"]=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Submission');
	 			}else if($dataobject->type=='projectassessment'){
	 				$dataAddOn["assessmenturi"]=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Assessment');
	 			}
	 			 
	 		}
	 		 
	 	} else if($table=='forum_posts'){
	 		$returnValue=get_condition_value($table,'id','discussion',$dataobject->discussion,'parent',0);
	 		$dataAddOn['inreplyto']=$returnValue;
	 	}else if($table=='modelling_ideas'){

	 		$userId=$dataobject->userid;
	 		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$userId,'domainconcept','User');

	 		$brainstormid=$dataobject->instance;
	 		$brainstormuri=get_condition_value("modelling_urimapping",'uri','instanceid',$brainstormid,'domainconcept','Brainstorm');

	 		$dataAddOn['useruri']=$useruri;
	 		$dataAddOn['brainstormuri']=$brainstormuri;
	 		if($action=='update'){
	 			$brainstormingUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Brainstorming');
	 			$dataAddOn['brainstorminguri']=$brainstormingUri;
	 		}
	 			
	 	}else if($table=='modelling_projects'){

	 		$userId=$dataobject->user_groupid;
	 		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$userId,'domainconcept','User');
	 		$dataAddOn['useruri']=$useruri;

	 		$submissionId=$dataobject->taskid;

	 		$record=get_record("modelling_urimapping",'instanceid', $submissionId,'domainconcept','Submission');
	 		$submissionuri=$record->uri;
	 		print_message("myReturnValue:".$submissionuri,"Track");

	 		$dataAddOn['submissionuri']=$submissionuri;
	 		if($action=='update'){
	 			$submittingUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Submitting');
	 			$dataAddOn['designproblemuri']=$submittingUri;
	 		}
	 			
	 	}else if($table=='modelling'){
	 		$courseid=$dataobject->course;
	 		$dataobject->description="";
	 		$courseuri=get_condition_value("modelling_urimapping",'uri','instanceid',$courseid,'domainconcept','Course');
	 		$dataAddOn['courseuri']=$courseuri;

	 		$scaleid=$dataobject->scale;
	 		$scaleuri=get_condition_value("modelling_urimapping",'uri','instanceid',$scaleid,'domainconcept','Scale');

	 		$dataAddOn['scaleuri']=$scaleuri;
	 		if($action=='update'){
	 			$dpUri=get_condition_value("modelling","designproblemuri","id",$id);
	 			$dataAddOn['designproblemuri']=$dpUri;
	 		}
	 		 
	 		$groupinguri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->groupingid,'domainconcept','Grouping');
	 		$dataAddOn['groupinguri']=$groupinguri;


	 	}else if($table=='modelling_crit_name'){
	 		$task=get_record("modelling_tasks","id",$dataobject->assessmentid);

	 		$dataAddOn['assessmenturi']=$task->taskuri;
	 	}else if($table=='modelling_crit_rating'){

	 		$scaleuri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->criteriaid,'domainconcept','Scale');
	 		$dataAddOn['scaleuri']=$scaleuri;

	 	}else if($table=='groups'){

	 		$courseid=$dataobject->courseid;
	 		$courseuri=get_condition_value("modelling_urimapping",'uri','instanceid',$courseid,'domainconcept','Course');
	 		$dataAddOn['courseuri']=$courseuri;
	 		if($action=='update'){
	 			$groupUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Group');
	 			$dataAddOn['groupuri']=$groupUri;
	 		}
	 	}else if($table=='groups_members'){
	 		$userId=$dataobject->userid;
	 		$useruri=get_condition_value("modelling_urimapping",'uri','instanceid',$userId,'domainconcept','User');
	 		$dataAddOn['useruri']=$useruri;
	 		$groupId=$dataobject->groupid;
	 		$groupuri=get_condition_value("modelling_urimapping",'uri','instanceid',$groupId,'domainconcept','Group');
	 		$dataAddOn['groupuri']=$groupuri;
	 	}else if($table=="groupings"){
	 		$courseid=$dataobject->courseid;
	 		$courseuri=get_condition_value("modelling_urimapping",'uri','instanceid',$courseid,'domainconcept','Course');
	 		$dataAddOn['courseuri']=$courseuri;
	 		 	
	 	}else if($table=='groupings_groups'){
	 	
	 		$groupinguri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->groupingid,'domainconcept','Grouping');
	 		$dataAddOn['groupinguri']=$groupinguri;
	 		$groupuri=get_condition_value("modelling_urimapping",'uri','instanceid',$dataobject->groupid,'domainconcept','Group');
	 		$dataAddOn['groupuri']=$groupuri;
	 		
	 		 	
	 	}else if($table=='scale'){
	 		if($action=='update'){
	 			$scaleUri=get_condition_value("modelling_urimapping",'uri','instanceid',$id,'domainconcept','Scale');
	 			$dataAddOn['scaleuri']=$scaleUri;
	 		}else{
	 			$dataAddOn["additionaldata"]="no";
	 		}
	 	}

	 	else{
	 		$dataAddOn["additionaldata"]="no";
	 	}

	 	print_message("***insert_table_to_rdf 5:".$table,"insert");
	 	$dataArray['dataobject']=json_encode($dataobject);
	 	$dataArray['addon']=json_encode($dataAddOn);
	 	$data=json_encode($dataArray);
	 		
	 	print_message("***insert_table_to_rdf table:".$table,"tables_to_insert");
	 	print_message("***insert_table_to_rdf service:".$url,"tables_to_insert");
	 	print_message("***insert_table_to_rdf data:".$data,"tables_to_insert");
	 	$response=curl_call($url,'POST', $data );

	 	$datatochange=json_decode($response, true);

	 	foreach($datatochange as $key=>$value){

	 		$operation=$value["operation"];
	 		if($operation=="setfield"){

	 			foreach($value as $k=>$v){

	 				if($k!="operation"){
	 						
	 					set_field($table, $k, $v, "id", $id);
	 				}
	 			}

	 		}else if($operation=="urimapping"){

	 			$urimapping=new object;
	 			$urimapping->domainconcept=$value['domainconcept'];
	 			$urimapping->instanceid=$value['instanceid'];
	 			$urimapping->uri=$value['uri'];
	 			insert_record("modelling_urimapping", $urimapping);
	 		}else if($operation=="urimappingwithparameter"){
	 			$urimapping=new object;
	 			$urimapping->domainconcept=$value['domainconcept'];
	 			$urimapping->instanceid=$value['instanceid'];
	 			$urimapping->parameter=$value['parameter'];
	 			$urimapping->uri=$value['uri'];
	 			insert_record("modelling_urimapping", $urimapping);
	 		}
	 	}
	 }
	 }
	 return "";
	
	}

}



?>
