<?php
require_once('../../config.php');
global $CFG;
require_once('lib.php');
require_once($CFG->dirroot.'/depths/lib/depths_tagslib.php');


$ideaUri=$_POST['ideaUri'];
$userDefinedTagsList=$_POST['tags'];
//$notes=$_POST['notes'];
$visibility=$_POST['visibility'];

if($visibility=='on'){
	$visibility='private';
}else{
	$visibility='public';
}

 $tags=make_array_from_separated_list($userDefinedTagsList);

//storeNewTagsForIdea($ideaUri,$tags,$notes,$visibility);
storeNewTagsForIdea($ideaUri,$tags,$visibility);


?>