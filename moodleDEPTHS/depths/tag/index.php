<?php // $Id: index.php,v 1.1 2008/02/20 10:26:19 cvsadmin Exp $

require_once('../../config.php');
 
require_once('lib.php');
 
require_once('pagelib.php');
 
require_once($CFG->dirroot.'/lib/weblib.php');
 
//require_once($CFG->dirroot.'/blog/lib.php');
 
require_once($CFG->dirroot.'/depths/mapper/utility.php');
 
print_message("index.php start","tag_index_php");
 
require_login();

if (empty($CFG->usetags)) {
    error(get_string('tagsaredisabled', 'tag'));
}

$tagid       = optional_param('id',     0,      PARAM_INT);   // tag id
$edit        = optional_param('edit', -1, PARAM_BOOL);
$userpage    = optional_param('userpage', 0, PARAM_INT);      // which page to show
$perpage     = optional_param('perpage', 24, PARAM_INT);

$tag      = tag_by_id($tagid);

print_message("tag_id:".$tag->name,"tag_index_php");

if (!$tag) {
    redirect($CFG->wwwroot.'/depths/tag/search.php');
}

//create a new page_tag object, defined in pagelib.php
$PAGE = page_create_object(PAGE_TAG_INDEX, $tag->id);
$pageblocks = blocks_setup($PAGE,BLOCKS_PINNED_BOTH);
$PAGE->tag_object = $tag;
 
if (($edit != -1) and $PAGE->user_allowed_editing()) {
    $USER->editing = $edit;
}
//require_once($CFG->dirroot.'/depths/mapper/depths_update.php');
//explore_dataobject($PAGE,"navigation:","tag_index");
//$PAGE->print_header();

$title = format_string($COURSE->shortname);
$navlinks = array();
    $navlinks[] = array('name' => 'DEPTHS Tags', 'link' => 'search.php', 'type' => 'activity');
    $navlinks[] = array('name' => $tag->name, 'link' => '', 'type' => 'activityinstance');
    $navigation = build_navigation($navlinks);

//    $navigation = "<a href=\"../../course/view.php?id=$COURSE->id\">$COURSE->shortname</a> ->";
//    echo '1';
//  $strmodulename  = "depths_tag";
  
     // $button = update_module_button($cm->id, $COURSE->id, $strmodulename);
     $buttonEdit='<form  method="get" action="http://192.168.1.5/moodleDEPTHS/depths/tag/index.php">
     <div><input type="hidden" name="edit" value="1" />
     <input type="hidden" name="id" value="27" />
     <input type="submit" value="Turn editing on?" />
     </div></form>';
 
//echo '3'.$COURSE->id;
//echo '4'. $COURSE->shortname;
//echo '5'.$COURSE->fullname;
//echo '6'.$navigation;
//echo '7';
//echo '8'.$button;

//     print_header("$COURSE->shortname: $COURSE->fullname",
//                 "$navigation \"<a href=\"/depths/tag/search.php\">Tags</a> -> tags", 
//                  "", "", true, $button, 
//                  navmenu($COURSE)); 
$PAGE->print_header($title, $heading, $navigation, "", "", true, $buttonEdit);
                  
            

echo '<table border="0" cellpadding="3" cellspacing="0" width="100%" id="layout-table">';
echo '<tr valign="top">';

//----------------- left column -----------------

$blocks_preferred_width = bounded_number(180, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]), 210);

    

//----------------- middle column -----------------

echo '<td valign="top" id="middle-column">';

$tagname  = tag_display_name($tag);
  
$systemcontext   = get_context_instance(CONTEXT_SYSTEM);
if ($tag->flag > 0 && has_capability('moodle/tag:manage', $systemcontext)) {
    $tagname =  '<span class="flagged-tag">' . $tagname . '</span>';
}
 
print_heading($tagname, '', 2, 'headingblock header tag-heading');
 
print_tag_management_box($tag);
 
print_tag_description_box($tag);
 
 
$usercount = count_items_tagged_with($tag->id,'user');

if ($usercount > 0) {

    //user table box
    print_box_start('generalbox', 'tag-user-table');

    $heading = get_string('userstaggedwith', 'tag', $tagname) . ': ' . $usercount;
    print_heading($heading, '', 3);

    $baseurl = $CFG->wwwroot.'/depths/tag/index.php?id='.$tag->id;

    print_paging_bar($usercount, $userpage, $perpage, $baseurl.'&amp;', 'userpage');

    print_tagged_users_table($tag, $userpage * $perpage, $perpage);

    print_box_end();

}
 



// Print last 10 blogs

// I was not able to use get_items_tagged_with() because it automatically 
// tries to join on 'blog' table, since the itemtype is 'blog'. However blogs
// uses the post table so this would not really work.    - Yu 29/8/07
/*if ($blogs = blog_fetch_entries('', 10, 0, 'site', '', $tag->id)) {

    print_box_start('generalbox', 'tag-blogs');

    print_heading(get_string('relatedblogs', 'tag'), '', 3);

    echo '<ul id="tagblogentries">';
    foreach ($blogs as $blog) {
        if ($blog->publishstate == 'draft') {
            $class = 'class="dimmed"';
        } else {
            $class = '';
        }
        echo '<li '.$class.'>';
        echo '<a '.$class.' href="'.$CFG->wwwroot.'/blog/index.php?postid='.$blog->id.'">';
        echo format_string($blog->subject);
        echo '</a>';
        echo ' - '; 
        echo '<a '.$class.' href="'.$CFG->wwwroot.'/user/view.php?id='.$blog->userid.'">';
        echo fullname($blog);
        echo '</a>';
        echo ', '. userdate($blog->lastmodified);
        echo '</li>';
    }
    echo '</ul>';

    echo '<p class="moreblogs"><a href="'.$CFG->wwwroot.'/blog/index.php?filtertype=site&filterselect=0&tagid='.$tag->id.'">'.get_string('seeallblogs', 'tag').'</a>...</p>';

    print_box_end();
}*/
 
if ($depths_tags = depths_tags_fetch_entries('', 10, 0, 'site', '', $tag->id)) {
 
    print_box_start('generalbox', 'tag-blogs');
 
    print_heading(get_string('relatedideas', 'depths'), '', 3);

    echo '<ul id="tagblogentries">';
    foreach ($depths_tags as $idea) {
     
       // if ($idea->publishstate == 'draft') {
            //$class = 'class="dimmed"';
       // } else {
             $class = '';
       // }
        echo '<li '.$class.'>';
        echo '<a '.$class.' href="'.$CFG->wwwroot.'/mod/modelling/brainstorm.php?id='.$idea->modellingid.'&amp;t='.$idea->taskid.'&amp;selIdea='.$idea->id.'">';
        echo format_string($idea->content);
        echo '</a>';
        echo ' - '; 
        echo '<a '.$class.' href="'.$CFG->wwwroot.'/user/view.php?id='.$idea->userid.'">';
        echo fullname($idea);
        echo '</a>';
        echo ', '. userdate($idea->time);
        echo '</li>';
    }
    echo '</ul>';

    echo '<p class="moreblogs"><a href="'.$CFG->wwwroot.'/depths/tag/index.php?filtertype=site&filterselect=0&tagid='.$tag->id.'">'.get_string('seealltags', 'depths').'</a>...</p>';

    print_box_end();
}


echo '</td>';


//----------------- right column -----------------

$blocks_preferred_width = bounded_number(180, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]), 210);

if (blocks_have_content($pageblocks, BLOCK_POS_RIGHT) || $PAGE->user_is_editing()) {
    echo '<td style="vertical-align: top; width: '.$blocks_preferred_width.'px;" id="right-column">';
    blocks_print_group($PAGE, $pageblocks, BLOCK_POS_RIGHT);
    echo '</td>';
}

/// Finish the page
echo '</tr></table>';



$PAGE->print_footer();


?>
