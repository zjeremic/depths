<?php
require_once('../../config.php');
require_once('lib.php');

require_once($CFG->dirroot.'/depths/lib/depths_tagslib.php');

$ideaVal=optional_param('ideaUri',PARAM_URL);
echo '<link rel="stylesheet" type="text/css" href="../../depths/lib/styles/ticker.css" media="screen" />';
echo'
<script>
$(function() {
	
		 $(".newsticker-jcarousellite").jCarouselLite({
		vertical: true,
		hoverPause:true,
		visible: 3,
		auto:500,
		speed:1000
	});
	
	$( "#tabs" ).tabs();
 

	
});
</script>
<script>
function closeModalDialog(){
	$(\'#depths_dialog_dummy\').dialog(\'close\');
}
</script>
<script>
function addSelAction(text) {

var out = document.getElementById("tags");
var outVal=out.value;
if (outVal==""){
outVal=text;
}else{
outVal += ", "+text;
}
out.value=outVal;
}
</script>';
echo '
  <script type="text/javascript">
	$(document).ready(function(){
	
 	
		$("#tagform").validate({
			debug: false,
			rules: {
			},
			messages: {
				
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post("'.$CFG->wwwroot.'/depths/tag/process_tags.php", $("#tagform").serialize(), function(data) {
					//$(\'#results\').html(data);
				});
				closeModalDialog();
			}
		});
	});
	</script>
 
 ';

$myAnnotations=getMyAnnotationsForIdea($ideaVal);

//$noteuri=$myAnnotations['noteuri'];
//$note=$myAnnotations['note'];
$annVisibility=$myAnnotations['visibility'];
if($annVisibility==""){
	$annVisibilityVal='';
}else if ($annVisibility=="private"){
	$annVisibilityVal='on';
}
$tagsJson=$myAnnotations['tags'];
$oldtags= array();
foreach($tagsJson as $key=>$tag){
	$oldtags[]=$tag['tag'];
}

echo '<form id="tagform" name="tagform" method="post" action="">
 
<input type="hidden" name="ideaUri" value="'.$ideaVal.'" />

  <table width="100%">
  	<tr>
  		 
  		<td  width="40%">&nbsp;</td>
  		<td  width="30%"><div align="right"><input type="checkbox" name="visibility" id="visibility" ';
if($annVisibility=="private"){
	echo 'checked="checked"';
}
echo '"/>Mark as Private<br /></div></td>
  	</tr>';
echo '
     <tr>
	   
    	<td colspan="2"><textarea cols="65" name="oldtagsarea" disabled="true">'.implode(",",$oldtags).'</textarea></td>
     </tr> 
     <tr>
        <td colspan="2"><textarea cols="65" rows="1" name="tags" id="tags"></textarea></td>
     </tr>';
echo '
	 <tr>
 		<td colspan="3">
		 <div id="tabs">
	<ul>
		<li><a href="#tabs-1">My tags</a></li>
		<li><a href="#tabs-2">Peers\'s tags</a></li>
		<li><a href="#tabs-3">Domain concepts</a></li>
	</ul>
	<div id="tabs-1">
 		<div id="loading" style="display:none; text-align:center"> 
    		<img src="../../depths/lib/images/ajax-loader.gif" alt="Loader" />
		</div>
		 <div id="tagCloud1"/>	
	</div> 
	<div id="tabs-2">
	<div id="loading2" style="display:none; text-align:center">
    		<img src="../../depths/lib/images/ajax-loader.gif" alt="Loader" />
		</div>
		<div id="tagCloud2"/>
	</div> 
	<div id="tabs-3">
	<div id="loading3" style="display:none; text-align:center"> 
	<img src="../../depths/lib/images/ajax-loader.gif" alt="Loader" />
		</div>
		<div id="tagCloud3"/>
	</div>'
;

echo '
		<script type="text/javascript">  
    $(function() {  
       //get tag feed  
       
    $("#loading").show();
    $.getJSON("'.$CFG->wwwroot.'/depths/tag/tagcloud.php?tagstype=mytags&callback=?", function(data) {  
    //process JSON object 
    //create list for tag links
 		$("<ul>").attr("id", "tagList1").appendTo("#tagCloud1");
	//create tags
		$.each(data.tags, function(i, val) {
 	//create item
  		var li = $("<li>");
  	//create link
  		$("<a>").text(val.tag).attr({title:"Select tag " + val.tag, href:"#", onclick:"addSelAction(\'" + val.tag + "\'); return false;"}).appendTo(li);
  	//add to list
  		li.appendTo("#tagList1");
    //set tag size  
    	li.children().css("fontSize", (val.freq / 10 < 1) ? val.freq / 10 + 1 + "em": (val.freq / 10 > 2) ? "2em" : val.freq / 10 + "em");  
	});
	
	
    }).success(function(){
		     
		         $("#loading").hide();
				}); 
    
    
    });  
          </script>  
		';
echo '
		<script type="text/javascript">  
    		$(function() {  
    		$("#loading2").show();
    		 
       		//get tag feed  
      		$.getJSON("'.$CFG->wwwroot.'/depths/tag/tagcloud.php?tagstype=peerstags&callback=?", function(data) {  
      		//process JSON object 
      		//create list for tag links
 			$("<ul>").attr("id", "tagList2").appendTo("#tagCloud2");

			//create tags
			$.each(data.tags, function(i, val) {

  			//create item
  			var li = $("<li>");

  			//create link
  			$("<a>").text(val.tag).attr({title:"Select tag " + val.tag, href:"#", onclick:"addSelAction(\'" + val.tag + "\'); return false;"}).appendTo(li);

  			//add to list
  			li.appendTo("#tagList2");
      		//set tag size  
    		li.children().css("fontSize", (val.freq / 10 < 1) ? val.freq / 10 + 1 + "em": (val.freq / 10 > 2) ? "2em" : val.freq / 10 + "em");  
			}); 
        	}).success(function(){
		         $("#loading2").hide();
				})  
      		});  
          </script>  
 ';
echo '
				<script type="text/javascript">  
		    $(function() {  
		       //get tag feed  
		       
		   $("#loading3").show();
		   
		    $.getJSON("'.$CFG->wwwroot.'/depths/tag/domainconcepts.php?callback=?", function(data) {  
		       //process JSON object 
		      //create list for tag links
		   
		 		$("<ul>").attr("id", "tagList3").appendTo("#tagCloud3");
		//create tags
		 
		$.each(data.concepts, function(i, val) {
		  
		  //create item
		  var li = $("<li>");
		  //create link
		  $("<a>").text(val.concept).attr({title:"Select concept " + val.concept, href:"#", onclick:"addSelAction(\'" + val.concept + "\'); return false;"}).appendTo(li);
		  //add to list
		  li.appendTo("#tagList3");
		      //set tag size  
		    li.children().css("fontSize", (val.freq / 10 < 1) ? val.freq / 10 + 1 + "em": (val.freq / 10 > 2) ? "2em" : val.freq / 10 + "em");  
		});
		        }).success(function(){
		         
		         $("#loading3").hide();
				}); 
		      }); 
		      
		          </script>  
				';
 


 
 

echo '</div></td> </tr> ';

echo '

    <tr>
     <td>&nbsp;</td>
       
     
      <td><div align="center"><input type="submit" name="Submit" id="Submit" value="Submit" /><input type="submit" name="Cancel" id="cancel" value="Cancel" onclick="closeModalDialog();"    /></div></td>
      
    </tr>
  </table>
</form>





';


?>