<head>

<script type="text/javascript" src="../../lib/yoxview/yoxview-init.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery.noConflict();
	jQuery(".yoxview").yoxview( {
		backgroundColor: '#808080'});
			
});
</script>
</head>
<?php  


    require_once(dirname(__FILE__).'/../../../config.php');
    require_once($CFG->dirroot.'/depths/mapper/utility.php');
    require_once($CFG->dirroot.'/depths/setup/depths_config.php');
     require_once($CFG->dirroot.'/depths/lib/depths_settingslib.php'); 
    require_once($CFG->dirroot.'/depths/lib/depths_lib.php');
    require_once("$CFG->libdir/rsslib.php");
    
 
 
	 $reload=optional_param ('reload', 0, PARAM_BOOL);
    $calledFrom = optional_param('calledFrom', '', PARAM_TEXT);
	 //////////////////////////////////
	global $SESSION; 

  
    $id = optional_param('id', 0, PARAM_INT);           // Course Module ID
    $tab  = optional_param('tab', GLOSSARY_NO_VIEW, PARAM_ALPHA);    // browsing entries by categories?
    
    $displayformat = optional_param('displayformat',-1, PARAM_INT);  // override of the glossary display format
 
    $mode       = optional_param('mode', '', PARAM_ALPHA);           // term entry cat date available search author approval
 
    $hook       = optional_param('hook', '', PARAM_CLEAN);           // the term, entry, cat, etc... to look for based on mode
   
    $fullsearch = optional_param('fullsearch', 0,PARAM_INT);         // full search (concept and definition) when searching?
   
    $sortkey    = optional_param('sortkey', '', PARAM_ALPHA);// Sorted view: CREATION | UPDATE | FIRSTNAME | LASTNAME...
    
    $sortorder  = optional_param('sortorder', 'ASC', PARAM_ALPHA);   // it defines the order of the sorting (ASC or DESC)
    
    $offset     = optional_param('offset', 0,PARAM_INT);             // entries to bypass (for paging purposes)
     
    $page       = optional_param('page', 0,PARAM_INT);               // Page to show (for paging purposes)
    
    $show       = optional_param('show', '', PARAM_ALPHA);           // [ concept | alias ] => mode=term hook=$show
    
    $problemuri = optional_param('problemuri','',PARAM_URL);
    
    
  
    

   require_course_login($course->id, true, $cm);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

/// Loading the textlib singleton instance. We are going to need it.
    $textlib = textlib_get_instance();
 
/// setting the defaut number of entries per page if not set

    if ( !$entriesbypage = $glossary->entbypage ) {
        $entriesbypage = $CFG->glossary_entbypage;
    }

/// If we have received a page, recalculate offset
    if ($page != 0 && $offset == 0) {
        $offset = $page * $entriesbypage;
    }

/// setting the default values for the display mode of the current glossary
/// only if the glossary is viewed by the first time
    if ( $dp = get_record('glossary_formats','name', addslashes($glossary->displayformat)) ) {
    /// Based on format->defaultmode, we build the defaulttab to be showed sometimes
    $dp->defaultmode='cat';
        switch ($dp->defaultmode) {
            case 'cat':
                $defaulttab = DEPTHS_CATEGORY_VIEW;
                break;
            case 'date':
                $defaulttab = DEPTHS_DATE_VIEW;
                break;
            case 'author':
                $defaulttab = DEPTHS_AUTHOR_VIEW;
                break;
            case 'rating':
                $defaulttab = DEPTHS_RATING_VIEW;
                break;
            default:
                $defaulttab = DEPTHS_STANDARD_VIEW;
        }
    /// Fetch the rest of variables
        $printpivot = $dp->showgroup;
        if ( $mode == '' and $hook == '' and $show == '') {
            $mode      = $dp->defaultmode;
            $hook      = $dp->defaulthook;
            $sortkey   = $dp->sortkey;
            $sortorder = $dp->sortorder;
        }
    } else {
        $defaulttab = DEPTHS_STANDARD_VIEW;
        $printpivot = 1;
        if ( $mode == '' and $hook == '' and $show == '') {
            $mode = 'available';
            $hook = 'ALL';
        }
    }
 
    if ( $displayformat == -1 ) {
         $displayformat = $glossary->displayformat;
    }

    if ( $show ) {
        $mode = 'term';
        $hook = $show;
        $show = '';
    }
/// Processing standard security processes
    if ($course->id != SITEID) {
        require_login($course->id);
    }
  
/// stablishing flag variables
    if ( $sortorder = strtolower($sortorder) ) {
        if ($sortorder != 'asc' and $sortorder != 'desc') {
            $sortorder = '';
        }
    }
    if ( $sortkey = strtoupper($sortkey) ) {
        if ($sortkey != 'CREATION' and
            $sortkey != 'UPDATE' and
            $sortkey != 'FIRSTNAME' and
            $sortkey != 'LASTNAME'
            ) {
            $sortkey = '';
        }
    }
 
    switch ( $mode = strtolower($mode) ) {
 

    case 'cat':    /// Looking for a certain cat
        $tab = DEPTHS_STANDARD_VIEW;
        if ( $hook > 0 ) {
            $category = get_record("glossary_categories","id",$hook);
            
        }
    break;

 
    case 'available':  /// Looking for entries that begin with a certain available, ALL or SPECIAL characters
    default:
        $tab = DEPTHS_STANDARD_VIEW;
        if ( !$hook ) {
            $hook = 'ALL';
        }
    break;
    }

    switch ( $tab ) {
    case DEPTHS_IMPORT_VIEW:
    case DEPTHS_EXPORT_VIEW:
    case DEPTHS_APPROVAL_VIEW:
        $showcommonelements = 0;
    break;

    default:
        $showcommonelements = 1;
    break;
    }

/// Printing the heading
    $strglossaries = get_string("modulenameplural", "glossary");
    $strglossary = get_string("modulename", "glossary");
    $strallcategories = get_string("allcategories", "glossary");
    $straddentry = get_string("addentry", "glossary");
    $strnoentries = get_string("noentries", "glossary");
    $strsearchconcept = get_string("searchconcept", "glossary");
    $strsearchindefinition = get_string("searchindefinition", "glossary");
    $strsearch = get_string("search");
    $strwaitingapproval = get_string('waitingapproval', 'glossary');

    $navlinks = array();
    $navlinks[] = array('name' => "Projects", 'link' => "projects_view.php", 'type' => 'activity');
    $navlinks[] = array('name' => format_string($glossary->name), 'link' => "view.php?id=$id", 'type' => 'activityinstance');

 
        $navigation = build_navigation($navlinks);
        print_header_simple(format_string(/*$glossary->name*/"Projects"), "", $navigation, "", "", true,
            /*update_module_button($cm->id, $course->id, $strglossary),*/ navmenu($course, $cm));
 
 
    echo '<br />';
 
    include("projects_tabs.php");
 
   // include_once("sql.php");

/// printing the entries
    $entriesshown = 0;
    $currentpivot = '';
    $ratingsmenuused = NULL;
    $paging = NULL;
    
    
    
    $entriesbypage=5;
    if (!$page){
    $page=0;
    //$count=20;
    }
    //$Array=java ("java.lang.reflect.Array");
 
  //////////////////////////////////test
   
       if ($mode=="available"){
       	print_box("Browse all projects available to me at the moment", 'generalbox', 'intro');
       	$allProjectsUnsorted=getAvailableProjectsUnsorted();
      $count=sizeof($allProjectsUnsorted);
      
       	include("allprojectsContent.php");
 	 
	          
    }
    if ($mode=="myprojects"){
    	$allProjectsUnsorted=getMyProjectsUnsorted();
    
    print_box("Browse my projects", 'generalbox', 'intro');
    	$count=sizeof($allProjectsUnsorted);
    
    	include("allprojectsContent.php");
    		
    	 
    }
    if ($mode=="thisproblem"){
    	$allProjectsUnsorted=getProjectsForThisProblemUnsorted($problemuri);
    
    	print_box("Browse available projects for this problem", 'generalbox', 'intro');
    	$count=sizeof($allProjectsUnsorted);
    
    	include("allprojectsContent.php");
    
    
    }
  /////////////////////////////////
  
  if($calledFrom=='selectModel'){
   
  	$baseurlforpaging="projects_view.php?mode=$mode&amp;calledFrom=selectModel&amp;";
  }else{
   
  	$baseurlforpaging="projects_view.php?mode=$mode&amp;";
  }
 
  $paging = depths_get_paging_bar($count, $page, $entriesbypage, $baseurlforpaging,9999,10,'&nbsp;&nbsp;', $specialtext, -1);               
 
    if ($ratingsmenuused) {
 
        echo "<div class=\"boxaligncenter\"><input type=\"submit\" value=\"".get_string("sendinratings", "glossary")."\" />";
        if ($glossary->scale < 0) {
            if ($scale = get_record("scale", "id", abs($glossary->scale))) {
                print_scale_menu_helpbutton($course->id, $scale );
            }
        }
        echo "</div>";
    }

    if (!empty($formsent)) {
        // close the form properly if used
        echo "</div>";
        echo "</form>";
    }
 
    if ( $paging ) {
    
        echo '<hr />';
        echo '<div class="paging">';
        echo $paging;
        echo '</div>';
   }
    echo '<br />';
    depths_print_tabbed_table_end();

/// Finish the page
 
    print_footer($course);
     

?>
