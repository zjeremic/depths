<?php
require_once(dirname(__FILE__).'/../../config.php');
$site = get_site();
    $strPageTitle = get_string('setFacebook','depths');
    $strdepths       = get_string('setFacebook', 'depths');
   $navlinks = array(array('name' => $strdepths, 'link' => "$CFG->wwwroot/depths/", 'type' => 'misc'));
   //$navlinks[] = array('name' => $strpreferences, 'link' => null, 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    print_header("$site->shortname: $strdepths : $strPageTitle", $strbdepths, $navigation);
    //print_header("Depths title","Depths heading");
    print_heading($strPageTitle);

    print_box_start('boxwidthwide boxaligncenter generalbox');
    $message=$_GET['message'];
 
    if ($message=='wrong_hostname_user_or_pass')
    {
    
    	echo "<font color='red'>".get_string("wrong_hostname_user_or_pass", depths)."</font>";
    	
    }
    if ($message=='wrong_dbname')
    {
    
    	echo "<font color='red'>".get_string("wrong_dbname", depths)."</font>";
    	
    }
 
    if ($message=='no_config_file')
    {
    	echo "<font color='red'>".get_string("no_config_file", depths)."</font>";
    	 
    }
 
    require('setFacebookForm.html');
 
    print_box_end();
    print_footer();
?>