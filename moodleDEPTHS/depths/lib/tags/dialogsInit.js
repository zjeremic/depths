/* global variables for the dialog */
var dialog;

/* global prefix to add to all URL for loading css, js, images, etc. */
var prefix = "";
var $ = jQuery.noConflict();
$(document).ready(function($) {
	

	initModalDialog();
}
);
 function initModalDialog() {
	/* creata dummy div for dialog */
	var div = document.createElement("div");
	div.setAttribute("id", "depths_dialog_dummy");
	// add div to page
	var body = document.getElementsByTagName("body").item(0);
	body.appendChild(div);
	/* initialise dialog */
	$dialog = $('#depths_dialog_dummy').html('').dialog( {
		modal : false,
		autoOpen : false,
		show : 'slide',
		hide : 'slide' 

	
	});
 }
 
 
function showDEPTHSModalDialog(url, target) {
var $loader='<div id="waitloading" align="center"><br><br><p><img src="../../depths/lib/images/waitloader.gif" /><br> Please Wait</p></div>';
	  $dialog.html($loader);
	$dialog.dialog("option","width", 560);
	$dialog.dialog("option","height", 440);
	$dialog.dialog("option","title","Add tags");
	$dialog.load(url);
	$dialog.dialog('open');
	var myDialogX = $(target).position().left - $(this).outerWidth();
	var myDialogY = $(target).position().top - ( $(document).scrollTop() + $('.ui-dialog').outerHeight() );
	$dialog.dialog("option","position",[myDialogX, myDialogY]); 
    return false;
}

function showDEPTHSAddCommentDialog(url, target) {
var $loader='<div id="waitloading" align="center"><br><br><p><img src="../../depths/lib/images/waitloader.gif" /><br> Please Wait</p></div>';
	  $dialog.html($loader);
	$dialog.dialog("option","width", 380);
	$dialog.dialog("option","height", 230);
	$dialog.dialog("option","title","Add comment");
	$dialog.load(url);
	$dialog.dialog('open');
	var myDialogX = $(target).position().left - $(this).outerWidth();
    var myDialogY = $(target).position().top - ( $(document).scrollTop() + $('.ui-dialog').outerHeight() );
	$dialog.dialog("option","position",[myDialogX, myDialogY]); 
    return false;
}
function getAllCommentsForIdea(ideaId,ideaUri,appRoot){
	var callbackUrl=appRoot+"/depths/tag/comments.php?ideauri="+ideaUri+"&callback=?";
	var div=$("#"+ideaId);
	 
 
	div.empty();
	   $("#loading"+ideaId).show();
	 $.getJSON(callbackUrl, function(data) { 
		 if(data.success=="true"){ 
 			  if(data.notes.length==0){
				  $("<p>").attr("class","comment").text("No available comments for this idea.").appendTo(div);
			 }
 
 			 $.each(data.notes, function(i, val) {
 				 $("<br>").appendTo(div);
 				var commentDiv= $("<div>").attr("class","comment").appendTo(div);
 				var commentType=val.commenttype;
 				 
 				 var table=$("<table>").appendTo(commentDiv);
 				 var tr0=$("<tr>").appendTo(table);
 				var td0=$("<td>").attr("width","40").appendTo(tr0);
 				if(commentType!=""){
 				$("<img>").attr("src",appRoot+"/mod/modelling/images/"+commentType+"16.png").attr("title",commentType).appendTo(td0);
 			 	}
 				
 				$("<td>").attr("width","450").attr("colspan","3").text(val.note).appendTo(tr0);
 				 var tr1=$("<tr>").appendTo(table);
 				 $("<td>").attr("width","30").text(" ").appendTo(tr1);
 				 $("<td>").attr("style","color:blue").text(val.firstname+" "+val.lastname).appendTo(tr1);
 				$("<td>").attr("width","30").text(" ").appendTo(tr1);
 				$("<td>").attr("align","right").text(val.created).appendTo(tr1);
 
		 });
 			 
	 }else{
		  
		 $("<p>").attr("class","comment").text("No comments added for this idea.").appendTo(div);
		
	 }
	 }).success(function(){
	     
		 $("#loading"+ideaId).hide();
		}); 
 
	// });
	
 
}
 
 