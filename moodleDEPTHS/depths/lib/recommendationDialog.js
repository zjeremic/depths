 /* global variables for the dialog */
var recommendationdialog;

/* global prefix to add to all URL for loading css, js, images, etc. */
var prefix = "";
var $ = jQuery.noConflict();
$(document).ready(function($) {
	

	initRecommendationDialog();
}
);
function setRating(pageUri,ratingValue,conceptUri,wwwroot){
 
	var callbackUrl=wwwroot+"/depths/services/online_resources/process_rating.php";
	
	$.get(callbackUrl,{
		pageuri: pageUri ,
		ratingvalue: ratingValue,
	    concepturi: conceptUri, 
	    success: function(data) {
	     var divEl=document.getElementById(pageUri);
	     divEl.innerHTML="Thank you";
	    }

	});
	
}
 function initRecommendationDialog() {
	
 
	/* creata dummy div for dialog */
 
	var div = document.createElement("div");
	div.setAttribute("id", "depths_dialog_recommendation");
 
	// add div to page
 
	var body = document.getElementsByTagName("body").item(0);
	body.appendChild(div);

	$recommendationdialog = $('#depths_dialog_recommendation').html('').dialog( {
	
		modal : false,
		autoOpen : false,
		show : 'slide',
		hide : 'slide',
		buttons: [
		          {
		              text: "Ok",
		              click: function() { $(this).dialog("close"); }
		          }
		      ] 

	
	});
 }
 
 
function showDEPTHSRecommendationDialog(url,conceptname) {
 var $loader='<div id="waitloading" align="center"><br><br><p><img src="../../depths/lib/images/waitloader.gif" /><br> Please Wait</p></div>';
	  $recommendationdialog.html($loader);
	$recommendationdialog.dialog("option","width", 800);
	$recommendationdialog.dialog("option","height", 440);
	$recommendationdialog.dialog("option","title","View recommended resources for '"+conceptname+"'");
	$recommendationdialog.load(url);
	$recommendationdialog.dialog('open');

	return false;
	 
 
 
}
 